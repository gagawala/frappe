-- MySQL dump 10.16  Distrib 10.1.24-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: 6d4b9452aef14153
-- ------------------------------------------------------
-- Server version	10.1.24-MariaDB-1~trusty

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `__Auth`
--

DROP TABLE IF EXISTS `__Auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `__Auth` (
  `doctype` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldname` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `encrypted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`doctype`,`name`,`fieldname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `__Auth`
--

LOCK TABLES `__Auth` WRITE;
/*!40000 ALTER TABLE `__Auth` DISABLE KEYS */;
INSERT INTO `__Auth` VALUES ('User','Administrator','password','*B5BAEF287489AAFBDCAD0214059A40B215E88393','23206ea1a5a316b370eeb9c8f658c46b55dddf748af2c1297a902f9d',0);
/*!40000 ALTER TABLE `__Auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `__UserSettings`
--

DROP TABLE IF EXISTS `__UserSettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `__UserSettings` (
  `user` varchar(180) NOT NULL,
  `doctype` varchar(180) NOT NULL,
  `data` text,
  UNIQUE KEY `user` (`user`,`doctype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `__UserSettings`
--

LOCK TABLES `__UserSettings` WRITE;
/*!40000 ALTER TABLE `__UserSettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `__UserSettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `__global_search`
--

DROP TABLE IF EXISTS `__global_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `__global_search` (
  `doctype` varchar(100) DEFAULT NULL,
  `name` varchar(140) DEFAULT NULL,
  `title` varchar(140) DEFAULT NULL,
  `content` text,
  `route` varchar(140) DEFAULT NULL,
  `published` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `doctype_name` (`doctype`,`name`),
  FULLTEXT KEY `content` (`content`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `__global_search`
--

LOCK TABLES `__global_search` WRITE;
/*!40000 ALTER TABLE `__global_search` DISABLE KEYS */;
INSERT INTO `__global_search` VALUES ('DocType','DocField','DocField','Name : DocField',NULL,0),('DocType','DocPerm','DocPerm','Name : DocPerm',NULL,0),('DocType','DocType','DocType','Name : DocType',NULL,0),('DocType','Has Role','Has Role','Name : Has Role',NULL,0),('DocType','User','User','Name : User',NULL,0),('DocType','Role','Role','Name : Role',NULL,0),('DocType','Custom Field','Custom Field','Name : Custom Field',NULL,0),('DocType','Property Setter','Property Setter','Name : Property Setter',NULL,0),('DocType','Web Form','Web Form','Name : Web Form',NULL,0),('DocType','Web Form Field','Web Form Field','Name : Web Form Field',NULL,0),('DocType','Portal Menu Item','Portal Menu Item','Name : Portal Menu Item',NULL,0),('DocType','User Permission','User Permission','Name : User Permission',NULL,0),('DocType','Dynamic Link','Dynamic Link','Name : Dynamic Link',NULL,0),('DocType','Version','Version','Name : Version',NULL,0),('DocType','Block Module','Block Module','Name : Block Module',NULL,0),('DocType','Feedback Trigger','Feedback Trigger','Name : Feedback Trigger',NULL,0),('DocType','Has Domain','Has Domain','Name : Has Domain',NULL,0),('DocType','DocShare','DocShare','Name : DocShare',NULL,0),('DocType','Domain','Domain','Name : Domain',NULL,0),('DocType','Test Runner','Test Runner','Name : Test Runner',NULL,0),('DocType','Tag Doc Category','Tag Doc Category','Name : Tag Doc Category',NULL,0),('Module Def','Core','Core','Name : Core',NULL,0),('DocType','Module Def','Module Def','Name : Module Def',NULL,0),('DocType','Tag Category','Tag Category','Name : Tag Category',NULL,0),('DocType','SMS Settings','SMS Settings','Name : SMS Settings',NULL,0),('DocType','Custom Role','Custom Role','Name : Custom Role',NULL,0),('DocType','Activity Log','Activity Log','Name : Activity Log',NULL,0),('DocType','DefaultValue','DefaultValue','Name : DefaultValue',NULL,0),('DocType','File','File','Name : File',NULL,0),('DocType','Page','Page','Name : Page',NULL,0),('DocType','User Permission for Page and Report','User Permission for Page and Report','Name : User Permission for Page and Report',NULL,0),('DocType','Patch Log','Patch Log','Name : Patch Log',NULL,0),('DocType','Translation','Translation','Name : Translation',NULL,0),('DocType','Deleted Document','Deleted Document','Name : Deleted Document',NULL,0),('DocType','Feedback Request','Feedback Request','Name : Feedback Request',NULL,0),('DocType','Role Profile','Role Profile','Name : Role Profile',NULL,0),('DocType','Report','Report','Name : Report',NULL,0),('DocType','Domain Settings','Domain Settings','Name : Domain Settings',NULL,0),('DocType','Data Import','Data Import','Name : Data Import',NULL,0),('DocType','Custom DocPerm','Custom DocPerm','Name : Custom DocPerm',NULL,0),('DocType','Error Log','Error Log','Name : Error Log',NULL,0),('DocType','Error Snapshot','Error Snapshot','Name : Error Snapshot',NULL,0),('DocType','Communication','Communication','Name : Communication',NULL,0),('DocType','Payment Gateway','Payment Gateway','Name : Payment Gateway',NULL,0),('DocType','SMS Parameter','SMS Parameter','Name : SMS Parameter',NULL,0),('DocType','Role Permission for Page and Report','Role Permission for Page and Report','Name : Role Permission for Page and Report',NULL,0),('DocType','Language','Language','Name : Language',NULL,0),('DocType','System Settings','System Settings','Name : System Settings',NULL,0),('DocType','User Email','User Email','Name : User Email',NULL,0),('DocType','Tag','Tag','Name : Tag',NULL,0),('Report','Feedback Ratings','Feedback Ratings','Name : Feedback Ratings',NULL,0),('Report','Document Share Report','Document Share Report','Name : Document Share Report',NULL,0),('Report','Permitted Documents For User','Permitted Documents For User','Name : Permitted Documents For User',NULL,0),('Module Def','Website','Website','Name : Website',NULL,0),('DocType','About Us Team Member','About Us Team Member','Name : About Us Team Member',NULL,0),('DocType','Blog Settings','Blog Settings','Name : Blog Settings',NULL,0),('DocType','Help Article','Help Article','Name : Help Article',NULL,0),('DocType','Footer Item','Footer Item','Name : Footer Item',NULL,0),('DocType','Contact Us Settings','Contact Us Settings','Name : Contact Us Settings',NULL,0),('DocType','Portal Settings','Portal Settings','Name : Portal Settings',NULL,0),('DocType','Website Theme','Website Theme','Name : Website Theme',NULL,0),('DocType','Blog Post','Blog Post','Name : Blog Post','/blog',0),('DocType','About Us Settings','About Us Settings','Name : About Us Settings',NULL,0),('DocType','Blogger','Blogger','Name : Blogger',NULL,0),('DocType','Web Page','Web Page','Name : Web Page',NULL,0),('DocType','Website Settings','Website Settings','Name : Website Settings',NULL,0),('DocType','Company History','Company History','Name : Company History',NULL,0),('DocType','Website Sidebar Item','Website Sidebar Item','Name : Website Sidebar Item',NULL,0),('DocType','Website Slideshow','Website Slideshow','Name : Website Slideshow',NULL,0),('DocType','Website Slideshow Item','Website Slideshow Item','Name : Website Slideshow Item',NULL,0),('DocType','Website Script','Website Script','Name : Website Script',NULL,0),('DocType','Website Sidebar','Website Sidebar','Name : Website Sidebar',NULL,0),('DocType','Top Bar Item','Top Bar Item','Name : Top Bar Item',NULL,0),('DocType','Blog Category','Blog Category','Name : Blog Category',NULL,0),('DocType','Help Category','Help Category','Name : Help Category',NULL,0),('Module Def','Workflow','Workflow','Name : Workflow',NULL,0),('DocType','Workflow State','Workflow State','Name : Workflow State',NULL,0),('DocType','Workflow Transition','Workflow Transition','Name : Workflow Transition',NULL,0),('DocType','Workflow Action','Workflow Action','Name : Workflow Action',NULL,0),('DocType','Workflow','Workflow','Name : Workflow',NULL,0),('DocType','Workflow Document State','Workflow Document State','Name : Workflow Document State',NULL,0),('Module Def','Email','Email','Name : Email',NULL,0),('DocType','Email Alert Recipient','Email Alert Recipient','Name : Email Alert Recipient',NULL,0),('DocType','Newsletter','Newsletter','Name : Newsletter','newsletters',0),('DocType','Auto Email Report','Auto Email Report','Name : Auto Email Report',NULL,0),('DocType','Email Queue','Email Queue','Name : Email Queue',NULL,0),('DocType','Email Alert','Email Alert','Name : Email Alert',NULL,0),('DocType','Email Rule','Email Rule','Name : Email Rule',NULL,0),('DocType','Email Group Member','Email Group Member','Name : Email Group Member',NULL,0),('DocType','Email Flag Queue','Email Flag Queue','Name : Email Flag Queue',NULL,0),('DocType','Newsletter Email Group','Newsletter Email Group','Name : Newsletter Email Group',NULL,0),('DocType','Standard Reply','Standard Reply','Name : Standard Reply',NULL,0),('DocType','Email Account','Email Account','Name : Email Account',NULL,0),('DocType','Email Queue Recipient','Email Queue Recipient','Name : Email Queue Recipient',NULL,0),('DocType','Unhandled Email','Unhandled Email','Name : Unhandled Email',NULL,0),('DocType','Email Group','Email Group','Name : Email Group',NULL,0),('DocType','Email Domain','Email Domain','Name : Email Domain',NULL,0),('DocType','Email Unsubscribe','Email Unsubscribe','Name : Email Unsubscribe',NULL,0),('Module Def','Custom','Custom','Name : Custom',NULL,0),('DocType','Customize Form Field','Customize Form Field','Name : Customize Form Field',NULL,0),('DocType','Custom Script','Custom Script','Name : Custom Script',NULL,0),('DocType','Customize Form','Customize Form','Name : Customize Form',NULL,0),('Module Def','Geo','Geo','Name : Geo',NULL,0),('DocType','Currency','Currency','Name : Currency',NULL,0),('DocType','Country','Country','Name : Country',NULL,0),('Module Def','Desk','Desk','Name : Desk',NULL,0),('DocType','Kanban Board Column','Kanban Board Column','Name : Kanban Board Column',NULL,0),('DocType','Event','Event','Name : Event',NULL,0),('DocType','Note','Note','Name : Note',NULL,0),('DocType','Note Seen By','Note Seen By','Name : Note Seen By',NULL,0),('DocType','Desktop Icon','Desktop Icon','Name : Desktop Icon',NULL,0),('DocType','Calendar View','Calendar View','Name : Calendar View',NULL,0),('DocType','Bulk Update','Bulk Update','Name : Bulk Update',NULL,0),('DocType','Kanban Board','Kanban Board','Name : Kanban Board',NULL,0),('DocType','ToDo','ToDo','Name : ToDo',NULL,0),('Report','ToDo','ToDo','Name : ToDo',NULL,0),('Module Def','Integrations','Integrations','Name : Integrations',NULL,0),('DocType','Google Maps','Google Maps','Name : Google Maps',NULL,0),('DocType','Webhook Header','Webhook Header','Name : Webhook Header',NULL,0),('DocType','OAuth Provider Settings','OAuth Provider Settings','Name : OAuth Provider Settings',NULL,0),('DocType','Stripe Settings','Stripe Settings','Name : Stripe Settings',NULL,0),('DocType','S3 Backup Settings','S3 Backup Settings','Name : S3 Backup Settings',NULL,0),('DocType','Integration Request','Integration Request','Name : Integration Request',NULL,0),('DocType','Razorpay Settings','Razorpay Settings','Name : Razorpay Settings',NULL,0),('DocType','OAuth Authorization Code','OAuth Authorization Code','Name : OAuth Authorization Code',NULL,0),('DocType','Social Login Keys','Social Login Keys','Name : Social Login Keys',NULL,0),('DocType','GSuite Settings','GSuite Settings','Name : GSuite Settings',NULL,0),('DocType','Webhook Data','Webhook Data','Name : Webhook Data',NULL,0),('DocType','Webhook','Webhook','Name : Webhook',NULL,0),('DocType','OAuth Client','OAuth Client','Name : OAuth Client',NULL,0),('DocType','GSuite Templates','GSuite Templates','Name : GSuite Templates',NULL,0),('DocType','PayPal Settings','PayPal Settings','Name : PayPal Settings',NULL,0),('DocType','Dropbox Settings','Dropbox Settings','Name : Dropbox Settings',NULL,0),('DocType','OAuth Bearer Token','OAuth Bearer Token','Name : OAuth Bearer Token',NULL,0),('DocType','LDAP Settings','LDAP Settings','Name : LDAP Settings',NULL,0),('Module Def','Printing','Printing','Name : Printing',NULL,0),('DocType','Letter Head','Letter Head','Name : Letter Head',NULL,0),('DocType','Print Style','Print Style','Name : Print Style',NULL,0),('DocType','Print Heading','Print Heading','Name : Print Heading',NULL,0),('DocType','Print Format','Print Format','Name : Print Format',NULL,0),('DocType','Print Settings','Print Settings','Name : Print Settings',NULL,0),('Module Def','Contacts','Contacts','Name : Contacts',NULL,0),('DocType','Address Template','Address Template','Name : Address Template',NULL,0),('DocType','Address','Address','Name : Address',NULL,0),('DocType','Contact','Contact','Name : Contact',NULL,0),('DocType','Salutation','Salutation','Name : Salutation',NULL,0),('DocType','Gender','Gender','Name : Gender',NULL,0),('Report','Addresses And Contacts','Addresses And Contacts','Name : Addresses And Contacts',NULL,0),('Module Def','Data Migration','Data Migration','Name : Data Migration',NULL,0),('DocType','Data Migration Plan Mapping','Data Migration Plan Mapping','Name : Data Migration Plan Mapping',NULL,0),('DocType','Data Migration Connector','Data Migration Connector','Name : Data Migration Connector',NULL,0),('DocType','Data Migration Run','Data Migration Run','Name : Data Migration Run',NULL,0),('DocType','Data Migration Plan','Data Migration Plan','Name : Data Migration Plan',NULL,0),('DocType','Data Migration Mapping Detail','Data Migration Mapping Detail','Name : Data Migration Mapping Detail',NULL,0),('DocType','Data Migration Mapping','Data Migration Mapping','Name : Data Migration Mapping',NULL,0),('Communication','41ba385368','A new document User: Administrator has been shared by with you Administ','Subject : A new document User: Administrator has been shared by with you Administ ||| From : admin@example.com',NULL,0),('User','Administrator','Administrator','Full Name : Administrator ||| Name : Administrator',NULL,0),('Communication','ed27c67c22','A new document User: Guest has been shared by with you Guest.','Subject : A new document User: Guest has been shared by with you Guest. ||| From : admin@example.com',NULL,0),('User','Guest','Guest','Full Name : Guest ||| Name : Guest',NULL,0),('Workflow State','Pending','Pending','Name : Pending',NULL,0),('Workflow State','Approved','Approved','Name : Approved',NULL,0),('Workflow State','Rejected','Rejected','Name : Rejected',NULL,0),('Email Account','Notifications','Notifications','Email Address : notifications@example.com',NULL,0),('Email Account','Replies','Replies','Email Address : replies@example.com',NULL,0),('File','Home','Home','File Name : Home',NULL,0),('File','Home/Attachments','Attachments','File Name : Attachments',NULL,0),('Communication','8b13e46842','A new document User: Administrator has been shared by with you Administ','Subject : A new document User: Administrator has been shared by with you Administ ||| From : admin@example.com',NULL,0),('Module Def','Bench Manager','Bench Manager','Name : Bench Manager',NULL,0),('DocType','Site','Site','Name : Site',NULL,0),('DocType','Bench Settings','Bench Settings','Name : Bench Settings',NULL,0),('DocType','Site Backup','Site Backup','Name : Site Backup',NULL,0),('DocType','App','App','Name : App',NULL,0),('DocType','Bench Manager Command','Bench Manager Command','Name : Bench Manager Command',NULL,0);
/*!40000 ALTER TABLE `__global_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabAbout Us Team Member`
--

DROP TABLE IF EXISTS `tabAbout Us Team Member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabAbout Us Team Member` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `image_link` text COLLATE utf8mb4_unicode_ci,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `full_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabAbout Us Team Member`
--

LOCK TABLES `tabAbout Us Team Member` WRITE;
/*!40000 ALTER TABLE `tabAbout Us Team Member` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabAbout Us Team Member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabActivity Log`
--

DROP TABLE IF EXISTS `tabActivity Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabActivity Log` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `link_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timeline_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operation` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci,
  `reference_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `link_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `reference_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_seen` text COLLATE utf8mb4_unicode_ci,
  `user` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `timeline_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `communication_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `reference_owner` (`reference_owner`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`),
  KEY `reference_doctype_reference_name_index` (`reference_doctype`,`reference_name`),
  KEY `timeline_doctype_timeline_name_index` (`timeline_doctype`,`timeline_name`),
  KEY `link_doctype_link_name_index` (`link_doctype`,`link_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabActivity Log`
--

LOCK TABLES `tabActivity Log` WRITE;
/*!40000 ALTER TABLE `tabActivity Log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabActivity Log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabAddress`
--

DROP TABLE IF EXISTS `tabAddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabAddress` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `email_id` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_your_company_address` int(1) NOT NULL DEFAULT '0',
  `address_line2` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line1` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `is_primary_address` int(1) NOT NULL DEFAULT '0',
  `state` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `address_title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `country` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_shipping_address` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `pincode` (`pincode`),
  KEY `city` (`city`),
  KEY `country` (`country`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabAddress`
--

LOCK TABLES `tabAddress` WRITE;
/*!40000 ALTER TABLE `tabAddress` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabAddress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabAddress Template`
--

DROP TABLE IF EXISTS `tabAddress Template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabAddress Template` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `country` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `is_default` int(1) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `template` longtext COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `country` (`country`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabAddress Template`
--

LOCK TABLES `tabAddress Template` WRITE;
/*!40000 ALTER TABLE `tabAddress Template` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabAddress Template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabApp`
--

DROP TABLE IF EXISTS `tabApp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabApp` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `is_git_repo` int(1) NOT NULL DEFAULT '0',
  `app_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_icon` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'fa fa-gamepad',
  `app_color` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT '''grey''',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `version` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_description` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_publisher` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `current_git_branch` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bench_settings` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_license` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'MIT',
  `app_title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `developer_flag` int(11) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `app_email` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabApp`
--

LOCK TABLES `tabApp` WRITE;
/*!40000 ALTER TABLE `tabApp` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabApp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabAuto Email Report`
--

DROP TABLE IF EXISTS `tabAuto Email Report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabAuto Email Report` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `filters` text COLLATE utf8mb4_unicode_ci,
  `frequency` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_if_data` int(1) NOT NULL DEFAULT '1',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `filter_meta` text COLLATE utf8mb4_unicode_ci,
  `report_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_rows` int(11) NOT NULL DEFAULT '100',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `format` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `email_to` text COLLATE utf8mb4_unicode_ci,
  `user` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'User',
  `report` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `data_modified_till` int(11) NOT NULL DEFAULT '0',
  `enabled` int(1) NOT NULL DEFAULT '1',
  `day_of_week` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Monday',
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabAuto Email Report`
--

LOCK TABLES `tabAuto Email Report` WRITE;
/*!40000 ALTER TABLE `tabAuto Email Report` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabAuto Email Report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabBench Manager Command`
--

DROP TABLE IF EXISTS `tabBench Manager Command`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabBench Manager Command` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Ongoing',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `console` longtext COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `source` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `bench_settings` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `command` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `time_taken` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabBench Manager Command`
--

LOCK TABLES `tabBench Manager Command` WRITE;
/*!40000 ALTER TABLE `tabBench Manager Command` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabBench Manager Command` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabBlock Module`
--

DROP TABLE IF EXISTS `tabBlock Module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabBlock Module` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `module` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabBlock Module`
--

LOCK TABLES `tabBlock Module` WRITE;
/*!40000 ALTER TABLE `tabBlock Module` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabBlock Module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabBlog Category`
--

DROP TABLE IF EXISTS `tabBlog Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabBlog Category` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `route` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `published` int(1) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `category_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `route` (`route`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabBlog Category`
--

LOCK TABLES `tabBlog Category` WRITE;
/*!40000 ALTER TABLE `tabBlog Category` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabBlog Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabBlog Post`
--

DROP TABLE IF EXISTS `tabBlog Post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabBlog Post` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `published_on` date DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blogger` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `email_sent` int(1) NOT NULL DEFAULT '0',
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `published` int(1) NOT NULL DEFAULT '0',
  `blog_intro` text COLLATE utf8mb4_unicode_ci,
  `blog_category` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `route` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `route` (`route`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabBlog Post`
--

LOCK TABLES `tabBlog Post` WRITE;
/*!40000 ALTER TABLE `tabBlog Post` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabBlog Post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabBlogger`
--

DROP TABLE IF EXISTS `tabBlogger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabBlogger` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `bio` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `short_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `posts` int(11) NOT NULL DEFAULT '0',
  `disabled` int(1) NOT NULL DEFAULT '0',
  `avatar` text COLLATE utf8mb4_unicode_ci,
  `full_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `user` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabBlogger`
--

LOCK TABLES `tabBlogger` WRITE;
/*!40000 ALTER TABLE `tabBlogger` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabBlogger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabCalendar View`
--

DROP TABLE IF EXISTS `tabCalendar View`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabCalendar View` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `subject_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `end_date_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `start_date_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabCalendar View`
--

LOCK TABLES `tabCalendar View` WRITE;
/*!40000 ALTER TABLE `tabCalendar View` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabCalendar View` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabCommunication`
--

DROP TABLE IF EXISTS `tabCommunication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabCommunication` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '0',
  `comment_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc` longtext COLLATE utf8mb4_unicode_ci,
  `link_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timeline_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_account` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seen` int(1) NOT NULL DEFAULT '0',
  `reference_owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci,
  `reference_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feedback_request` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `unread_notification_sent` int(1) NOT NULL DEFAULT '0',
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `communication_medium` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_attachment` int(1) NOT NULL DEFAULT '0',
  `reference_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_id` varchar(995) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `recipients` longtext COLLATE utf8mb4_unicode_ci,
  `timeline_label` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `communication_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Communication',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `text_content` longtext COLLATE utf8mb4_unicode_ci,
  `user` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `in_reply_to` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `read_receipt` int(1) NOT NULL DEFAULT '0',
  `sender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sent_or_received` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_full_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bcc` longtext COLLATE utf8mb4_unicode_ci,
  `timeline_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_no` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_seen` text COLLATE utf8mb4_unicode_ci,
  `communication_date` datetime(6) DEFAULT NULL,
  `email_status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `reference_owner` (`reference_owner`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`),
  KEY `reference_doctype_reference_name_index` (`reference_doctype`,`reference_name`),
  KEY `timeline_doctype_timeline_name_index` (`timeline_doctype`,`timeline_name`),
  KEY `link_doctype_link_name_index` (`link_doctype`,`link_name`),
  KEY `status_communication_type_index` (`status`,`communication_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabCommunication`
--

LOCK TABLES `tabCommunication` WRITE;
/*!40000 ALTER TABLE `tabCommunication` DISABLE KEYS */;
INSERT INTO `tabCommunication` VALUES ('41ba385368','2018-05-17 15:33:51.137681','2018-05-17 15:33:51.137681','Administrator','Administrator',0,NULL,NULL,NULL,0,0,0,'',NULL,NULL,NULL,NULL,0,'Administrator','A new document User: Administrator has been shared by with you Administ','User',NULL,NULL,0,'A new document <a href=\"http://bench-manager.local/desk#Form/User/Administrator\">User: Administrator</a> has been shared by with you Administrator.','',NULL,0,'Administrator',NULL,'Linked',NULL,NULL,NULL,'Chat',NULL,NULL,'Administrator','',NULL,NULL,0,'admin@example.com','Sent','Administrator',NULL,NULL,NULL,NULL,'2018-05-17 15:33:51.137008','Open'),('8b13e46842','2018-05-17 15:33:52.134432','2018-05-17 15:33:52.134432','Administrator','Administrator',0,NULL,NULL,NULL,0,0,0,'',NULL,NULL,NULL,NULL,0,'Administrator','A new document User: Administrator has been shared by with you Administ','User',NULL,NULL,0,'A new document <a href=\"http://bench-manager.local/desk#Form/User/Administrator\">User: Administrator</a> has been shared by with you Administrator.','',NULL,0,'Administrator',NULL,'Linked',NULL,NULL,NULL,'Chat',NULL,NULL,'Administrator','',NULL,NULL,0,'admin@example.com','Sent','Administrator',NULL,NULL,NULL,NULL,'2018-05-17 15:33:52.133838','Open'),('ed27c67c22','2018-05-17 15:33:51.195234','2018-05-17 15:33:51.195234','Administrator','Administrator',0,NULL,NULL,NULL,0,0,0,'',NULL,NULL,NULL,NULL,0,'Administrator','A new document User: Guest has been shared by with you Guest.','User',NULL,NULL,0,'A new document <a href=\"http://bench-manager.local/desk#Form/User/Guest\">User: Guest</a> has been shared by with you Guest.','',NULL,0,'Guest',NULL,'Linked',NULL,NULL,NULL,'Chat',NULL,NULL,'Administrator','',NULL,NULL,0,'admin@example.com','Sent','Administrator',NULL,NULL,NULL,NULL,'2018-05-17 15:33:51.194619','Open');
/*!40000 ALTER TABLE `tabCommunication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabCompany History`
--

DROP TABLE IF EXISTS `tabCompany History`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabCompany History` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `highlight` text COLLATE utf8mb4_unicode_ci,
  `year` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabCompany History`
--

LOCK TABLES `tabCompany History` WRITE;
/*!40000 ALTER TABLE `tabCompany History` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabCompany History` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabContact`
--

DROP TABLE IF EXISTS `tabContact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabContact` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `last_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_id` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salutation` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `unsubscribed` int(1) NOT NULL DEFAULT '0',
  `department` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Passive',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `designation` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_primary_contact` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `email_id` (`email_id`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabContact`
--

LOCK TABLES `tabContact` WRITE;
/*!40000 ALTER TABLE `tabContact` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabContact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabCountry`
--

DROP TABLE IF EXISTS `tabCountry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabCountry` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `date_format` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `time_zones` text COLLATE utf8mb4_unicode_ci,
  `country_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabCountry`
--

LOCK TABLES `tabCountry` WRITE;
/*!40000 ALTER TABLE `tabCountry` DISABLE KEYS */;
INSERT INTO `tabCountry` VALUES ('Afghanistan','2018-05-17 15:33:51.578563','2018-05-17 15:33:51.578563',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','af',NULL,NULL,NULL,'Asia/Kabul','Afghanistan',NULL),('Åland Islands','2018-05-17 15:33:51.759618','2018-05-17 15:33:51.759618',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ax',NULL,NULL,NULL,'','Åland Islands',NULL),('Albania','2018-05-17 15:33:51.547653','2018-05-17 15:33:51.547653',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','al',NULL,NULL,NULL,'Europe/Tirane','Albania',NULL),('Algeria','2018-05-17 15:33:51.716852','2018-05-17 15:33:51.716852',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','dz',NULL,NULL,NULL,'Africa/Algiers','Algeria',NULL),('American Samoa','2018-05-17 15:33:51.517831','2018-05-17 15:33:51.517831',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','as',NULL,NULL,NULL,'','American Samoa',NULL),('Andorra','2018-05-17 15:33:51.557989','2018-05-17 15:33:51.557989',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ad',NULL,NULL,NULL,'Europe/Andorra','Andorra',NULL),('Angola','2018-05-17 15:33:51.656880','2018-05-17 15:33:51.656880',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ao',NULL,NULL,NULL,'Africa/Luanda','Angola',NULL),('Anguilla','2018-05-17 15:33:51.806589','2018-05-17 15:33:51.806589',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ai',NULL,NULL,NULL,'America/Anguilla','Anguilla',NULL),('Antarctica','2018-05-17 15:33:51.871807','2018-05-17 15:33:51.871807',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','aq',NULL,NULL,NULL,'Antarctica/Casey\nAntarctica/Davis\nAntarctica/DumontDUrville\nAntarctica/Macquarie\nAntarctica/Mawson\nAntarctica/McMurdo\nAntarctica/Palmer\nAntarctica/Rothera\nAntarctica/Syowa\nAntarctica/Vostok','Antarctica',NULL),('Antigua and Barbuda','2018-05-17 15:33:51.779369','2018-05-17 15:33:51.779369',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ag',NULL,NULL,NULL,'America/Antigua','Antigua and Barbuda',NULL),('Argentina','2018-05-17 15:33:51.502239','2018-05-17 15:33:51.502239',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ar',NULL,NULL,NULL,'America/Argentina/Buenos_Aires\nAmerica/Argentina/Catamarca\nAmerica/Argentina/Cordoba\nAmerica/Argentina/Jujuy\nAmerica/Argentina/La_Rioja\nAmerica/Argentina/Mendoza\nAmerica/Argentina/Rio_Gallegos\nAmerica/Argentina/Salta\nAmerica/Argentina/San_Juan\nAmerica/Argentina/San_Luis\nAmerica/Argentina/Tucuman\nAmerica/Argentina/Ushuaia','Argentina',NULL),('Armenia','2018-05-17 15:33:51.622453','2018-05-17 15:33:51.622453',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','am',NULL,NULL,NULL,'Asia/Yerevan','Armenia',NULL),('Aruba','2018-05-17 15:33:51.499412','2018-05-17 15:33:51.499412',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','aw',NULL,NULL,NULL,'America/Aruba','Aruba',NULL),('Australia','2018-05-17 15:33:51.644612','2018-05-17 15:33:51.644612',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','au',NULL,NULL,NULL,'Australia/Adelaide\nAustralia/Brisbane\nAustralia/Broken_Hill\nAustralia/Currie\nAustralia/Darwin\nAustralia/Eucla\nAustralia/Hobart\nAustralia/Lindeman\nAustralia/Lord_Howe\nAustralia/Melbourne\nAustralia/Perth\nAustralia/Sydney','Australia',NULL),('Austria','2018-05-17 15:33:51.673856','2018-05-17 15:33:51.673856',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','at',NULL,NULL,NULL,'Europe/Vienna','Austria',NULL),('Azerbaijan','2018-05-17 15:33:51.568715','2018-05-17 15:33:51.568715',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','az',NULL,NULL,NULL,'Asia/Baku','Azerbaijan',NULL),('Bahamas','2018-05-17 15:33:51.700613','2018-05-17 15:33:51.700613',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bs',NULL,NULL,NULL,'America/Nassau','Bahamas',NULL),('Bahrain','2018-05-17 15:33:51.629332','2018-05-17 15:33:51.629332',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bh',NULL,NULL,NULL,'Asia/Bahrain','Bahrain',NULL),('Bangladesh','2018-05-17 15:33:51.580438','2018-05-17 15:33:51.580438',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bd',NULL,NULL,NULL,'Asia/Dhaka','Bangladesh',NULL),('Barbados','2018-05-17 15:33:51.790080','2018-05-17 15:33:51.790080',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bb',NULL,NULL,NULL,'America/Barbados','Barbados',NULL),('Belarus','2018-05-17 15:33:51.714070','2018-05-17 15:33:51.714070',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','by',NULL,NULL,NULL,'Europe/Minsk','Belarus',NULL),('Belgium','2018-05-17 15:33:51.729382','2018-05-17 15:33:51.729382',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','be',NULL,NULL,NULL,'Europe/Brussels','Belgium',NULL),('Belize','2018-05-17 15:33:51.733507','2018-05-17 15:33:51.733507',NULL,NULL,0,NULL,NULL,NULL,0,'mm-dd-yyyy','bz',NULL,NULL,NULL,'America/Belize','Belize',NULL),('Benin','2018-05-17 15:33:51.611452','2018-05-17 15:33:51.611452',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bj',NULL,NULL,NULL,'Africa/Porto-Novo','Benin',NULL),('Bermuda','2018-05-17 15:33:51.594122','2018-05-17 15:33:51.594122',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bm',NULL,NULL,NULL,'Atlantic/Bermuda','Bermuda',NULL),('Bhutan','2018-05-17 15:33:51.794137','2018-05-17 15:33:51.794137',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bt',NULL,NULL,NULL,'Asia/Thimphu','Bhutan',NULL),('Bolivia, Plurinational State of','2018-05-17 15:33:51.796991','2018-05-17 15:33:51.796991',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bo',NULL,NULL,NULL,'','Bolivia, Plurinational State of',NULL),('Bonaire, Sint Eustatius and Saba','2018-05-17 15:33:51.531087','2018-05-17 15:33:51.531087',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bq',NULL,NULL,NULL,'','Bonaire, Sint Eustatius and Saba',NULL),('Bosnia and Herzegovina','2018-05-17 15:33:51.523424','2018-05-17 15:33:51.523424',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ba',NULL,NULL,NULL,'Europe/Sarajevo','Bosnia and Herzegovina',NULL),('Botswana','2018-05-17 15:33:51.867431','2018-05-17 15:33:51.867431',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bw',NULL,NULL,NULL,'Africa/Gaborone','Botswana',NULL),('Bouvet Island','2018-05-17 15:33:51.858450','2018-05-17 15:33:51.858450',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bv',NULL,NULL,NULL,'','Bouvet Island',NULL),('Brazil','2018-05-17 15:33:51.683991','2018-05-17 15:33:51.683991',NULL,NULL,0,NULL,NULL,NULL,0,'dd/mm/yyyy','br',NULL,NULL,NULL,'America/Araguaina\nAmerica/Bahia\nAmerica/Belem\nAmerica/Boa_Vista\nAmerica/Campo_Grande\nAmerica/Cuiaba\nAmerica/Eirunepe\nAmerica/Fortaleza\nAmerica/Maceio\nAmerica/Manaus\nAmerica/Noronha\nAmerica/Porto_Velho\nAmerica/Recife\nAmerica/Rio_Branco\nAmerica/Santarem\nAmerica/Sao_Paulo','Brazil',NULL),('British Indian Ocean Territory','2018-05-17 15:33:51.840466','2018-05-17 15:33:51.840466',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','io',NULL,NULL,NULL,'Indian/Chagos','British Indian Ocean Territory',NULL),('Brunei Darussalam','2018-05-17 15:33:51.668247','2018-05-17 15:33:51.668247',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bn',NULL,NULL,NULL,'Asia/Brunei','Brunei Darussalam',NULL),('Bulgaria','2018-05-17 15:33:51.650102','2018-05-17 15:33:51.650102',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bg',NULL,NULL,NULL,'Europe/Sofia','Bulgaria',NULL),('Burkina Faso','2018-05-17 15:33:51.506061','2018-05-17 15:33:51.506061',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bf',NULL,NULL,NULL,'Africa/Ouagadougou','Burkina Faso',NULL),('Burundi','2018-05-17 15:33:51.784061','2018-05-17 15:33:51.784061',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bi',NULL,NULL,NULL,'Africa/Bujumbura','Burundi',NULL),('Cambodia','2018-05-17 15:33:51.492468','2018-05-17 15:33:51.492468',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','kh',NULL,NULL,NULL,'Asia/Phnom_Penh','Cambodia',NULL),('Cameroon','2018-05-17 15:33:51.504164','2018-05-17 15:33:51.504164',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cm',NULL,NULL,NULL,'Africa/Douala','Cameroon',NULL),('Canada','2018-05-17 15:33:51.459429','2018-05-17 15:33:51.459429',NULL,NULL,0,NULL,NULL,NULL,0,'mm-dd-yyyy','ca',NULL,NULL,NULL,'America/Atikokan\nAmerica/Blanc-Sablon\nAmerica/Cambridge_Bay\nAmerica/Creston\nAmerica/Dawson\nAmerica/Dawson_Creek\nAmerica/Edmonton\nAmerica/Glace_Bay\nAmerica/Goose_Bay\nAmerica/Halifax\nAmerica/Inuvik\nAmerica/Iqaluit\nAmerica/Moncton\nAmerica/Montreal\nAmerica/Nipigon\nAmerica/Pangnirtung\nAmerica/Rainy_River\nAmerica/Rankin_Inlet\nAmerica/Regina\nAmerica/Resolute\nAmerica/St_Johns\nAmerica/Swift_Current\nAmerica/Thunder_Bay\nAmerica/Toronto\nAmerica/Vancouver\nAmerica/Whitehorse\nAmerica/Winnipeg\nAmerica/Yellowknife','Canada',NULL),('Cape Verde','2018-05-17 15:33:51.698749','2018-05-17 15:33:51.698749',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cv',NULL,NULL,NULL,'Atlantic/Cape_Verde','Cape Verde',NULL),('Cayman Islands','2018-05-17 15:33:51.636338','2018-05-17 15:33:51.636338',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ky',NULL,NULL,NULL,'America/Cayman','Cayman Islands',NULL),('Central African Republic','2018-05-17 15:33:51.638206','2018-05-17 15:33:51.638206',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cf',NULL,NULL,NULL,'Africa/Bangui','Central African Republic',NULL),('Chad','2018-05-17 15:33:51.660121','2018-05-17 15:33:51.660121',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','td',NULL,NULL,NULL,'Africa/Ndjamena','Chad',NULL),('Chile','2018-05-17 15:33:51.726533','2018-05-17 15:33:51.726533',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cl',NULL,NULL,NULL,'America/Santiago\nPacific/Easter','Chile',NULL),('China','2018-05-17 15:33:51.620574','2018-05-17 15:33:51.620574',NULL,NULL,0,NULL,NULL,NULL,0,'yyyy-mm-dd','cn',NULL,NULL,NULL,'Asia/Chongqing\nAsia/Harbin\nAsia/Kashgar\nAsia/Shanghai\nAsia/Urumqi','China',NULL),('Christmas Island','2018-05-17 15:33:51.807829','2018-05-17 15:33:51.807829',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cx',NULL,NULL,NULL,'Indian/Christmas','Christmas Island',NULL),('Cocos (Keeling) Islands','2018-05-17 15:33:51.519693','2018-05-17 15:33:51.519693',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cc',NULL,NULL,NULL,'Indian/Cocos','Cocos (Keeling) Islands',NULL),('Colombia','2018-05-17 15:33:51.781904','2018-05-17 15:33:51.781904',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','co',NULL,NULL,NULL,'America/Bogota','Colombia',NULL),('Comoros','2018-05-17 15:33:51.880105','2018-05-17 15:33:51.880105',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','km',NULL,NULL,NULL,'Indian/Comoro','Comoros',NULL),('Congo','2018-05-17 15:33:51.872781','2018-05-17 15:33:51.872781',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cg',NULL,NULL,NULL,'','Congo',NULL),('Congo, The Democratic Republic of the','2018-05-17 15:33:51.838636','2018-05-17 15:33:51.838636',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cd',NULL,NULL,NULL,'','Congo, The Democratic Republic of the',NULL),('Cook Islands','2018-05-17 15:33:51.610537','2018-05-17 15:33:51.610537',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ck',NULL,NULL,NULL,'Pacific/Rarotonga','Cook Islands',NULL),('Costa Rica','2018-05-17 15:33:51.695496','2018-05-17 15:33:51.695496',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cr',NULL,NULL,NULL,'America/Costa_Rica','Costa Rica',NULL),('Croatia','2018-05-17 15:33:51.749008','2018-05-17 15:33:51.749008',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','hr',NULL,NULL,NULL,'Europe/Zagreb','Croatia',NULL),('Cuba','2018-05-17 15:33:51.613599','2018-05-17 15:33:51.613599',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cu',NULL,NULL,NULL,'America/Havana','Cuba',NULL),('Curaçao','2018-05-17 15:33:51.850906','2018-05-17 15:33:51.850906',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cw',NULL,NULL,NULL,'','Curaçao',NULL),('Cyprus','2018-05-17 15:33:51.664907','2018-05-17 15:33:51.664907',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cy',NULL,NULL,NULL,'Asia/Nicosia','Cyprus',NULL),('Czech Republic','2018-05-17 15:33:51.712135','2018-05-17 15:33:51.712135',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','cz',NULL,NULL,NULL,'Europe/Prague','Czech Republic',NULL),('Denmark','2018-05-17 15:33:51.827746','2018-05-17 15:33:51.827746',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','dk',NULL,NULL,NULL,'Europe/Copenhagen','Denmark',NULL),('Djibouti','2018-05-17 15:33:51.776443','2018-05-17 15:33:51.776443',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','dj',NULL,NULL,NULL,'Africa/Djibouti','Djibouti',NULL),('Dominica','2018-05-17 15:33:51.532946','2018-05-17 15:33:51.532946',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','dm',NULL,NULL,NULL,'America/Dominica','Dominica',NULL),('Dominican Republic','2018-05-17 15:33:51.625550','2018-05-17 15:33:51.625550',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','do',NULL,NULL,NULL,'America/Santo_Domingo','Dominican Republic',NULL),('Ecuador','2018-05-17 15:33:51.711152','2018-05-17 15:33:51.711152',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ec',NULL,NULL,NULL,'America/Guayaquil\nPacific/Galapagos','Ecuador',NULL),('Egypt','2018-05-17 15:33:51.859364','2018-05-17 15:33:51.859364',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','eg',NULL,NULL,NULL,'Africa/Cairo','Egypt',NULL),('El Salvador','2018-05-17 15:33:51.721610','2018-05-17 15:33:51.721610',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sv',NULL,NULL,NULL,'America/El_Salvador','El Salvador',NULL),('Equatorial Guinea','2018-05-17 15:33:51.768115','2018-05-17 15:33:51.768115',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gq',NULL,NULL,NULL,'Africa/Malabo','Equatorial Guinea',NULL),('Eritrea','2018-05-17 15:33:51.835047','2018-05-17 15:33:51.835047',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','er',NULL,NULL,NULL,'Africa/Asmara','Eritrea',NULL),('Estonia','2018-05-17 15:33:51.763994','2018-05-17 15:33:51.763994',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ee',NULL,NULL,NULL,'Europe/Tallinn','Estonia',NULL),('Ethiopia','2018-05-17 15:33:51.498496','2018-05-17 15:33:51.498496',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','et',NULL,NULL,NULL,'Africa/Addis_Ababa','Ethiopia',NULL),('Falkland Islands (Malvinas)','2018-05-17 15:33:51.616852','2018-05-17 15:33:51.616852',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','fk',NULL,NULL,NULL,'','Falkland Islands (Malvinas)',NULL),('Faroe Islands','2018-05-17 15:33:51.689448','2018-05-17 15:33:51.689448',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','fo',NULL,NULL,NULL,'Atlantic/Faroe','Faroe Islands',NULL),('Fiji','2018-05-17 15:33:51.788196','2018-05-17 15:33:51.788196',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','fj',NULL,NULL,NULL,'Pacific/Fiji','Fiji',NULL),('Finland','2018-05-17 15:33:51.633099','2018-05-17 15:33:51.633099',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','fi',NULL,NULL,NULL,'Europe/Helsinki','Finland',NULL),('France','2018-05-17 15:33:51.591038','2018-05-17 15:33:51.591038',NULL,NULL,0,NULL,NULL,NULL,0,'dd/mm/yyyy','fr',NULL,NULL,NULL,'Europe/Paris','France',NULL),('French Guiana','2018-05-17 15:33:51.549564','2018-05-17 15:33:51.549564',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gf',NULL,NULL,NULL,'America/Cayenne','French Guiana',NULL),('French Polynesia','2018-05-17 15:33:51.590115','2018-05-17 15:33:51.590115',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','pf',NULL,NULL,NULL,'Pacific/Gambier\nPacific/Marquesas\nPacific/Tahiti','French Polynesia',NULL),('French Southern Territories','2018-05-17 15:33:51.659052','2018-05-17 15:33:51.659052',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','tf',NULL,NULL,NULL,'','French Southern Territories',NULL),('Gabon','2018-05-17 15:33:51.877979','2018-05-17 15:33:51.877979',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ga',NULL,NULL,NULL,'Africa/Libreville','Gabon',NULL),('Gambia','2018-05-17 15:33:51.742085','2018-05-17 15:33:51.742085',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gm',NULL,NULL,NULL,'Africa/Banjul','Gambia',NULL),('Georgia','2018-05-17 15:33:51.739218','2018-05-17 15:33:51.739218',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ge',NULL,NULL,NULL,'Asia/Tbilisi','Georgia',NULL),('Germany','2018-05-17 15:33:51.824682','2018-05-17 15:33:51.824682',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','de',NULL,NULL,NULL,'Europe/Berlin','Germany',NULL),('Ghana','2018-05-17 15:33:51.509144','2018-05-17 15:33:51.509144',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gh',NULL,NULL,NULL,'Africa/Accra','Ghana',NULL),('Gibraltar','2018-05-17 15:33:51.702491','2018-05-17 15:33:51.702491',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gi',NULL,NULL,NULL,'Europe/Gibraltar','Gibraltar',NULL),('Greece','2018-05-17 15:33:51.874893','2018-05-17 15:33:51.874893',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gr',NULL,NULL,NULL,'Europe/Athens','Greece',NULL),('Greenland','2018-05-17 15:33:51.560144','2018-05-17 15:33:51.560144',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gl',NULL,NULL,NULL,'America/Danmarkshavn\nAmerica/Godthab\nAmerica/Scoresbysund\nAmerica/Thule','Greenland',NULL),('Grenada','2018-05-17 15:33:51.758327','2018-05-17 15:33:51.758327',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gd',NULL,NULL,NULL,'America/Grenada','Grenada',NULL),('Guadeloupe','2018-05-17 15:33:51.853598','2018-05-17 15:33:51.853598',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gp',NULL,NULL,NULL,'America/Guadeloupe','Guadeloupe',NULL),('Guam','2018-05-17 15:33:51.565717','2018-05-17 15:33:51.565717',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gu',NULL,NULL,NULL,'Pacific/Guam','Guam',NULL),('Guatemala','2018-05-17 15:33:51.521547','2018-05-17 15:33:51.521547',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gt',NULL,NULL,NULL,'America/Guatemala','Guatemala',NULL),('Guernsey','2018-05-17 15:33:51.752858','2018-05-17 15:33:51.752858',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gg',NULL,NULL,NULL,'Europe/London','Guernsey',NULL),('Guinea','2018-05-17 15:33:51.690393','2018-05-17 15:33:51.690393',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gn',NULL,NULL,NULL,'Africa/Conakry','Guinea',NULL),('Guinea-Bissau','2018-05-17 15:33:51.487991','2018-05-17 15:33:51.487991',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gw',NULL,NULL,NULL,'Africa/Bissau','Guinea-Bissau',NULL),('Guyana','2018-05-17 15:33:51.693617','2018-05-17 15:33:51.693617',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gy',NULL,NULL,NULL,'America/Guyana','Guyana',NULL),('Haiti','2018-05-17 15:33:51.731584','2018-05-17 15:33:51.731584',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ht',NULL,NULL,NULL,'America/Guatemala\nAmerica/Port-au-Prince','Haiti',NULL),('Heard Island and McDonald Islands','2018-05-17 15:33:51.778399','2018-05-17 15:33:51.778399',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','hm',NULL,NULL,NULL,'','Heard Island and McDonald Islands',NULL),('Holy See (Vatican City State)','2018-05-17 15:33:51.619630','2018-05-17 15:33:51.619630',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','va',NULL,NULL,NULL,'','Holy See (Vatican City State)',NULL),('Honduras','2018-05-17 15:33:51.854493','2018-05-17 15:33:51.854493',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','hn',NULL,NULL,NULL,'America/Tegucigalpa','Honduras',NULL),('Hong Kong','2018-05-17 15:33:51.735416','2018-05-17 15:33:51.735416',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','hk',NULL,NULL,NULL,'Asia/Hong_Kong','Hong Kong',NULL),('Hungary','2018-05-17 15:33:51.679829','2018-05-17 15:33:51.679829',NULL,NULL,0,NULL,NULL,NULL,0,'yyyy-mm-dd','hu',NULL,NULL,NULL,'Europe/Budapest','Hungary',NULL),('Iceland','2018-05-17 15:33:51.811622','2018-05-17 15:33:51.811622',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','is',NULL,NULL,NULL,'Atlantic/Reykjavik','Iceland',NULL),('India','2018-05-17 15:33:51.566625','2018-05-17 15:33:51.566625',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','in',NULL,NULL,NULL,'Asia/Kolkata','India',NULL),('Indonesia','2018-05-17 15:33:51.809724','2018-05-17 15:33:51.809724',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','id',NULL,NULL,NULL,'Asia/Jakarta\nAsia/Jayapura\nAsia/Makassar\nAsia/Pontianak','Indonesia',NULL),('Iran','2018-05-17 15:33:51.714975','2018-05-17 15:33:51.714975',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ir',NULL,NULL,NULL,'Asia/Tehran','Iran',NULL),('Iraq','2018-05-17 15:33:51.760577','2018-05-17 15:33:51.760577',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','iq',NULL,NULL,NULL,'Asia/Baghdad','Iraq',NULL),('Ireland','2018-05-17 15:33:51.705129','2018-05-17 15:33:51.705129',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ie',NULL,NULL,NULL,'Europe/Dublin','Ireland',NULL),('Isle of Man','2018-05-17 15:33:51.683068','2018-05-17 15:33:51.683068',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','im',NULL,NULL,NULL,'Europe/London','Isle of Man',NULL),('Israel','2018-05-17 15:33:51.842775','2018-05-17 15:33:51.842775',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','il',NULL,NULL,NULL,'Asia/Jerusalem','Israel',NULL),('Italy','2018-05-17 15:33:51.792889','2018-05-17 15:33:51.792889',NULL,NULL,0,NULL,NULL,NULL,0,'dd/mm/yyyy','it',NULL,NULL,NULL,'Europe/Rome','Italy',NULL),('Ivory Coast','2018-05-17 15:33:51.546451','2018-05-17 15:33:51.546451',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ci',NULL,NULL,NULL,'','Ivory Coast',NULL),('Jamaica','2018-05-17 15:33:51.539639','2018-05-17 15:33:51.539639',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','jm',NULL,NULL,NULL,'America/Jamaica','Jamaica',NULL),('Japan','2018-05-17 15:33:51.515951','2018-05-17 15:33:51.515951',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','jp',NULL,NULL,NULL,'Asia/Tokyo','Japan',NULL),('Jersey','2018-05-17 15:33:51.557074','2018-05-17 15:33:51.557074',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','je',NULL,NULL,NULL,'Europe/London','Jersey',NULL),('Jordan','2018-05-17 15:33:51.529208','2018-05-17 15:33:51.529208',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','jo',NULL,NULL,NULL,'Asia/Amman','Jordan',NULL),('Kazakhstan','2018-05-17 15:33:51.829742','2018-05-17 15:33:51.829742',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','kz',NULL,NULL,NULL,'Asia/Almaty\nAsia/Aqtau\nAsia/Aqtobe\nAsia/Oral\nAsia/Qyzylorda','Kazakhstan',NULL),('Kenya','2018-05-17 15:33:51.572915','2018-05-17 15:33:51.572915',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ke',NULL,NULL,NULL,'Africa/Nairobi','Kenya',NULL),('Kiribati','2018-05-17 15:33:51.730632','2018-05-17 15:33:51.730632',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ki',NULL,NULL,NULL,'Pacific/Enderbury\nPacific/Kiritimati\nPacific/Tarawa','Kiribati',NULL),('Korea, Democratic Peoples Republic of','2018-05-17 15:33:51.592268','2018-05-17 15:33:51.592268',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','kp',NULL,NULL,NULL,'','Korea, Democratic Peoples Republic of',NULL),('Korea, Republic of','2018-05-17 15:33:51.536791','2018-05-17 15:33:51.536791',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','kr',NULL,NULL,NULL,'','Korea, Republic of',NULL),('Kuwait','2018-05-17 15:33:51.525346','2018-05-17 15:33:51.525346',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','kw',NULL,NULL,NULL,'Asia/Kuwait','Kuwait',NULL),('Kyrgyzstan','2018-05-17 15:33:51.836872','2018-05-17 15:33:51.836872',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','kg',NULL,NULL,NULL,'Asia/Bishkek','Kyrgyzstan',NULL),('Lao Peoples Democratic Republic','2018-05-17 15:33:51.576698','2018-05-17 15:33:51.576698',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','la',NULL,NULL,NULL,'','Lao Peoples Democratic Republic',NULL),('Latvia','2018-05-17 15:33:51.848235','2018-05-17 15:33:51.848235',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','lv',NULL,NULL,NULL,'Europe/Riga','Latvia',NULL),('Lebanon','2018-05-17 15:33:51.769415','2018-05-17 15:33:51.769415',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','lb',NULL,NULL,NULL,'Asia/Beirut','Lebanon',NULL),('Lesotho','2018-05-17 15:33:51.569778','2018-05-17 15:33:51.569778',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ls',NULL,NULL,NULL,'Africa/Maseru','Lesotho',NULL),('Liberia','2018-05-17 15:33:51.534925','2018-05-17 15:33:51.534925',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','lr',NULL,NULL,NULL,'Africa/Monrovia','Liberia',NULL),('Libya','2018-05-17 15:33:51.634487','2018-05-17 15:33:51.634487',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ly',NULL,NULL,NULL,'Africa/Tripoli','Libya',NULL),('Liechtenstein','2018-05-17 15:33:51.643580','2018-05-17 15:33:51.643580',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','li',NULL,NULL,NULL,'Europe/Vaduz','Liechtenstein',NULL),('Lithuania','2018-05-17 15:33:51.490354','2018-05-17 15:33:51.490354',NULL,NULL,0,NULL,NULL,NULL,0,'yyyy-mm-dd','lt',NULL,NULL,NULL,'Europe/Vilnius','Lithuania',NULL),('Luxembourg','2018-05-17 15:33:51.697488','2018-05-17 15:33:51.697488',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','lu',NULL,NULL,NULL,'Europe/Luxembourg','Luxembourg',NULL),('Macao','2018-05-17 15:33:51.574827','2018-05-17 15:33:51.574827',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mo',NULL,NULL,NULL,'','Macao',NULL),('Macedonia','2018-05-17 15:33:51.844714','2018-05-17 15:33:51.844714',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mk',NULL,NULL,NULL,'','Macedonia',NULL),('Madagascar','2018-05-17 15:33:51.791961','2018-05-17 15:33:51.791961',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mg',NULL,NULL,NULL,'Indian/Antananarivo','Madagascar',NULL),('Malawi','2018-05-17 15:33:51.608669','2018-05-17 15:33:51.608669',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mw',NULL,NULL,NULL,'Africa/Blantyre','Malawi',NULL),('Malaysia','2018-05-17 15:33:51.671977','2018-05-17 15:33:51.671977',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','my',NULL,NULL,NULL,'Asia/Kuala_Lumpur\nAsia/Kuching','Malaysia',NULL),('Maldives','2018-05-17 15:33:51.802720','2018-05-17 15:33:51.802720',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mv',NULL,NULL,NULL,'Indian/Maldives','Maldives',NULL),('Mali','2018-05-17 15:33:51.646492','2018-05-17 15:33:51.646492',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ml',NULL,NULL,NULL,'Africa/Bamako','Mali',NULL),('Malta','2018-05-17 15:33:51.800803','2018-05-17 15:33:51.800803',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mt',NULL,NULL,NULL,'Europe/Malta','Malta',NULL),('Marshall Islands','2018-05-17 15:33:51.725566','2018-05-17 15:33:51.725566',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mh',NULL,NULL,NULL,'Pacific/Kwajalein\nPacific/Majuro','Marshall Islands',NULL),('Martinique','2018-05-17 15:33:51.545498','2018-05-17 15:33:51.545498',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mq',NULL,NULL,NULL,'America/Martinique','Martinique',NULL),('Mauritania','2018-05-17 15:33:51.582342','2018-05-17 15:33:51.582342',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mr',NULL,NULL,NULL,'Africa/Nouakchott','Mauritania',NULL),('Mauritius','2018-05-17 15:33:51.640570','2018-05-17 15:33:51.640570',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mu',NULL,NULL,NULL,'Indian/Mauritius','Mauritius',NULL),('Mayotte','2018-05-17 15:33:51.618718','2018-05-17 15:33:51.618718',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','yt',NULL,NULL,NULL,'Indian/Mayotte','Mayotte',NULL),('Mexico','2018-05-17 15:33:51.740170','2018-05-17 15:33:51.740170',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mx',NULL,NULL,NULL,'America/Bahia_Banderas\nAmerica/Cancun\nAmerica/Chihuahua\nAmerica/Hermosillo\nAmerica/Matamoros\nAmerica/Mazatlan\nAmerica/Merida\nAmerica/Mexico_City\nAmerica/Monterrey\nAmerica/Ojinaga\nAmerica/Santa_Isabel\nAmerica/Tijuana','Mexico',NULL),('Micronesia, Federated States of','2018-05-17 15:33:51.538658','2018-05-17 15:33:51.538658',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','fm',NULL,NULL,NULL,'','Micronesia, Federated States of',NULL),('Moldova, Republic of','2018-05-17 15:33:51.833307','2018-05-17 15:33:51.833307',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','md',NULL,NULL,NULL,'','Moldova, Republic of',NULL),('Monaco','2018-05-17 15:33:51.551465','2018-05-17 15:33:51.551465',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mc',NULL,NULL,NULL,'Europe/Monaco','Monaco',NULL),('Mongolia','2018-05-17 15:33:51.750925','2018-05-17 15:33:51.750925',NULL,NULL,0,NULL,NULL,NULL,0,'yyyy-mm-dd','mn',NULL,NULL,NULL,'Asia/Choibalsan\nAsia/Hovd\nAsia/Ulaanbaatar','Mongolia',NULL),('Montenegro','2018-05-17 15:33:51.615607','2018-05-17 15:33:51.615607',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','me',NULL,NULL,NULL,'Europe/Belgrade','Montenegro',NULL),('Montserrat','2018-05-17 15:33:51.841492','2018-05-17 15:33:51.841492',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ms',NULL,NULL,NULL,'America/Montserrat','Montserrat',NULL),('Morocco','2018-05-17 15:33:51.747090','2018-05-17 15:33:51.747090',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ma',NULL,NULL,NULL,'Africa/Casablanca','Morocco',NULL),('Mozambique','2018-05-17 15:33:51.676949','2018-05-17 15:33:51.676949',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mz',NULL,NULL,NULL,'Africa/Maputo','Mozambique',NULL),('Myanmar','2018-05-17 15:33:51.856610','2018-05-17 15:33:51.856610',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mm',NULL,NULL,NULL,'Asia/Rangoon','Myanmar',NULL),('Namibia','2018-05-17 15:33:51.756384','2018-05-17 15:33:51.756384',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','na',NULL,NULL,NULL,'Africa/Windhoek','Namibia',NULL),('Nauru','2018-05-17 15:33:51.603462','2018-05-17 15:33:51.603462',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','nr',NULL,NULL,NULL,'Pacific/Nauru','Nauru',NULL),('Nepal','2018-05-17 15:33:51.798876','2018-05-17 15:33:51.798876',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','np',NULL,NULL,NULL,'Asia/Kathmandu','Nepal',NULL),('Netherlands','2018-05-17 15:33:51.687972','2018-05-17 15:33:51.687972',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','nl',NULL,NULL,NULL,'Europe/Amsterdam','Netherlands',NULL),('New Caledonia','2018-05-17 15:33:51.639446','2018-05-17 15:33:51.639446',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','nc',NULL,NULL,NULL,'Pacific/Noumea','New Caledonia',NULL),('New Zealand','2018-05-17 15:33:51.554287','2018-05-17 15:33:51.554287',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','nz',NULL,NULL,NULL,'Pacific/Auckland\nPacific/Chatham','New Zealand',NULL),('Nicaragua','2018-05-17 15:33:51.862018','2018-05-17 15:33:51.862018',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ni',NULL,NULL,NULL,'America/Managua','Nicaragua',NULL),('Niger','2018-05-17 15:33:51.681695','2018-05-17 15:33:51.681695',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ne',NULL,NULL,NULL,'Africa/Niamey','Niger',NULL),('Nigeria','2018-05-17 15:33:51.709231','2018-05-17 15:33:51.709231',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ng',NULL,NULL,NULL,'Africa/Lagos','Nigeria',NULL),('Niue','2018-05-17 15:33:51.550556','2018-05-17 15:33:51.550556',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','nu',NULL,NULL,NULL,'Pacific/Niue','Niue',NULL),('Norfolk Island','2018-05-17 15:33:51.562931','2018-05-17 15:33:51.562931',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','nf',NULL,NULL,NULL,'Pacific/Norfolk','Norfolk Island',NULL),('Northern Mariana Islands','2018-05-17 15:33:51.804648','2018-05-17 15:33:51.804648',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mp',NULL,NULL,NULL,'Pacific/Saipan','Northern Mariana Islands',NULL),('Norway','2018-05-17 15:33:51.606802','2018-05-17 15:33:51.606802',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','no',NULL,NULL,NULL,'Europe/Oslo','Norway',NULL),('Oman','2018-05-17 15:33:51.541513','2018-05-17 15:33:51.541513',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','om',NULL,NULL,NULL,'Asia/Muscat','Oman',NULL),('Pakistan','2018-05-17 15:33:51.706404','2018-05-17 15:33:51.706404',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','pk',NULL,NULL,NULL,'Asia/Karachi','Pakistan',NULL),('Palau','2018-05-17 15:33:51.708267','2018-05-17 15:33:51.708267',NULL,NULL,0,NULL,NULL,NULL,0,'mm-dd-yyyy','pw',NULL,NULL,NULL,'Pacific/Palau','Palau',NULL),('Palestinian Territory, Occupied','2018-05-17 15:33:51.879178','2018-05-17 15:33:51.879178',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ps',NULL,NULL,NULL,'','Palestinian Territory, Occupied',NULL),('Panama','2018-05-17 15:33:51.692386','2018-05-17 15:33:51.692386',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','pa',NULL,NULL,NULL,'America/Panama','Panama',NULL),('Papua New Guinea','2018-05-17 15:33:51.817146','2018-05-17 15:33:51.817146',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','pg',NULL,NULL,NULL,'Pacific/Port_Moresby','Papua New Guinea',NULL),('Paraguay','2018-05-17 15:33:51.876170','2018-05-17 15:33:51.876170',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','py',NULL,NULL,NULL,'America/Asuncion','Paraguay',NULL),('Peru','2018-05-17 15:33:51.599095','2018-05-17 15:33:51.599095',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','pe',NULL,NULL,NULL,'America/Lima','Peru',NULL),('Philippines','2018-05-17 15:33:51.744299','2018-05-17 15:33:51.744299',NULL,NULL,0,NULL,NULL,NULL,0,'mm-dd-yyyy','ph',NULL,NULL,NULL,'Asia/Manila','Philippines',NULL),('Pitcairn','2018-05-17 15:33:51.520605','2018-05-17 15:33:51.520605',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','pn',NULL,NULL,NULL,'Pacific/Pitcairn','Pitcairn',NULL),('Poland','2018-05-17 15:33:51.831516','2018-05-17 15:33:51.831516',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','pl',NULL,NULL,NULL,'Europe/Warsaw','Poland',NULL),('Portugal','2018-05-17 15:33:51.762731','2018-05-17 15:33:51.762731',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','pt',NULL,NULL,NULL,'Atlantic/Azores\nAtlantic/Madeira\nEurope/Lisbon','Portugal',NULL),('Puerto Rico','2018-05-17 15:33:51.728392','2018-05-17 15:33:51.728392',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','pr',NULL,NULL,NULL,'America/Puerto_Rico','Puerto Rico',NULL),('Qatar','2018-05-17 15:33:51.670092','2018-05-17 15:33:51.670092',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','qa',NULL,NULL,NULL,'Asia/Qatar','Qatar',NULL),('Réunion','2018-05-17 15:33:51.808766','2018-05-17 15:33:51.808766',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','re',NULL,NULL,NULL,'','Réunion',NULL),('Romania','2018-05-17 15:33:51.654741','2018-05-17 15:33:51.654741',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ro',NULL,NULL,NULL,'Europe/Bucharest','Romania',NULL),('Russian Federation','2018-05-17 15:33:51.527325','2018-05-17 15:33:51.527325',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ru',NULL,NULL,NULL,'','Russian Federation',NULL),('Rwanda','2018-05-17 15:33:51.512887','2018-05-17 15:33:51.512887',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','rw',NULL,NULL,NULL,'Africa/Kigali','Rwanda',NULL),('Saint Barthélemy','2018-05-17 15:33:51.839525','2018-05-17 15:33:51.839525',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','bl',NULL,NULL,NULL,'','Saint Barthélemy',NULL),('Saint Helena, Ascension and Tristan da Cunha','2018-05-17 15:33:51.494599','2018-05-17 15:33:51.494599',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sh',NULL,NULL,NULL,'','Saint Helena, Ascension and Tristan da Cunha',NULL),('Saint Kitts and Nevis','2018-05-17 15:33:51.819051','2018-05-17 15:33:51.819051',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','kn',NULL,NULL,NULL,'America/St_Kitts','Saint Kitts and Nevis',NULL),('Saint Lucia','2018-05-17 15:33:51.587618','2018-05-17 15:33:51.587618',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','lc',NULL,NULL,NULL,'America/St_Lucia','Saint Lucia',NULL),('Saint Martin (French part)','2018-05-17 15:33:51.501297','2018-05-17 15:33:51.501297',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','mf',NULL,NULL,NULL,'','Saint Martin (French part)',NULL),('Saint Pierre and Miquelon','2018-05-17 15:33:51.724618','2018-05-17 15:33:51.724618',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','pm',NULL,NULL,NULL,'','Saint Pierre and Miquelon',NULL),('Saint Vincent and the Grenadines','2018-05-17 15:33:51.571711','2018-05-17 15:33:51.571711',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','vc',NULL,NULL,NULL,'America/St_Vincent','Saint Vincent and the Grenadines',NULL),('Samoa','2018-05-17 15:33:51.561093','2018-05-17 15:33:51.561093',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ws',NULL,NULL,NULL,'Pacific/Apia','Samoa',NULL),('San Marino','2018-05-17 15:33:51.588869','2018-05-17 15:33:51.588869',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sm',NULL,NULL,NULL,'Europe/Rome','San Marino',NULL),('Sao Tome and Principe','2018-05-17 15:33:51.481530','2018-05-17 15:33:51.481530',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','st',NULL,NULL,NULL,'','Sao Tome and Principe',NULL),('Saudi Arabia','2018-05-17 15:33:51.511014','2018-05-17 15:33:51.511014',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sa',NULL,NULL,NULL,'Asia/Riyadh','Saudi Arabia',NULL),('Senegal','2018-05-17 15:33:51.815585','2018-05-17 15:33:51.815585',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sn',NULL,NULL,NULL,'Africa/Dakar','Senegal',NULL),('Serbia','2018-05-17 15:33:51.865625','2018-05-17 15:33:51.865625',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','rs',NULL,NULL,NULL,'Europe/Belgrade','Serbia',NULL),('Seychelles','2018-05-17 15:33:51.604628','2018-05-17 15:33:51.604628',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sc',NULL,NULL,NULL,'Indian/Mahe','Seychelles',NULL),('Sierra Leone','2018-05-17 15:33:51.737345','2018-05-17 15:33:51.737345',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sl',NULL,NULL,NULL,'Africa/Freetown','Sierra Leone',NULL),('Singapore','2018-05-17 15:33:51.863822','2018-05-17 15:33:51.863822',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sg',NULL,NULL,NULL,'Asia/Singapore','Singapore',NULL),('Sint Maarten (Dutch part)','2018-05-17 15:33:51.873995','2018-05-17 15:33:51.873995',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sx',NULL,NULL,NULL,'','Sint Maarten (Dutch part)',NULL),('Slovakia','2018-05-17 15:33:51.595995','2018-05-17 15:33:51.595995',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sk',NULL,NULL,NULL,'Europe/Bratislava','Slovakia',NULL),('Slovenia','2018-05-17 15:33:51.720357','2018-05-17 15:33:51.720357',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','si',NULL,NULL,NULL,'Europe/Belgrade','Slovenia',NULL),('Solomon Islands','2018-05-17 15:33:51.584215','2018-05-17 15:33:51.584215',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sb',NULL,NULL,NULL,'Pacific/Guadalcanal','Solomon Islands',NULL),('Somalia','2018-05-17 15:33:51.597234','2018-05-17 15:33:51.597234',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','so',NULL,NULL,NULL,'Africa/Mogadishu','Somalia',NULL),('South Africa','2018-05-17 15:33:51.661445','2018-05-17 15:33:51.661445',NULL,NULL,0,NULL,NULL,NULL,0,'yyyy-mm-dd','za',NULL,NULL,NULL,'Africa/Johannesburg','South Africa',NULL),('South Georgia and the South Sandwich Islands','2018-05-17 15:33:51.667113','2018-05-17 15:33:51.667113',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gs',NULL,NULL,NULL,'','South Georgia and the South Sandwich Islands',NULL),('South Sudan','2018-05-17 15:33:51.850000','2018-05-17 15:33:51.850000',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ss',NULL,NULL,NULL,'Africa/Juba','South Sudan',NULL),('Spain','2018-05-17 15:33:51.780612','2018-05-17 15:33:51.780612',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','es',NULL,NULL,NULL,'Africa/Ceuta\nAtlantic/Canary\nEurope/Madrid','Spain',NULL),('Sri Lanka','2018-05-17 15:33:51.846489','2018-05-17 15:33:51.846489',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','lk',NULL,NULL,NULL,'Asia/Colombo','Sri Lanka',NULL),('Sudan','2018-05-17 15:33:51.796077','2018-05-17 15:33:51.796077',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sd',NULL,NULL,NULL,'Africa/Khartoum','Sudan',NULL),('Suriname','2018-05-17 15:33:51.805618','2018-05-17 15:33:51.805618',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sr',NULL,NULL,NULL,'America/Paramaribo','Suriname',NULL),('Svalbard and Jan Mayen','2018-05-17 15:33:51.771268','2018-05-17 15:33:51.771268',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sj',NULL,NULL,NULL,'','Svalbard and Jan Mayen',NULL),('Swaziland','2018-05-17 15:33:51.601465','2018-05-17 15:33:51.601465',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sz',NULL,NULL,NULL,'Africa/Mbabane','Swaziland',NULL),('Sweden','2018-05-17 15:33:51.647754','2018-05-17 15:33:51.647754',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','se',NULL,NULL,NULL,'Europe/Stockholm','Sweden',NULL),('Switzerland','2018-05-17 15:33:51.496514','2018-05-17 15:33:51.496514',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ch',NULL,NULL,NULL,'Europe/Zurich','Switzerland',NULL),('Syria','2018-05-17 15:33:51.851801','2018-05-17 15:33:51.851801',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','sy',NULL,NULL,NULL,'','Syria',NULL),('Taiwan','2018-05-17 15:33:51.785980','2018-05-17 15:33:51.785980',NULL,NULL,0,NULL,NULL,NULL,0,'yyyy-mm-dd','tw',NULL,NULL,NULL,'','Taiwan',NULL),('Tajikistan','2018-05-17 15:33:51.642663','2018-05-17 15:33:51.642663',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','tj',NULL,NULL,NULL,'Asia/Dushanbe','Tajikistan',NULL),('Tanzania','2018-05-17 15:33:51.543607','2018-05-17 15:33:51.543607',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','tz',NULL,NULL,NULL,'','Tanzania',NULL),('Thailand','2018-05-17 15:33:51.753802','2018-05-17 15:33:51.753802',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','th',NULL,NULL,NULL,'Asia/Bangkok','Thailand',NULL),('Timor-Leste','2018-05-17 15:33:51.624638','2018-05-17 15:33:51.624638',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','tl',NULL,NULL,NULL,'','Timor-Leste',NULL),('Togo','2018-05-17 15:33:51.514756','2018-05-17 15:33:51.514756',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','tg',NULL,NULL,NULL,'Africa/Lome','Togo',NULL),('Tokelau','2018-05-17 15:33:51.663903','2018-05-17 15:33:51.663903',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','tk',NULL,NULL,NULL,'Pacific/Fakaofo','Tokelau',NULL),('Tonga','2018-05-17 15:33:51.631203','2018-05-17 15:33:51.631203',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','to',NULL,NULL,NULL,'Pacific/Tongatapu','Tonga',NULL),('Trinidad and Tobago','2018-05-17 15:33:51.820434','2018-05-17 15:33:51.820434',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','tt',NULL,NULL,NULL,'America/Port_of_Spain','Trinidad and Tobago',NULL),('Tunisia','2018-05-17 15:33:51.774207','2018-05-17 15:33:51.774207',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','tn',NULL,NULL,NULL,'Africa/Tunis','Tunisia',NULL),('Turkey','2018-05-17 15:33:51.685879','2018-05-17 15:33:51.685879',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','tr',NULL,NULL,NULL,'Europe/Istanbul','Turkey',NULL),('Turkmenistan','2018-05-17 15:33:51.507307','2018-05-17 15:33:51.507307',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','tm',NULL,NULL,NULL,'Asia/Ashgabat','Turkmenistan',NULL),('Turks and Caicos Islands','2018-05-17 15:33:51.586647','2018-05-17 15:33:51.586647',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','tc',NULL,NULL,NULL,'','Turks and Caicos Islands',NULL),('Tuvalu','2018-05-17 15:33:51.723643','2018-05-17 15:33:51.723643',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','tv',NULL,NULL,NULL,'Pacific/Funafuti','Tuvalu',NULL),('Uganda','2018-05-17 15:33:51.677857','2018-05-17 15:33:51.677857',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ug',NULL,NULL,NULL,'Africa/Kampala','Uganda',NULL),('Ukraine','2018-05-17 15:33:51.627449','2018-05-17 15:33:51.627449',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ua',NULL,NULL,NULL,'Europe/Kiev\nEurope/Simferopol\nEurope/Uzhgorod\nEurope/Zaporozhye','Ukraine',NULL),('United Arab Emirates','2018-05-17 15:33:51.563847','2018-05-17 15:33:51.563847',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ae',NULL,NULL,NULL,'Asia/Dubai','United Arab Emirates',NULL),('United Kingdom','2018-05-17 15:33:51.869344','2018-05-17 15:33:51.869344',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','gb',NULL,NULL,NULL,'Europe/London','United Kingdom',NULL),('United States','2018-05-17 15:33:51.652457','2018-05-17 15:33:51.652457',NULL,NULL,0,NULL,NULL,NULL,0,'mm-dd-yyyy','us',NULL,NULL,NULL,'America/Adak\nAmerica/Anchorage\nAmerica/Boise\nAmerica/Chicago\nAmerica/Denver\nAmerica/Detroit\nAmerica/Indiana/Indianapolis\nAmerica/Indiana/Knox\nAmerica/Indiana/Marengo\nAmerica/Indiana/Petersburg\nAmerica/Indiana/Tell_City\nAmerica/Indiana/Vevay\nAmerica/Indiana/Vincennes\nAmerica/Indiana/Winamac\nAmerica/Juneau\nAmerica/Kentucky/Louisville\nAmerica/Kentucky/Monticello\nAmerica/Los_Angeles\nAmerica/Menominee\nAmerica/Metlakatla\nAmerica/New_York\nAmerica/Nome\nAmerica/North_Dakota/Beulah\nAmerica/North_Dakota/Center\nAmerica/North_Dakota/New_Salem\nAmerica/Phoenix\nAmerica/Denver\nAmerica/Sitka\nAmerica/Yakutat\nPacific/Honolulu','United States',NULL),('United States Minor Outlying Islands','2018-05-17 15:33:51.518752','2018-05-17 15:33:51.518752',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','um',NULL,NULL,NULL,'','United States Minor Outlying Islands',NULL),('Uruguay','2018-05-17 15:33:51.766157','2018-05-17 15:33:51.766157',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','uy',NULL,NULL,NULL,'America/Montevideo','Uruguay',NULL),('Uzbekistan','2018-05-17 15:33:51.772264','2018-05-17 15:33:51.772264',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','uz',NULL,NULL,NULL,'Asia/Samarkand\nAsia/Tashkent','Uzbekistan',NULL),('Vanuatu','2018-05-17 15:33:51.825862','2018-05-17 15:33:51.825862',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','vu',NULL,NULL,NULL,'Pacific/Efate','Vanuatu',NULL),('Venezuela, Bolivarian Republic of','2018-05-17 15:33:51.485914','2018-05-17 15:33:51.485914',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ve',NULL,NULL,NULL,'','Venezuela, Bolivarian Republic of',NULL),('Vietnam','2018-05-17 15:33:51.675096','2018-05-17 15:33:51.675096',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','vn',NULL,NULL,NULL,'','Vietnam',NULL),('Virgin Islands, British','2018-05-17 15:33:51.531995','2018-05-17 15:33:51.531995',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','vg',NULL,NULL,NULL,'','Virgin Islands, British',NULL),('Virgin Islands, U.S.','2018-05-17 15:33:51.556125','2018-05-17 15:33:51.556125',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','vi',NULL,NULL,NULL,'','Virgin Islands, U.S.',NULL),('Wallis and Futuna','2018-05-17 15:33:51.553345','2018-05-17 15:33:51.553345',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','wf',NULL,NULL,NULL,'','Wallis and Futuna',NULL),('Western Sahara','2018-05-17 15:33:51.612685','2018-05-17 15:33:51.612685',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','eh',NULL,NULL,NULL,'Africa/El_Aaiun','Western Sahara',NULL),('Yemen','2018-05-17 15:33:51.559233','2018-05-17 15:33:51.559233',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','ye',NULL,NULL,NULL,'Asia/Aden','Yemen',NULL),('Zambia','2018-05-17 15:33:51.813580','2018-05-17 15:33:51.813580',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','zm',NULL,NULL,NULL,'Africa/Lusaka','Zambia',NULL),('Zimbabwe','2018-05-17 15:33:51.822760','2018-05-17 15:33:51.822760',NULL,NULL,0,NULL,NULL,NULL,0,'dd-mm-yyyy','zw',NULL,NULL,NULL,'Africa/Harare','Zimbabwe',NULL);
/*!40000 ALTER TABLE `tabCountry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabCurrency`
--

DROP TABLE IF EXISTS `tabCurrency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabCurrency` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `currency_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `symbol` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `enabled` int(1) NOT NULL DEFAULT '0',
  `smallest_currency_fraction_value` decimal(18,6) NOT NULL DEFAULT '0.000000',
  `fraction_units` int(11) NOT NULL DEFAULT '0',
  `fraction` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `number_format` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabCurrency`
--

LOCK TABLES `tabCurrency` WRITE;
/*!40000 ALTER TABLE `tabCurrency` DISABLE KEYS */;
INSERT INTO `tabCurrency` VALUES ('AED','2018-05-17 15:33:51.564773','2018-05-17 15:33:51.883542','Administrator',NULL,0,NULL,NULL,NULL,0,'AED',NULL,NULL,'د.إ',NULL,1,0.000000,100,'Fils',NULL,'#,###.##'),('AFN','2018-05-17 15:33:51.579491','2018-05-17 15:33:51.579491',NULL,NULL,0,NULL,NULL,NULL,0,'AFN',NULL,NULL,'؋',NULL,0,0.000000,100,'Pul',NULL,'#,###.##'),('ALL','2018-05-17 15:33:51.548548','2018-05-17 15:33:51.548548',NULL,NULL,0,NULL,NULL,NULL,0,'ALL',NULL,NULL,'L',NULL,0,0.000000,100,'Qindarkë',NULL,'#,###.##'),('AMD','2018-05-17 15:33:51.623505','2018-05-17 15:33:51.623505',NULL,NULL,0,NULL,NULL,NULL,0,'AMD',NULL,NULL,'֏',NULL,0,0.000000,100,'Luma',NULL,'#,###.##'),('ARS','2018-05-17 15:33:51.503195','2018-05-17 15:33:51.503195',NULL,NULL,0,NULL,NULL,NULL,0,'ARS',NULL,NULL,'$',NULL,0,0.000000,100,'Centavo',NULL,'#.###,##'),('AUD','2018-05-17 15:33:51.645546','2018-05-17 15:33:51.883892','Administrator',NULL,0,NULL,NULL,NULL,0,'AUD',NULL,NULL,'$',NULL,1,0.000000,100,'Cent',NULL,'# ###.##'),('AWG','2018-05-17 15:33:51.500350','2018-05-17 15:33:51.500350',NULL,NULL,0,NULL,NULL,NULL,0,'AWG',NULL,NULL,'Afl',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('BAM','2018-05-17 15:33:51.524369','2018-05-17 15:33:51.524369',NULL,NULL,0,NULL,NULL,NULL,0,'BAM',NULL,NULL,'KM',NULL,0,0.000000,100,'Fening',NULL,'#.###,##'),('BBD','2018-05-17 15:33:51.790979','2018-05-17 15:33:51.790979',NULL,NULL,0,NULL,NULL,NULL,0,'BBD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('BDT','2018-05-17 15:33:51.581381','2018-05-17 15:33:51.581381',NULL,NULL,0,NULL,NULL,NULL,0,'BDT',NULL,NULL,'৳',NULL,0,0.000000,100,'Paisa',NULL,'#,###.##'),('BGN','2018-05-17 15:33:51.651281','2018-05-17 15:33:51.651281',NULL,NULL,0,NULL,NULL,NULL,0,'BGN',NULL,NULL,'лв',NULL,0,0.000000,100,'Stotinka',NULL,'#,###.##'),('BHD','2018-05-17 15:33:51.630253','2018-05-17 15:33:51.630253',NULL,NULL,0,NULL,NULL,NULL,0,'BHD',NULL,NULL,'.د.ب',NULL,0,0.000000,1000,'Fils',NULL,'#,###.###'),('BIF','2018-05-17 15:33:51.785008','2018-05-17 15:33:51.785008',NULL,NULL,0,NULL,NULL,NULL,0,'BIF',NULL,NULL,'Fr',NULL,0,0.000000,100,'Centime',NULL,'#,###.##'),('BMD','2018-05-17 15:33:51.595008','2018-05-17 15:33:51.595008',NULL,NULL,0,NULL,NULL,NULL,0,'BMD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('BND','2018-05-17 15:33:51.669142','2018-05-17 15:33:51.669142',NULL,NULL,0,NULL,NULL,NULL,0,'BND',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('BOB','2018-05-17 15:33:51.797905','2018-05-17 15:33:51.797905',NULL,NULL,0,NULL,NULL,NULL,0,'BOB',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('BRL','2018-05-17 15:33:51.684898','2018-05-17 15:33:51.684898',NULL,NULL,0,NULL,NULL,NULL,0,'BRL',NULL,NULL,'R$',NULL,0,0.000000,100,'Centavo',NULL,'#.###,##'),('BSD','2018-05-17 15:33:51.701511','2018-05-17 15:33:51.701511',NULL,NULL,0,NULL,NULL,NULL,0,'BSD',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('BTN','2018-05-17 15:33:51.795074','2018-05-17 15:33:51.795074',NULL,NULL,0,NULL,NULL,NULL,0,'BTN',NULL,NULL,'Nu.',NULL,0,0.000000,100,'Chetrum',NULL,'#,###.##'),('BWP','2018-05-17 15:33:51.868360','2018-05-17 15:33:51.868360',NULL,NULL,0,NULL,NULL,NULL,0,'BWP',NULL,NULL,'P',NULL,0,0.000000,100,'Thebe',NULL,'#,###.##'),('BZD','2018-05-17 15:33:51.734438','2018-05-17 15:33:51.734438',NULL,NULL,0,NULL,NULL,NULL,0,'BZD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('CAD','2018-05-17 15:33:51.480444','2018-05-17 15:33:51.480444',NULL,NULL,0,NULL,NULL,NULL,0,'CAD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('CHF','2018-05-17 15:33:51.497442','2018-05-17 15:33:51.884968','Administrator',NULL,0,NULL,NULL,NULL,0,'CHF',NULL,NULL,'Fr',NULL,1,0.050000,100,'Rappen[K]',NULL,'#\'###.##'),('CLP','2018-05-17 15:33:51.727428','2018-05-17 15:33:51.727428',NULL,NULL,0,NULL,NULL,NULL,0,'CLP',NULL,NULL,'$',NULL,0,0.000000,100,'Centavo',NULL,'#.###'),('CNY','2018-05-17 15:33:51.621503','2018-05-17 15:33:51.884624','Administrator',NULL,0,NULL,NULL,NULL,0,'CNY',NULL,NULL,NULL,NULL,1,0.000000,0,NULL,NULL,'#,###.##'),('COP','2018-05-17 15:33:51.782903','2018-05-17 15:33:51.782903',NULL,NULL,0,NULL,NULL,NULL,0,'COP',NULL,NULL,'$',NULL,0,0.000000,100,'Centavo',NULL,'#.###,##'),('CRC','2018-05-17 15:33:51.696419','2018-05-17 15:33:51.696419',NULL,NULL,0,NULL,NULL,NULL,0,'CRC',NULL,NULL,'₡',NULL,0,0.000000,100,'Céntimo',NULL,'#.###,##'),('CUP','2018-05-17 15:33:51.614607','2018-05-17 15:33:51.614607',NULL,NULL,0,NULL,NULL,NULL,0,'CUP',NULL,NULL,'$',NULL,0,0.000000,100,'Centavo',NULL,'#,###.##'),('CVE','2018-05-17 15:33:51.699643','2018-05-17 15:33:51.699643',NULL,NULL,0,NULL,NULL,NULL,0,'CVE',NULL,NULL,'Esc or $',NULL,0,0.000000,100,'Centavo',NULL,'#,###.##'),('CYP','2018-05-17 15:33:51.666002','2018-05-17 15:33:51.666002',NULL,NULL,0,NULL,NULL,NULL,0,'CYP',NULL,NULL,'€',NULL,0,0.000000,100,'Cent',NULL,'#.###,##'),('CZK','2018-05-17 15:33:51.713072','2018-05-17 15:33:51.713072',NULL,NULL,0,NULL,NULL,NULL,0,'CZK',NULL,NULL,'Kč',NULL,0,0.000000,100,'Haléř',NULL,'#.###,##'),('DJF','2018-05-17 15:33:51.777411','2018-05-17 15:33:51.777411',NULL,NULL,0,NULL,NULL,NULL,0,'DJF',NULL,NULL,'Fr',NULL,0,0.000000,100,'Centime',NULL,'#,###.##'),('DKK','2018-05-17 15:33:51.828792','2018-05-17 15:33:51.828792',NULL,NULL,0,NULL,NULL,NULL,0,'DKK',NULL,NULL,'kr',NULL,0,0.000000,100,'Øre',NULL,'#.###,##'),('DOP','2018-05-17 15:33:51.626487','2018-05-17 15:33:51.626487',NULL,NULL,0,NULL,NULL,NULL,0,'DOP',NULL,NULL,'$',NULL,0,0.000000,100,'Centavo',NULL,'#,###.##'),('DZD','2018-05-17 15:33:51.719404','2018-05-17 15:33:51.719404',NULL,NULL,0,NULL,NULL,NULL,0,'DZD',NULL,NULL,'د.ج',NULL,0,0.000000,100,'Santeem',NULL,'#,###.##'),('EEK','2018-05-17 15:33:51.765041','2018-05-17 15:33:51.765041',NULL,NULL,0,NULL,NULL,NULL,0,'EEK',NULL,NULL,'€',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('EGP','2018-05-17 15:33:51.861103','2018-05-17 15:33:51.861103',NULL,NULL,0,NULL,NULL,NULL,0,'EGP',NULL,NULL,'£ or ج.م',NULL,0,0.000000,100,'Piastre[F]',NULL,'#,###.##'),('ERN','2018-05-17 15:33:51.835915','2018-05-17 15:33:51.835915',NULL,NULL,0,NULL,NULL,NULL,0,'ERN',NULL,NULL,'Nfk',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('EUR','2018-05-17 15:33:51.552390','2018-05-17 15:33:51.883198','Administrator',NULL,0,NULL,NULL,NULL,0,'EUR',NULL,NULL,'€',NULL,1,0.000000,100,'Cent',NULL,'#,###.##'),('FJD','2018-05-17 15:33:51.789115','2018-05-17 15:33:51.789115',NULL,NULL,0,NULL,NULL,NULL,0,'FJD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('FKP','2018-05-17 15:33:51.617742','2018-05-17 15:33:51.617742',NULL,NULL,0,NULL,NULL,NULL,0,'FKP',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('GBP','2018-05-17 15:33:51.870805','2018-05-17 15:33:51.882204','Administrator',NULL,0,NULL,NULL,NULL,0,'GBP',NULL,NULL,'£',NULL,1,0.000000,100,'Penny',NULL,'#,###.##'),('GHS','2018-05-17 15:33:51.510065','2018-05-17 15:33:51.510065',NULL,NULL,0,NULL,NULL,NULL,0,'GHS',NULL,NULL,'₵',NULL,0,0.000000,100,'Pesewa',NULL,'#,###.##'),('GIP','2018-05-17 15:33:51.704134','2018-05-17 15:33:51.704134',NULL,NULL,0,NULL,NULL,NULL,0,'GIP',NULL,NULL,'£',NULL,0,0.000000,100,'Penny',NULL,'#,###.##'),('GMD','2018-05-17 15:33:51.743189','2018-05-17 15:33:51.743189',NULL,NULL,0,NULL,NULL,NULL,0,'GMD',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('GNF','2018-05-17 15:33:51.691299','2018-05-17 15:33:51.691299',NULL,NULL,0,NULL,NULL,NULL,0,'GNF',NULL,NULL,'Fr',NULL,0,0.000000,100,'Centime',NULL,'#,###.##'),('GTQ','2018-05-17 15:33:51.522461','2018-05-17 15:33:51.522461',NULL,NULL,0,NULL,NULL,NULL,0,'GTQ',NULL,NULL,'Q',NULL,0,0.000000,100,'Centavo',NULL,'#,###.##'),('GYD','2018-05-17 15:33:51.694548','2018-05-17 15:33:51.694548',NULL,NULL,0,NULL,NULL,NULL,0,'GYD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('HKD','2018-05-17 15:33:51.736359','2018-05-17 15:33:51.736359',NULL,NULL,0,NULL,NULL,NULL,0,'HKD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('HNL','2018-05-17 15:33:51.855507','2018-05-17 15:33:51.855507',NULL,NULL,0,NULL,NULL,NULL,0,'HNL',NULL,NULL,'L',NULL,0,0.000000,100,'Centavo',NULL,'#,###.##'),('HRK','2018-05-17 15:33:51.749921','2018-05-17 15:33:51.749921',NULL,NULL,0,NULL,NULL,NULL,0,'HRK',NULL,NULL,'kn',NULL,0,0.000000,100,'Lipa',NULL,'#.###,##'),('HTG','2018-05-17 15:33:51.732526','2018-05-17 15:33:51.732526',NULL,NULL,0,NULL,NULL,NULL,0,'HTG',NULL,NULL,'G',NULL,0,0.000000,100,'Centime',NULL,'#,###.##'),('HUF','2018-05-17 15:33:51.680720','2018-05-17 15:33:51.680720',NULL,NULL,0,NULL,NULL,NULL,0,'HUF',NULL,NULL,'Ft',NULL,0,0.000000,100,'Fillér',NULL,'#.###'),('IDR','2018-05-17 15:33:51.810636','2018-05-17 15:33:51.810636',NULL,NULL,0,NULL,NULL,NULL,0,'IDR',NULL,NULL,'Rp',NULL,0,0.000000,100,'Sen',NULL,'#.###,##'),('ILS','2018-05-17 15:33:51.843717','2018-05-17 15:33:51.843717',NULL,NULL,0,NULL,NULL,NULL,0,'ILS',NULL,NULL,'₪',NULL,0,0.000000,100,'Agora',NULL,'#,###.##'),('INR','2018-05-17 15:33:51.567545','2018-05-17 15:33:51.881454','Administrator',NULL,0,NULL,NULL,NULL,0,'INR',NULL,NULL,'₹',NULL,1,0.000000,100,'Paisa',NULL,'#,##,###.##'),('IQD','2018-05-17 15:33:51.761761','2018-05-17 15:33:51.761761',NULL,NULL,0,NULL,NULL,NULL,0,'IQD',NULL,NULL,'ع.د',NULL,0,0.000000,1000,'Fils',NULL,'#,###.###'),('IRR','2018-05-17 15:33:51.715904','2018-05-17 15:33:51.715904',NULL,NULL,0,NULL,NULL,NULL,0,'IRR',NULL,NULL,'﷼',NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('ISK','2018-05-17 15:33:51.812568','2018-05-17 15:33:51.812568',NULL,NULL,0,NULL,NULL,NULL,0,'ISK',NULL,NULL,'kr',NULL,0,0.000000,100,'Eyrir',NULL,'#.###'),('JMD','2018-05-17 15:33:51.540563','2018-05-17 15:33:51.540563',NULL,NULL,0,NULL,NULL,NULL,0,'JMD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('JOD','2018-05-17 15:33:51.530141','2018-05-17 15:33:51.530141',NULL,NULL,0,NULL,NULL,NULL,0,'JOD',NULL,NULL,'د.ا',NULL,0,0.000000,100,'Piastre[H]',NULL,'#,###.###'),('JPY','2018-05-17 15:33:51.516882','2018-05-17 15:33:51.884300','Administrator',NULL,0,NULL,NULL,NULL,0,'JPY',NULL,NULL,'¥',NULL,1,0.000000,100,'Sen[G]',NULL,'#,###'),('KES','2018-05-17 15:33:51.573854','2018-05-17 15:33:51.573854',NULL,NULL,0,NULL,NULL,NULL,0,'KES',NULL,NULL,'Sh',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('KGS','2018-05-17 15:33:51.837729','2018-05-17 15:33:51.837729',NULL,NULL,0,NULL,NULL,NULL,0,'KGS',NULL,NULL,'лв',NULL,0,0.000000,100,'Tyiyn',NULL,'#,###.##'),('KHR','2018-05-17 15:33:51.493546','2018-05-17 15:33:51.493546',NULL,NULL,0,NULL,NULL,NULL,0,'KHR',NULL,NULL,'៛',NULL,0,0.000000,100,'Sen',NULL,'#,###.##'),('KMF','2018-05-17 15:33:51.880948','2018-05-17 15:33:51.880948',NULL,NULL,0,NULL,NULL,NULL,0,'KMF',NULL,NULL,'Fr',NULL,0,0.000000,100,'Centime',NULL,'#,###.##'),('KPW','2018-05-17 15:33:51.593170','2018-05-17 15:33:51.593170',NULL,NULL,0,NULL,NULL,NULL,0,'KPW',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('KRW','2018-05-17 15:33:51.537681','2018-05-17 15:33:51.537681',NULL,NULL,0,NULL,NULL,NULL,0,'KRW',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###'),('KWD','2018-05-17 15:33:51.526328','2018-05-17 15:33:51.526328',NULL,NULL,0,NULL,NULL,NULL,0,'KWD',NULL,NULL,'د.ك',NULL,0,0.000000,1000,'Fils',NULL,'#,###.###'),('KYD','2018-05-17 15:33:51.637256','2018-05-17 15:33:51.637256',NULL,NULL,0,NULL,NULL,NULL,0,'KYD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('KZ','2018-05-17 15:33:51.657918','2018-05-17 15:33:51.657918',NULL,NULL,0,NULL,NULL,NULL,0,'KZ',NULL,NULL,'AOA',NULL,0,0.000000,100,'Cêntimo',NULL,'#,###.##'),('KZT','2018-05-17 15:33:51.830591','2018-05-17 15:33:51.830591',NULL,NULL,0,NULL,NULL,NULL,0,'KZT',NULL,NULL,'₸',NULL,0,0.000000,100,'Tïın',NULL,'#,###.##'),('LAK','2018-05-17 15:33:51.577585','2018-05-17 15:33:51.577585',NULL,NULL,0,NULL,NULL,NULL,0,'LAK',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('LBP','2018-05-17 15:33:51.770316','2018-05-17 15:33:51.770316',NULL,NULL,0,NULL,NULL,NULL,0,'LBP',NULL,NULL,'ل.ل',NULL,0,0.000000,100,'Piastre',NULL,'#,###.##'),('LKR','2018-05-17 15:33:51.847343','2018-05-17 15:33:51.847343',NULL,NULL,0,NULL,NULL,NULL,0,'LKR',NULL,NULL,'Rs',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('LRD','2018-05-17 15:33:51.535814','2018-05-17 15:33:51.535814',NULL,NULL,0,NULL,NULL,NULL,0,'LRD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('LSL','2018-05-17 15:33:51.570712','2018-05-17 15:33:51.570712',NULL,NULL,0,NULL,NULL,NULL,0,'LSL',NULL,NULL,'L',NULL,0,0.000000,100,'Sente',NULL,'#,###.##'),('LTL','2018-05-17 15:33:51.491360','2018-05-17 15:33:51.491360',NULL,NULL,0,NULL,NULL,NULL,0,'LTL',NULL,NULL,'Lt',NULL,0,0.000000,100,'Centas',NULL,'# ###,##'),('LVL','2018-05-17 15:33:51.849102','2018-05-17 15:33:51.849102',NULL,NULL,0,NULL,NULL,NULL,0,'LVL',NULL,NULL,'Ls',NULL,0,0.000000,100,'Santīms',NULL,'#,###.##'),('LYD','2018-05-17 15:33:51.635385','2018-05-17 15:33:51.635385',NULL,NULL,0,NULL,NULL,NULL,0,'LYD',NULL,NULL,'ل.د',NULL,0,0.000000,1000,'Dirham',NULL,'#,###.###'),('MAD','2018-05-17 15:33:51.748011','2018-05-17 15:33:51.748011',NULL,NULL,0,NULL,NULL,NULL,0,'MAD',NULL,NULL,'د.م.',NULL,0,0.000000,100,'Centime',NULL,'#,###.##'),('MDL','2018-05-17 15:33:51.834159','2018-05-17 15:33:51.834159',NULL,NULL,0,NULL,NULL,NULL,0,'MDL',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('MKD','2018-05-17 15:33:51.845580','2018-05-17 15:33:51.845580',NULL,NULL,0,NULL,NULL,NULL,0,'MKD',NULL,NULL,'ден',NULL,0,0.000000,100,'Deni',NULL,'#,###.##'),('MMK','2018-05-17 15:33:51.857520','2018-05-17 15:33:51.857520',NULL,NULL,0,NULL,NULL,NULL,0,'MMK',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('MNT','2018-05-17 15:33:51.751838','2018-05-17 15:33:51.751838',NULL,NULL,0,NULL,NULL,NULL,0,'MNT',NULL,NULL,'₮',NULL,0,0.000000,100,'Möngö',NULL,'#,###.##'),('MOP','2018-05-17 15:33:51.575720','2018-05-17 15:33:51.575720',NULL,NULL,0,NULL,NULL,NULL,0,'MOP',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('MRO','2018-05-17 15:33:51.583268','2018-05-17 15:33:51.583268',NULL,NULL,0,NULL,NULL,NULL,0,'MRO',NULL,NULL,'UM',NULL,0,0.000000,5,'Khoums',NULL,'#,###.##'),('MTL','2018-05-17 15:33:51.801735','2018-05-17 15:33:51.801735',NULL,NULL,0,NULL,NULL,NULL,0,'MTL',NULL,NULL,'€',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('MUR','2018-05-17 15:33:51.641666','2018-05-17 15:33:51.641666',NULL,NULL,0,NULL,NULL,NULL,0,'MUR',NULL,NULL,'₨',NULL,0,0.000000,100,'Cent',NULL,'#,###'),('MVR','2018-05-17 15:33:51.803675','2018-05-17 15:33:51.803675',NULL,NULL,0,NULL,NULL,NULL,0,'MVR',NULL,NULL,'.ރ',NULL,0,0.000000,100,'Laari',NULL,'#,###.##'),('MWK','2018-05-17 15:33:51.609557','2018-05-17 15:33:51.609557',NULL,NULL,0,NULL,NULL,NULL,0,'MWK',NULL,NULL,'MK',NULL,0,0.000000,100,'Tambala',NULL,'#,###.##'),('MXN','2018-05-17 15:33:51.741110','2018-05-17 15:33:51.741110',NULL,NULL,0,NULL,NULL,NULL,0,'MXN',NULL,NULL,'$',NULL,0,0.000000,100,'Centavo',NULL,'#,###.##'),('MYR','2018-05-17 15:33:51.672874','2018-05-17 15:33:51.672874',NULL,NULL,0,NULL,NULL,NULL,0,'MYR',NULL,NULL,'RM',NULL,0,0.000000,100,'Sen',NULL,'#,###.##'),('NAD','2018-05-17 15:33:51.757347','2018-05-17 15:33:51.757347',NULL,NULL,0,NULL,NULL,NULL,0,'NAD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('NGN','2018-05-17 15:33:51.710175','2018-05-17 15:33:51.710175',NULL,NULL,0,NULL,NULL,NULL,0,'NGN',NULL,NULL,'₦',NULL,0,0.000000,100,'Kobo',NULL,'#,###.##'),('NIO','2018-05-17 15:33:51.862883','2018-05-17 15:33:51.862883',NULL,NULL,0,NULL,NULL,NULL,0,'NIO',NULL,NULL,'C$',NULL,0,0.000000,100,'Centavo',NULL,'#,###.##'),('NOK','2018-05-17 15:33:51.607695','2018-05-17 15:33:51.607695',NULL,NULL,0,NULL,NULL,NULL,0,'NOK',NULL,NULL,'kr',NULL,0,0.000000,100,'Øre',NULL,'#.###,##'),('NPR','2018-05-17 15:33:51.799820','2018-05-17 15:33:51.799820',NULL,NULL,0,NULL,NULL,NULL,0,'NPR',NULL,NULL,'₨',NULL,0,0.000000,100,'Paisa',NULL,'#,###.##'),('NZD','2018-05-17 15:33:51.555175','2018-05-17 15:33:51.555175',NULL,NULL,0,NULL,NULL,NULL,0,'NZD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('OMR','2018-05-17 15:33:51.542492','2018-05-17 15:33:51.542492',NULL,NULL,0,NULL,NULL,NULL,0,'OMR',NULL,NULL,'ر.ع.',NULL,0,0.000000,1000,'Baisa',NULL,'#,###.###'),('PEN','2018-05-17 15:33:51.600503','2018-05-17 15:33:51.600503',NULL,NULL,0,NULL,NULL,NULL,0,'PEN',NULL,NULL,'S/.',NULL,0,0.000000,100,'Céntimo',NULL,'#,###.##'),('PGK','2018-05-17 15:33:51.818075','2018-05-17 15:33:51.818075',NULL,NULL,0,NULL,NULL,NULL,0,'PGK',NULL,NULL,'K',NULL,0,0.000000,100,'Toea',NULL,'#,###.##'),('PHP','2018-05-17 15:33:51.745308','2018-05-17 15:33:51.745308',NULL,NULL,0,NULL,NULL,NULL,0,'PHP',NULL,NULL,'₱',NULL,0,0.000000,100,'Centavo',NULL,'#,###.##'),('PKR','2018-05-17 15:33:51.707316','2018-05-17 15:33:51.707316',NULL,NULL,0,NULL,NULL,NULL,0,'PKR',NULL,NULL,'₨',NULL,0,0.000000,100,'Paisa',NULL,'#,###.##'),('PLN','2018-05-17 15:33:51.832383','2018-05-17 15:33:51.832383',NULL,NULL,0,NULL,NULL,NULL,0,'PLN',NULL,NULL,'zł',NULL,0,0.000000,100,'Grosz',NULL,'#.###,##'),('PYG','2018-05-17 15:33:51.877042','2018-05-17 15:33:51.877042',NULL,NULL,0,NULL,NULL,NULL,0,'PYG',NULL,NULL,'₲',NULL,0,0.000000,100,'Céntimo',NULL,'#,###.##'),('QAR','2018-05-17 15:33:51.670974','2018-05-17 15:33:51.670974',NULL,NULL,0,NULL,NULL,NULL,0,'QAR',NULL,NULL,'ر.ق',NULL,0,0.000000,100,'Dirham',NULL,'#,###.##'),('RON','2018-05-17 15:33:51.655719','2018-05-17 15:33:51.655719',NULL,NULL,0,NULL,NULL,NULL,0,'RON',NULL,NULL,'lei',NULL,0,0.000000,100,'Bani',NULL,'#,###.##'),('RSD','2018-05-17 15:33:51.866531','2018-05-17 15:33:51.866531',NULL,NULL,0,NULL,NULL,NULL,0,'RSD',NULL,NULL,'дин.',NULL,0,0.000000,100,'Para',NULL,'#,###.##'),('RUB','2018-05-17 15:33:51.528253','2018-05-17 15:33:51.528253',NULL,NULL,0,NULL,NULL,NULL,0,'RUB',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#.###,##'),('RWF','2018-05-17 15:33:51.513784','2018-05-17 15:33:51.513784',NULL,NULL,0,NULL,NULL,NULL,0,'RWF',NULL,NULL,'Fr',NULL,0,0.000000,100,'Centime',NULL,'#,###.##'),('SAR','2018-05-17 15:33:51.511939','2018-05-17 15:33:51.511939',NULL,NULL,0,NULL,NULL,NULL,0,'SAR',NULL,NULL,'ر.س',NULL,0,0.000000,100,'Halala',NULL,'#,###.##'),('SBD','2018-05-17 15:33:51.585338','2018-05-17 15:33:51.585338',NULL,NULL,0,NULL,NULL,NULL,0,'SBD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('SCR','2018-05-17 15:33:51.605564','2018-05-17 15:33:51.605564',NULL,NULL,0,NULL,NULL,NULL,0,'SCR',NULL,NULL,'₨',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('SEK','2018-05-17 15:33:51.648885','2018-05-17 15:33:51.648885',NULL,NULL,0,NULL,NULL,NULL,0,'SEK',NULL,NULL,'kr',NULL,0,0.000000,100,'Öre',NULL,'#.###,##'),('SGD','2018-05-17 15:33:51.864684','2018-05-17 15:33:51.864684',NULL,NULL,0,NULL,NULL,NULL,0,'SGD',NULL,NULL,'$',NULL,0,0.000000,100,'Sen',NULL,'#,###.##'),('SHP','2018-05-17 15:33:51.495564','2018-05-17 15:33:51.495564',NULL,NULL,0,NULL,NULL,NULL,0,'SHP',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('SLL','2018-05-17 15:33:51.738261','2018-05-17 15:33:51.738261',NULL,NULL,0,NULL,NULL,NULL,0,'SLL',NULL,NULL,'Le',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('SOS','2018-05-17 15:33:51.598147','2018-05-17 15:33:51.598147',NULL,NULL,0,NULL,NULL,NULL,0,'SOS',NULL,NULL,'Sh',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('STD','2018-05-17 15:33:51.482691','2018-05-17 15:33:51.482691',NULL,NULL,0,NULL,NULL,NULL,0,'STD',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('SVC','2018-05-17 15:33:51.722565','2018-05-17 15:33:51.722565',NULL,NULL,0,NULL,NULL,NULL,0,'SVC',NULL,NULL,'₡',NULL,0,0.000000,100,'Centavo',NULL,'#,###.##'),('SYP','2018-05-17 15:33:51.852661','2018-05-17 15:33:51.852661',NULL,NULL,0,NULL,NULL,NULL,0,'SYP',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('SZL','2018-05-17 15:33:51.602400','2018-05-17 15:33:51.602400',NULL,NULL,0,NULL,NULL,NULL,0,'SZL',NULL,NULL,'L',NULL,0,0.000000,100,'Cent',NULL,'#, ###.##'),('THB','2018-05-17 15:33:51.754947','2018-05-17 15:33:51.754947',NULL,NULL,0,NULL,NULL,NULL,0,'THB',NULL,NULL,'฿',NULL,0,0.000000,100,'Satang',NULL,'#,###.##'),('TMM','2018-05-17 15:33:51.508193','2018-05-17 15:33:51.508193',NULL,NULL,0,NULL,NULL,NULL,0,'TMM',NULL,NULL,'m',NULL,0,0.000000,100,'Tennesi',NULL,'#,###.##'),('TND','2018-05-17 15:33:51.775179','2018-05-17 15:33:51.775179',NULL,NULL,0,NULL,NULL,NULL,0,'TND',NULL,NULL,'د.ت',NULL,0,0.000000,1000,'Millime',NULL,'#,###.###'),('TOP','2018-05-17 15:33:51.632138','2018-05-17 15:33:51.632138',NULL,NULL,0,NULL,NULL,NULL,0,'TOP',NULL,NULL,'T$',NULL,0,0.000000,100,'Seniti[L]',NULL,'#,###.##'),('TRY','2018-05-17 15:33:51.686767','2018-05-17 15:33:51.686767',NULL,NULL,0,NULL,NULL,NULL,0,'TRY',NULL,NULL,'₺',NULL,0,0.000000,100,'Kuruş',NULL,'#,###.##'),('TTD','2018-05-17 15:33:51.821498','2018-05-17 15:33:51.821498',NULL,NULL,0,NULL,NULL,NULL,0,'TTD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('TWD','2018-05-17 15:33:51.786884','2018-05-17 15:33:51.786884',NULL,NULL,0,NULL,NULL,NULL,0,'TWD',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('TZS','2018-05-17 15:33:51.544533','2018-05-17 15:33:51.544533',NULL,NULL,0,NULL,NULL,NULL,0,'TZS',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#,###.##'),('UAH','2018-05-17 15:33:51.628381','2018-05-17 15:33:51.628381',NULL,NULL,0,NULL,NULL,NULL,0,'UAH',NULL,NULL,'₴',NULL,0,0.000000,100,'Kopiyka',NULL,'#,###.##'),('UGX','2018-05-17 15:33:51.678849','2018-05-17 15:33:51.678849',NULL,NULL,0,NULL,NULL,NULL,0,'UGX',NULL,NULL,'Sh',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('USD','2018-05-17 15:33:51.653581','2018-05-17 15:33:51.881846','Administrator',NULL,0,NULL,NULL,NULL,0,'USD',NULL,NULL,'$',NULL,1,0.050000,100,'Cent',NULL,'#,###.##'),('UYU','2018-05-17 15:33:51.767110','2018-05-17 15:33:51.767110',NULL,NULL,0,NULL,NULL,NULL,0,'UYU',NULL,NULL,'$',NULL,0,0.000000,100,'Centésimo',NULL,'#.###,##'),('UZS','2018-05-17 15:33:51.773206','2018-05-17 15:33:51.773206',NULL,NULL,0,NULL,NULL,NULL,0,'UZS',NULL,NULL,'лв',NULL,0,0.000000,100,'Tiyin',NULL,'#,###.##'),('VEF','2018-05-17 15:33:51.486818','2018-05-17 15:33:51.486818',NULL,NULL,0,NULL,NULL,NULL,0,'VEF',NULL,NULL,'Bs.',NULL,0,0.000000,100,'Centimos',NULL,'#.###,##'),('VND','2018-05-17 15:33:51.675985','2018-05-17 15:33:51.675985',NULL,NULL,0,NULL,NULL,NULL,0,'VND',NULL,NULL,NULL,NULL,0,0.000000,0,NULL,NULL,'#.###'),('VUV','2018-05-17 15:33:51.826734','2018-05-17 15:33:51.826734',NULL,NULL,0,NULL,NULL,NULL,0,'VUV',NULL,NULL,'Vt',NULL,0,0.000000,0,'None',NULL,'#,###'),('WST','2018-05-17 15:33:51.561982','2018-05-17 15:33:51.561982',NULL,NULL,0,NULL,NULL,NULL,0,'WST',NULL,NULL,'T',NULL,0,0.000000,100,'Sene',NULL,'#,###.##'),('XAF','2018-05-17 15:33:51.505110','2018-05-17 15:33:51.505110',NULL,NULL,0,NULL,NULL,NULL,0,'XAF',NULL,NULL,'FCFA',NULL,0,0.000000,100,'Centime',NULL,'#,###.##'),('XCD','2018-05-17 15:33:51.533888','2018-05-17 15:33:51.533888',NULL,NULL,0,NULL,NULL,NULL,0,'XCD',NULL,NULL,'$',NULL,0,0.000000,100,'Cent',NULL,'#,###.##'),('XOF','2018-05-17 15:33:51.489167','2018-05-17 15:33:51.489167',NULL,NULL,0,NULL,NULL,NULL,0,'XOF',NULL,NULL,'CFA',NULL,0,0.000000,100,'Centime',NULL,'#,###.##'),('ZAR','2018-05-17 15:33:51.662951','2018-05-17 15:33:51.662951',NULL,NULL,0,NULL,NULL,NULL,0,'ZAR',NULL,NULL,'R',NULL,0,0.000000,100,'Cent',NULL,'# ###.##'),('ZMW','2018-05-17 15:33:51.814529','2018-05-17 15:33:51.814529',NULL,NULL,0,NULL,NULL,NULL,0,'ZMW',NULL,NULL,'ZK',NULL,0,0.000000,100,'Ngwee',NULL,'#,###.##'),('ZWD','2018-05-17 15:33:51.823698','2018-05-17 15:33:51.823698',NULL,NULL,0,NULL,NULL,NULL,0,'ZWD',NULL,NULL,'P',NULL,0,0.000000,100,'Thebe',NULL,'# ###.##');
/*!40000 ALTER TABLE `tabCurrency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabCustom DocPerm`
--

DROP TABLE IF EXISTS `tabCustom DocPerm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabCustom DocPerm` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `share` int(1) NOT NULL DEFAULT '1',
  `export` int(1) NOT NULL DEFAULT '1',
  `cancel` int(1) NOT NULL DEFAULT '0',
  `user_permission_doctypes` longtext COLLATE utf8mb4_unicode_ci,
  `create` int(1) NOT NULL DEFAULT '1',
  `submit` int(1) NOT NULL DEFAULT '0',
  `write` int(1) NOT NULL DEFAULT '1',
  `role` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print` int(1) NOT NULL DEFAULT '1',
  `import` int(1) NOT NULL DEFAULT '0',
  `permlevel` int(11) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `apply_user_permissions` int(1) NOT NULL DEFAULT '0',
  `read` int(1) NOT NULL DEFAULT '1',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `set_user_permissions` int(1) NOT NULL DEFAULT '0',
  `report` int(1) NOT NULL DEFAULT '1',
  `amend` int(1) NOT NULL DEFAULT '0',
  `email` int(1) NOT NULL DEFAULT '1',
  `if_owner` int(1) NOT NULL DEFAULT '0',
  `delete` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabCustom DocPerm`
--

LOCK TABLES `tabCustom DocPerm` WRITE;
/*!40000 ALTER TABLE `tabCustom DocPerm` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabCustom DocPerm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabCustom Field`
--

DROP TABLE IF EXISTS `tabCustom Field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabCustom Field` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `collapsible` int(1) NOT NULL DEFAULT '0',
  `print_width` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_copy` int(1) NOT NULL DEFAULT '0',
  `depends_on` longtext COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `in_list_view` int(1) NOT NULL DEFAULT '0',
  `reqd` int(1) NOT NULL DEFAULT '0',
  `ignore_xss_filter` int(1) NOT NULL DEFAULT '0',
  `in_global_search` int(1) NOT NULL DEFAULT '0',
  `collapsible_depends_on` longtext COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `read_only` int(1) NOT NULL DEFAULT '0',
  `print_hide` int(1) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `ignore_user_permissions` int(1) NOT NULL DEFAULT '0',
  `label` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_hide_if_no_value` int(1) NOT NULL DEFAULT '0',
  `width` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hidden` int(1) NOT NULL DEFAULT '0',
  `permlevel` int(11) NOT NULL DEFAULT '0',
  `columns` int(11) NOT NULL DEFAULT '0',
  `insert_after` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `bold` int(1) NOT NULL DEFAULT '0',
  `search_index` int(1) NOT NULL DEFAULT '0',
  `allow_on_submit` int(1) NOT NULL DEFAULT '0',
  `precision` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dt` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unique` int(1) NOT NULL DEFAULT '0',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `default` text COLLATE utf8mb4_unicode_ci,
  `in_standard_filter` int(1) NOT NULL DEFAULT '0',
  `fieldname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fieldtype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Data',
  `options` text COLLATE utf8mb4_unicode_ci,
  `report_hide` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `dt` (`dt`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabCustom Field`
--

LOCK TABLES `tabCustom Field` WRITE;
/*!40000 ALTER TABLE `tabCustom Field` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabCustom Field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabCustom Role`
--

DROP TABLE IF EXISTS `tabCustom Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabCustom Role` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `ref_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `report` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `page` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabCustom Role`
--

LOCK TABLES `tabCustom Role` WRITE;
/*!40000 ALTER TABLE `tabCustom Role` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabCustom Role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabCustom Script`
--

DROP TABLE IF EXISTS `tabCustom Script`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabCustom Script` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `script` longtext COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `dt` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `script_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Client',
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabCustom Script`
--

LOCK TABLES `tabCustom Script` WRITE;
/*!40000 ALTER TABLE `tabCustom Script` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabCustom Script` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabCustomize Form Field`
--

DROP TABLE IF EXISTS `tabCustomize Form Field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabCustomize Form Field` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `collapsible` int(1) NOT NULL DEFAULT '0',
  `print_width` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `depends_on` longtext COLLATE utf8mb4_unicode_ci,
  `in_list_view` int(1) NOT NULL DEFAULT '0',
  `reqd` int(1) NOT NULL DEFAULT '0',
  `in_global_search` int(1) NOT NULL DEFAULT '0',
  `collapsible_depends_on` longtext COLLATE utf8mb4_unicode_ci,
  `bold` int(1) NOT NULL DEFAULT '0',
  `read_only` int(1) NOT NULL DEFAULT '0',
  `print_hide` int(1) NOT NULL DEFAULT '0',
  `ignore_user_permissions` int(1) NOT NULL DEFAULT '0',
  `label` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_hide_if_no_value` int(1) NOT NULL DEFAULT '0',
  `width` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_bulk_edit` int(1) NOT NULL DEFAULT '0',
  `hidden` int(1) NOT NULL DEFAULT '0',
  `in_filter` int(1) NOT NULL DEFAULT '0',
  `permlevel` int(11) NOT NULL DEFAULT '0',
  `columns` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `allow_on_submit` int(1) NOT NULL DEFAULT '0',
  `precision` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_custom_field` int(1) NOT NULL DEFAULT '0',
  `remember_last_selected_value` int(1) NOT NULL DEFAULT '0',
  `unique` int(1) NOT NULL DEFAULT '0',
  `default` text COLLATE utf8mb4_unicode_ci,
  `in_standard_filter` int(1) NOT NULL DEFAULT '0',
  `options` text COLLATE utf8mb4_unicode_ci,
  `length` int(11) NOT NULL DEFAULT '0',
  `fieldname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fieldtype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Data',
  `report_hide` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `label` (`label`),
  KEY `fieldname` (`fieldname`),
  KEY `fieldtype` (`fieldtype`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabCustomize Form Field`
--

LOCK TABLES `tabCustomize Form Field` WRITE;
/*!40000 ALTER TABLE `tabCustomize Form Field` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabCustomize Form Field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabData Import`
--

DROP TABLE IF EXISTS `tabData Import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabData Import` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `no_email` int(1) NOT NULL DEFAULT '1',
  `total_rows` int(11) NOT NULL DEFAULT '0',
  `skip_errors` int(1) NOT NULL DEFAULT '0',
  `overwrite` int(1) NOT NULL DEFAULT '0',
  `reference_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_only_errors` int(1) NOT NULL DEFAULT '1',
  `only_update` int(1) NOT NULL DEFAULT '0',
  `amended_from` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `submit_after_import` int(1) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `import_status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `error_file` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `log_details` longtext COLLATE utf8mb4_unicode_ci,
  `ignore_encoding_errors` int(1) NOT NULL DEFAULT '0',
  `import_file` text COLLATE utf8mb4_unicode_ci,
  `_seen` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabData Import`
--

LOCK TABLES `tabData Import` WRITE;
/*!40000 ALTER TABLE `tabData Import` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabData Import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabData Migration Connector`
--

DROP TABLE IF EXISTS `tabData Migration Connector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabData Migration Connector` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `username` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `python_module` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `database_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hostname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connector_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connector_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabData Migration Connector`
--

LOCK TABLES `tabData Migration Connector` WRITE;
/*!40000 ALTER TABLE `tabData Migration Connector` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabData Migration Connector` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabData Migration Mapping`
--

DROP TABLE IF EXISTS `tabData Migration Mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabData Migration Mapping` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `remote_objectname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `local_primary_key` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mapping_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migration_id_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `local_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `mapping_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remote_primary_key` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_length` int(11) NOT NULL DEFAULT '10',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `condition` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabData Migration Mapping`
--

LOCK TABLES `tabData Migration Mapping` WRITE;
/*!40000 ALTER TABLE `tabData Migration Mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabData Migration Mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabData Migration Mapping Detail`
--

DROP TABLE IF EXISTS `tabData Migration Mapping Detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabData Migration Mapping Detail` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `is_child_table` int(1) NOT NULL DEFAULT '0',
  `remote_fieldname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `child_table_mapping` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `local_fieldname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabData Migration Mapping Detail`
--

LOCK TABLES `tabData Migration Mapping Detail` WRITE;
/*!40000 ALTER TABLE `tabData Migration Mapping Detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabData Migration Mapping Detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabData Migration Plan`
--

DROP TABLE IF EXISTS `tabData Migration Plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabData Migration Plan` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `plan_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `module` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabData Migration Plan`
--

LOCK TABLES `tabData Migration Plan` WRITE;
/*!40000 ALTER TABLE `tabData Migration Plan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabData Migration Plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabData Migration Plan Mapping`
--

DROP TABLE IF EXISTS `tabData Migration Plan Mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabData Migration Plan Mapping` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `enabled` int(1) NOT NULL DEFAULT '1',
  `mapping` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabData Migration Plan Mapping`
--

LOCK TABLES `tabData Migration Plan Mapping` WRITE;
/*!40000 ALTER TABLE `tabData Migration Plan Mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabData Migration Plan Mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabData Migration Run`
--

DROP TABLE IF EXISTS `tabData Migration Run`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabData Migration Run` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `current_mapping_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_migration_connector` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `push_insert` int(11) NOT NULL DEFAULT '0',
  `pull_update` int(11) NOT NULL DEFAULT '0',
  `current_mapping_delete_start` int(11) NOT NULL DEFAULT '0',
  `log` longtext COLLATE utf8mb4_unicode_ci,
  `data_migration_plan` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `push_failed` longtext COLLATE utf8mb4_unicode_ci,
  `pull_insert` int(11) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Pending',
  `push_update` int(11) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `total_pages` int(11) NOT NULL DEFAULT '0',
  `percent_complete` decimal(18,6) NOT NULL DEFAULT '0.000000',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `push_delete` int(11) NOT NULL DEFAULT '0',
  `current_mapping_action` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pull_failed` longtext COLLATE utf8mb4_unicode_ci,
  `current_mapping_start` int(11) NOT NULL DEFAULT '0',
  `current_mapping` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabData Migration Run`
--

LOCK TABLES `tabData Migration Run` WRITE;
/*!40000 ALTER TABLE `tabData Migration Run` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabData Migration Run` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabDefaultValue`
--

DROP TABLE IF EXISTS `tabDefaultValue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabDefaultValue` (
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `defvalue` text COLLATE utf8mb4_unicode_ci,
  `defkey` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `defaultvalue_parent_defkey_index` (`parent`,`defkey`),
  KEY `defaultvalue_parent_parenttype_index` (`parent`,`parenttype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabDefaultValue`
--

LOCK TABLES `tabDefaultValue` WRITE;
/*!40000 ALTER TABLE `tabDefaultValue` DISABLE KEYS */;
INSERT INTO `tabDefaultValue` VALUES ('02d0cd6238','2018-05-17 15:34:11.772201','2018-05-17 15:34:11.772201','Administrator','Administrator',0,'__default','system_defaults','__default',0,'setup-wizard','desktop:home_page'),('02f1e319b2','2018-05-17 15:33:49.560208','2018-05-17 15:33:49.560208','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','enable_two_factor_auth'),('1339d39f4a','2018-05-17 15:33:49.361874','2018-05-17 15:33:49.361874','Administrator','Administrator',0,'__default','system_defaults','__default',0,'','currency_precision'),('16a1d22b62','2018-05-17 15:33:49.619156','2018-05-17 15:33:49.619156','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','disable_standard_email_footer'),('31d80074d5','2018-05-17 15:33:49.399718','2018-05-17 15:33:49.399718','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','enable_scheduler'),('3aadcafbc1','2018-05-17 15:34:43.546659','2018-05-17 15:34:43.546659','Administrator','Administrator',0,'__global','system_defaults','__default',0,'[\"frappe\", \"bench_manager\"]','installed_apps'),('40f3e14651','2018-05-17 15:33:49.428694','2018-05-17 15:33:49.428694','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','apply_strict_user_permissions'),('4b6acc8d6f','2018-05-17 15:33:49.334361','2018-05-17 15:33:49.334361','Administrator','Administrator',0,'__default','system_defaults','__default',0,'#,###.##','number_format'),('4d1f6ddabe','2018-05-17 15:33:49.633271','2018-05-17 15:33:49.633271','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','hide_footer_in_auto_email_reports'),('549a6dc6a0','2018-05-17 15:33:49.348209','2018-05-17 15:33:49.348209','Administrator','Administrator',0,'__default','system_defaults','__default',0,'','float_precision'),('5518331b6f','2018-05-17 15:33:49.384448','2018-05-17 15:33:49.384448','Administrator','Administrator',0,'__default','system_defaults','__default',0,'3','backup_limit'),('58f23302a7','2018-05-17 15:33:52.090010','2018-05-17 15:33:52.090010','Administrator','Administrator',0,'__default','system_defaults','__default',0,'','email_user_password'),('687a4045a3','2018-05-17 15:33:49.306173','2018-05-17 15:33:49.306173','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','setup_complete'),('6e3e16e014','2018-05-17 15:33:49.515392','2018-05-17 15:33:49.515392','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','allow_login_using_mobile_number'),('7611676872','2018-05-17 15:33:49.604270','2018-05-17 15:33:49.604270','Administrator','Administrator',0,'__default','system_defaults','__default',0,'Frappe Framework','otp_issuer_name'),('795c70ecd7','2018-05-17 15:33:49.320379','2018-05-17 15:33:49.320379','Administrator','Administrator',0,'__default','system_defaults','__default',0,'yyyy-mm-dd','date_format'),('932c5bcf6f','2018-05-17 15:33:49.458527','2018-05-17 15:33:49.458527','Administrator','Administrator',0,'__default','system_defaults','__default',0,'720:00','session_expiry_mobile'),('b1dbc1f45f','2018-05-17 15:33:49.529545','2018-05-17 15:33:49.529545','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','allow_login_using_user_name'),('b47a14b57b','2018-05-17 15:33:49.543855','2018-05-17 15:33:49.543855','Administrator','Administrator',0,'__default','system_defaults','__default',0,'1','allow_error_traceback'),('be46d8a9cb','2018-05-17 15:33:49.574752','2018-05-17 15:33:49.574752','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','bypass_2fa_for_retricted_ip_users'),('bfb5f8770b','2018-05-17 15:33:49.472508','2018-05-17 15:33:49.472508','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','enable_password_policy'),('cc6c282635','2018-05-17 15:33:49.414044','2018-05-17 15:33:49.414044','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','ignore_user_permissions_if_missing'),('d0098b6cbb','2018-05-17 15:33:49.292531','2018-05-17 15:33:49.292531','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','is_first_startup'),('d00ac5a413','2018-05-17 15:33:49.589214','2018-05-17 15:33:49.589214','Administrator','Administrator',0,'__default','system_defaults','__default',0,'OTP App','two_factor_method'),('d11c9ae809','2018-05-17 15:33:49.487057','2018-05-17 15:33:49.487057','Administrator','Administrator',0,'__default','system_defaults','__default',0,'2','minimum_password_score'),('ed3d03bb9e','2018-05-17 15:33:49.501307','2018-05-17 15:33:49.501307','Administrator','Administrator',0,'__default','system_defaults','__default',0,'0','deny_multiple_sessions'),('ffb874bf59','2018-05-17 15:33:49.444225','2018-05-17 15:33:49.444225','Administrator','Administrator',0,'__default','system_defaults','__default',0,'06:00','session_expiry');
/*!40000 ALTER TABLE `tabDefaultValue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabDeleted Document`
--

DROP TABLE IF EXISTS `tabDeleted Document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabDeleted Document` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `new_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `deleted_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `data` longtext COLLATE utf8mb4_unicode_ci,
  `restored` int(1) NOT NULL DEFAULT '0',
  `deleted_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabDeleted Document`
--

LOCK TABLES `tabDeleted Document` WRITE;
/*!40000 ALTER TABLE `tabDeleted Document` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabDeleted Document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabDesktop Icon`
--

DROP TABLE IF EXISTS `tabDesktop Icon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabDesktop Icon` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `color` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blocked` int(1) NOT NULL DEFAULT '0',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `custom` int(1) NOT NULL DEFAULT '0',
  `_report` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hidden` int(1) NOT NULL DEFAULT '0',
  `type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `standard` int(1) NOT NULL DEFAULT '0',
  `link` text COLLATE utf8mb4_unicode_ci,
  `force_show` int(1) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `icon` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reverse` int(1) NOT NULL DEFAULT '0',
  `module_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `unique_module_name_owner_standard` (`module_name`,`owner`,`standard`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabDesktop Icon`
--

LOCK TABLES `tabDesktop Icon` WRITE;
/*!40000 ALTER TABLE `tabDesktop Icon` DISABLE KEYS */;
INSERT INTO `tabDesktop Icon` VALUES ('167ce67b36','2018-05-17 15:33:48.902937','2018-05-17 15:33:48.902937','Administrator','Administrator',0,NULL,NULL,NULL,0,'#FFF5A7','frappe','Tools',0,NULL,0,NULL,0,'module',NULL,NULL,1,NULL,0,NULL,'octicon octicon-calendar',1,'Desk',NULL),('1a27d76a2f','2018-05-17 15:33:48.922543','2018-05-17 15:33:48.922543','Administrator','Administrator',0,NULL,NULL,NULL,6,'#589494','frappe','Developer',0,NULL,0,NULL,1,'module',NULL,NULL,1,NULL,0,NULL,'octicon octicon-circuit-board',0,'Core',NULL),('30f7dfe655','2018-05-17 15:33:48.925435','2018-05-17 15:33:48.925435','Administrator','Administrator',0,NULL,NULL,NULL,7,'#FFAEDB','frappe','Contacts',0,NULL,0,NULL,1,'module',NULL,NULL,1,NULL,0,NULL,'octicon octicon-book',0,'Contacts',NULL),('3b028cec8b','2018-05-17 15:34:43.528034','2018-05-17 15:34:43.528034','Administrator','Administrator',0,NULL,NULL,NULL,0,'black','bench_manager','Bench Manager',0,NULL,0,NULL,0,'module',NULL,NULL,1,'Form/Bench Settings',0,NULL,'fa fa-gamepad',0,'Bench Manager',NULL),('88a04dbb22','2018-05-17 15:33:48.916153','2018-05-17 15:33:48.916153','Administrator','Administrator',0,NULL,NULL,NULL,4,'#bdc3c7','frappe','Setup',0,NULL,0,NULL,1,'module',NULL,NULL,1,NULL,0,NULL,'octicon octicon-settings',1,'Setup',NULL),('8c3cc20833','2018-05-17 15:33:48.910385','2018-05-17 15:33:48.910385','Administrator','Administrator',0,NULL,NULL,NULL,2,'#16a085','frappe','Website',0,NULL,0,NULL,1,'module',NULL,NULL,1,NULL,0,NULL,'octicon octicon-globe',0,'Website',NULL),('caf0aa597b','2018-05-17 15:33:48.913274','2018-05-17 15:33:48.913274','Administrator','Administrator',0,NULL,NULL,NULL,3,'#16a085','frappe','Integrations',0,NULL,0,NULL,1,'module',NULL,NULL,1,NULL,0,NULL,'octicon octicon-globe',0,'Integrations',NULL),('d471f218ed','2018-05-17 15:33:48.906023','2018-05-17 15:33:48.906023','Administrator','Administrator',0,NULL,NULL,NULL,1,'#AA784D','frappe','File Manager',0,NULL,0,NULL,1,'list',NULL,NULL,1,'List/File',0,NULL,'octicon octicon-file-directory',0,'File Manager','File'),('f2da4e8033','2018-05-17 15:33:48.919211','2018-05-17 15:33:48.919211','Administrator','Administrator',0,NULL,NULL,NULL,5,'#589494','frappe','Email Inbox',0,NULL,0,NULL,0,'list',NULL,NULL,1,'List/Communication/Inbox',0,NULL,'fa fa-envelope-o',0,'Email Inbox','Communication');
/*!40000 ALTER TABLE `tabDesktop Icon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabDocField`
--

DROP TABLE IF EXISTS `tabDocField`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabDocField` (
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `fieldname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oldfieldname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fieldtype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Data',
  `oldfieldtype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options` text COLLATE utf8mb4_unicode_ci,
  `search_index` int(1) NOT NULL DEFAULT '0',
  `hidden` int(1) NOT NULL DEFAULT '0',
  `set_only_once` int(1) NOT NULL DEFAULT '0',
  `print_hide` int(1) NOT NULL DEFAULT '0',
  `report_hide` int(1) NOT NULL DEFAULT '0',
  `reqd` int(1) NOT NULL DEFAULT '0',
  `bold` int(1) NOT NULL DEFAULT '0',
  `in_global_search` int(1) NOT NULL DEFAULT '0',
  `collapsible` int(1) NOT NULL DEFAULT '0',
  `unique` int(1) NOT NULL DEFAULT '0',
  `no_copy` int(1) NOT NULL DEFAULT '0',
  `allow_on_submit` int(1) NOT NULL DEFAULT '0',
  `trigger` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collapsible_depends_on` longtext COLLATE utf8mb4_unicode_ci,
  `depends_on` longtext COLLATE utf8mb4_unicode_ci,
  `permlevel` int(11) NOT NULL DEFAULT '0',
  `ignore_user_permissions` int(1) NOT NULL DEFAULT '0',
  `width` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_width` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `columns` int(11) NOT NULL DEFAULT '0',
  `default` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `in_list_view` int(1) NOT NULL DEFAULT '0',
  `in_standard_filter` int(1) NOT NULL DEFAULT '0',
  `read_only` int(1) NOT NULL DEFAULT '0',
  `precision` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `length` int(11) NOT NULL DEFAULT '0',
  `ignore_xss_filter` int(1) NOT NULL DEFAULT '0',
  `in_filter` int(1) NOT NULL DEFAULT '0',
  `print_hide_if_no_value` int(1) NOT NULL DEFAULT '0',
  `allow_bulk_edit` int(1) NOT NULL DEFAULT '0',
  `remember_last_selected_value` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `label` (`label`),
  KEY `fieldtype` (`fieldtype`),
  KEY `fieldname` (`fieldname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabDocField`
--

LOCK TABLES `tabDocField` WRITE;
/*!40000 ALTER TABLE `tabDocField` DISABLE KEYS */;
INSERT INTO `tabDocField` VALUES ('003f3dc25e','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',8,'section_break_4','Permissions',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('00a4f358f4','2013-01-10 16:34:24.000000','2018-05-17 15:33:43.053495','Administrator','Administrator',0,'SMS Settings','fields','DocType',4,'static_parameters_section',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50%',NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('00d171b6e6','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',12,'condition_detail','Condition Detail',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0112d4ca20','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','fields','DocType',8,'restrict_to_domain','Restrict To Domain',NULL,'Link',NULL,'Domain',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('01194ba606','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',17,'skype','Skype',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('012dec7c9d','2017-08-17 14:51:33.577230','2018-05-17 15:34:43.329531','Administrator','Administrator',0,'Site Backup','fields','DocType',1,'site_name','Site Name',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,'',0,0,0,0,0,0),('0147407e34','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',48,'feedback_request','Feedback Request',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('0183a2ff6e','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',32,'amount_based_on_field','Amount Based On Field',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'accept_payment',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('018ec4b270','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',6,'is_first_startup','Is First Startup',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('01d1abe728','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',14,'message','Message',NULL,'Code',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('01fc5f4b74','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',18,'pull_update','Pull Update',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0202515a9f','2013-03-07 12:26:33.000000','2018-05-17 15:33:45.033410','Administrator','Administrator',0,'Website Slideshow Item','fields','DocType',1,'image','Image',NULL,'Attach',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('020a0ffdbb','2017-02-13 17:33:25.157332','2018-05-17 15:33:43.349585','Administrator','Administrator',0,'User Permission for Page and Report','fields','DocType',3,'report','Report',NULL,'Link',NULL,'Report',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.set_role_for == \'Report\'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0240740e2e','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',4,'email','Email','email','Data','Data','Email',0,0,0,0,0,1,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('02548f4126','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',37,'otp_issuer_name','OTP Issuer Name',NULL,'Data',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'enable_two_factor_auth',0,0,NULL,NULL,0,'Frappe Framework',NULL,0,0,0,'',0,0,0,0,0,0),('026c62804a','2016-09-21 08:03:01.009852','2018-05-17 15:33:47.615721','Administrator','Administrator',0,'PayPal Settings','fields','DocType',4,'paypal_sandbox','Use Sandbox',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Check this if you are testing your payment using the Sandbox API',0,0,0,'',0,0,0,0,0,0),('02972f5503','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',20,'short_bio','More Information',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,'enabled',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('02a6cd083b','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',7,'reference_doctype','Reference DocType',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,2,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('02b0b8eb44','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',25,'hidden','Hidden',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('02f44d836e','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',46,'block_modules','Block Modules',NULL,'Table',NULL,'Block Module',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,1,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('036abcf351','2016-09-21 08:03:01.009852','2018-05-17 15:33:47.615721','Administrator','Administrator',0,'PayPal Settings','fields','DocType',5,'redirect_to','Redirect To',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Mention transaction completion page URL',0,0,0,'',0,0,0,0,0,0),('0383f6181b','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',7,'show_title','Show Title',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('03b17e37c5','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',15,'attachment_limit','Attachment Limit (MB)',NULL,'Int',NULL,'domain.attachment_limit',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.domain && doc.enable_incoming',0,0,NULL,NULL,0,'1','Ignore attachments over this size',0,0,0,'',0,0,0,0,0,0),('03dd3914e2','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',10,'track_changes','Track Changes',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('041ce26d17','2016-10-19 12:26:42.569185','2018-05-17 15:33:46.626499','Administrator','Administrator',0,'Kanban Board Column','fields','DocType',2,'status','Status',NULL,'Select',NULL,'Active\nArchived',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Active',NULL,1,0,0,'',0,0,0,0,0,0),('04411fc79c','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',6,'module','Module',NULL,'Link',NULL,'Module Def',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'is_standard',0,0,NULL,NULL,0,NULL,NULL,0,1,0,'',0,0,0,0,0,0),('047f631382','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',19,'links','Links',NULL,'Table',NULL,'Dynamic Link',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0494418c9a','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',6,'communication_date','Date',NULL,'Datetime',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Now',NULL,0,0,0,'',0,0,0,0,0,0),('04a45a7b86','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',21,'footer_items','Footer Items',NULL,'Table',NULL,'Top Bar Item',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('04d8443321','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',12,'query','Query',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.report_type==\"Query Report\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('04df4ff17d','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',6,'app','App',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('04f4c504fb','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',32,'head_html','&lt;head&gt; HTML',NULL,'Code',NULL,'HTML',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Added HTML in the &lt;head&gt; section of the web page, primarily used for website verification and SEO',0,0,0,'',0,0,0,0,0,0),('05347d2fd0','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',38,'width','Width','width','Data','Data',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('0582d150e4','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','fields','DocType',9,'blog_intro','Blog Intro',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Description for listing page, in plain text, only a couple of lines. (max 140 characters)',0,0,0,NULL,0,0,0,0,0,0),('05aa56287c','2017-05-03 16:28:11.295095','2018-05-17 15:33:43.606890','Administrator','Administrator',0,'Domain Settings','fields','DocType',3,'active_domains','Active Domains',NULL,'Table',NULL,'Has Domain',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('05e634bf66','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',1,'address_details','',NULL,'Section Break',NULL,'fa fa-map-marker',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('0605130cdd','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',3,'event_type','Event Type','event_type','Select','Select','Private\nPublic\nCancel',1,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('061243a184','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',7,'is_attachments_folder','Is Attachments Folder',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0664ac0472','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','fields','DocType',1,'view','Snapshot View',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('06ab0020c3','2017-04-24 09:53:41.813982','2018-05-17 15:33:47.580015','Administrator','Administrator',0,'GSuite Templates','fields','DocType',4,'document_name','Document Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'New Document for {name} ',NULL,0,0,0,'',0,0,0,0,0,0),('0701f0ef85','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',32,'hide_toolbar','Hide Toolbar','hide_toolbar','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('0719b269d8','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',15,'phone','Phone',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('073098b3cc','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',36,'lifespan_qrcode_image','Expiry time of QR Code Image Page',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.two_factor_method == \"OTP App\"',0,0,NULL,NULL,0,NULL,'Time in seconds to retain QR code image on server. Min:<strong>240</strong>',0,0,0,'',0,0,0,0,0,0),('0768282ec5','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',6,'error','Error',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('0829809175','2013-01-10 16:34:03.000000','2018-05-17 15:33:42.969016','Administrator','Administrator',0,'Module Def','fields','DocType',2,'app_name','App Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('083711dc3b','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',3,'apply_user_permissions','Apply User Permissions',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Filter records based on User Permissions defined for a user',0,0,0,NULL,0,0,0,0,0,0),('083bc29530','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',19,'permissions','Permissions',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('084a49158d','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',42,'cb31','Other Settings',NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('084c2c89c3','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',14,'restart_supervisor_on_update','Restart Supervisor On Update',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0871162cad','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',34,'bold','Bold',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0886d12493','2017-02-13 14:53:36.240122','2018-05-17 15:33:43.077118','Administrator','Administrator',0,'Custom Role','fields','DocType',2,'report','Report',NULL,'Link',NULL,'Report',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('088b607e00','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',6,'google_client_secret','Google Client Secret',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('08a17b57b6','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',8,'attachment_limit','Attachment Limit (MB)',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,'1','Ignore attachments over this size',0,0,0,'',0,0,0,0,0,0),('08b1844cce','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',18,'set_property_after_alert','Set Property After Alert',NULL,'Select',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('08f481e5bc','2016-08-04 04:58:40.457416','2018-05-17 15:33:47.300993','Administrator','Administrator',0,'Integration Request','fields','DocType',4,'data','Data',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('090866dffc','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',23,'sidebar_settings','Sidebar Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('09108c1efb','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',25,'email','Email',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('0925262db4','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',39,'is_submittable','Is Submittable',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('09538bf55b','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',3,'priority','Priority','priority','Select','Data','High\nMedium\nLow',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Medium',NULL,1,1,0,NULL,0,0,0,0,0,0),('097f0dc1c3','2016-09-22 04:16:48.829658','2018-05-17 15:33:48.026461','Administrator','Administrator',0,'LDAP Settings','fields','DocType',7,'ldap_search_string','LDAP Search String',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0993143038','2017-08-17 14:51:33.577230','2018-05-17 15:34:43.329531','Administrator','Administrator',0,'Site Backup','fields','DocType',2,'date','Date',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,'',0,0,0,0,0,0),('09985f6493','2016-09-21 10:12:57.399174','2018-05-17 15:33:47.640308','Administrator','Administrator',0,'Dropbox Settings','fields','DocType',1,'enabled','Enabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('09a1aacd07','2015-03-18 06:08:32.729800','2018-05-17 15:33:46.258530','Administrator','Administrator',0,'Email Group','fields','DocType',2,'total_subscribers','Total Subscribers',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,1,0,1,'',0,0,0,0,0,0),('09ad0be738','2017-08-17 14:51:33.577230','2018-05-17 15:34:43.329531','Administrator','Administrator',0,'Site Backup','fields','DocType',10,'bench_settings','Bench Settings',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('0a1db4825e','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',29,'default','Default','default','Text','Text',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('0a4f3fd05c','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','fields','DocType',3,'evalue','Friendly Title',NULL,'Code',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,'',0,0,0,0,0,0),('0a590bf5a9','2017-04-21 16:57:30.264478','2018-05-17 15:33:47.429402','Administrator','Administrator',0,'GSuite Settings','fields','DocType',11,'authorization_code','Authorization Code',NULL,'Password',NULL,NULL,0,1,0,1,1,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0a5c7d8c11','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',13,'condition','Condition',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0a61bd5527','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',20,'fields_section_break','Fields',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'doc_type',0,0,NULL,NULL,0,NULL,'Customize Label, Print Hide, Default etc.',0,0,0,NULL,0,0,0,0,0,0),('0a94b9b3a7','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',19,'description','Description','description','Small Text','Text',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('0ab079c0cb','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',22,'column_break_19',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('0af31278b3','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',4,'custom','Custom',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('0b0d6813ca','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',19,'total_rows','Total Rows',NULL,'Int',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('0b415283a0','2017-01-13 04:55:18.835023','2018-05-17 15:33:42.653992','Administrator','Administrator',0,'Dynamic Link','fields','DocType',1,'link_doctype','Link DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('0bc6624e98','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','fields','DocType',1,'title','Title',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,1,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('0bc8ae8714','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',24,'sender_full_name','From Full Name',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('0be3d2535e','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',4,'title_prefix','Title Prefix',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Show title in browser window as \"Prefix - title\"',1,0,0,NULL,0,0,0,0,0,0),('0c355f4290','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',3,'subject','Subject',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'To add dynamic subject, use jinja tags like\n\n<div><pre><code>{{ doc.name }} Delivered</code></pre></div>',1,0,0,NULL,0,1,0,0,0,0),('0c95118b39','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',20,'root_password','Root Password',NULL,'Password',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('0c98f4e6f1','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',13,'push_update','Push Update',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0ccb5842ca','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',24,'print','Print',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,NULL,0,0,0,0,0,0),('0d08a1caf6','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',18,'designation','Designation',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('0d19665d4d','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',67,'frappe_userid','Frappe User ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('0d27a92a7e','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',10,'section_break_9','Style Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.custom_format',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0dc2a79ecf','2013-01-10 16:34:04.000000','2018-05-17 15:33:42.420279','Administrator','Administrator',0,'Property Setter','fields','DocType',9,'property_type','Property Type',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('0dde0aeec4','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',4,'document_type','Document Type',NULL,'Link',NULL,'DocType',1,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('0e0f392fbc','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',9,'service','Service',NULL,'Select',NULL,'\nGMail\nSendgrid\nSparkPost\nYahoo Mail\nOutlook.com\nYandex.Mail',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0e1d53f168','2015-02-04 04:33:36.330477','2018-05-17 15:33:42.835173','Administrator','Administrator',0,'DocShare','fields','DocType',4,'read','Read',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('0e1d911d20','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',25,'github_settings','Github Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0e237bebb1','2013-02-22 01:27:58.000000','2018-05-17 15:33:44.024893','Administrator','Administrator',0,'SMS Parameter','fields','DocType',3,'header','Header',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('0e4b990381','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',23,'read_only','Read Only',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('0eed597ebe','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',14,'push_delete','Push Delete',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0efd9a2bbe','2013-02-22 01:28:08.000000','2018-05-17 15:33:45.105834','Administrator','Administrator',0,'Top Bar Item','fields','DocType',2,'parent_label','Parent Label',NULL,'Select',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'If you set this, this Item will come in a drop-down under the selected parent.',1,0,0,NULL,0,0,0,0,0,0),('0f02dbd843','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',15,'serve_default_site','Serve Default Site',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0f1281c6ff','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',11,'enable_incoming','Enable Incoming',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'','Check this to pull emails from your mailbox',0,0,0,'',0,0,0,0,0,0),('0f2ee007a0','2015-02-04 04:33:36.330477','2018-05-17 15:33:42.835173','Administrator','Administrator',0,'DocShare','fields','DocType',7,'everyone','Everyone',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('0f750e33ff','2013-01-08 15:50:01.000000','2018-05-17 15:33:42.301437','Administrator','Administrator',0,'Role','fields','DocType',2,'disabled','Disabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'If disabled, this role will be removed from all users.',0,0,0,'',0,0,0,0,0,0),('0fd77aa237','2012-12-28 10:49:55.000000','2018-05-17 15:33:45.395726','Administrator','Administrator',0,'Workflow','fields','DocType',6,'states','Document States',NULL,'Table',NULL,'Workflow Document State',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'All possible Workflow States and roles of the workflow. Docstatus Options: 0 is\"Saved\", 1 is \"Submitted\" and 2 is \"Cancelled\"',0,0,0,NULL,0,0,0,0,0,0),('100b059d26','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',21,'gender','Gender','gender','Link','Select','Gender',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('10111791eb','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',14,'submit','Submit','submit','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('1022079fd1','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',2,'label','Label','label','Data','Data',NULL,1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('10bf9eef5d','2013-03-19 12:02:15.000000','2018-05-17 15:33:44.755304','Administrator','Administrator',0,'About Us Settings','fields','DocType',6,'sb1','Team Members',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('10c2754544','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',7,'report_type','Report Type',NULL,'Select',NULL,'Report Builder\nQuery Report\nScript Report',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('10ed06ebd6','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',2,'title','Title',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,1,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',1,0,0,NULL,0,0,0,0,0,0),('10fa86ad48','2017-08-11 05:15:51.482165','2018-05-17 15:33:48.742058','Administrator','Administrator',0,'Data Migration Plan','fields','DocType',2,'module','Module',NULL,'Link',NULL,'Module Def',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('11105fc25e','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',24,'section_break_6',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('11724b3e91','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',15,'allow_incomplete','Allow Incomplete Forms',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Allow saving if mandatory fields are not filled',0,0,0,'',0,0,0,0,0,0),('117e46642e','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',13,'length','Length',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:in_list([\'Data\', \'Link\', \'Dynamic Link\', \'Password\', \'Select\', \'Read Only\', \'Attach\', \'Attach Image\'], doc.fieldtype)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1187080f5b','2015-03-18 06:15:59.321619','2018-05-17 15:33:45.955127','Administrator','Administrator',0,'Email Group Member','fields','DocType',3,'unsubscribed','Unsubscribed',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('11baac8ca6','2014-07-11 17:19:37.037109','2018-05-17 15:33:45.474737','Administrator','Administrator',0,'Email Alert Recipient','fields','DocType',2,'email_by_role','Email By Role',NULL,'Link',NULL,'Role',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('120839e80e','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',8,'domain','Domain',NULL,'Link',NULL,'Email Domain',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('121ea26e60','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',8,'only_update','Don\'t create new records',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'overwrite',0,0,NULL,NULL,0,'0','If you don\'t want to create any new records while updating the older records.',0,0,0,'',0,0,0,0,0,0),('12248c58b9','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','fields','DocType',8,'content','Content',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('124263b28b','2016-08-24 14:12:13.647159','2018-05-17 15:33:47.360639','Administrator','Administrator',0,'OAuth Authorization Code','fields','DocType',4,'authorization_code','Authorization Code',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('127824dae6','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',10,'section_break_36',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('129a1798d7','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',24,'print','Print',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('12bff79e48','2017-02-13 17:33:25.157332','2018-05-17 15:33:44.054341','Administrator','Administrator',0,'Role Permission for Page and Report','fields','DocType',3,'report','Report',NULL,'Link',NULL,'Report',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.set_role_for == \'Report\'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1302c10ad9','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',43,'max_attachments','Max Attachments','max_attachments','Int','Int',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('1303c34dd6','2012-12-28 10:49:56.000000','2018-05-17 15:33:45.302169','Administrator','Administrator',0,'Workflow State','fields','DocType',3,'style','Style',NULL,'Select',NULL,'\nPrimary\nInfo\nSuccess\nWarning\nDanger\nInverse',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Style represents the button color: Success - Green, Danger - Red, Inverse - Black, Primary - Dark Blue, Info - Light Blue, Warning - Orange',0,0,0,NULL,0,0,0,0,0,0),('131a093e4d','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',25,'email','Email',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,NULL,0,0,0,0,0,0),('131bd7ba01','2016-08-24 14:10:17.471264','2018-05-17 15:33:47.989214','Administrator','Administrator',0,'OAuth Bearer Token','fields','DocType',1,'client','Client',NULL,'Link',NULL,'OAuth Client',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('132253bf65','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',22,'content_hash','Content Hash',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('133c29528c','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',27,'shallow_clone','Shallow Clone',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('137b7191ff','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','fields','DocType',7,'route','Route',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,1,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('13852c970a','2016-12-08 12:01:07.993900','2018-05-17 15:33:46.198242','Administrator','Administrator',0,'Email Queue Recipient','fields','DocType',2,'status','Status',NULL,'Select',NULL,'\nNot Sent\nSending\nSent\nError\nExpired',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Not Sent',NULL,1,0,0,'',0,0,0,0,0,0),('13997ba3cc','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',8,'allow_print_for_draft','Allow Print for Draft',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1','',0,0,0,'',0,0,0,0,0,0),('13a2c37b71','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',9,'brand','Brand',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('13d93fe644','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',17,'sort_field','Sort Field',NULL,'Select',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('1417ff7e15','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',2,'send_print_as_pdf','Send Print as PDF',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1','Send Email Print Attachments as PDF (Recommended)',0,0,0,NULL,0,0,0,0,0,0),('14513e440a','2017-02-13 14:53:36.240122','2018-05-17 15:33:43.077118','Administrator','Administrator',0,'Custom Role','fields','DocType',4,'roles','Role',NULL,'Table',NULL,'Has Role',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1451b0480b','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',7,'subject','Subject',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'To add dynamic subject, use jinja tags like\n\n<div><pre><code>{{ doc.name }} Delivered</code></pre></div>',0,0,0,'',0,0,0,0,0,0),('1495a35e4a','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',13,'link_color','Link Color',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_text_styles',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('14a3ce5c6d','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',23,'mobile_no','Mobile No',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,1,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('14a776bf27','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',1,'doc_type','Enter Form Type',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('14ae6ddf59','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',22,'column_break_6',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('157cd559d4','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',1,'details','',NULL,'Section Break','Section Break',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('1591f91042','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',18,'tuesday','Tuesday',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.repeat_this_event && doc.repeat_on===\"Every Day\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('1598e5a18a','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',5,'app_publisher','App Publisher',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('15bcd5b3b5','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',2,'section_break_4',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,'','eval:(!doc.__islocal)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('16103698e1','2013-02-22 01:27:36.000000','2018-05-17 15:33:45.439712','Administrator','Administrator',0,'Workflow Document State','fields','DocType',3,'update_field','Update Field',NULL,'Select',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('162ffd8972','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',53,'login_before','Login Before',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,1,0,NULL,NULL,0,NULL,'Allow user to login only before this hour (0-24)',0,0,0,NULL,0,0,0,0,0,0),('170231a84d','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',6,'app_email','App Email',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('174e5949f9','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',12,'column_break_5',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('175b12f43b','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',31,'in_list_view','In List View',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('17a3b396d7','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',21,'set_user_permissions','Set User Permissions',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'This role update User Permissions for a user',0,0,0,'',0,0,0,0,0,0),('17b15e0b6d','2014-08-22 16:12:17.249590','2018-05-17 15:33:44.077470','Administrator','Administrator',0,'Language','fields','DocType',3,'flag','Flag',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('17c827f496','2013-03-07 15:53:15.000000','2018-05-17 15:33:45.003160','Administrator','Administrator',0,'Website Slideshow','fields','DocType',3,'slideshow_items','Slideshow Items',NULL,'Table',NULL,'Website Slideshow Item',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.__islocal',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('1822829f26','2016-09-20 03:44:03.799402','2018-05-17 15:33:47.340008','Administrator','Administrator',0,'Razorpay Settings','fields','DocType',2,'api_secret','API Secret',NULL,'Password',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1838b139b2','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',17,'additional_permissions','Additional Permissions',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1840df5227','2017-08-11 05:15:38.390831','2018-05-17 15:33:48.573691','Administrator','Administrator',0,'Data Migration Plan Mapping','fields','DocType',1,'mapping','Mapping',NULL,'Link',NULL,'Data Migration Mapping',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('1874c67364','2017-10-23 13:02:10.295824','2018-05-17 15:33:46.879565','Administrator','faris@erpnext.com',0,'Calendar View','fields','DocType',4,'end_date_field','End Date Field',NULL,'Select',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('18770ce977','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',12,'show_section_headings','Show Section Headings',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('188beced4e','2017-09-08 16:16:13.060641','2018-05-17 15:33:47.488876','Administrator','Administrator',0,'Webhook','fields','DocType',5,'sb_webhook','Webhook Request',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('189293b37b','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',19,'show_sidebar','Show Sidebar',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('18a124981c','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',7,'starts_on','Starts on',NULL,'Datetime',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('18c483f79f','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',29,'column_break_28',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('192479b8de','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',10,'file_url','File URL',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.is_folder',0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('194e228117','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',10,'date_changed','Reference Date',NULL,'Select',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.event==\"Days After\" || doc.event==\"Days Before\"',0,0,NULL,NULL,0,NULL,'Send alert if date matches this field\'s value',0,0,0,NULL,0,0,0,0,0,0),('196755484f','2017-08-11 05:03:27.091416','2018-05-17 15:33:48.613509','Administrator','Administrator',0,'Data Migration Connector','fields','DocType',3,'python_module','Python Module',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.connector_type == \'Custom\'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('19b3da20e4','2017-09-08 16:16:13.060641','2018-05-17 15:33:47.488876','Administrator','Administrator',0,'Webhook','fields','DocType',7,'sb_webhook_headers','Webhook Headers',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1a2b2b1c56','2017-08-11 05:15:51.482165','2018-05-17 15:33:48.742058','Administrator','Administrator',0,'Data Migration Plan','fields','DocType',1,'plan_name','Plan Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('1a3c876d3f','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',14,'json','JSON',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.report_type==\"Report Builder\"',0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('1a50e7b161','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',11,'section_break_1','Feedback Rating',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,'eval: doc.rating',NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1a81ec8e82','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',4,'issingle','Is Single','issingle','Check','Check',NULL,0,0,1,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.istable',0,0,NULL,NULL,0,NULL,'Single Types have only one record no tables associated. Values are stored in tabSingles',0,1,0,NULL,0,0,0,0,0,0),('1a84846c88','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',15,'is_primary_address','Preferred Billing Address',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0','',0,0,0,NULL,0,0,0,0,0,0),('1a84ee60b0','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',28,'developer_flag','Developer Flag',NULL,'Int',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,1,'',0,0,0,0,0,0),('1a8fe5dea7','2016-03-30 01:39:20.586927','2018-05-17 15:33:42.598490','Administrator','Administrator',0,'Portal Menu Item','fields','DocType',3,'route','Route',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,3,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('1ace28c7c6','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',46,'sort_field','Sort Field',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.istable',0,0,NULL,NULL,0,'modified','',0,0,0,NULL,0,0,0,0,0,0),('1ad8694a39','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',5,'color','Color',NULL,'Color',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('1ad9054af9','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',44,'section_break_41',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1ae5593fb6','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',12,'javascript','Javascript',NULL,'Code',NULL,'Javascript',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'insert_code',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('1b12a3f50d','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',30,'payment_button_help','Button Help',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'accept_payment',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1b1ef1f03f','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',10,'outgoing_mail_settings','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1b72f7b748','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',29,'report_hide','Report Hide','report_hide','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('1b862e4d7d','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',20,'import','Import',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1b8e583a9a','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',11,'align_labels_right','Align Labels to the Right',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('1b9c987837','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',41,'uidnext','UIDNEXT',NULL,'Int',NULL,NULL,0,1,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1bd1f189fe','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',19,'space','Space',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('1bd5a1ffc1','2016-02-17 12:21:16.175465','2018-05-17 15:33:43.395191','Administrator','Administrator',0,'Translation','fields','DocType',1,'language','Language',NULL,'Link',NULL,'Language',1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,1,0,'',0,0,0,0,0,0),('1be7ef984c','2017-05-03 16:28:11.295095','2018-05-17 15:33:43.606890','Administrator','Administrator',0,'Domain Settings','fields','DocType',1,'active_domains_sb','Active Domains',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1bec773f83','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',8,'blocked','Blocked',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1c2bd12bba','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',24,'sb2','Header and Description',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,'eval:!doc.__islocal',0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('1c58a65c40','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',2,'recipients','Recipient',NULL,'Table',NULL,'Email Queue Recipient',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('1c9a34a754','2017-09-04 20:57:20.129205','2018-05-17 15:33:47.174204','Administrator','Administrator',0,'S3 Backup Settings','fields','DocType',3,'frequency','Backup Frequency',NULL,'Select',NULL,'Daily\nWeekly\nMonthly\nNone',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('1c9cda24d9','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',4,'fieldname','Name','fieldname','Data','Data',NULL,1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,NULL,0,0,0,0,0,0),('1cc06565eb','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',17,'sender','Sender',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1d1f44c1dd','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',10,'redirect_uris','Redirect URIs',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'URIs for receiving authorization code once the user allows access, as well as failure responses. Typically a REST endpoint exposed by the Client App.\n<br>e.g. http://hostname//api/method/frappe.www.login.login_via_facebook',0,0,0,NULL,0,0,0,0,0,0),('1d6b8da751','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',14,'fax','Fax',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('1d790ce1c8','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',16,'fields','Fields',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('1db70281e5','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',38,'breadcrumbs','Breadcrumbs',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'List as [{\"label\": _(\"Jobs\"), \"route\":\"jobs\"}]',0,0,0,'',0,0,0,0,0,0),('1dc33e0307','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',43,'file_watcher_port','File Watcher Port',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1e2ba822b3','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',14,'navbar_search','Include Search in Top Bar',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1e3a6d97a3','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',12,'allow_page_break_inside_tables','Allow page break inside tables',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1e6b667271','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',6,'cb01',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('1eafc004ee','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',12,'username','Username',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,1,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1ec1c882be','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',15,'show_only_errors','Show only errors',NULL,'Check',NULL,NULL,0,0,0,1,0,0,0,0,0,0,1,1,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('1ef17f5eb4','2017-09-08 16:16:13.060641','2018-05-17 15:33:47.488876','Administrator','Administrator',0,'Webhook','fields','DocType',3,'cb_doc_events',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1f34317a4f','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',16,'disable_website_cache','Disable Website Cache',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1f35497575','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',23,'backup_size','Backup Size',NULL,'Float',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('1f6301dd74','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',22,'unique','Unique',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('1f6bb691fe','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',16,'depends_on','Depends On','depends_on','Code','Data',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'This field will appear only if the fieldname defined here has value OR the rules are true (examples): \nmyfield\neval:doc.myfield==\'My Value\'\neval:doc.age&gt;18',0,0,0,NULL,0,0,0,0,0,0),('1f99bd41d4','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',13,'custom_css','Style',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('1fa8ce0385','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',1,'sender','Sender',NULL,'Data',NULL,'Email',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('2002743551','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',3,'heading','Heading',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Default: \"Contact Us\"',0,0,0,NULL,0,0,0,0,0,0),('201370296d','2016-10-19 12:26:04.809812','2018-05-17 15:33:46.932403','Administrator','Administrator',0,'Kanban Board','fields','DocType',4,'section_break_3',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('20b98201ca','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',1,'enabled','Enabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,NULL,0,0,0,0,0,0),('20dafffa74','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',12,'email_id','Email Address',NULL,'Data',NULL,'Email',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('20ebccb8ff','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',33,'reset_password_key','Reset Password Key',NULL,'Data',NULL,NULL,0,1,0,1,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('2144a8b8b2','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',13,'collapsible','Collapsible',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.fieldtype===\"Section Break\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',255,0,0,0,0,0),('217a67be5f','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',19,'wednesday','Wednesday',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.repeat_this_event && doc.repeat_on===\"Every Day\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('21b3060d5c','2017-06-26 10:57:19.976624','2018-05-17 15:33:42.924877','Administrator','Administrator',0,'Test Runner','fields','DocType',2,'app','App',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('21e5bdb6c3','2013-01-28 10:06:02.000000','2018-05-17 15:33:46.538180','Administrator','Administrator',0,'Currency','fields','DocType',6,'symbol','Symbol',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'A symbol for this currency. For e.g. $',1,0,0,NULL,0,0,0,0,0,0),('2240f88b79','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',37,'web_page_link_text','Web Page Link Text',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Text to be displayed for Link to Web Page if this form has a web page. Link route will be automatically generated based on `page_name` and `parent_website_route`',0,0,0,'',0,0,0,0,0,0),('224f9c6208','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',38,'redis_queue','Redis Queue',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('226dcb97a4','2013-01-19 10:23:30.000000','2018-05-17 15:33:46.593643','Administrator','Administrator',0,'Country','fields','DocType',3,'time_zones','Time Zones',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('22d03d895e','2017-08-11 05:03:27.091416','2018-05-17 15:33:48.613509','Administrator','Administrator',0,'Data Migration Connector','fields','DocType',1,'connector_name','Connector Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('22d45427bb','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',29,'ref_name','Ref Name','ref_name','Dynamic Link','Data','ref_type',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('231541a712','2014-10-30 14:23:30.958074','2018-05-17 15:33:45.170674','Administrator','Administrator',0,'Help Category','fields','DocType',5,'route','Route',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('236d766eab','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',5,'read_only','Read Only',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('23b1ca4fce','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',17,'banner_html','Banner HTML',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Banner is above the Top Menu Bar.',0,0,0,NULL,0,0,0,0,0,0),('23c8c10052','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',16,'color','Color',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('23deed1cae','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',19,'footer_text_color','Footer Text Color',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_text_styles',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('23f352481b','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',13,'grant_type','Grant Type',NULL,'Select',NULL,'Authorization Code\nImplicit',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('248033be82','2017-09-08 16:16:13.060641','2018-05-17 15:33:47.488876','Administrator','Administrator',0,'Webhook','fields','DocType',8,'webhook_headers','Headers',NULL,'Table',NULL,'Webhook Header',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('248627a924','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',7,'length','Length',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:in_list([\'Data\', \'Link\', \'Dynamic Link\', \'Password\', \'Select\', \'Read Only\', \'Attach\', \'Attach Image\'], doc.fieldtype)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('24a3da87aa','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',5,'query_options','Query Options',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Contact options, like \"Sales Query, Support Query\" etc each on a new line or separated by commas.',0,0,0,NULL,0,0,0,0,0,0),('24bc2e9678','2012-12-28 10:49:56.000000','2018-05-17 15:33:45.367553','Administrator','Administrator',0,'Workflow Action','fields','DocType',1,'workflow_action_name','Workflow Action Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('24ce8d4121','2017-08-11 05:03:27.091416','2018-05-17 15:33:48.613509','Administrator','Administrator',0,'Data Migration Connector','fields','DocType',4,'authentication_credentials','Authentication Credentials',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('24f8fdca5d','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',13,'column_break_8',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2516c3cbd7','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',33,'allow_copy','Hide Copy','allow_copy','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('2545b0bc25','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',13,'language','Language',NULL,'Link',NULL,'Language',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('255cd2d393','2016-04-12 18:40:16.315024','2018-05-17 15:33:44.562489','Administrator','Administrator',0,'Footer Item','fields','DocType',2,'group_label','Group Label',NULL,'Select',NULL,'\nCompany\nContact\nPolicy',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',1,0,0,'',0,0,0,0,0,0),('25875e2ff6','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',3,'login_id','Email Login ID',NULL,'Data',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'login_id_is_different',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('25894a628e','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',24,'ignore_user_permissions','Ignore User Permissions',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('25998d4a78','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',41,'allow_rename','Allow Rename','allow_rename','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('25e4504f7b','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',12,'allow_print','Allow Print',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('25e80330b5','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',18,'report','Report',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,'1',NULL,0,0,0,NULL,0,0,0,0,0,0),('25f525adc4','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',19,'depends_on','Display Depends On','depends_on','Code','Data',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,255,0,0,0,0,0),('25f695aea2','2017-04-24 09:53:41.813982','2018-05-17 15:33:47.580015','Administrator','Administrator',0,'GSuite Templates','fields','DocType',2,'related_doctype','Related DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('260eafc52c','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',7,'setup_complete','Setup Complete',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('261a3893ed','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',6,'bootstrap','Link to Bootstrap CSS',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_style',0,0,NULL,NULL,0,NULL,'Link to your Bootstrap theme',0,0,0,'',0,0,0,0,0,0),('26301f11cc','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',9,'no_of_rows','No of Rows (Max 500)',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'100','',0,0,0,'',0,0,0,0,0,0),('2633f685ec','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',5,'local_primary_key','Local Primary Key',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2638be70d6','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',6,'column_break_4',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('26c236d8e1','2013-01-28 10:06:02.000000','2018-05-17 15:33:46.538180','Administrator','Administrator',0,'Currency','fields','DocType',4,'fraction_units','Fraction Units',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'1 Currency = [?] Fraction\nFor e.g. 1 USD = 100 Cent',1,0,0,NULL,0,0,0,0,0,0),('26ee63e2ef','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',31,'user','User',NULL,'Link',NULL,'User',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,1,NULL,NULL,0,'__user',NULL,0,0,1,NULL,0,0,0,0,0,0),('26ff254b82','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',24,'ignore_user_permissions','Ignore User Permissions',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.fieldtype===\"Link\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('270e9a202b','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','fields','DocType',8,'section_break_5',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('27588e608b','2017-09-08 16:16:13.060641','2018-05-17 15:33:47.488876','Administrator','Administrator',0,'Webhook','fields','DocType',10,'webhook_data','Data',NULL,'Table',NULL,'Webhook Data',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('278251cc14','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',12,'set_banner_from_image','Set Banner from Image',NULL,'Button',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('27ac489e76','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',16,'is_shipping_address','Preferred Shipping Address',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0','',0,0,0,NULL,0,0,0,0,0,0),('287fe3314b','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',18,'scheduler_last_event','Scheduler Last Event',NULL,'Data',NULL,NULL,0,1,0,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('289f44763d','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',42,'webserver_port','Webserver Port',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('28ccc65707','2013-01-16 13:09:40.000000','2018-05-17 15:33:43.738664','Administrator','Administrator',0,'Error Log','fields','DocType',1,'seen','Seen',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('28d48ea61e','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',15,'heading_style','Heading Style',NULL,'Select',NULL,'\nUPPERCASE\nTitle Case\nlowercase',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_text_styles',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2908dade8e','2014-06-05 02:22:36.029850','2018-05-17 15:33:48.310068','Administrator','Administrator',0,'Address Template','fields','DocType',1,'country','Country',NULL,'Link',NULL,'Country',1,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('2930aabff4','2017-08-11 05:09:10.900237','2018-05-17 15:33:48.771854','Administrator','Administrator',0,'Data Migration Mapping Detail','fields','DocType',2,'local_fieldname','Local Fieldname',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('2938f0bb38','2013-01-08 15:50:01.000000','2018-05-17 15:33:42.301437','Administrator','Administrator',0,'Role','fields','DocType',3,'desk_access','Desk Access',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,1,0,0,'',0,0,0,0,0,0),('297aa41647','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',7,'app_icon','App Icon',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'fa fa-gamepad',NULL,0,0,0,'',0,0,0,0,0,0),('29b6f13863','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',9,'column_break_5',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('29f6c09343','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',8,'section_break_4','Permissions',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('2a05ee27fa','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',28,'display','Display',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2a0cf519c5','2016-08-24 14:10:17.471264','2018-05-17 15:33:47.989214','Administrator','Administrator',0,'OAuth Bearer Token','fields','DocType',4,'access_token','Access Token',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('2a420d3af0','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',15,'cancel','Cancel','cancel','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('2a4f10ba38','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',2,'is_feedback_submitted','Feedback Submitted',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2aa9cab4ae','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',31,'column_break_28',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2ab8a5965d','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',16,'description','Field Description','description','Text','Text',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'300px','300px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('2ac7453179','2017-04-21 16:57:30.264478','2018-05-17 15:33:47.429402','Administrator','Administrator',0,'GSuite Settings','fields','DocType',2,'google_credentials','Google Credentials',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.enable',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2b4bdd86a1','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',40,'column_break_31',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2b85a1c00b','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',12,'text_color','Text Color',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_text_styles',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2b886fa288','2016-10-19 12:26:04.809812','2018-05-17 15:33:46.932403','Administrator','Administrator',0,'Kanban Board','fields','DocType',1,'kanban_board_name','Kanban Board Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,1,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2b8fdd596c','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',1,'role_and_level','Role and Level',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('2ba2c1376c','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',3,'import_file','Attach file for Import',NULL,'Attach',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2bd4617c7f','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',19,'pull_failed','Pull Failed',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2c060855de','2017-08-31 04:16:38.764465','2018-05-17 15:33:43.506765','Administrator','Administrator',0,'Role Profile','fields','DocType',2,'roles_html','Roles HTML',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('2c4ddd9a88','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',6,'subject','Subject',NULL,'Small Text',NULL,NULL,0,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('2cba84e23c','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',11,'insert_code','Insert Code',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Add code as &lt;script&gt;',0,0,0,NULL,0,0,0,0,0,0),('2d3b005530','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',28,'subdomain','Subdomain',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Sub-domain provided by erpnext.com',0,0,1,NULL,0,0,0,0,0,0),('2d626d193f','2013-01-10 16:34:03.000000','2018-05-17 15:33:42.969016','Administrator','Administrator',0,'Module Def','fields','DocType',3,'restrict_to_domain','Restrict To Domain',NULL,'Link',NULL,'Domain',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2d6a440c83','2017-09-04 20:57:20.129205','2018-05-17 15:33:47.174204','Administrator','Administrator',0,'S3 Backup Settings','fields','DocType',6,'bucket','Bucket',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2d79adb42f','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',21,'success_message','Success Message',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Message to be displayed on successful completion (only for Guest users)',0,0,0,NULL,0,0,0,0,0,0),('2d821e5809','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',11,'section_break_6',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('2ddd24a681','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',7,'mapping_type','Mapping Type',NULL,'Select',NULL,'Push\nPull\nSync',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2deaa0241f','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',11,'column_break0',NULL,NULL,'Column Break','Column Break',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50%','50%',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('2e027c6830','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',20,'thursday','Thursday',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.repeat_this_event && doc.repeat_on===\"Every Day\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('2e12c31d69','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',25,'column_break_13',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('2e21d722d6','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',6,'website_theme','Website Theme',NULL,'Link',NULL,'Website Theme',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Standard',NULL,0,0,0,'',0,0,0,0,0,0),('2e3afb7091','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',12,'push_insert','Push Insert',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2e5b65b38d','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',27,'bio','Bio',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('2e7b0c24f7','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','fields','DocType',1,'system_page','System Page',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2e93484d1b','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',56,'last_login','Last Login','last_login','Read Only','Read Only',NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('2eeb894d52','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',14,'column_break_18',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2ef2d00276','2013-03-07 12:26:33.000000','2018-05-17 15:33:45.033410','Administrator','Administrator',0,'Website Slideshow Item','fields','DocType',3,'description','Description',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'200px','200px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('2f12a9870c','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',3,'communication_medium','Type',NULL,'Select',NULL,'\nEmail\nChat\nPhone\nSMS\nVisit\nOther',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.communication_type===\"Communication\"',0,0,NULL,NULL,0,'',NULL,0,0,0,NULL,0,0,0,0,0,0),('2f24b423b2','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',26,'document_type','Show in Module Section','document_type','Select','Select','\nDocument\nSetup\nSystem\nOther',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('2f35a3ec59','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',16,'banner','Banner',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('2f8fb8a8ab','2017-02-13 17:33:25.157332','2018-05-17 15:33:43.349585','Administrator','Administrator',0,'User Permission for Page and Report','fields','DocType',5,'roles_html','Roles Html',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('2f9f0fabbf','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',11,'mobile_no','Mobile No','mobile_no','Data','Data',NULL,0,0,0,0,0,0,1,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('3013e3c1a3','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',1,'dt','Document','dt','Link','Link','DocType',1,0,0,0,0,1,1,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,1,0,0,0),('307f72f71b','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',1,'doc_type','DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',1,1,0,NULL,0,0,1,0,0,0),('3099db4a33','2017-06-26 10:57:19.976624','2018-05-17 15:33:42.924877','Administrator','Administrator',0,'Test Runner','fields','DocType',3,'output','Output',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('30aa19d30a','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',40,'user_emails','User Emails',NULL,'Table',NULL,'User Email',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,1,0,NULL,NULL,0,NULL,'',0,0,0,'',0,0,0,0,0,0),('30c792728b','2013-02-22 01:27:36.000000','2018-05-17 15:33:45.439712','Administrator','Administrator',0,'Workflow Document State','fields','DocType',4,'update_value','Update Value',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('30eb909cc2','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',16,'column_break_16',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3100b67ff2','2013-01-28 10:06:02.000000','2018-05-17 15:33:46.538180','Administrator','Administrator',0,'Currency','fields','DocType',5,'smallest_currency_fraction_value','Smallest Currency Fraction Value',NULL,'Currency',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Smallest circulating fraction unit (coin). For e.g. 1 cent for USD and it should be entered as 0.01',0,0,0,'',0,0,0,0,0,0),('311843e458','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',16,'css','CSS',NULL,'Code',NULL,'CSS',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'insert_style',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('317cbdab42','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',9,'force_show','Force Show',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('31892cc990','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',25,'column_break_29',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('325f4f8493','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',27,'favicon','FavIcon',NULL,'Attach',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'An icon file with .ico extension. Should be 16 x 16 px. Generated using a favicon generator. [favicon-generator.org]',0,0,0,NULL,0,0,0,0,0,0),('326632a9c4','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',35,'two_factor_method','Two Factor Authentication method',NULL,'Select',NULL,'OTP App\nSMS\nEmail',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,'OTP App','Choose authentication method to be used by all users',0,0,0,'',0,0,0,0,0,0),('32d5cc6755','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',20,'website_sidebar','Website Sidebar',NULL,'Link',NULL,'Website Sidebar',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('32e268ed50','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',6,'background_workers','Background Workers',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('32f3d6fd82','2017-09-08 16:16:13.060641','2018-05-17 15:33:47.488876','Administrator','Administrator',0,'Webhook','fields','DocType',6,'request_url','Request URL',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('3328ae8e2b','2012-12-28 10:49:56.000000','2018-05-17 15:33:45.302169','Administrator','Administrator',0,'Workflow State','fields','DocType',2,'icon','Icon',NULL,'Select',NULL,'\nglass\nmusic\nsearch\nenvelope\nheart\nstar\nstar-empty\nuser\nfilm\nth-large\nth\nth-list\nok\nremove\nzoom-in\nzoom-out\noff\nsignal\ncog\ntrash\nhome\nfile\ntime\nroad\ndownload-alt\ndownload\nupload\ninbox\nplay-circle\nrepeat\nrefresh\nlist-alt\nlock\nflag\nheadphones\nvolume-off\nvolume-down\nvolume-up\nqrcode\nbarcode\ntag\ntags\nbook\nbookmark\nprint\ncamera\nfont\nbold\nitalic\ntext-height\ntext-width\nalign-left\nalign-center\nalign-right\nalign-justify\nlist\nindent-left\nindent-right\nfacetime-video\npicture\npencil\nmap-marker\nadjust\ntint\nedit\nshare\ncheck\nmove\nstep-backward\nfast-backward\nbackward\nplay\npause\nstop\nforward\nfast-forward\nstep-forward\neject\nchevron-left\nchevron-right\nplus-sign\nminus-sign\nremove-sign\nok-sign\nquestion-sign\ninfo-sign\nscreenshot\nremove-circle\nok-circle\nban-circle\narrow-left\narrow-right\narrow-up\narrow-down\nshare-alt\nresize-full\nresize-small\nplus\nminus\nasterisk\nexclamation-sign\ngift\nleaf\nfire\neye-open\neye-close\nwarning-sign\nplane\ncalendar\nrandom\ncomment\nmagnet\nchevron-up\nchevron-down\nretweet\nshopping-cart\nfolder-close\nfolder-open\nresize-vertical\nresize-horizontal\nhdd\nbullhorn\nbell\ncertificate\nthumbs-up\nthumbs-down\nhand-right\nhand-left\nhand-up\nhand-down\ncircle-arrow-right\ncircle-arrow-left\ncircle-arrow-up\ncircle-arrow-down\nglobe\nwrench\ntasks\nfilter\nbriefcase\nfullscreen',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Icon will appear on the button',0,0,0,NULL,0,0,0,0,0,0),('335d86c1ff','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',17,'default_incoming','Default Incoming',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'enable_incoming',0,0,NULL,NULL,0,NULL,'e.g. replies@yourcomany.com. All replies will come to this inbox.',0,0,0,'',0,0,0,0,0,0),('33c2b06261','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',22,'unique','Unique',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3417af2096','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',8,'date_and_number_format','Date and Number Format',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('342c9d269f','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',11,'fields','Field Maps',NULL,'Table',NULL,'Data Migration Mapping Detail',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3453c5d6c2','2017-09-08 16:16:13.060641','2018-05-17 15:33:47.488876','Administrator','Administrator',0,'Webhook','fields','DocType',9,'sb_webhook_data','Webhook Data',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('34706d3232','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',11,'heading_webfont','Google Font (Heading)',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_text_styles',0,0,NULL,NULL,0,NULL,'Add the name of a \"Google Web Font\" e.g. \"Open Sans\"',0,0,0,'',0,0,0,0,0,0),('34b69c80b3','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',4,'fieldname','Name','fieldname','Data','Data',NULL,1,0,0,0,0,0,1,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('34bcb6a7fa','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',29,'email_account','Email Account',NULL,'Link',NULL,'Email Account',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.communication_medium===\"Email\"',0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('350d230b21','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',3,'doc_type','Select DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('354c68b1ea','2016-10-19 12:26:04.809812','2018-05-17 15:33:46.932403','Administrator','Administrator',0,'Kanban Board','fields','DocType',7,'private','Private',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('35f95659cc','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',1,'pdf_settings','PDF Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('35fcba9691','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',17,'additional_permissions','Additional Permissions',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('36170fbd66','2016-04-14 09:41:45.892975','2018-05-17 15:33:46.225853','Administrator','Administrator',0,'Unhandled Email','fields','DocType',5,'raw','Raw Email',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('36321068c9','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',15,'print_style','Print Style',NULL,'Link',NULL,'Print Style',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Modern',NULL,1,0,0,NULL,0,0,0,0,0,0),('366c3f4ba5','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',9,'all_day','All Day',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('36705001d7','2013-03-25 16:00:51.000000','2018-05-17 15:33:44.781044','Administrator','Administrator',0,'Blogger','fields','DocType',6,'avatar','Avatar',NULL,'Attach',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('36965017e6','2013-02-22 01:27:36.000000','2018-05-17 15:33:45.439712','Administrator','Administrator',0,'Workflow Document State','fields','DocType',6,'message','Message',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'160px','160px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('36cb86e226','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',16,'timeline_name','Timeline Name',NULL,'Dynamic Link',NULL,'timeline_doctype',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('36dbaa4e2c','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',32,'signature_section','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('370957f0e4','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',5,'standard','Standard','standard','Select','Select','No\nYes',1,0,0,0,0,1,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'No',NULL,0,0,0,NULL,0,0,1,0,0,0),('371b629f33','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',2,'role','Role','role','Link','Link','Role',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'150px','150px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('373a09b41c','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',3,'last_name','Last Name','last_name','Data','Data',NULL,0,0,0,0,0,0,1,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('377959dc5a','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',34,'print_hide_if_no_value','Print Hide If No Value',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:[\"Int\", \"Float\", \"Currency\", \"Percent\"].indexOf(doc.fieldtype)!==-1',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('37b33bfc63','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',14,'pause_scheduler','Pause Scheduler',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('37eb0c5939','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',24,'dropbox_access_key','Dropbox Access Key',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('37ee723cc7','2017-09-08 16:16:13.060641','2018-05-17 15:33:47.488876','Administrator','Administrator',0,'Webhook','fields','DocType',2,'webhook_doctype','DocType',NULL,'Link',NULL,'DocType',0,0,1,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,'',0,0,0,0,0,0),('37efd34ecb','2016-03-30 01:40:20.001775','2018-05-17 15:33:44.625767','Administrator','Administrator',0,'Portal Settings','fields','DocType',5,'custom_sidebar_menu','Custom Sidebar Menu',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('384cdd2759','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',18,'column_break_15',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('38d357e431','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',20,'view_settings','View Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('394227df51','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',6,'column_break_5',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('39ac688f62','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',34,'bypass_2fa_for_retricted_ip_users','Bypass Two Factor Auth for users who login from restricted IP Address',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'enable_two_factor_auth',0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('39b6f94287','2016-10-19 12:26:42.569185','2018-05-17 15:33:46.626499','Administrator','Administrator',0,'Kanban Board Column','fields','DocType',3,'indicator','Indicator',NULL,'Select',NULL,'blue\norange\nred\ngreen\ndarkgrey\npurple\nyellow\nlightblue',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'darkgrey',NULL,1,0,0,'',0,0,0,0,0,0),('39bc7e5b98','2017-03-13 09:20:56.387135','2018-05-17 15:33:45.930240','Administrator','Administrator',0,'Email Rule','fields','DocType',2,'is_spam','Is Spam',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('39fb5a3627','2016-04-12 18:40:16.315024','2018-05-17 15:33:44.562489','Administrator','Administrator',0,'Footer Item','fields','DocType',3,'url','URL',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'200px','200px',0,NULL,'Link to the page you want to open. Leave blank if you want to make it a group parent.',1,0,0,'',0,0,0,0,0,0),('3a048542e9','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',25,'location','Location',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('3a289e0115','2014-06-19 05:20:26.331041','2018-05-17 15:33:46.049180','Administrator','Administrator',0,'Standard Reply','fields','DocType',4,'section_break_4',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3a533d8805','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',65,'github_userid','Github User ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('3a53d57fa8','2013-05-24 13:41:00.000000','2018-05-17 15:33:46.746140','Administrator','Administrator',0,'Note','fields','DocType',8,'seen_by','Seen By Table',NULL,'Table',NULL,'Note Seen By',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3aa3c93f83','2017-08-17 14:51:33.577230','2018-05-17 15:34:43.329531','Administrator','Administrator',0,'Site Backup','fields','DocType',7,'hash','Hash',NULL,'Data',NULL,NULL,0,1,0,1,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('3acb18ce4a','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',29,'bench_settings','Bench Settings',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('3aea6ca8d5','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',27,'breadcrumbs','Breadcrumbs',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'List as [{\"label\": _(\"Jobs\"), \"route\":\"jobs\"}]',0,0,0,'',0,0,0,0,0,0),('3aea7c7601','2013-01-19 10:23:30.000000','2018-05-17 15:33:46.593643','Administrator','Administrator',0,'Country','fields','DocType',1,'country_name','Country Name','country_name','Data','Data',NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('3aede91671','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',21,'recipients','Recipients',NULL,'Table',NULL,'Email Alert Recipient',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('3af60c4b22','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',21,'old_parent','old_parent',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3b01c5801b','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',18,'link_name','Link Name',NULL,'Dynamic Link',NULL,'link_doctype',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('3b0d13428a','2013-01-10 16:34:24.000000','2018-05-17 15:33:43.053495','Administrator','Administrator',0,'SMS Settings','fields','DocType',1,'sms_gateway_url','SMS Gateway URL',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Eg. smsgateway.com/api/send_sms.cgi',1,0,0,NULL,0,0,0,0,0,0),('3bced4519a','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',16,'css_section',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3c172a13d8','2016-12-29 12:59:48.638970','2018-05-17 15:33:43.427855','Administrator','Administrator',0,'Deleted Document','fields','DocType',1,'deleted_name','Deleted Name',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('3c19104877','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',10,'reference_section','Reference',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3c4f3646d5','2017-09-04 20:57:20.129205','2018-05-17 15:33:47.174204','Administrator','Administrator',0,'S3 Backup Settings','fields','DocType',4,'access_key_id','Access Key ID',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('3c859dce7c','2013-01-10 16:34:01.000000','2018-05-17 15:33:46.459140','Administrator','Administrator',0,'Custom Script','fields','DocType',4,'sample','Sample',NULL,'HTML',NULL,'<h3>Custom Script Help</h3>\n<p>Custom Scripts are executed only on the client-side (i.e. in Forms). Here are some examples to get you started</p>\n<pre><code>\n\n// fetch local_tax_no on selection of customer\n// cur_frm.add_fetch(link_field, source_fieldname, target_fieldname);\ncur_frm.add_fetch(\"customer\", \"local_tax_no\", \"local_tax_no\");\n\n// additional validation on dates\nfrappe.ui.form.on(\"Task\", \"validate\", function(frm) {\n    if (frm.doc.from_date &lt; get_today()) {\n        msgprint(\"You can not select past date in From Date\");\n        validated = false;\n    }\n});\n\n// make a field read-only after saving\nfrappe.ui.form.on(\"Task\", {\n    refresh: function(frm) {\n        // use the __islocal value of doc, to check if the doc is saved or not\n        frm.set_df_property(\"myfield\", \"read_only\", frm.doc.__islocal ? 0 : 1);\n    }\n});\n\n// additional permission check\nfrappe.ui.form.on(\"Task\", {\n    validate: function(frm) {\n        if(user==\"user1@example.com\" &amp;&amp; frm.doc.purpose!=\"Material Receipt\") {\n            msgprint(\"You are only allowed Material Receipt\");\n            validated = false;\n        }\n    }\n});\n\n// calculate sales incentive\nfrappe.ui.form.on(\"Sales Invoice\", {\n    validate: function(frm) {\n        // calculate incentives for each person on the deal\n        total_incentive = 0\n        $.each(frm.doc.sales_team), function(i, d) {\n\n            // calculate incentive\n            var incentive_percent = 2;\n            if(frm.doc.base_grand_total &gt; 400) incentive_percent = 4;\n\n            // actual incentive\n            d.incentives = flt(frm.doc.base_grand_total) * incentive_percent / 100;\n            total_incentive += flt(d.incentives)\n        });\n\n        frm.doc.total_incentive = total_incentive;\n    }\n})\n\n</code>\n</pre>',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('3c96246b0a','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',37,'send_me_a_copy','Send Me A Copy of Outgoing Emails',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3ca07fa192','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',51,'user_type','User Type','user_type','Select','Select','System User\nWebsite User',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,1,0,NULL,NULL,0,'System User','If the user has any role checked,then the user becomes a \"System User\". \"System User\" has access to the desktop',1,1,1,NULL,0,0,0,0,0,0),('3caa78926a','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',18,'frequency','Frequency',NULL,'Select',NULL,'Daily\nWeekly\nMonthly',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,'',0,0,0,0,0,0),('3cb004388d','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',32,'two_factor_authentication','Two Factor Authentication',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3cf3555d96','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',9,'max_length','Max Length',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3cf8adc079','2014-10-30 14:23:30.958074','2018-05-17 15:33:45.170674','Administrator','Administrator',0,'Help Category','fields','DocType',3,'published','Published',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3d046ade2b','2014-07-11 17:19:37.037109','2018-05-17 15:33:45.474737','Administrator','Administrator',0,'Email Alert Recipient','fields','DocType',1,'email_by_document_field','Email By Document Field',NULL,'Select',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',1,0,0,NULL,0,0,0,0,0,0),('3d360d17a9','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',5,'user','User Id',NULL,'Link',NULL,'User',0,0,0,0,0,0,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3d40a0d610','2013-01-10 16:34:24.000000','2018-05-17 15:33:48.135202','Administrator','Administrator',0,'Print Heading','fields','DocType',1,'print_heading','Print Heading','print_heading','Data','Data',NULL,0,0,0,0,0,1,0,0,0,0,0,1,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,1,0,0,0),('3d9961ce0b','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',15,'text_align','Text Align',NULL,'Select',NULL,'Left\nCenter\nRight',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('3dd84d4469','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',9,'send_welcome_email','Send Welcome Email',NULL,'Check',NULL,NULL,0,0,0,0,0,0,1,0,0,0,0,0,NULL,NULL,'eval:doc.__islocal',0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('3e9fbdf60e','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',7,'website_theme_image','Website Theme Image',NULL,'Image',NULL,'website_theme_image_link',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3ea144d994','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',20,'column_break_5','Recipients',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('3eab68b054','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',29,'allow_login_using_mobile_number','Allow Login using Mobile Number',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0','User can login using Email id or Mobile number',0,0,0,'',0,0,0,0,0,0),('3eb755f299','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',7,'county','County',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3ebde7238f','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',17,'column_break_13',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3ebf63cda1','2016-04-14 09:41:45.892975','2018-05-17 15:33:46.225853','Administrator','Administrator',0,'Unhandled Email','fields','DocType',1,'email_account','Email Account',NULL,'Link',NULL,'Email Account',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('3ed0be070e','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',8,'page_length','Page Length',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'10',NULL,0,0,0,'',0,0,0,0,0,0),('3ee9e5cba8','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',5,'column_break_4',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('3f997071a6','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',34,'signature','Signature',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'add_signature',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('3fe82992a6','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',25,'description','Description',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Description for search engine optimization.',0,0,0,NULL,0,0,0,0,0,0),('40026842d9','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',7,'in_list_view','In List View',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('400e7ca9cd','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',15,'timeline_doctype','Timeline DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('405fbe6cb4','2017-08-11 05:03:27.091416','2018-05-17 15:33:48.613509','Administrator','Administrator',0,'Data Migration Connector','fields','DocType',6,'database_name','Database Name',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4074619c13','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',13,'filter_meta','Filter Meta',NULL,'Text',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('40e22c8b51','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',8,'github_client_id','GitHub Client ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('40ec21627f','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',3,'istable','Is Child Table','istable','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Child Tables are shown as a Grid in other DocTypes.',0,1,0,NULL,0,0,0,0,0,0),('41e72afd4f','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',63,'google_userid','Google User ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('4265f6e322','2017-08-17 14:51:33.577230','2018-05-17 15:34:43.329531','Administrator','Administrator',0,'Site Backup','fields','DocType',6,'private_file_backup','Private File Backup',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('4272095aa4','2013-03-07 15:53:15.000000','2018-05-17 15:33:45.003160','Administrator','Administrator',0,'Website Slideshow','fields','DocType',1,'slideshow_name','Slideshow Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('427828eb44','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',5,'editable_grid','Editable Grid',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'istable',0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('42b0caddcd','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',52,'login_after','Login After',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,1,0,NULL,NULL,0,NULL,'Allow user to login only after this hour (0-24)',0,0,0,NULL,0,0,0,0,0,0),('42dac499d4','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',33,'robots_txt','Robots.txt',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('42ea82be5a','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',6,'unique','Unique',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('42effc1336','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',1,'data_migration_plan','Data Migration Plan',NULL,'Link',NULL,'Data Migration Plan',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('432187fa0a','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',14,'cb_2',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4358ffc36b','2013-02-22 01:27:58.000000','2018-05-17 15:33:44.024893','Administrator','Administrator',0,'SMS Parameter','fields','DocType',1,'parameter','Parameter',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'150px','150px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('439103afed','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',19,'max_attachment_size','Max Attachment Size (in MB)',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('43b4f89486','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',35,'report_hide','Report Hide','report_hide','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('43be7ef51e','2017-07-17 14:25:27.881871','2018-05-17 15:33:42.627169','Administrator','Administrator',0,'User Permission','fields','DocType',2,'allow','Allow',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,'',0,0,0,0,0,0),('43defd8df0','2016-03-30 10:04:25.828742','2018-05-17 15:33:44.194278','Administrator','Administrator',0,'User Email','fields','DocType',2,'email_id','Email ID',NULL,'Data',NULL,'email_account.email_id',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('44708ae4f9','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',60,'third_party_authentication','Third Party Authentication',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,'enabled',1,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('449ed1fa4c','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',11,'allow_delete','Allow Delete',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'allow_multiple',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('44b59531e0','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',9,'address_line2','Address Line 2',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('44db5a649c','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',28,'print_width','Print Width',NULL,'Data',NULL,NULL,0,1,0,1,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('44e0d72206','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',12,'priority','Priority',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,1,'',0,0,0,0,0,0),('450f6ae89d','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',20,'section_break_15','Message',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4545a01087','2016-08-04 04:58:40.457416','2018-05-17 15:33:47.300993','Administrator','Administrator',0,'Integration Request','fields','DocType',3,'status','Status',NULL,'Select',NULL,'\nQueued\nAuthorized\nCompleted\nCancelled\nFailed\n',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Queued',NULL,1,1,0,'',0,0,0,0,0,0),('45856e1869','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',12,'use_imap','Use IMAP',NULL,'Check',NULL,'domain.use_imap',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval: !doc.domain && doc.enable_incoming',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('45982f22b1','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',16,'section_break_8',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'doc_type',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('45d78ed9d8','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',16,'print_style_preview','Print Style Preview',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('45edb17003','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',13,'title_field','Title Field',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Use this fieldname to generate title',0,0,0,'',0,0,0,0,0,0),('45f9dab673','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',11,'in_global_search','In Global Search',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:([\"Data\", \"Select\", \"Table\", \"Text\", \"Text Editor\", \"Link\", \"Small Text\", \"Long Text\", \"Read Only\", \"Heading\", \"Dynamic Link\"].indexOf(doc.fieldtype) !== -1)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('464222755f','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',9,'country','Country',NULL,'Link',NULL,'Country',1,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,1,0,NULL,0,0,0,0,0,0),('465cb425fa','2016-10-19 12:26:42.569185','2018-05-17 15:33:46.626499','Administrator','Administrator',0,'Kanban Board Column','fields','DocType',4,'order','Order',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4669feff1d','2012-12-28 10:49:55.000000','2018-05-17 15:33:45.395726','Administrator','Administrator',0,'Workflow','fields','DocType',5,'states_head','States',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Different \"States\" this document can exist in. Like \"Open\", \"Pending Approval\" etc.',0,0,0,NULL,0,0,0,0,0,0),('4670bf5ee0','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',56,'is_published_field','Is Published Field',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'has_web_view',0,0,NULL,NULL,0,'',NULL,0,0,0,'',0,0,0,0,0,0),('4673680584','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','fields','DocType',4,'column_break_4',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4692738b9d','2017-08-17 01:25:56.910716','2018-05-17 15:33:48.101538','Administrator','Administrator',0,'Print Style','fields','DocType',2,'disabled','Disabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('46a128e53a','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',1,'file_name','File Name','file_name','Data','Data',NULL,0,0,0,0,0,0,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('46b95d1fa7','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',23,'integrations','Integrations',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('46c6b0ea43','2014-06-05 02:22:36.029850','2018-05-17 15:33:48.310068','Administrator','Administrator',0,'Address Template','fields','DocType',3,'template','Template',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'','<h4>Default Template</h4>\n<p>Uses <a href=\"http://jinja.pocoo.org/docs/templates/\">Jinja Templating</a> and all the fields of Address (including Custom Fields if any) will be available</p>\n<pre><code>{{ address_line1 }}&lt;br&gt;\n{% if address_line2 %}{{ address_line2 }}&lt;br&gt;{% endif -%}\n{{ city }}&lt;br&gt;\n{% if state %}{{ state }}&lt;br&gt;{% endif -%}\n{% if pincode %} PIN:  {{ pincode }}&lt;br&gt;{% endif -%}\n{{ country }}&lt;br&gt;\n{% if phone %}Phone: {{ phone }}&lt;br&gt;{% endif -%}\n{% if fax %}Fax: {{ fax }}&lt;br&gt;{% endif -%}\n{% if email_id %}Email: {{ email_id }}&lt;br&gt;{% endif -%}\n</code></pre>',0,0,0,NULL,0,0,0,0,0,0),('46d641a13c','2016-02-17 12:21:16.175465','2018-05-17 15:33:43.395191','Administrator','Administrator',0,'Translation','fields','DocType',3,'source_name','Source Text',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'If your data is in HTML, please copy paste the exact HTML code with the tags.',0,0,0,'',0,0,0,0,0,0),('46dbbd7b18','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',54,'restrict_ip','Restrict IP',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,1,0,NULL,NULL,0,NULL,'Restrict user from this IP address only. Multiple IP addresses can be added by separating with commas. Also accepts partial IP addresses like (111.111.111)',0,0,0,NULL,0,0,0,0,0,0),('4709e85491','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',30,'in_reply_to','In Reply To',NULL,'Link',NULL,'Communication',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('4721712839','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',10,'in_standard_filter','In Standard Filter',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('476dfdfe30','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',3,'section_break_3',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'enabled',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('477a1eb189','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',12,'reference_name','Reference Name','reference_name','Dynamic Link','Data','reference_type',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('478b0a74cc','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',37,'timeline_label','Timeline field Name',NULL,'Data',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('47d5d7360a','2016-07-15 05:51:29.224123','2018-05-17 15:33:46.911760','Administrator','Administrator',0,'Bulk Update','fields','DocType',5,'limit','Limit',NULL,'Int',NULL,NULL,0,0,0,0,0,0,1,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'500','Max 500 records at a time',0,0,0,'',0,0,0,0,0,0),('48026948ea','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',5,'google_client_id','Google Client ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('48406b8003','2017-08-22 17:15:31.459897','2018-05-17 15:34:43.427815','Administrator','Administrator',0,'Bench Manager Command','fields','DocType',7,'section_break_1',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4862b5d35e','2012-12-27 11:51:24.000000','2018-05-17 15:33:45.061220','Administrator','Administrator',0,'Website Script','fields','DocType',1,'javascript','Javascript',NULL,'Code',NULL,'Javascript',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('486ed60b99','2012-12-28 10:49:55.000000','2018-05-17 15:33:45.395726','Administrator','Administrator',0,'Workflow','fields','DocType',7,'transition_rules','Transition Rules',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Rules for how states are transitions, like next state and which role is allowed to change state etc.',0,0,0,NULL,0,0,0,0,0,0),('48840466fe','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',6,'view_link_in_email','Page Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('492df0369e','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',8,'state','State',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('4950c9c7f1','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',47,'sb2','Defaults',NULL,'Section Break','Column Break',NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,1,0,'50%','50%',0,NULL,'These values will be automatically updated in transactions and also will be useful to restrict permissions for this user on transactions containing these values.',0,0,1,NULL,0,0,0,0,0,0),('495525238b','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',3,'column_break_2',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('49781e366e','2016-08-24 14:10:17.471264','2018-05-17 15:33:47.989214','Administrator','Administrator',0,'OAuth Bearer Token','fields','DocType',8,'status','Status',NULL,'Select',NULL,'Active\nRevoked',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,1,0,'',0,0,0,0,0,0),('4987e77dd1','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',16,'more_info','More Information',NULL,'Section Break',NULL,'fa fa-file-text',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('49ca0b5ce9','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',19,'sort_order','Sort Order',NULL,'Select',NULL,'ASC\nDESC',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('49e462840d','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',12,'delete','Delete',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,1,0,0,NULL,0,0,0,0,0,0),('49f85a8859','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',24,'section_break_21',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4a0b5f4d42','2017-09-14 12:08:50.302810','2018-05-17 15:33:47.458968','Administrator','Administrator',0,'Webhook Data','fields','DocType',1,'fieldname','Fieldname',NULL,'Select',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('4b15c1a4a3','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',12,'reference_name','Reference Name',NULL,'Dynamic Link',NULL,'reference_doctype',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4b550c5e81','2016-07-15 05:51:29.224123','2018-05-17 15:33:46.911760','Administrator','Administrator',0,'Bulk Update','fields','DocType',1,'document_type','Document Type',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4b600319a3','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',26,'js','JavaScript',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_style',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4b746a0712','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',5,'first_name','First Name','first_name','Data','Data',NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('4b870a092e','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',2,'module','Module','module','Link','Link','Module Def',1,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('4ba4c4aecd','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',16,'top_bar_color','Top Bar Color',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_text_styles',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4bab4c729b','2017-02-13 17:33:25.157332','2018-05-17 15:33:43.349585','Administrator','Administrator',0,'User Permission for Page and Report','fields','DocType',8,'update','Update',NULL,'Button',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4bb1e4cb66','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',22,'space_usage','Space Usage',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!(doc.__islocal)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4bda1fb207','2014-08-22 16:12:17.249590','2018-05-17 15:33:44.077470','Administrator','Administrator',0,'Language','fields','DocType',2,'language_name','Language Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('4bdc191822','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',4,'slideshow','Slideshow',NULL,'Link',NULL,'Website Slideshow',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('4c3eb3bb0a','2013-01-28 10:06:02.000000','2018-05-17 15:33:46.538180','Administrator','Administrator',0,'Currency','fields','DocType',7,'number_format','Number Format',NULL,'Select',NULL,'\n#,###.##\n#.###,##\n# ###.##\n# ###,##\n#\'###.##\n#, ###.##\n#,##,###.##\n#,###.###\n#.###\n#,###',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'How should this currency be formatted? If not set, will use system defaults',1,0,0,NULL,0,0,0,0,0,0),('4c45bb0d7e','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',27,'column_break_13',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4c4d451aad','2013-01-28 10:06:02.000000','2018-05-17 15:33:46.538180','Administrator','Administrator',0,'Currency','fields','DocType',2,'enabled','Enabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('4ca501a07b','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',23,'message','Message',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,'Add your message here',NULL,0,0,0,NULL,0,1,0,0,0,0),('4cc066efac','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',16,'amend','Amend','amend','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4cca1a63cc','2016-10-19 12:26:04.809812','2018-05-17 15:33:46.932403','Administrator','Administrator',0,'Kanban Board','fields','DocType',3,'field_name','Field Name',NULL,'Select',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4d0fce7e93','2016-03-30 01:39:20.586927','2018-05-17 15:33:42.598490','Administrator','Administrator',0,'Portal Menu Item','fields','DocType',4,'reference_doctype','Reference Doctype',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,2,NULL,'',1,0,0,'',0,0,0,0,0,0),('4d31fe2f8d','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',4,'password','Password',NULL,'Password',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4d3e3dd25b','2016-09-21 08:03:01.009852','2018-05-17 15:33:47.615721','Administrator','Administrator',0,'PayPal Settings','fields','DocType',3,'signature','Signature',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4d9969ecc3','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',9,'gunicorn_workers','Gunicorn Workers',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('4da7891800','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',8,'address_line1','Address Line 1',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('4e22f77fdc','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',3,'label','Change Label (via Custom Translation)',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4e57dd5cdc','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',18,'permissions','Permissions',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('4e960c1f7b','2017-02-13 17:33:25.157332','2018-05-17 15:33:44.054341','Administrator','Administrator',0,'Role Permission for Page and Report','fields','DocType',2,'page','Page',NULL,'Link',NULL,'Page',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.set_role_for == \'Page\'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4ea24ac12c','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',10,'column_break_8',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4ec1e66402','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',22,'message_sb','Message',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('4eeb8ff59c','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',20,'ignore_user_permissions_if_missing','Ignore User Permissions If Missing',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'If Apply User Permissions is checked for Report DocType but no User Permissions are defined for Report for a User, then all Reports are shown to that User',0,0,0,'',0,0,0,0,0,0),('4f5314f84c','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',16,'background_workers','Background Workers',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('4f64d5110e','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',31,'send_unsubscribe_message','Send unsubscribe message in email',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('4fa12332b5','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',7,'column_break_4',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5029bda9db','2016-03-30 10:04:25.828742','2018-05-17 15:33:44.194278','Administrator','Administrator',0,'User Email','fields','DocType',4,'awaiting_password','Awaiting Password',NULL,'Check',NULL,'email_account.awaiting_password',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,'',0,0,0,0,0,0),('50649987a2','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',66,'github_username','Github Username',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('5065605059','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',18,'attached_to_field','Attached To Field',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('5091004534','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',12,'collapsible','Collapsible',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.fieldtype==\"Section Break\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('50a86798c8','2017-02-13 17:33:25.157332','2018-05-17 15:33:43.349585','Administrator','Administrator',0,'User Permission for Page and Report','fields','DocType',4,'roles_permission','Roles Permission',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('511262ce6e','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',14,'test_send','Test',NULL,'Button',NULL,'test_send',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('511f7c70c9','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',16,'expose_recipients','Expose Recipients',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('512608a2b7','2013-01-16 13:09:40.000000','2018-05-17 15:33:43.738664','Administrator','Administrator',0,'Error Log','fields','DocType',2,'method','Title',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,NULL,0,0,0,0,0,0),('515725b486','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',5,'column_break_2',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('516c17c120','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',23,'session_expiry','Session Expiry',NULL,'Data',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'06:00','Session Expiry in Hours e.g. 06:00',0,0,0,NULL,0,0,0,0,0,0),('51b7a0762a','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',6,'section_break_6',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,'','eval:(!doc.__islocal)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5204567f35','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',1,'label_and_type','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5213eadf8c','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',11,'reference_doctype','Reference DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5216cd72c1','2016-10-19 12:26:04.809812','2018-05-17 15:33:46.932403','Administrator','Administrator',0,'Kanban Board','fields','DocType',5,'columns','Columns',NULL,'Table',NULL,'Kanban Board Column',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('52585b2eed','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',3,'preview','Preview',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('527683be13','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',19,'format','Format',NULL,'Select',NULL,'HTML\nXLSX\nCSV',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('52aa0c9ae3','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',15,'push_failed','Push Failed',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('52fab83133','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',13,'phone','Phone',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('530d3f876e','2017-03-13 09:20:56.387135','2018-05-17 15:33:45.930240','Administrator','Administrator',0,'Email Rule','fields','DocType',1,'email_id','Email ID',NULL,'Data',NULL,'Email',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('533d08e1cf','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',21,'title_field','Title Field',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.istable',0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('537c08c33e','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',4,'column_break_2',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5383d8ee66','2017-02-13 14:53:36.240122','2018-05-17 15:33:43.077118','Administrator','Administrator',0,'Custom Role','fields','DocType',5,'response','response',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5385ec142f','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',11,'type','Type',NULL,'Select',NULL,'module\nlist\nlink\npage\nquery-report',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,'',0,0,0,0,0,0),('53884ef2b1','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',34,'mail_password','Mail Password',NULL,'Password',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('53ac7c6d3d','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',11,'column_break_7',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('53e130ef81','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',5,'reqd','Mandatory','reqd','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('53fc8d49d5','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',15,'backup_limit','Number of Backups',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'3','Older backups will be automatically deleted',0,0,0,'',0,0,0,0,0,0),('5409dacab6','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',26,'smtp_server','SMTP Server',NULL,'Data',NULL,'domain.smtp_server',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.domain && doc.enable_outgoing',0,0,NULL,NULL,0,NULL,'e.g. smtp.gmail.com',0,0,0,'',0,0,0,0,0,0),('541313ba6d','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',7,'address_title','Address Title',NULL,'Data',NULL,NULL,0,0,0,0,0,0,1,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5426184cf0','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',44,'email_status','Email Status',NULL,'Select',NULL,'Open\nSpam\nTrash',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5474de949f','2013-02-22 01:27:32.000000','2018-05-17 15:33:43.181578','Administrator','Administrator',0,'DefaultValue','fields','DocType',2,'defvalue','Value','defvalue','Text','Text',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'200px','200px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('547af8c7c1','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',15,'column_break_6',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('552fe9ecc7','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',10,'banner_image','Brand Image',NULL,'Attach Image',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Select an image of approx width 150px with a transparent background for best results.',0,0,0,NULL,0,0,0,0,0,0),('553ee49b5a','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',6,'permlevel','Level','permlevel','Int','Int',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'40px','40px',0,'0',NULL,1,0,0,'',0,0,0,0,0,0),('553f0b5efd','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',13,'section_break_9','Message',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5541db8269','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',25,'enable_password_policy','Enable Password Policy',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0','If enabled, the password strength will be enforced based on the Minimum Password Score value. A value of 2 being medium strong and 4 being very strong.',0,0,0,'',0,0,0,0,0,0),('5543b7d6f9','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',2,'data_migration_connector','Data Migration Connector',NULL,'Link',NULL,'Data Migration Connector',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('554bad6846','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',45,'modules_html','Modules HTML',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,1,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('555ad7aee4','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',31,'column_break_21',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('55a57a534d','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',33,'print_hide','Print Hide','print_hide','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('55ca3ffad2','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',3,'install_erpnext','Install Erpnext',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.__islocal',0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('55cb8f95d5','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',18,'custom_html_help','Custom HTML Help',NULL,'HTML',NULL,'<h3>Custom CSS Help</h3>\n\n<p>Notes:</p>\n\n<ol>\n<li>All field groups (label + value) are set attributes <code>data-fieldtype</code> and <code>data-fieldname</code></li>\n<li>All values are given class <code>value</code></li>\n<li>All Section Breaks are given class <code>section-break</code></li>\n<li>All Column Breaks are given class <code>column-break</code></li>\n</ol>\n\n<h4>Examples</h4>\n\n<p>1. Left align integers</p>\n\n<pre><code>[data-fieldtype=\"Int\"] .value { text-left: left; }</code></pre>\n\n<p>1. Add border to sections except the last section</p>\n\n<pre><code>.section-break { padding: 30px 0px; border-bottom: 1px solid #eee; }\n.section-break:last-child { padding-bottom: 0px; border-bottom: 0px;  }</code></pre>\n',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('55da194811','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',10,'pincode','Postal Code',NULL,'Data',NULL,NULL,1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('55e825107b','2016-04-12 18:40:16.315024','2018-05-17 15:33:44.562489','Administrator','Administrator',0,'Footer Item','fields','DocType',1,'label','Label',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'120px','120px',0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('5681461c90','2014-06-19 05:20:26.331041','2018-05-17 15:33:46.049180','Administrator','Administrator',0,'Standard Reply','fields','DocType',5,'standard_reply_help','Standard Reply Help',NULL,'HTML',NULL,'<h4>Standard Reply Example</h4>\n\n<pre>Order Overdue\n\nTransaction {{ name }} has exceeded Due Date. Please take necessary action.\n\nDetails\n\n- Customer: {{ customer }}\n- Amount: {{ grand_total }}\n</pre>\n\n<h4>How to get fieldnames</h4>\n\n<p>The fieldnames you can use in your standard reply are the fields in the document from which you are sending the email. You can find out the fields of any documents via Setup &gt; Customize Form View and selecting the document type (e.g. Sales Invoice)</p>\n\n<h4>Templating</h4>\n\n<p>Templates are compiled using the Jinja Templating Langauge. To learn more about Jinja, <a class=\"strong\" href=\"http://jinja.pocoo.org/docs/dev/templates/\">read this documentation.</a></p>\n',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('56a86d6360','2016-04-20 15:29:39.785172','2018-05-17 15:33:45.989784','Administrator','Administrator',0,'Email Flag Queue','fields','DocType',5,'uid','UID',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('56c178f737','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',36,'search_index','Index',NULL,'Check',NULL,NULL,0,1,0,1,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('56f93343c3','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',9,'gender','Gender',NULL,'Link',NULL,'Gender',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('573d685d6c','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',9,'in_list_view','In List View',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'70px','70px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('57523400f0','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','fields','DocType',7,'section_break_7',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5761731ba2','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',25,'column_break_22',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('57a90d91f0','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',37,'sb3',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.istable',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('57d07e3d5f','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',4,'message','Message',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5814005cff','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',24,'birth_date','Birth Date','birth_date','Date','Date',NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('58247606db','2013-02-22 01:28:08.000000','2018-05-17 15:33:45.105834','Administrator','Administrator',0,'Top Bar Item','fields','DocType',5,'right','Right',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1','For top bar',0,0,0,NULL,0,0,0,0,0,0),('5854dc58f5','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',2,'app_list','App List',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!(doc.__islocal)',0,0,NULL,NULL,0,'default (frappe)',NULL,1,0,1,'',0,0,0,0,0,0),('586633c578','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',2,'ref_doctype','Ref DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('587c2f6781','2012-12-28 10:49:55.000000','2018-05-17 15:33:45.395726','Administrator','Administrator',0,'Workflow','fields','DocType',1,'workflow_name','Workflow Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('58c457711d','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',13,'import_detail','Import Log',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,'eval: doc.import_status == \"Failed\"','import_status',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5918a2bef9','2013-01-10 16:34:04.000000','2018-05-17 15:33:42.420279','Administrator','Administrator',0,'Property Setter','fields','DocType',5,'column_break0',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('592f384384','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',8,'event','Send Alert On',NULL,'Select',NULL,'\nNew\nSave\nSubmit\nCancel\nDays After\nDays Before\nValue Change\nMethod\nCustom',1,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('59559ee9ec','2016-09-21 10:12:57.399174','2018-05-17 15:33:47.640308','Administrator','Administrator',0,'Dropbox Settings','fields','DocType',3,'backup_frequency','Backup Frequency',NULL,'Select',NULL,'\nDaily\nWeekly',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('5974836055','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',16,'options','Options','options','Small Text','Text',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'For Links, enter the DocType as range.\nFor Select, enter list of Options, each on a new line.',1,0,0,NULL,0,0,0,0,0,0),('59d5da0526','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',3,'apply_style','Apply Style',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1','',0,0,0,'',0,0,0,0,0,0),('59de20939c','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',5,'is_standard','Is Standard',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5a011cfac0','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',9,'send_attachements','Send Attachements',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('5a2675e279','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',22,'dropbox_secret_key','Dropbox Secret Key',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5a52e72a90','2016-08-24 14:12:13.647159','2018-05-17 15:33:47.360639','Administrator','Administrator',0,'OAuth Authorization Code','fields','DocType',2,'user','User',NULL,'Link',NULL,'User',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5a7d6c3a4c','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',30,'disable_signup','Disable Signup',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Disable Customer Signup link in Login page',0,0,0,NULL,0,0,0,0,0,0),('5acf7017ca','2014-07-11 17:19:37.037109','2018-05-17 15:33:45.474737','Administrator','Administrator',0,'Email Alert Recipient','fields','DocType',3,'cc','CC',NULL,'Code',NULL,'Email',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Optional: Always send to these ids. Each Email Address on a new row',1,0,0,NULL,0,0,0,0,0,0),('5b01905a3e','2016-08-24 14:12:13.647159','2018-05-17 15:33:47.360639','Administrator','Administrator',0,'OAuth Authorization Code','fields','DocType',7,'validity','Validity',NULL,'Select',NULL,'Valid\nInvalid',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('5b7199b70b','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',4,'column_break_3',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5b71c5073a','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',23,'share','Share',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('5ba32a71ce','2014-10-30 14:23:30.958074','2018-05-17 15:33:45.170674','Administrator','Administrator',0,'Help Category','fields','DocType',4,'help_articles','Help Articles',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,'',0,0,0,0,0,0),('5bb12a677e','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',36,'advanced','Advanced',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5bcadd62a0','2013-01-19 10:23:30.000000','2018-05-17 15:33:46.593643','Administrator','Administrator',0,'Country','fields','DocType',4,'code','Code',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('5bcbc1bdc9','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',19,'idx','Idx',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5be47b795a','2013-02-22 01:27:32.000000','2018-05-17 15:33:43.181578','Administrator','Administrator',0,'DefaultValue','fields','DocType',1,'defkey','Key','defkey','Data','Data',NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'200px','200px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('5bf197e069','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',32,'mail_server','Mail Server',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5c219be4b5','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',12,'use_tls','Use TLS',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5c2dd76a25','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',19,'property_value','Value To Be Set',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5c2e61b2d3','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',11,'thumbnail_url','Thumbnail URL',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('5c3ef2307e','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',5,'column_break_2',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5c45fac2c1','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',2,'role','Role','role','Link','Link','Role',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'150px','150px',0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('5c5c2f2348','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',9,'column_break_5',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5c88bf06b4','2017-07-17 14:25:27.881871','2018-05-17 15:33:42.627169','Administrator','Administrator',0,'User Permission','fields','DocType',1,'user','User',NULL,'Link',NULL,'User',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,'',0,0,0,0,0,0),('5c97cfd86e','2017-08-22 17:15:31.459897','2018-05-17 15:34:43.427815','Administrator','Administrator',0,'Bench Manager Command','fields','DocType',6,'command','Command',NULL,'Text',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,'',0,0,0,0,0,0),('5c9fb8e0fc','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',2,'user','Based on Permissions For User',NULL,'Link',NULL,'User',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'User',NULL,0,0,0,'',0,0,0,0,0,0),('5ca50f12e9','2016-12-29 12:59:48.638970','2018-05-17 15:33:43.427855','Administrator','Administrator',0,'Deleted Document','fields','DocType',4,'restored','Restored',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,'',0,0,0,0,0,0),('5cb722b90e','2013-01-10 16:34:24.000000','2018-05-17 15:33:43.053495','Administrator','Administrator',0,'SMS Settings','fields','DocType',3,'receiver_parameter','Receiver Parameter',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Enter url parameter for receiver nos',1,0,0,NULL,0,0,0,0,0,0),('5cde40e2cb','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',8,'print_format_type','Print Format Type',NULL,'Select',NULL,'Server\nClient\nJs',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'custom_format',0,0,NULL,NULL,0,'Server','',0,0,0,NULL,0,0,0,0,0,0),('5d1aaad027','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',12,'app','App',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5d32bc5ca9','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',16,'roles','Roles',NULL,'Table',NULL,'Has Role',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.is_standard == \'Yes\'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5d476e7e5f','2016-04-14 09:41:45.892975','2018-05-17 15:33:46.225853','Administrator','Administrator',0,'Unhandled Email','fields','DocType',3,'reason','Reason',NULL,'Long Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('5d6830bcd3','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',15,'unsubscribe_method','Unsubscribe Method',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5d7bb80a01','2016-12-29 07:48:06.319665','2018-05-17 15:33:45.075668','Administrator','Administrator',0,'Website Sidebar','fields','DocType',2,'sidebar_items','Sidebar Items',NULL,'Table',NULL,'Website Sidebar Item',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5da948bfc8','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',10,'write','Write','write','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,'1',NULL,1,0,0,NULL,0,0,0,0,0,0),('5daac9c73c','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',21,'background_color','Background Color',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5debaf45b7','2017-08-11 05:09:10.900237','2018-05-17 15:33:48.771854','Administrator','Administrator',0,'Data Migration Mapping Detail','fields','DocType',1,'remote_fieldname','Remote Fieldname',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('5e31c403c8','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',17,'permlevel','Permission Level','permlevel','Int','Int',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,NULL,0,0,0,0,0,0),('5e67d379d0','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',23,'sunday','Sunday',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.repeat_this_event && doc.repeat_on===\"Every Day\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5e6dbd10d3','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',1,'title','Title',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5e83117ffa','2017-01-13 04:55:18.835023','2018-05-17 15:33:42.653992','Administrator','Administrator',0,'Dynamic Link','fields','DocType',3,'link_title','Link Title',NULL,'Read Only',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,'',0,0,0,0,0,0),('5e89f0daba','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',13,'column_break_8',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5ed03d5282','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',23,'share','Share',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('5f6ca14ba9','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','fields','DocType',11,'locals','Locals',NULL,'Code',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5f71947dc1','2013-03-11 17:48:16.000000','2018-05-17 15:33:44.495707','Administrator','Administrator',0,'Blog Settings','fields','DocType',2,'blog_introduction','Blog Introduction',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5f760406c5','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',4,'custom','Custom?',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('5f9a157ac8','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',10,'mapping','Mapping',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5fbaacad50','2016-09-22 04:16:48.829658','2018-05-17 15:33:48.026461','Administrator','Administrator',0,'LDAP Settings','fields','DocType',6,'section_break_5',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5fd92223ca','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',18,'web_form_fields','Web Form Fields',NULL,'Table',NULL,'Web Form Field',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('5fee67dca6','2017-08-11 05:09:10.900237','2018-05-17 15:33:48.771854','Administrator','Administrator',0,'Data Migration Mapping Detail','fields','DocType',4,'child_table_mapping','Child Table Mapping',NULL,'Link',NULL,'Data Migration Mapping',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'is_child_table',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('5ff227be7d','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',1,'sb0_5','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('6020a8a459','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',1,'site_name','Site Name',NULL,'Data',NULL,NULL,0,0,1,0,0,1,0,0,0,1,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('605abe4ece','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',9,'phone_no','Phone No.',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:in_list([\"Phone\",\"SMS\"],doc.communication_medium)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('606d5d4dac','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',24,'session_expiry_mobile','Session Expiry Mobile',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'720:00','In Hours',0,0,0,'',0,0,0,0,0,0),('607a31e562','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',9,'append_to','Append To',NULL,'Link',NULL,'DocType',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,'Append as communication against this DocType (must have fields, \"Status\", \"Subject\")',1,0,0,'',0,0,0,0,0,0),('6098ec9853','2013-03-19 12:02:15.000000','2018-05-17 15:33:44.755304','Administrator','Administrator',0,'About Us Settings','fields','DocType',3,'sb0','Org History',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('609f4a48da','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',28,'mute_sounds','Mute Sounds',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('615aaddacb','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',14,'text_content','Text Content',NULL,'Code',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('616d010b2e','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',35,'mail_login','Mail Login',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6174f318fb','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',14,'unsubscribe_param','Unsubscribe Param',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('61b5ea437b','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',12,'precision','Precision',NULL,'Select',NULL,'\n1\n2\n3\n4\n5\n6\n7\n8\n9',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:in_list([\"Float\", \"Currency\", \"Percent\"], doc.fieldtype)',0,0,NULL,NULL,0,NULL,'Set non-standard precision for a Float or Currency field',0,0,0,'',0,0,0,0,0,0),('62355e46d6','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',26,'report_hide','Report Hide','report_hide','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('623b146660','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','fields','DocType',5,'icon','icon',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('6291b3ec54','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',4,'reqd','Mandatory',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('62dc401f6b','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',4,'local_doctype','Local DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('6377d39c6b','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',25,'reference_section','Reference',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('639afd6bfe','2013-02-22 01:27:36.000000','2018-05-17 15:33:45.439712','Administrator','Administrator',0,'Workflow Document State','fields','DocType',5,'allow_edit','Only Allow Edit For',NULL,'Link',NULL,'Role',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'160px','160px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('639b9d9e02','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',7,'column_break_7',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('63b0cabc6c','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',12,'no_email','Do not send Emails',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('63d740f070','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','fields','DocType',4,'column_break_3',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('63e9a4d16b','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',39,'redis_cache','Redis Cache',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('63f873167a','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',5,'error_file','Generated File',NULL,'Attach',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval: doc.import_status == \"Partially Successful\"',0,0,NULL,NULL,0,NULL,'This is the template file generated with only the rows having some error. You should use this file for correction and import.',0,0,0,'',0,0,0,0,0,0),('640a6d50ed','2017-05-03 15:07:39.752820','2018-05-17 15:33:42.894332','Administrator','makarand@erpnext.com',0,'Domain','fields','DocType',1,'domain','Domain',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('64252fae3a','2013-01-19 10:23:30.000000','2018-05-17 15:33:46.593643','Administrator','Administrator',0,'Country','fields','DocType',2,'date_format','Date Format',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('6444801e1d','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','fields','DocType',4,'title','Title',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('646a21bcff','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',33,'add_signature','Add Signature',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('646c03d97a','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',26,'print_hide','Print Hide','print_hide','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('646f785f7e','2014-10-30 14:23:30.958074','2018-05-17 15:33:45.170674','Administrator','Administrator',0,'Help Category','fields','DocType',2,'category_description','Category Description',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('64a89b55ef','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',5,'awaiting_password','Awaiting password',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('64b3537b9c','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',2,'app_title','App Title',NULL,'Data',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('64d65d40e7','2017-02-13 17:33:25.157332','2018-05-17 15:33:44.054341','Administrator','Administrator',0,'Role Permission for Page and Report','fields','DocType',1,'set_role_for','Set Role For',NULL,'Select',NULL,'\nPage\nReport',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6521a8c537','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',46,'feedback_section','Feedback',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,'eval: doc.rating > 0',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('65530be7c9','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',4,'column_break_3',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6564d7cd33','2013-03-19 12:02:15.000000','2018-05-17 15:33:44.755304','Administrator','Administrator',0,'About Us Settings','fields','DocType',2,'company_introduction','Company Introduction',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Introduce your company to the website visitor.',1,0,0,NULL,0,0,0,0,0,0),('65cccce70c','2013-01-10 16:34:01.000000','2018-05-17 15:33:46.459140','Administrator','Administrator',0,'Custom Script','fields','DocType',3,'script','Script','script','Code','Code','Script',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('65d43096eb','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',18,'report','Report',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('66126d4b63','2013-03-25 16:00:51.000000','2018-05-17 15:33:44.781044','Administrator','Administrator',0,'Blogger','fields','DocType',5,'bio','Bio',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('66c3a27087','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',7,'email_settings','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('66d8135b41','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',1,'sb0','Landing Page',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('674cf992ee','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',9,'allow_edit','Allow Edit',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'login_required',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('6798925931','2017-08-11 05:15:38.390831','2018-05-17 15:33:48.573691','Administrator','Administrator',0,'Data Migration Plan Mapping','fields','DocType',2,'enabled','Enabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,1,0,0,'',0,0,0,0,0,0),('679b5ad0fd','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',14,'default','Default Value','default','Text','Text',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('67fbc8f0f7','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',34,'redirect_url','Redirect URL',NULL,'Small Text',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('68d00748df','2016-08-24 14:10:17.471264','2018-05-17 15:33:47.989214','Administrator','Administrator',0,'OAuth Bearer Token','fields','DocType',2,'user','User',NULL,'Link',NULL,'User',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('68e35e7e68','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',6,'middle_name','Middle Name (Optional)','middle_name','Data','Data',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('68eb8230f9','2016-05-25 09:49:07.125394','2018-05-17 15:33:43.025756','Administrator','Administrator',0,'Tag Category','fields','DocType',3,'tagdocs','Doctypes',NULL,'Table',NULL,'Tag Doc Category',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('68fb10577a','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',6,'column_break_5',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('68fb1d88ed','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',15,'links','Links',NULL,'Table',NULL,'Dynamic Link',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('690af651c1','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',2,'is_private','Is Private',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.is_folder',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('698beaa37e','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',15,'column_break_12',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('69b7cb7e44','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',11,'column_break_11',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('69f5b1aa13','2013-01-10 16:34:04.000000','2018-05-17 15:33:42.420279','Administrator','Administrator',0,'Property Setter','fields','DocType',3,'doctype_or_field','DocType or Field',NULL,'Select',NULL,'\nDocField\nDocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,'eval:doc.__islocal',0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('6a1abb0dbc','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',21,'column_break_20',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6a71668334','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',2,'home_page','Home Page',NULL,'Data',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Link that is the website home page. Standard Links (index, login, products, blog, about, contact)',1,0,0,NULL,0,0,0,0,0,0),('6a71ae21f6','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',14,'print_style_section','Print Style',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('6a7242f432','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',30,'always_use_account_email_id_as_sender','Always use Account\'s Email Address as Sender',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'enable_outgoing',0,0,NULL,NULL,0,NULL,'Uses the Email Address mentioned in this Account as the Sender for all emails sent using this Account. ',0,0,0,'',0,0,0,0,0,0),('6ab07ea996','2017-04-24 09:53:41.813982','2018-05-17 15:33:47.580015','Administrator','Administrator',0,'GSuite Templates','fields','DocType',3,'template_id','Template ID',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6aba1f06e6','2015-02-04 04:33:36.330477','2018-05-17 15:33:42.835173','Administrator','Administrator',0,'DocShare','fields','DocType',6,'share','Share',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('6acbce7de0','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',5,'email_fieldname','Email Fieldname',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('6aeb0104e6','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',20,'rgt','rgt',NULL,'Int',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6aecf98c32','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',58,'engine','Database Engine',NULL,'Select',NULL,'InnoDB\nMyISAM',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.issingle',0,0,NULL,NULL,0,'InnoDB',NULL,0,0,0,'',0,0,0,0,0,0),('6b5d02e898','2016-04-20 15:29:39.785172','2018-05-17 15:33:45.989784','Administrator','Administrator',0,'Email Flag Queue','fields','DocType',4,'email_account','Email Account',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6b665e1bec','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',30,'allow_on_submit','Allow on Submit','allow_on_submit','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('6b709c8155','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','fields','DocType',9,'standard','Standard','standard','Select','Select','\nYes\nNo',1,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('6b719dcb40','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',4,'module','Module',NULL,'Link',NULL,'Module Def',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6b8173c278','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',45,'has_attachment','Has  Attachment',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6b86f29fb5','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',1,'email_settings','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6ba12b5d5e','2013-02-22 01:27:34.000000','2018-05-17 15:33:41.954180','Administrator','Administrator',0,'Has Role','fields','DocType',1,'role','Role','role','Link','Link','Role',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('6ba41dd9fa','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','fields','DocType',9,'pyver','Pyver',NULL,'Code',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('6c0965f735','2013-03-19 12:02:15.000000','2018-05-17 15:33:44.755304','Administrator','Administrator',0,'About Us Settings','fields','DocType',7,'team_members_heading','Team Members Heading',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'\"Team Members\" or \"Management\"',0,0,0,NULL,0,0,0,0,0,0),('6c11a39816','2013-01-17 11:36:45.000000','2018-05-17 15:33:43.372622','Administrator','Administrator',0,'Patch Log','fields','DocType',1,'patch','Patch',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('6c276f7d0e','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','fields','DocType',3,'published','Published',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6c5821ce8e','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',39,'footer','Footer',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6c5c5cc8e8','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',13,'line_breaks','Show Line Breaks after Sections',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('6c5fb198ad','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',47,'sort_order','Sort Order',NULL,'Select',NULL,'ASC\nDESC',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.istable',0,0,NULL,NULL,0,'DESC',NULL,0,0,0,NULL,0,0,0,0,0,0),('6cc869a5b4','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',28,'ref_type','Ref Type','ref_type','Link','Data','DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('6d65722f74','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',12,'folder','Folder',NULL,'Link',NULL,'File',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,1,1,'',0,0,0,0,0,0),('6d88c27d81','2015-03-18 06:15:59.321619','2018-05-17 15:33:45.955127','Administrator','Administrator',0,'Email Group Member','fields','DocType',1,'email_group','Email Group',NULL,'Link',NULL,'Email Group',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,'',0,0,0,0,0,0),('6dbe6f31da','2017-05-03 15:20:22.326623','2018-05-17 15:33:42.806897','Administrator','makarand@erpnext.com',0,'Has Domain','fields','DocType',1,'domain','Domain',NULL,'Link',NULL,'Domain',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('6dc00093f6','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',16,'comment_type','Comment Type',NULL,'Select',NULL,'\nComment\nLike\nInfo\nLabel\nWorkflow\nCreated\nSubmitted\nCancelled\nUpdated\nDeleted\nAssigned\nAssignment Completed\nAttachment\nAttachment Removed\nShared\nUnshared\nBot\nRelinked',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,1,'',0,0,0,0,0,0),('6df58c1485','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',15,'cancel','Cancel','cancel','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('6e0febe7d1','2012-12-28 10:49:55.000000','2018-05-17 15:33:45.395726','Administrator','Administrator',0,'Workflow','fields','DocType',2,'document_type','Document Type',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'DocType on which this Workflow is applicable.',0,1,0,NULL,0,0,0,0,0,0),('6e2cfd5950','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',1,'app_name','App Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6e3cf46a5a','2017-08-22 17:15:31.459897','2018-05-17 15:34:43.427815','Administrator','Administrator',0,'Bench Manager Command','fields','DocType',4,'status','Status',NULL,'Select',NULL,'Success\nFailed\nOngoing',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Ongoing',NULL,0,0,1,'',0,0,0,0,0,0),('6e55981c54','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',14,'time_zone','Timezone',NULL,'Select',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('6e63618bec','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',17,'department','Department',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('6e971e2909','2016-08-24 14:10:17.471264','2018-05-17 15:33:47.989214','Administrator','Administrator',0,'OAuth Bearer Token','fields','DocType',5,'refresh_token','Refresh Token',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('6eddb12f6f','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',28,'payment_gateway','Payment Gateway',NULL,'Link',NULL,'Payment Gateway',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'accept_payment',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6f1c28eabd','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',8,'search_index','Index','search_index','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('6f4c81ee33','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','fields','DocType',11,'roles','Roles','roles','Table','Table','Has Role',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.standard == \'Yes\'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('6f7c32a94c','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',7,'last_name','Last Name','last_name','Data','Data',NULL,0,0,0,0,0,0,1,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('6f83005451','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',37,'width','Width','width','Data','Data',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('6f8378fd79','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',18,'footer','Footer',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('6fbe0d07af','2016-05-25 09:49:07.125394','2018-05-17 15:33:43.025756','Administrator','Administrator',0,'Tag Category','fields','DocType',2,'tags','Tags',NULL,'Table',NULL,'Tag',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6fd34c25eb','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',35,'currency','Currency',NULL,'Link',NULL,'Currency',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'accept_payment',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('6ff9a0830e','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',15,'depends_on','Depends On',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,255,0,0,0,0,0),('6ffc0f5d0d','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',7,'section_break_6',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'custom_format',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('701f8bd664','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',38,'is_custom_field','Is Custom Field',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('70796e62be','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',5,'key','Key',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7083aa5d32','2014-02-20 17:22:37.000000','2018-05-17 15:33:42.687506','Administrator','Administrator',0,'Version','fields','DocType',2,'column_break_3',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7091ed9272','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',3,'language','Language',NULL,'Link',NULL,'Language',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('70aab4c047','2016-03-30 01:39:20.586927','2018-05-17 15:33:42.598490','Administrator','Administrator',0,'Portal Menu Item','fields','DocType',2,'enabled','Enabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,1,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('70c090e4cb','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',23,'image_field','Image Field (Must of type \"Attach Image\")',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,'',0,0,0,0,0,0),('70d65ed9ef','2016-03-30 01:40:20.001775','2018-05-17 15:33:44.625767','Administrator','Administrator',0,'Portal Settings','fields','DocType',4,'menu','Portal Menu',NULL,'Table',NULL,'Portal Menu Item',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('71101aa362','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',38,'cb30','Permissions Settings',NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('713307c555','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',1,'subject','Subject',NULL,'Small Text',NULL,NULL,0,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('717a3b9cad','2017-02-13 14:53:36.240122','2018-05-17 15:33:43.077118','Administrator','Administrator',0,'Custom Role','fields','DocType',3,'permission_rules','Permission Rules',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7189bcd8ee','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',6,'email_account_name','Email Account Name',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'e.g. \"Support\", \"Sales\", \"Jerry Yang\"',0,0,0,'',0,0,0,0,0,0),('71b49721cb','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',36,'permissions','Permissions','permissions','Table','Table','DocPerm',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('71e6f2fdf3','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',6,'column_break_4',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('71f3841d6a','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',2,'status','Status',NULL,'Select',NULL,'Open\nClosed',0,0,0,0,0,0,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Open',NULL,1,1,0,NULL,0,0,0,0,0,0),('720462788c','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',25,'sidebar_items','Sidebar Items',NULL,'Table',NULL,'Portal Menu Item',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('72a00d494e','2016-08-04 04:58:40.457416','2018-05-17 15:33:47.300993','Administrator','Administrator',0,'Integration Request','fields','DocType',7,'reference_doctype','Reference Doctype',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('72b2e94c63','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',15,'communication_type','Communication Type',NULL,'Select',NULL,'Communication\nComment\nChat\nBot\nNotification\nFeedback',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Communication',NULL,0,0,1,'',0,0,0,0,0,0),('73629f6e47','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',2,'section_break_8',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7376b54ed8','2017-08-17 01:25:56.910716','2018-05-17 15:33:48.101538','Administrator','Administrator',0,'Print Style','fields','DocType',3,'standard','Standard',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('73b495f17f','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',12,'_doctype','_doctype',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('73bc7687e2','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',34,'link_name','Link Name',NULL,'Dynamic Link',NULL,'link_doctype',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('7421af69b7','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',14,'insert_style','Insert Style',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('7430a478d9','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',17,'attached_to_name','Attached To Name',NULL,'Data',NULL,NULL,1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('745726b0e9','2017-08-17 01:25:56.910716','2018-05-17 15:33:48.101538','Administrator','Administrator',0,'Print Style','fields','DocType',4,'css','CSS',NULL,'Code',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7477a9ce19','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',26,'header','Header',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'HTML for header section. Optional',0,0,0,NULL,0,0,0,0,0,0),('7478f76d87','2017-08-11 05:09:10.900237','2018-05-17 15:33:48.771854','Administrator','Administrator',0,'Data Migration Mapping Detail','fields','DocType',3,'is_child_table','Is Child Table',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('749b8e27f2','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',5,'max_attachments','Max Attachments',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('74d715dbe6','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',7,'message_id','Message ID',NULL,'Data',NULL,NULL,1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('753b9e4adc','2017-03-09 17:18:29.458397','2018-05-17 15:33:47.156384','Administrator','Administrator',0,'Stripe Settings','fields','DocType',1,'publishable_key','Publishable Key',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('753cda53cd','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',1,'fieldname','Fieldname',NULL,'Select',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('7556e8a37a','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',4,'mailbox_settings','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('759e592790','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',19,'initial_sync_count','Initial Sync Count',NULL,'Select',NULL,'100\n250\n500',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'250','Total number of emails to sync in initial sync process ',0,0,0,'',0,0,0,0,0,0),('762b7238f4','2016-07-15 05:51:29.224123','2018-05-17 15:33:46.911760','Administrator','Administrator',0,'Bulk Update','fields','DocType',3,'update_value','Update Value',NULL,'Small Text',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7687c7de59','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',10,'write','Write','write','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,'1',NULL,1,0,0,'',0,0,0,0,0,0),('76a6ad1711','2016-09-20 03:44:03.799402','2018-05-17 15:33:47.340008','Administrator','Administrator',0,'Razorpay Settings','fields','DocType',1,'api_key','API Key',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('76c2a707ab','2017-08-17 14:51:33.577230','2018-05-17 15:34:43.329531','Administrator','Administrator',0,'Site Backup','fields','DocType',8,'developer_flag','Developer Flag',NULL,'Int',NULL,NULL,0,1,0,1,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,1,'',0,0,0,0,0,0),('774b62c108','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',11,'state','State',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('775ea5baff','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',7,'istable','Is Table',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('77c8d7fc28','2016-12-08 12:01:07.993900','2018-05-17 15:33:46.198242','Administrator','Administrator',0,'Email Queue Recipient','fields','DocType',3,'error','Error',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('77e4cab8b7','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',10,'allow_multiple','Allow Multiple',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'login_required',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('77eeef79e1','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',8,'options','Options',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('7800754786','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',33,'column_break_30',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('781ed839b9','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',32,'description','Description','description','Text','Text',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'300px','300px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('787507d6ab','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',17,'settings','Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('787ea8cadf','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',14,'reference_communication','Reference Communication',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('789a5ab360','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',8,'login_required','Login Required',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('789fd4874b','2016-05-25 13:09:20.996154','2018-05-17 15:33:42.945814','Administrator','Administrator',0,'Tag Doc Category','fields','DocType',1,'tagdoc','Doctype to Assign Tags',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('78ceb5500c','2017-08-22 17:15:31.459897','2018-05-17 15:34:43.427815','Administrator','Administrator',0,'Bench Manager Command','fields','DocType',9,'key','Key',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('78d815d7b5','2013-03-19 12:02:15.000000','2018-05-17 15:33:44.755304','Administrator','Administrator',0,'About Us Settings','fields','DocType',5,'company_history','Org History',NULL,'Table',NULL,'Company History',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('78ef4f9327','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',15,'search_fields','Search Fields',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Fields separated by comma (,) will be included in the \"Search By\" list of Search dialog box',1,0,0,NULL,0,0,0,0,0,0),('78efa0f556','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',28,'allow_on_submit','Allow on Submit','allow_on_submit','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval: parent.is_submittable',0,0,'50px','50px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('78f254dec8','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',29,'track_seen','Track Seen',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('78fb3bf752','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',7,'auto_email_id','Auto Email ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7906ebd229','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',2,'remote_objectname','Remote Objectname',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('790a425d2c','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',17,'permlevel','Perm Level','permlevel','Int','Int',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,1,0,0,NULL,0,0,0,0,0,0),('791de5997c','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',17,'property_section','Set Property After Alert',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('791e4456dc','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',3,'is_standard','Is Standard',NULL,'Select',NULL,'No\nYes',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('792c09ce6b','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',42,'hide_footer_in_auto_email_reports','Hide footer in auto email reports',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('79575a96bf','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',19,'column_break_16',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('797a5f66af','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',42,'message_id','Message ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',995,1,0,0,0,0),('79b13b3feb','2013-05-24 13:41:00.000000','2018-05-17 15:33:46.746140','Administrator','Administrator',0,'Note','fields','DocType',2,'public','Public',NULL,'Check',NULL,NULL,0,0,0,1,0,0,1,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('7a1078a308','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',3,'enabled','Enabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('7a2de71e02','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',21,'allow_bulk_edit','Allow Bulk Edit',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval: doc.fieldtype == \"Table\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7a4d05f4a3','2015-02-04 04:33:36.330477','2018-05-17 15:33:42.835173','Administrator','Administrator',0,'DocShare','fields','DocType',5,'write','Write',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('7a59df985b','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',16,'append_to','Append To',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'enable_incoming',0,0,NULL,NULL,0,'','Append as communication against this DocType (must have fields, \"Status\", \"Subject\")',0,1,0,'',0,0,0,0,0,0),('7ab32ed7d6','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',22,'collapsible_depends_on','Collapsible Depends On',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.fieldtype==\"Section Break\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7aea430347','2016-04-20 15:29:39.785172','2018-05-17 15:33:45.989784','Administrator','Administrator',0,'Email Flag Queue','fields','DocType',3,'action','Action',NULL,'Select',NULL,'Read\nUnread',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7b448059eb','2013-03-11 17:48:16.000000','2018-05-17 15:33:44.495707','Administrator','Administrator',0,'Blog Settings','fields','DocType',1,'blog_title','Blog Title',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('7b4634977e','2016-08-24 14:12:13.647159','2018-05-17 15:33:47.360639','Administrator','Administrator',0,'OAuth Authorization Code','fields','DocType',6,'redirect_uri_bound_to_authorization_code','Redirect URI Bound To Auth Code',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7bca98da6b','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',20,'print_format_help','Print Format Help',NULL,'HTML',NULL,'<h3>Print Format Help</h3>\n<hr>\n<h4>Introduction</h4>\n<p>Print itemsFormats are rendered on the server side using the Jinja Templating Language. All forms have access to the <code>doc</code> object which contains information about the document that is being formatted. You can also access common utilities via the <code>frappe</code> module.</p>\n<p>For styling, the Boostrap CSS framework is provided and you can enjoy the full range of classes.</p>\n<hr>\n<h4>References</h4>\n<ol>\n	<li><a href=\"http://jinja.pocoo.org/docs/templates/\" target=\"_blank\">Jinja Tempalting Language: Reference</a></li>\n	<li><a href=\"http://getbootstrap.com\" target=\"_blank\">Bootstrap CSS Framework</a></li>\n</ol>\n<hr>\n<h4>Example</h4>\n<pre><code>&lt;h3&gt;{{ doc.select_print_heading or \"Invoice\" }}&lt;/h3&gt;\n&lt;div class=\"row\"&gt;\n	&lt;div class=\"col-md-3 text-right\"&gt;Customer Name&lt;/div&gt;\n	&lt;div class=\"col-md-9\"&gt;{{ doc.customer_name }}&lt;/div&gt;\n&lt;/div&gt;\n&lt;div class=\"row\"&gt;\n	&lt;div class=\"col-md-3 text-right\"&gt;Date&lt;/div&gt;\n	&lt;div class=\"col-md-9\"&gt;{{ doc.get_formatted(\"invoice_date\") }}&lt;/div&gt;\n&lt;/div&gt;\n&lt;table class=\"table table-bordered\"&gt;\n	&lt;tbody&gt;\n		&lt;tr&gt;\n			&lt;th&gt;Sr&lt;/th&gt;\n			&lt;th&gt;Item Name&lt;/th&gt;\n			&lt;th&gt;Description&lt;/th&gt;\n			&lt;th class=\"text-right\"&gt;Qty&lt;/th&gt;\n			&lt;th class=\"text-right\"&gt;Rate&lt;/th&gt;\n			&lt;th class=\"text-right\"&gt;Amount&lt;/th&gt;\n		&lt;/tr&gt;\n		{%- for row in doc.items -%}\n		&lt;tr&gt;\n			&lt;td style=\"width: 3%;\"&gt;{{ row.idx }}&lt;/td&gt;\n			&lt;td style=\"width: 20%;\"&gt;\n				{{ row.item_name }}\n				{% if row.item_code != row.item_name -%}\n				&lt;br&gt;Item Code: {{ row.item_code}}\n				{%- endif %}\n			&lt;/td&gt;\n			&lt;td style=\"width: 37%;\"&gt;\n				&lt;div style=\"border: 0px;\"&gt;{{ row.description }}&lt;/div&gt;&lt;/td&gt;\n			&lt;td style=\"width: 10%; text-align: right;\"&gt;{{ row.qty }} {{ row.uom or row.stock_uom }}&lt;/td&gt;\n			&lt;td style=\"width: 15%; text-align: right;\"&gt;{{\n				row.get_formatted(\"rate\", doc) }}&lt;/td&gt;\n			&lt;td style=\"width: 15%; text-align: right;\"&gt;{{\n				row.get_formatted(\"amount\", doc) }}&lt;/td&gt;\n		&lt;/tr&gt;\n		{%- endfor -%}\n	&lt;/tbody&gt;\n&lt;/table&gt;</code></pre>\n<hr>\n<h4>Common Functions</h4>\n<table class=\"table table-bordered\">\n	<tbody>\n		<tr>\n			<td style=\"width: 30%;\"><code>doc.get_formatted(\"[fieldname]\", [parent_doc])</code></td>\n			<td>Get document value formatted as Date, Currency etc. Pass parent <code>doc</code> for curreny type fields.</td>\n		</tr>\n		<tr>\n			<td style=\"width: 30%;\"><code>frappe.db.get_value(\"[doctype]\", \"[name]\", \"fieldname\")</code></td>\n			<td>Get value from another document.</td>\n		</tr>\n	</tbody>\n</table>\n',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'custom_format',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('7be362ef78','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',14,'submit','Submit','submit','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('7be4e6fd72','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',20,'section_break_13',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'enable_incoming',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7c26c81c3e','2016-09-22 04:16:48.829658','2018-05-17 15:33:48.026461','Administrator','Administrator',0,'LDAP Settings','fields','DocType',1,'enabled','Enabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7c90b6aa04','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',10,'communication','Communication',NULL,'Link',NULL,'Communication',1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7c9aebfb51','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',7,'published','Published',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('7ca8640c1c','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',15,'user_image','User Image',NULL,'Attach Image',NULL,NULL,0,1,0,1,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Get your globally recognized avatar from Gravatar.com',0,0,0,NULL,0,0,0,0,0,0),('7d2a7d098d','2017-08-22 17:15:31.459897','2018-05-17 15:34:43.427815','Administrator','Administrator',0,'Bench Manager Command','fields','DocType',10,'bench_settings','Bench Settings',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('7d725a02e9','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',7,'current_mapping_type','Current Mapping Type',NULL,'Select',NULL,'Push\nPull',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7d81abc53e','2013-05-24 13:41:00.000000','2018-05-17 15:33:46.746140','Administrator','Administrator',0,'Note','fields','DocType',6,'content','Content',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,1,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Help: To link to another record in the system, use \"#Form/Note/[Note Name]\" as the Link URL. (don\'t use \"http://\")',0,0,0,NULL,0,0,0,0,0,0),('7db68c08c1','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',1,'sb0','',NULL,'Section Break','Section Break',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('7dc3ee5724','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',3,'show_as_cc','Show as cc',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7dcd241196','2013-03-08 09:41:11.000000','2018-05-17 15:33:45.136706','Administrator','Administrator',0,'Blog Category','fields','DocType',2,'title','Title',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('7e44f4f2b1','2016-08-24 14:12:13.647159','2018-05-17 15:33:47.360639','Administrator','Administrator',0,'OAuth Authorization Code','fields','DocType',1,'client','Client',NULL,'Link',NULL,'OAuth Client',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('7e96c2d9c8','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',8,'app_color','App Color',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'\'grey\'',NULL,0,0,0,'',0,0,0,0,0,0),('7eee8be348','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',1,'section_title','Title',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('7ef9c8d786','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',17,'icon','Icon',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7f16cb8175','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',4,'preview_html','Preview HTML',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7f4c7bea4f','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',27,'use_tls','Use TLS',NULL,'Check',NULL,'domain.use_tls',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.domain && doc.enable_outgoing',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('7f4fd4994e','2013-02-22 01:28:08.000000','2018-05-17 15:33:44.943298','Administrator','Administrator',0,'Company History','fields','DocType',1,'year','Year',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('7f7fbb2451','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',22,'enable_comments','Enable Comments',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('7f97a438ef','2015-02-04 04:33:36.330477','2018-05-17 15:33:42.835173','Administrator','Administrator',0,'DocShare','fields','DocType',2,'share_doctype','Document Type',NULL,'Link',NULL,'DocType',1,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('7f9f415924','2012-12-28 10:49:56.000000','2018-05-17 15:33:45.302169','Administrator','Administrator',0,'Workflow State','fields','DocType',1,'workflow_state_name','State',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('7fa241d7a1','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',1,'contact_section','',NULL,'Section Break',NULL,'fa fa-user',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('804c7f14f8','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',2,'label','Label','label','Data','Data',NULL,1,0,0,0,0,0,1,0,0,0,0,0,NULL,NULL,NULL,0,0,'163','163',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('8067634680','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',2,'module','Module',NULL,'Link',NULL,'Module Def',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('80b3bcaf72','2012-12-28 10:49:55.000000','2018-05-17 15:33:45.395726','Administrator','Administrator',0,'Workflow','fields','DocType',9,'workflow_state_field','Workflow State Field',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'workflow_state','Field that represents the Workflow State of the transaction (if field is not present, a new hidden Custom Field will be created)',0,0,0,NULL,0,0,0,0,0,0),('80c17d5839','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',40,'column_break_18',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('81028738c3','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',16,'amend','Amend','amend','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('810b00f884','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','fields','DocType',11,'email_sent','Email Sent',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('814b9afcf0','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',8,'current_mapping_action','Current Mapping Action',NULL,'Select',NULL,'Insert\nDelete',0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,'eval:(doc.status !== \'Pending\')',0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('817bd9b48f','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',11,'route','Route',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('82217f2c93','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','fields','DocType',6,'blogger','Blogger',NULL,'Link',NULL,'Blogger',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('823c730846','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',9,'github_client_secret','GitHub Client Secret',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('823d43e5f8','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',2,'module','Module',NULL,'Link',NULL,'Module Def',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Website',NULL,0,0,0,'',0,0,0,0,0,0),('827e16e154','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',11,'column_break_7',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8281a9d488','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',13,'_report','_report',NULL,'Link',NULL,'Report',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('82aaaa0dce','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',4,'column_break_4',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('82be65a29c','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',37,'ignore_xss_filter','Ignore XSS Filter',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Don\'t HTML Encode HTML tags like &lt;script&gt; or just characters like &lt; or &gt;, as they could be intentionally used in this field',0,0,0,'',0,0,0,0,0,0),('82ed7c980c','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',20,'additional_info','More Information',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('82f9342b92','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',9,'date_format','Date Format',NULL,'Select',NULL,'yyyy-mm-dd\ndd-mm-yyyy\ndd/mm/yyyy\ndd.mm.yyyy\nmm/dd/yyyy\nmm-dd-yyyy',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('830f9a5376','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',18,'email_sync_option','Email Sync Option',NULL,'Select',NULL,'ALL\nUNSEEN',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval: doc.enable_incoming',0,0,NULL,NULL,0,'UNSEEN',NULL,0,0,0,'',0,0,0,0,0,0),('833b5f59bd','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',13,'currency_precision','Currency Precision',NULL,'Select',NULL,'\n1\n2\n3\n4\n5\n6\n7\n8\n9',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'If not set, the currency precision will depend on number format',0,0,0,'',0,0,0,0,0,0),('837a60b31d','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',1,'role_and_level','Role and Level',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('83d0c37f5b','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',6,'cb00',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('83e427ecc0','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',2,'section_break_2',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('843ef392c0','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',8,'data_modified_till','Only Send Records Updated in Last X Hours',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.report_type==\'Report Builder\'',0,0,NULL,NULL,0,'','Zero means send records updated at anytime',0,0,0,'',0,0,0,0,0,0),('843fd32aef','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',25,'css','Style using CSS',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_style',0,0,NULL,NULL,0,NULL,'',0,0,0,'',0,0,0,0,0,0),('84703726f6','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',3,'label','Label',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('8477acc96d','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',13,'fields_section_break','Fields',NULL,'Section Break','Section Break',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('849793cc96','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',14,'column_break_11',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('84a265f82c','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',5,'column_break_3',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('84bd345df3','2013-03-07 15:53:15.000000','2018-05-17 15:33:45.003160','Administrator','Administrator',0,'Website Slideshow','fields','DocType',4,'header','Header',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.__islocal',0,0,NULL,NULL,0,NULL,'This goes above the slideshow.',0,0,0,NULL,0,0,0,0,0,0),('84c06cc614','2016-09-22 04:16:48.829658','2018-05-17 15:33:48.026461','Administrator','Administrator',0,'LDAP Settings','fields','DocType',10,'ldap_username_field','LDAP Username Field',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('84cc42d18b','2016-05-25 09:43:44.767581','2018-05-17 15:33:44.231274','Administrator','Administrator',0,'Tag','fields','DocType',1,'tag_name','Tags',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('84fb0fa86c','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',29,'github_password','Github Password',NULL,'Password',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('85368d1a5b','2016-04-20 15:29:39.785172','2018-05-17 15:33:45.989784','Administrator','Administrator',0,'Email Flag Queue','fields','DocType',1,'is_completed','Is Completed',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('859634bd89','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',5,'email_server','Email Server',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,'e.g. pop.gmail.com / imap.gmail.com',0,0,0,'',0,0,0,0,0,0),('85b17a75c4','2016-12-29 12:59:48.638970','2018-05-17 15:33:43.427855','Administrator','Administrator',0,'Deleted Document','fields','DocType',7,'data','Data',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('85b5571930','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',22,'print_format_builder','Print Format Builder',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('85cfebdad3','2017-02-13 14:53:36.240122','2018-05-17 15:33:43.077118','Administrator','Administrator',0,'Custom Role','fields','DocType',1,'page','Page',NULL,'Link',NULL,'Page',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('85fae88df0','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',7,'status','Status',NULL,'Select',NULL,'Passive\nOpen\nReplied',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Passive',NULL,1,1,0,NULL,0,0,0,0,0,0),('8628280986','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',54,'column_break_53',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('864f3966de','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',10,'letter_head','Letter Head',NULL,'Link',NULL,'Letter Head',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval: doc.is_standard == \"No\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8655ace53e','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',3,'email_id','Example Email Address',NULL,'Data',NULL,'Email',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,'',0,0,0,0,0,0),('8660019b98','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',18,'is_your_company_address','Is Your Company Address',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('8669de4e38','2012-11-22 17:45:46.000000','2018-05-17 15:33:48.057067','Administrator','Administrator',0,'Letter Head','fields','DocType',3,'is_default','Is Default','is_default','Check','Check',NULL,1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'letter_head_name',0,0,NULL,NULL,0,NULL,'Check this to make this the default letter head in all prints',1,0,0,NULL,0,0,0,0,0,0),('8677ac7573','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',8,'scopes','Scopes',NULL,'Text',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'all openid','A list of resources which the Client App will have access to after the user allows it.<br> e.g. project',0,0,0,NULL,0,0,0,0,0,0),('86c4e96324','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',17,'password_settings','Password Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('86ff75587f','2016-10-19 12:26:04.809812','2018-05-17 15:33:46.932403','Administrator','Administrator',0,'Kanban Board','fields','DocType',2,'reference_doctype','Reference DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('870c28edb3','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',9,'custom','Custom?',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('871fd1194b','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',6,'custom_format','Custom Format',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('878cdf9846','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',15,'repeat_till','Repeat Till',NULL,'Date',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'repeat_this_event',0,0,NULL,NULL,0,NULL,'Leave blank to repeat always',0,0,0,NULL,0,0,0,0,0,0),('879060179a','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',18,'width','Width','width','Data','Data',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('8790fff45a','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',43,'background_style','Background Style',NULL,'Select',NULL,'Fill Screen\nTile',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('87991ee377','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',10,'custom_javascript','Script',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('879b989ec8','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',24,'database_size','Database Size',NULL,'Float',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('879c25607d','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','fields','DocType',6,'etype','Exception Type',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('87bfdee680','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',19,'section_break_13',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'custom_format',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('87eefededc','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',13,'javascript','Javascript',NULL,'Code',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,'JavaScript Format: frappe.query_reports[\'REPORTNAME\'] = {}',0,0,0,NULL,0,0,0,0,0,0),('87f7d22f72','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','fields','DocType',10,'content','Content',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,1,0,0,0,0),('88607a4df4','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',9,'main_section','Main Section',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,1,0,0,0,0),('88aa61ec8f','2014-08-22 16:12:17.249590','2018-05-17 15:33:44.077470','Administrator','Administrator',0,'Language','fields','DocType',4,'based_on','Based On',NULL,'Link',NULL,'Language',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('88c28eae09','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',22,'security','Security',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('8927322b86','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','fields','DocType',7,'traceback','Traceback',NULL,'Code',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('893f17f2b6','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',6,'use_imap','Use IMAP',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('89a2cc062b','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',13,'is_git_repo','Is Git Repo',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('89a68bdfe3','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',8,'reference_doctype','Reference DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('89cf58d716','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',12,'image','Image',NULL,'Attach Image',NULL,NULL,0,1,0,1,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('89cf85b245','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',34,'show_name_in_global_search','Make \"name\" searchable in Global Search',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('89f06a5b69','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',2,'fieldtype','Fieldtype',NULL,'Select',NULL,'Attach\nCheck\nData\nDate\nDatetime\nFloat\nHTML\nInt\nLink\nSelect\nSmall Text\nText\nText Editor\nTable\nSection Break\nColumn Break\nPage Break',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('8a3b53a0db','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',12,'column_break_11',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8a884368ef','2015-03-18 09:41:20.216319','2018-05-17 15:33:46.333335','Administrator','Administrator',0,'Email Unsubscribe','fields','DocType',4,'global_unsubscribe','Global Unsubscribe',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('8b04d36dab','2013-03-08 09:41:11.000000','2018-05-17 15:33:45.136706','Administrator','Administrator',0,'Blog Category','fields','DocType',4,'route','Route',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,1,0,0,NULL,NULL,'published',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8b2e8c9a1f','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',22,'column_break_19',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8b2fffcf27','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',36,'print_hide_if_no_value','Print Hide If No Value',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:[\"Int\", \"Float\", \"Currency\", \"Percent\"].indexOf(doc.fieldtype)!==-1',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8b5b519b7a','2017-08-22 17:15:31.459897','2018-05-17 15:34:43.427815','Administrator','Administrator',0,'Bench Manager Command','fields','DocType',1,'source','Source',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('8b5ff2c149','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',61,'fb_username','Facebook Username',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('8b794d0190','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',1,'report_name','Report Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('8b85be52f3','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',30,'allow_login_using_user_name','Allow Login using User Name',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0','User can login using Email id or User Name',0,0,0,'',0,0,0,0,0,0),('8b878d37ea','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',40,'column_break_22',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('8bb686ea48','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',6,'published','Published',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('8bbd22f644','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',11,'logs_sb','Logs',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,'eval:(doc.status !== \'Pending\')',0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('8bc6e8289c','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',20,'actions','Actions',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('8c070dfeb0','2013-03-11 17:48:16.000000','2018-05-17 15:33:44.495707','Administrator','Administrator',0,'Blog Settings','fields','DocType',3,'writers_introduction','Writers Introduction',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('8c0d310cff','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',5,'client_secret','App Client Secret',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('8c4e185f6b','2016-04-20 15:29:39.785172','2018-05-17 15:33:45.989784','Administrator','Administrator',0,'Email Flag Queue','fields','DocType',2,'communication','Communication',NULL,'Data',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8c59480d98','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',8,'editable_grid','Editable Grid',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'istable',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8c63fe75ec','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',6,'city','City/Town',NULL,'Data',NULL,NULL,1,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('8c663fb572','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',4,'sender','From',NULL,'Data',NULL,'Email',0,0,0,0,0,0,0,1,0,0,0,0,NULL,NULL,'eval:doc.communication_medium===\"Email\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,255,0,0,0,0,0),('8cb2508230','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',4,'column_break_4',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8cd5190de2','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',10,'section_break_7',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8cda2baffc','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',7,'send_if_data','Send only if there is any data',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('8d12876f4a','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',19,'export','Export',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,NULL,0,0,0,0,0,0),('8d3645e1e0','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',1,'localization','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('8d3a1cd100','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',9,'total_pages','Total Pages',NULL,'Int',NULL,NULL,0,1,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8d9da6631d','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',5,'current_mapping_start','Current Mapping Start',NULL,'Int',NULL,NULL,0,1,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('8da61e179b','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',2,'route','Route',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,1,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8dd8290648','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',18,'expiry','Expiry',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('8dee0438f4','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',9,'check_communication','Check Communication',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1','Send Feedback Request only if there is at least one communication is available for the document.',0,0,0,'',0,0,0,0,0,0),('8df6420bb1','2013-01-10 16:34:24.000000','2018-05-17 15:33:48.135202','Administrator','Administrator',0,'Print Heading','fields','DocType',2,'description','Description','description','Small Text','Small Text',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'300px',NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('8e6d623ca1','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',27,'attach_print','Attach Print',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8e85bd9b7c','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',3,'send_from','Sender',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,1,0,0,0,0),('8eb2609ebc','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',9,'app_license','App License',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'MIT',NULL,0,0,0,'',0,0,0,0,0,0),('8ece24c5d6','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',26,'payments','Payments',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,'accept_payment',NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8f367a81a2','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',17,'limits','Limits',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!(doc.__islocal)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8f3787419e','2014-06-19 05:20:26.331041','2018-05-17 15:33:46.049180','Administrator','Administrator',0,'Standard Reply','fields','DocType',2,'response','Response',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('8f50b38124','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',10,'bold','Bold',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8f66e2e978','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',2,'enabled','Enabled','enabled','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,1,NULL,0,0,0,0,0,0),('8fbe526bcf','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',49,'sb3','Security Settings',NULL,'Section Break','Section Break',NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,'enabled',0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('8fe8db2fbc','2017-09-08 16:16:13.060641','2018-05-17 15:33:47.488876','Administrator','Administrator',0,'Webhook','fields','DocType',4,'webhook_docevent','Doc Event',NULL,'Select',NULL,'after_insert\non_update\non_submit\non_cancel\non_trash\non_update_after_submit\non_change',0,0,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('8ff83457b4','2016-12-29 07:42:26.246725','2018-05-17 15:33:44.975289','Administrator','Administrator',0,'Website Sidebar Item','fields','DocType',1,'title','Title',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('9066e071b3','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',25,'enable_outgoing','Enable Outgoing',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'','SMTP Settings for outgoing emails',0,0,0,'',0,0,0,0,0,0),('90c61a0ec1','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',47,'rating','Rating',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('90d6c3ce4a','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',41,'background','Desktop Background',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,'enabled',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('90e7786fb3','2017-08-11 05:15:51.482165','2018-05-17 15:33:48.742058','Administrator','Administrator',0,'Data Migration Plan','fields','DocType',3,'mappings','Mappings',NULL,'Table',NULL,'Data Migration Plan Mapping',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('90f4953d56','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',22,'unreplied_for_mins','Notify if unreplied for (in mins)',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'notify_if_unreplied',0,0,NULL,NULL,0,'30',NULL,0,0,0,'',0,0,0,0,0,0),('90fbdac753','2013-01-28 10:06:02.000000','2018-05-17 15:33:46.538180','Administrator','Administrator',0,'Currency','fields','DocType',3,'fraction','Fraction',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Sub-currency. For e.g. \"Cent\"',1,0,0,NULL,0,0,0,0,0,0),('9104219722','2016-08-24 14:10:17.471264','2018-05-17 15:33:47.989214','Administrator','Administrator',0,'OAuth Bearer Token','fields','DocType',6,'expiration_time','Expiration time',NULL,'Datetime',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('910510523b','2017-04-10 12:17:58.071915','2018-05-17 15:33:48.503675','Administrator','Administrator',0,'Salutation','fields','DocType',1,'salutation','Salutation',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('915019a859','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',16,'import_log','Import Log',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,1,NULL,NULL,'import_status',0,0,NULL,NULL,0,'',NULL,0,0,0,'',0,0,0,0,0,0),('917b8962b3','2016-04-14 09:41:45.892975','2018-05-17 15:33:46.225853','Administrator','Administrator',0,'Unhandled Email','fields','DocType',4,'message_id','Message-id',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('91febe3305','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',28,'print_format','Print Format',NULL,'Link',NULL,'Print Format',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'attach_print',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('91ff37a62b','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',12,'current_git_branch','Current Git Branch',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,'',0,0,0,0,0,0),('9211eca7d4','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',8,'description_section','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('928952c6df','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',8,'reference_name','Reference Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,2,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('92b6fc6bb6','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',55,'route','Route',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'has_web_view',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('92c4f57265','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',5,'insert_after','Insert After','insert_after','Select','Select',NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,'Select the label after which you want to insert new field.',0,0,0,NULL,0,0,0,0,0,0),('93dfd0ba33','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','fields','DocType',1,'title','Title',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('93e10c677a','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',38,'set_footer','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('93e25b2ee3','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',14,'sec_backup_limit','Backups',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('942ec17eb7','2013-05-24 13:41:00.000000','2018-05-17 15:33:46.746140','Administrator','Administrator',0,'Note','fields','DocType',4,'notify_on_every_login','Notify Users On Every Login',NULL,'Check',NULL,NULL,0,0,0,0,0,0,1,0,0,0,0,0,NULL,NULL,'notify_on_login',0,0,NULL,NULL,0,'0','If enabled, users will be notified every time they login. If not enabled, users will only be notified once.',0,0,0,'',0,0,0,0,0,0),('94450c324d','2017-02-26 16:20:52.654136','2018-05-17 15:33:46.022273','Administrator','Administrator',0,'Newsletter Email Group','fields','DocType',2,'total_subscribers','Total Subscribers',NULL,'Read Only',NULL,'email_group.total_subscribers',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('946463e0fc','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',15,'top_bar_items','Top Bar Items',NULL,'Table',NULL,'Top Bar Item',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('948dc3fe5c','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',8,'apply_text_styles','Apply Text Styles',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('94ffb04ee6','2016-03-30 01:39:20.586927','2018-05-17 15:33:42.598490','Administrator','Administrator',0,'Portal Menu Item','fields','DocType',6,'target','Target',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('952107243e','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',4,'column_break_4',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('95907dac6f','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',62,'fb_userid','Facebook User ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('95952539e4','2013-01-10 16:34:04.000000','2018-05-17 15:33:42.420279','Administrator','Administrator',0,'Property Setter','fields','DocType',2,'sb0',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('9597315244','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',11,'create','Create','create','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,'1',NULL,1,0,0,NULL,0,0,0,0,0,0),('95a676cf61','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',9,'in_global_search','In Global Search',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:([\"Data\", \"Select\", \"Table\", \"Text\", \"Text Editor\", \"Link\", \"Small Text\", \"Long Text\", \"Read Only\", \"Heading\", \"Dynamic Link\"].indexOf(doc.fieldtype) !== -1)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('95ffdf6bfd','2013-01-10 16:34:04.000000','2018-05-17 15:33:42.420279','Administrator','Administrator',0,'Property Setter','fields','DocType',8,'property','Property',NULL,'Data',NULL,NULL,1,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,1,0,NULL,0,0,0,0,0,0),('9614e1e5bd','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',12,'sb_advanced',' Advanced Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,'1',NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('963abfbd6d','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',45,'list_view_settings','List View Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('96c3c777eb','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',4,'address_line1','Address Line 1',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('96e0761847','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',15,'response_type','Response Type',NULL,'Select',NULL,'Code\nToken',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Code',NULL,1,1,0,NULL,0,0,0,0,0,0),('96ef6ba6f2','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',17,'introduction_text','Introduction',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('9726ca362f','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',11,'reference_type','Reference Type','reference_type','Link','Data','DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('9733148488','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',55,'column_break1',NULL,NULL,'Column Break','Column Break',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50%','50%',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('97693e7391','2017-02-13 17:33:25.157332','2018-05-17 15:33:43.349585','Administrator','Administrator',0,'User Permission for Page and Report','fields','DocType',2,'page','Page',NULL,'Link',NULL,'Page',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.set_role_for == \'Page\'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('977946d5fa','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',10,'max_value','Max Value',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.fieldtype==\'Int\'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('979e9e41a3','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',11,'days_in_advance','Days Before or After',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.event==\"Days After\" || doc.event==\"Days Before\"',0,0,NULL,NULL,0,'0','Send days before or after the reference date',0,0,0,NULL,0,0,0,0,0,0),('97d3ee1c68','2017-08-17 01:25:56.910716','2018-05-17 15:33:48.101538','Administrator','Administrator',0,'Print Style','fields','DocType',5,'preview','Preview',NULL,'Attach Image',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('97fc336af6','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',5,'report_type','Report Type',NULL,'Read Only',NULL,'report.report_type',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('9818f1bde3','2012-12-28 10:49:55.000000','2018-05-17 15:33:45.395726','Administrator','Administrator',0,'Workflow','fields','DocType',4,'override_status','Don\'t Override Status',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'If Checked workflow status will not override status in list view',0,0,0,'',0,0,0,0,0,0),('98404334eb','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',39,'email_footer_address','Email Footer Address',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Your organization name and address for the email footer.',0,0,0,'',0,0,0,0,0,0),('9877161279','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',10,'section_break_8',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('988b8af5a7','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',5,'add_total_row','Add Total Row',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('98d20b572a','2016-09-22 04:16:48.829658','2018-05-17 15:33:48.026461','Administrator','Administrator',0,'LDAP Settings','fields','DocType',4,'base_dn','Base Distinguished Name (DN)',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('98e4ae388b','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','fields','DocType',2,'published_on','Published On',NULL,'Date',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('98e564c44d','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',16,'column_break_16',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('98eee67a0b','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',25,'view_properties','View Properties (via Customize Form)',NULL,'Button',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('98ff54a4a3','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',26,'participants','Participants',NULL,'Section Break','Section Break',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('997823aee7','2013-03-07 11:55:11.000000','2018-05-17 15:33:44.447683','Administrator','Administrator',0,'About Us Team Member','fields','DocType',3,'bio','Bio',NULL,'Small Text',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'200px',NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('99adad602a','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',4,'email_id','Email Address','email_id','Data','Data','Email',1,0,0,0,0,0,1,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('99bfce787a','2016-09-21 10:12:57.399174','2018-05-17 15:33:47.640308','Administrator','Administrator',0,'Dropbox Settings','fields','DocType',4,'app_access_key','App Access Key',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('99e291aab2','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',19,'lft','lft',NULL,'Int',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('99e2b8a373','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','fields','DocType',4,'timestamp','Timestamp',NULL,'Datetime',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('9a05324b99','2017-08-17 14:51:33.577230','2018-05-17 15:34:43.329531','Administrator','Administrator',0,'Site Backup','fields','DocType',9,'file_path','File Path',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('9a26a30997','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',16,'assigned_by_full_name','Assigned By Full Name',NULL,'Read Only',NULL,'assigned_by.full_name',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('9a3d876228','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',26,'minimum_password_score','Minimum Password Score',NULL,'Select',NULL,'2\n4',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.enable_password_policy==1',0,0,NULL,NULL,0,'2',NULL,0,0,0,'',0,0,0,0,0,0),('9a728ba0b5','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',3,'fieldtype','Type','fieldtype','Select','Select','Attach\nAttach Image\nBarcode\nButton\nCheck\nCode\nColor\nColumn Break\nCurrency\nData\nDate\nDatetime\nDynamic Link\nFloat\nFold\nGeolocation\nHeading\nHTML\nImage\nInt\nLink\nLong Text\nPassword\nPercent\nRead Only\nSection Break\nSelect\nSmall Text\nTable\nText\nText Editor\nTime\nSignature',1,0,0,0,0,1,1,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Data',NULL,1,0,0,NULL,0,0,0,0,0,0),('9abe4738c1','2012-11-22 17:45:46.000000','2018-05-17 15:33:48.057067','Administrator','Administrator',0,'Letter Head','fields','DocType',4,'content','Content','content','Text Editor','Text Editor',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.__islocal',0,0,NULL,NULL,0,NULL,'Letter Head in HTML',1,0,0,NULL,0,0,0,0,0,0),('9ad1ff0db9','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',24,'allow_bulk_edit','Allow Bulk Edit',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval: doc.fieldtype == \"Table\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('9addca8c48','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',27,'restrict_to_domain','Restrict To Domain',NULL,'Link',NULL,'Domain',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('9b2394eb93','2014-06-19 05:20:26.331041','2018-05-17 15:33:46.049180','Administrator','Administrator',0,'Standard Reply','fields','DocType',1,'subject','Subject',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('9b334c12a8','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',11,'db_password','DB Password',NULL,'Password',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('9b4578d5b7','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',49,'read_only','User Cannot Search','read_only','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('9b4cafe26a','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',53,'allow_guest_to_view','Allow Guest to View',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'has_web_view',0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('9b7bac3ea6','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',14,'is_primary_contact','Is Primary Contact','is_primary_contact','Check','Select',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,'0',NULL,0,0,0,NULL,0,0,0,0,0,0),('9ba7632a4f','2016-05-25 09:49:07.125394','2018-05-17 15:33:43.025756','Administrator','Administrator',0,'Tag Category','fields','DocType',1,'category_name','Category Name',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('9bb2f3330a','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',8,'website_theme_image_link','Website Theme Image Link',NULL,'Code',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('9c9f15657c','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',8,'send_unsubscribe_link','Send Unsubscribe Link',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('9cc12986ac','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',3,'user','User',NULL,'Link',NULL,'User',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('9cf347ca71','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',1,'report','Report',NULL,'Link',NULL,'Report',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('9d219ca8da','2013-03-19 12:02:15.000000','2018-05-17 15:33:44.755304','Administrator','Administrator',0,'About Us Settings','fields','DocType',9,'footer','Footer',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'More content for the bottom of the page.',0,0,0,NULL,0,0,0,0,0,0),('9d327eda95','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',11,'image_view','Image View',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval: doc.image_field',0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('9d6e1e6a85','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',50,'simultaneous_sessions','Simultaneous Sessions',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('9d9de5d85d','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',17,'enable_scheduler','Enable Scheduled Jobs',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Run scheduled jobs only if checked',0,0,0,NULL,0,0,0,0,0,0),('9dce4ad22a','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',13,'email_server','Email Server',NULL,'Data',NULL,'domain.email_server',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.domain && doc.enable_incoming',0,0,NULL,NULL,0,NULL,'e.g. pop.gmail.com / imap.gmail.com',0,0,0,'',0,0,0,0,0,0),('9dd9af6454','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',11,'auto_update','Auto Update',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('9e178da6cf','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','fields','DocType',10,'section_break0',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('9e2eee641b','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',17,'log_details','Log Details',NULL,'Code',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,1,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('9ef5860df5','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',14,'repeat_on','Repeat On',NULL,'Select',NULL,'\nEvery Day\nEvery Week\nEvery Month\nEvery Year',0,0,0,0,0,0,0,1,0,0,0,0,NULL,NULL,'repeat_this_event',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('9f5ffb6993','2016-12-29 07:48:06.319665','2018-05-17 15:33:45.075668','Administrator','Administrator',0,'Website Sidebar','fields','DocType',1,'title','Title',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('9f7490928e','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',5,'column_break_2',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a020dd1c26','2013-03-19 12:02:15.000000','2018-05-17 15:33:44.755304','Administrator','Administrator',0,'About Us Settings','fields','DocType',4,'company_history_heading','Org History Heading',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'\"Company History\"',1,0,0,NULL,0,0,0,0,0,0),('a04cd676d8','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',11,'frappe_client_id','Frappe Client ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a089663d2e','2013-01-10 16:34:04.000000','2018-05-17 15:33:42.420279','Administrator','Administrator',0,'Property Setter','fields','DocType',4,'value','Set Value',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'New value to be set',1,0,0,NULL,0,0,0,0,0,0),('a0b75d1c8e','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',39,'seen','Seen',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('a0c00c8f6a','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',6,'recipients','To',NULL,'Code',NULL,'Email',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('a0e70527e7','2017-04-21 16:57:30.264478','2018-05-17 15:33:47.429402','Administrator','Administrator',0,'GSuite Settings','fields','DocType',4,'client_secret','Client Secret',NULL,'Password',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a123eafd6f','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',42,'background_image','Background Image',NULL,'Attach',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a12eb2876a','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',10,'city','City',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('a13c4f024d','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',19,'font_size','Font Size',NULL,'Float',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'In points. Default is 9.',0,0,0,NULL,0,0,0,0,0,0),('a196d0d0cb','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',13,'print_format','Print Format',NULL,'Link',NULL,'Print Format',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'allow_print',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a21d99ceaa','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',33,'in_global_search','In Global Search',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:([\"Data\", \"Select\", \"Table\", \"Text\", \"Text Editor\", \"Link\", \"Small Text\", \"Long Text\", \"Read Only\", \"Heading\", \"Dynamic Link\"].indexOf(doc.fieldtype) !== -1)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a2411f4b25','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',20,'full_name','Full Name',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('a2475b064f','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',8,'full_name','Full Name',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a26ec8103f','2013-03-08 09:41:11.000000','2018-05-17 15:33:45.136706','Administrator','Administrator',0,'Blog Category','fields','DocType',1,'category_name','Category Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('a28c3135bc','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',14,'default','Default',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('a2a44fd2a0','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',14,'allow_comments','Allow Comments',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'login_required',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('a2c4f7321e','2017-09-04 20:57:20.129205','2018-05-17 15:33:47.174204','Administrator','Administrator',0,'S3 Backup Settings','fields','DocType',1,'enabled','Enable Automatic Backup',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a333615bc7','2015-03-18 06:08:32.729800','2018-05-17 15:33:46.258530','Administrator','Administrator',0,'Email Group','fields','DocType',1,'title','Title',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a34042047f','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',37,'print_width','Print Width',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('a36a15f51a','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',4,'introduction','Introduction',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Introductory information for the Contact Us Page',0,0,0,NULL,0,0,0,0,0,0),('a36ff56892','2016-08-04 04:58:40.457416','2018-05-17 15:33:47.300993','Administrator','Administrator',0,'Integration Request','fields','DocType',8,'reference_docname','Reference Docname',NULL,'Dynamic Link',NULL,'reference_doctype',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('a3a951576f','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',6,'allow_copy','Hide Copy',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('a3f2cc6539','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',4,'if_owner','If user is the owner',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Apply this rule if the User is the Owner',0,0,0,'',0,0,0,0,0,0),('a40d4176f5','2016-03-30 01:40:20.001775','2018-05-17 15:33:44.625767','Administrator','Administrator',0,'Portal Settings','fields','DocType',3,'hide_standard_menu','Hide Standard Menu',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a4732ef9ba','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',9,'skip_errors','Skip rows with errors',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'If this is checked, rows with valid data will be imported and invalid rows will be dumped into a new file for you to import later.\n',0,0,0,'',0,0,0,0,0,0),('a491f4c1f7','2013-02-22 01:27:36.000000','2018-05-17 15:33:45.339696','Administrator','Administrator',0,'Workflow Transition','fields','DocType',2,'action','Action',NULL,'Link',NULL,'Workflow Action',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'200px','200px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('a5298aef95','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',19,'sent_or_received','Sent or Received',NULL,'Select',NULL,'Sent\nReceived',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,'eval:doc.communication_type===\"Communication\"',0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('a56d06582a','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',11,'send_after','Send After',NULL,'Datetime',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('a5af63f4ac','2014-06-19 05:20:26.331041','2018-05-17 15:33:46.049180','Administrator','Administrator',0,'Standard Reply','fields','DocType',3,'owner','Owner',NULL,'Link',NULL,'User',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'user',NULL,0,0,0,NULL,0,0,0,0,0,0),('a5b57d1f4c','2017-08-11 05:03:27.091416','2018-05-17 15:33:48.613509','Administrator','Administrator',0,'Data Migration Connector','fields','DocType',7,'username','Username',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a5d4da61d4','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',19,'read_only','Read Only',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a5f4cad85d','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',2,'section_break_10','To and CC',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,'',NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a629dce993','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',34,'no_copy','No Copy','no_copy','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('a6412f7691','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',6,'permlevel','Level','permlevel','Int','Int',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'40px','40px',0,'0',NULL,1,0,0,NULL,0,0,0,0,0,0),('a69e69f9b6','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',3,'cb4',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('a6af57ca5f','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',28,'reference_owner','Reference Owner',NULL,'Read Only',NULL,'reference_name.owner',1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a6bc15f6fc','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',35,'email_settings','Email Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,'enabled',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a709595319','2013-05-24 13:41:00.000000','2018-05-17 15:33:46.746140','Administrator','Administrator',0,'Note','fields','DocType',7,'seen_by_section','Seen By',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a762fc9ae9','2013-01-10 16:34:24.000000','2018-05-17 15:33:43.053495','Administrator','Administrator',0,'SMS Settings','fields','DocType',5,'parameters','Static Parameters',NULL,'Table',NULL,'SMS Parameter',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Enter static url parameters here (Eg. sender=ERPNext, username=ERPNext, password=1234 etc.)',0,0,0,NULL,0,0,0,0,0,0),('a789055bff','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',13,'section_break_9',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a7ce4fea51','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',39,'columns','Columns',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,'Number of columns for a field in a List View or a Grid (Total Columns should be less than 11)',0,0,0,'',0,0,0,0,0,0),('a7f67cf4e4','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',64,'column_break_49',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a8276394ba','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',18,'font','Font',NULL,'Select',NULL,'Default\nArial\nHelvetica\nVerdana\nMonospace',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Default',NULL,0,0,0,'',0,0,0,0,0,0),('a84a9e2b38','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',10,'mailbox_settings','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a86a15f90b','2017-01-13 04:55:18.835023','2018-05-17 15:33:42.653992','Administrator','Administrator',0,'Dynamic Link','fields','DocType',2,'link_name','Link Name',NULL,'Dynamic Link',NULL,'link_doctype',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('a872736887','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',8,'section_break_6','Database Details',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!(doc.__islocal)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a88338cb60','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',52,'has_web_view','Has Web View',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('a8938078df','2016-03-30 10:04:25.828742','2018-05-17 15:33:44.194278','Administrator','Administrator',0,'User Email','fields','DocType',3,'column_break_3',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a8bb7110f3','2013-01-10 16:34:04.000000','2018-05-17 15:33:42.420279','Administrator','Administrator',0,'Property Setter','fields','DocType',1,'help','Help',NULL,'HTML',NULL,'<div class=\"alert\">Please don\'t update it as it can mess up your form. Use the Customize Form View and Custom Fields to set properties!</div>',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('a92007626e','2014-06-05 02:22:36.029850','2018-05-17 15:33:48.310068','Administrator','Administrator',0,'Address Template','fields','DocType',2,'is_default','Is Default',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'This format is used if country specific format is not found',1,0,0,NULL,0,0,0,0,0,0),('a95bc5d27d','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',13,'add_unsubscribe_link','Add Unsubscribe Link',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('a9e60f24b9','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',6,'reference','Reference',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('a9f405104b','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','fields','DocType',5,'relapses','Relapses',NULL,'Int',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,1,0,1,'',0,0,0,0,0,0),('aa1b753e7a','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',14,'email_settings','Email Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('aa2b24f357','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',1,'reference_doctype','Document Type',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,'',0,1,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('aa333fa89d','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',9,'html','HTML','html','Code','Text Editor','HTML',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'custom_format',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('aa4ab62dd4','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',14,'column_break_14',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('aac0fe510f','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',4,'email_sent','Email Sent?',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('aac6733611','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',3,'repeat_header_footer','Repeat Header and Footer in PDF',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('aadc3e4258','2013-01-10 16:34:04.000000','2018-05-17 15:33:42.420279','Administrator','Administrator',0,'Property Setter','fields','DocType',6,'doc_type','DocType',NULL,'Link',NULL,'DocType',1,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,1,0,NULL,0,0,0,0,0,0),('aaf6f0027d','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','fields','DocType',10,'route','Route',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ab5d5936b3','2013-03-19 12:02:15.000000','2018-05-17 15:33:44.755304','Administrator','Administrator',0,'About Us Settings','fields','DocType',1,'help','Help',NULL,'HTML',NULL,'<div class=\"alert\">Link for About Us Page is \"/about\"</div>',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('ab6d9f39b0','2017-10-16 17:13:05.684227','2018-05-17 15:33:47.076864','Administrator','Administrator',0,'Google Maps','fields','DocType',2,'client_key','Client Key',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ab964f8cf1','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',13,'contact_details','Reference',NULL,'Section Break',NULL,'fa fa-pushpin',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('abd79b0145','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',29,'change_password','Change Password',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,'eval:doc.enabled && (!doc.__islocal || !cint(doc.send_welcome_email))',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('ac03d1ca39','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',26,'column_break_25','Print Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,'attach_print',NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ac171c547f','2013-02-22 01:27:36.000000','2018-05-17 15:33:45.339696','Administrator','Administrator',0,'Workflow Transition','fields','DocType',1,'state','State',NULL,'Link',NULL,'Workflow State',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'200px','200px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('ac185c14d4','2015-03-18 09:41:20.216319','2018-05-17 15:33:46.333335','Administrator','Administrator',0,'Email Unsubscribe','fields','DocType',2,'reference_doctype','Reference DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,'',0,0,0,0,0,0),('ac27662a31','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',14,'column_break_14',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ac2a7ec687','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',14,'collapsible_depends_on','Collapsible Depends On',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.fieldtype==\"Section Break\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('aca920eb20','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',10,'feedback_trigger','Feedback Trigger',NULL,'Data',NULL,'Feedback Trigger',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ad1e80d43f','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',41,'email_inbox','Email Inbox',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,1,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ad29a26c77','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',58,'last_active','Last Active',NULL,'Datetime',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('ad2f5a55a1','2012-11-22 17:45:46.000000','2018-05-17 15:33:48.057067','Administrator','Administrator',0,'Letter Head','fields','DocType',5,'footer','Footer',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.__islocal',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('adb840e208','2017-04-21 16:57:30.264478','2018-05-17 15:33:47.429402','Administrator','Administrator',0,'GSuite Settings','fields','DocType',7,'script_url','Script URL',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,'https://script.google.com/macros/s/AKfycbxIFOx3301xwtF2IFPJ4pUQGqkNF3hBiBebppWkeKn6fKZRQvk/exec','If you aren\'t using own publish Google Apps Script webapp you can use the default https://script.google.com/macros/s/AKfycbxIFOx3301xwtF2IFPJ4pUQGqkNF3hBiBebppWkeKn6fKZRQvk/exec ',0,0,0,'',0,0,0,0,0,0),('adc1b3b0db','2012-11-22 17:45:46.000000','2018-05-17 15:33:48.057067','Administrator','Administrator',0,'Letter Head','fields','DocType',2,'disabled','Disabled','disabled','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'letter_head_name',0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('adcbc79801','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',41,'socketio_port','Socketio Port',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('addb2dcf04','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',3,'fieldtype','Type','fieldtype','Select','Select','Attach\nAttach Image\nBarcode\nButton\nCheck\nCode\nColor\nColumn Break\nCurrency\nData\nDate\nDatetime\nDynamic Link\nFloat\nFold\nGeolocation\nHeading\nHTML\nImage\nInt\nLink\nLong Text\nPassword\nPercent\nRead Only\nSection Break\nSelect\nSignature\nSmall Text\nTable\nText\nText Editor\nTime',1,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Data',NULL,1,0,0,NULL,0,0,0,0,0,0),('ae03a64760','2016-03-30 10:04:25.828742','2018-05-17 15:33:44.194278','Administrator','Administrator',0,'User Email','fields','DocType',5,'enable_outgoing','Enable Outgoing',NULL,'Check',NULL,'email_account.enable_outgoing',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('ae2c0dc124','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',13,'is_folder','Is Folder',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('ae2d5cb92b','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',1,'label_and_type','Label and Type',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ae32d6ac83','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',23,'column_break_14',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ae4a9e31a8','2017-08-22 17:15:31.459897','2018-05-17 15:34:43.427815','Administrator','Administrator',0,'Bench Manager Command','fields','DocType',5,'section_break_2',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ae5f23fe3c','2015-02-04 04:33:36.330477','2018-05-17 15:33:42.835173','Administrator','Administrator',0,'DocShare','fields','DocType',1,'user','User',NULL,'Link',NULL,'User',1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('ae5feea5b5','2016-08-24 14:12:13.647159','2018-05-17 15:33:47.360639','Administrator','Administrator',0,'OAuth Authorization Code','fields','DocType',3,'scopes','Scopes',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('af05aa4a40','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',6,'column_break_6',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('af134346aa','2013-01-10 16:34:24.000000','2018-05-17 15:33:43.053495','Administrator','Administrator',0,'SMS Settings','fields','DocType',2,'message_parameter','Message Parameter',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Enter url parameter for message',1,0,0,NULL,0,0,0,0,0,0),('af50d0fa4d','2017-10-16 17:13:05.684227','2018-05-17 15:33:47.076864','Administrator','Administrator',0,'Google Maps','fields','DocType',3,'home_address','Home Address',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('af5a8085fd','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',41,'description','Description','description','Small Text','Text',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'300px','300px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('af64c48f12','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',18,'roles_html','Roles HTML',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('af800839ea','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',12,'description','Description',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('afa21eee3a','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',18,'admin_password','Admin Password',NULL,'Password',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('afc6dea6d4','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',15,'scheduled_to_send','Scheduled To Send',NULL,'Int',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('afea61b693','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',5,'status','Status',NULL,'Select',NULL,'\nNot Sent\nSending\nSent\nError\nExpired',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Not Sent',NULL,1,1,0,NULL,0,0,0,0,0,0),('aff24ac448','2014-02-20 17:22:37.000000','2018-05-17 15:33:42.687506','Administrator','Administrator',0,'Version','fields','DocType',5,'section_break_4',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b011d70fdf','2017-08-31 04:16:38.764465','2018-05-17 15:33:43.506765','Administrator','Administrator',0,'Role Profile','fields','DocType',1,'role_profile','Role Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,1,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('b019ae2fd2','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',10,'delivery_status','Delivery Status',NULL,'Select',NULL,'\nSent\nBounced\nOpened\nMarked As Spam\nRejected\nDelayed\nSoft-Bounced\nClicked\nRecipient Unsubscribed\nError\nExpired\nSending',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Integrations can use this field to set email delivery status',0,0,0,'',0,0,0,0,0,0),('b02525547e','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',5,'general_settings','General Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b0cee02b72','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',7,'developer_mode','Developer Mode',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b0d60da6f6','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',20,'hidden','Hidden','hidden','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b12b31c9b0','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',12,'rating','Rating',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,2,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('b13f4e28d9','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',24,'message_examples','Message Examples',NULL,'HTML',NULL,'<h5>Message Example</h5>\n\n<pre>&lt;h3&gt;Order Overdue&lt;/h3&gt;\n\n&lt;p&gt;Transaction {{ doc.name }} has exceeded Due Date. Please take necessary action.&lt;/p&gt;\n\n&lt;!-- show last comment --&gt;\n{% if comments %}\nLast comment: {{ comments[-1].comment }} by {{ comments[-1].by }}\n{% endif %}\n\n&lt;h4&gt;Details&lt;/h4&gt;\n\n&lt;ul&gt;\n&lt;li&gt;Customer: {{ doc.customer }}\n&lt;li&gt;Amount: {{ doc.total_amount }}\n&lt;/ul&gt;\n</pre>',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b17eed7736','2017-04-21 16:57:30.264478','2018-05-17 15:33:47.429402','Administrator','Administrator',0,'GSuite Settings','fields','DocType',3,'client_id','Client ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b1a531a4fc','2013-05-24 13:41:00.000000','2018-05-17 15:33:46.746140','Administrator','Administrator',0,'Note','fields','DocType',3,'notify_on_login','Notify users with a popup when they log in',NULL,'Check',NULL,NULL,0,0,0,0,0,0,1,0,0,0,0,0,NULL,NULL,'public',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b1a6e6832f','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',51,'web_view','Web View',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b1e1e2beeb','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',31,'send_password_update_notification','Send Password Update Notification',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.__islocal',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b23f0f6ecc','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',13,'status_section','Status',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b2735b90cc','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','fields','DocType',6,'column_break0',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b27f49f87e','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',19,'copyright','Copyright',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b2991fddd3','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',6,'section_break_4',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!(doc.__islocal)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b31c0c70f8','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',35,'print_width','Print Width',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,'Print Width of the field, if the field is a column in a table',0,0,0,NULL,0,0,0,0,0,0),('b31f374c9e','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',16,'autoname','Auto Name','autoname','Data','Data',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Naming Options:\n<ol><li><b>field:[fieldname]</b> - By Field</li><li><b>naming_series:</b> - By Naming Series (field called naming_series must be present</li><li><b>Prompt</b> - Prompt user for a name</li><li><b>[series]</b> - Series by prefix (separated by a dot); for example PRE.#####</li></ol>',0,0,0,NULL,0,0,0,0,0,0),('b38c8bfdf1','2017-04-24 09:53:41.813982','2018-05-17 15:33:47.580015','Administrator','Administrator',0,'GSuite Templates','fields','DocType',1,'template_name','Template Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b3a2ad3433','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',10,'beta','Beta',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b3dc7478d3','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',13,'column_break_8',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b3f19882da','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',16,'column_break_10',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b40213161a','2017-10-23 13:02:10.295824','2018-05-17 15:33:46.879565','Administrator','faris@erpnext.com',0,'Calendar View','fields','DocType',3,'start_date_field','Start Date Field',NULL,'Select',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b4775a04c9','2017-04-21 16:57:30.264478','2018-05-17 15:33:47.429402','Administrator','Administrator',0,'GSuite Settings','fields','DocType',10,'refresh_token','refresh_token',NULL,'Password',NULL,NULL,0,1,0,1,1,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b499183f85','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',4,'column_break_3',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b49d6f5937','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',23,'set_only_once','Set Only Once',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Do not allow user to change after set the first time',0,0,0,NULL,0,0,0,0,0,0),('b4acff0b0c','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',7,'overwrite','Update records',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,'0','If you are updating/overwriting already created records.',0,0,0,'',0,0,0,0,0,0),('b4d36e7e75','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',16,'html_7',NULL,NULL,'HTML',NULL,'<p><strong>Condition Examples:</strong></p>\n<pre>doc.status==\"Open\"\ndoc.due_date==nowdate()\ndoc.total &gt; 40000\n</pre>\n<p><strong>Hints:</strong></p>\n<ol>\n<li>To check for an event every day, select \"Date Change\" in Event</li>\n<li>To send an alert if a particular value changes, select \"Value Change\"</li>\n</ol>',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b4d8f64acd','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',32,'in_standard_filter','In Standard Filter',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b4effaa6aa','2015-03-18 09:41:20.216319','2018-05-17 15:33:46.333335','Administrator','Administrator',0,'Email Unsubscribe','fields','DocType',3,'reference_name','Reference Name',NULL,'Dynamic Link',NULL,'reference_doctype',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('b4f9b1a693','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',22,'success_url','Success URL',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Go to this URL after completing the form (only for Guest users)',0,0,0,NULL,0,0,0,0,0,0),('b54be596c7','2013-01-10 16:34:01.000000','2018-05-17 15:33:46.459140','Administrator','Administrator',0,'Custom Script','fields','DocType',2,'script_type','Script Type','script_type','Select','Select','Client',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Client',NULL,1,1,1,NULL,0,0,0,0,0,0),('b57d0ec9f7','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',17,'default','Default','default','Small Text','Text',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b5826d0dff','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',5,'cb1',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50%',NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b59c243520','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',14,'role','Role','role','Link','Link','Role',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b5b289f8f0','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','fields','DocType',3,'page_name','Page Name','page_name','Data','Data',NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('b5bb5cefcb','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',11,'number_format','Number Format',NULL,'Select',NULL,'#,###.##\n#.###,##\n# ###.##\n# ###,##\n#\'###.##\n#, ###.##\n#,##,###.##\n#,###.###\n#.###\n#,###',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b5c7951687','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',10,'submit_after_import','Submit after importing',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('b5c927215c','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',13,'top_bar','Top Bar',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('b5f577b772','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',3,'standard','Standard',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('b61a6adf5a','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',32,'column_break_27',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b68db5abc6','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',17,'role_profile_name','Role Profile',NULL,'Link',NULL,'Role Profile',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b6c8fbbf87','2017-09-08 16:16:13.060641','2018-05-17 15:33:47.488876','Administrator','Administrator',0,'Webhook','fields','DocType',1,'sb_doc_events','Doc Events',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b710d64309','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',21,'emails','Emails',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('b71926620b','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',39,'email_inbox','Email Inbox',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b74eeb2c98','2016-09-21 10:12:57.399174','2018-05-17 15:33:47.640308','Administrator','Administrator',0,'Dropbox Settings','fields','DocType',7,'dropbox_access_key','Dropbox Access Key',NULL,'Password',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('b75353a73e','2017-04-21 16:57:30.264478','2018-05-17 15:33:47.429402','Administrator','Administrator',0,'GSuite Settings','fields','DocType',5,'allow_gsuite_access','Allow GSuite access',NULL,'Button',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:(doc.client_secret && doc.client_id)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b753f98efd','2013-05-24 13:41:00.000000','2018-05-17 15:33:46.746140','Administrator','Administrator',0,'Note','fields','DocType',1,'title','Title',NULL,'Data',NULL,NULL,0,0,0,1,0,1,0,1,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b76811cba9','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',15,'attached_to_doctype','Attached To DocType',NULL,'Link',NULL,'DocType',1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,1,1,NULL,0,0,0,0,0,0),('b7745b802b','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',5,'newsletter_content','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b792466eef','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',36,'timeline_name','Timeline Name',NULL,'Dynamic Link',NULL,'timeline_doctype',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('b7924f8098','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',21,'set_user_permissions','Set User Permissions',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'This role update User Permissions for a user',0,0,0,NULL,0,0,0,0,0,0),('b7a3275ac0','2017-02-13 17:33:25.157332','2018-05-17 15:33:43.349585','Administrator','Administrator',0,'User Permission for Page and Report','fields','DocType',1,'set_role_for','Set Role For',NULL,'Select',NULL,'\nPage\nReport',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('b7c3f1852b','2016-12-29 12:59:48.638970','2018-05-17 15:33:43.427855','Administrator','Administrator',0,'Deleted Document','fields','DocType',5,'new_name','New Name',NULL,'Read Only',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b7cc3129ab','2017-08-22 17:15:31.459897','2018-05-17 15:34:43.427815','Administrator','Administrator',0,'Bench Manager Command','fields','DocType',8,'console','Console',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('b84b516f76','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',18,'reverse','Reverse Icon Color',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b876099a35','2014-10-30 14:23:30.958074','2018-05-17 15:33:45.170674','Administrator','Administrator',0,'Help Category','fields','DocType',1,'category_name','Category Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('b886d7d58d','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',20,'unsubscribed','Unsubscribed',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b8918e3435','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',38,'email','EMail',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b895a0e186','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',10,'condition','Condition',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Optional: The alert will be sent if this expression is true',0,0,0,'',0,0,0,0,0,0),('b89fd9e6ea','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',9,'description','Description','description','Text Editor','Text',NULL,0,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,'300px','300px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('b8b2622cd3','2013-02-22 01:27:36.000000','2018-05-17 15:33:45.339696','Administrator','Administrator',0,'Workflow Transition','fields','DocType',4,'allowed','Allowed',NULL,'Link',NULL,'Role',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'200px','200px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('b9229ec21e','2016-02-17 12:21:16.175465','2018-05-17 15:33:43.395191','Administrator','Administrator',0,'Translation','fields','DocType',5,'target_name','Translated Text',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('b9261bb6ef','2015-02-04 04:33:36.330477','2018-05-17 15:33:42.835173','Administrator','Administrator',0,'DocShare','fields','DocType',8,'notify_by_email','Notify by email',NULL,'Check',NULL,NULL,0,0,0,1,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('b93c327b81','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',8,'salutation','Salutation',NULL,'Link',NULL,'Salutation',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b95b67f325','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',20,'log','Log',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,'eval:doc.failed_log !== \'[]\'',0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('b99280e6bd','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',7,'message','Message',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('b9b3e0f654','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',11,'section_break_8',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b9c9938ad4','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',24,'outgoing_mail_settings','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('b9e0f4ea29','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',14,'bench_settings','Bench Settings',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('ba7789e551','2015-12-15 22:26:45.221162','2018-05-17 15:33:44.000821','Administrator','Administrator',0,'Payment Gateway','fields','DocType',1,'gateway','Gateway',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ba9f8a27d9','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','fields','DocType',5,'author','Author',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'user_fullname',NULL,0,0,0,'',0,0,0,0,0,0),('bae9c4e6f1','2013-01-10 16:34:04.000000','2018-05-17 15:33:42.420279','Administrator','Administrator',0,'Property Setter','fields','DocType',7,'field_name','Field Name',NULL,'Data',NULL,NULL,1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.doctype_or_field==\'DocField\'',0,0,NULL,NULL,0,NULL,'ID (name) of the entity whose property is to be set',0,1,0,NULL,0,0,0,0,0,0),('bb0ca56d69','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',43,'uid','UID',NULL,'Int',NULL,NULL,0,1,0,0,0,0,0,0,1,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('bb230ee4a8','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',6,'is_standard','Is Standard',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('bb32bfa3a5','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',16,'email_id','Email Id',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('bb336e09a5','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',36,'enable_auto_reply','Enable Auto Reply',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('bb4d23311a','2016-09-22 04:16:48.829658','2018-05-17 15:33:48.026461','Administrator','Administrator',0,'LDAP Settings','fields','DocType',2,'ldap_server_url','LDAP Server Url',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('bb6c726aab','2016-03-30 01:40:20.001775','2018-05-17 15:33:44.625767','Administrator','Administrator',0,'Portal Settings','fields','DocType',2,'standard_menu_items','Standard Sidebar Menu',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('bb7f9275f5','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',43,'oldfieldtype',NULL,'oldfieldtype','Data','Data',NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('bbbbf28e4b','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',9,'db_name','DB Name',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,1,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('bbdfa2bd78','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',11,'section_break_6',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('bbea755b2e','2016-12-29 07:42:26.246725','2018-05-17 15:33:44.975289','Administrator','Administrator',0,'Website Sidebar Item','fields','DocType',3,'group','Group',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('bbed5f3870','2016-08-24 14:12:13.647159','2018-05-17 15:33:47.360639','Administrator','Administrator',0,'OAuth Authorization Code','fields','DocType',5,'expiration_time','Expiration time',NULL,'Datetime',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('bbee757b8f','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',11,'add_draft_heading','Always add \"Draft\" Heading for printing draft documents',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('bbf8eee292','2016-12-29 07:42:26.246725','2018-05-17 15:33:44.975289','Administrator','Administrator',0,'Website Sidebar Item','fields','DocType',2,'route','Route',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('bc51625a45','2015-02-04 04:33:36.330477','2018-05-17 15:33:42.835173','Administrator','Administrator',0,'DocShare','fields','DocType',3,'share_name','Document Name',NULL,'Dynamic Link',NULL,'share_doctype',1,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('bc77aa9b24','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',33,'enable_two_factor_auth','Enable Two Factor Auth',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('bc9b98ab5a','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',26,'files_size','Files Size',NULL,'Float',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('bd215964a2','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',10,'options_help','Options Help',NULL,'HTML','HTML',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('bd38ba3cbd','2016-09-22 04:16:48.829658','2018-05-17 15:33:48.026461','Administrator','Administrator',0,'LDAP Settings','fields','DocType',9,'ldap_email_field','LDAP Email Field',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('be15367079','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',5,'section_break_6',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('be5095e2f1','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',2,'frappe_user','Frappe User',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('be5a596248','2016-02-17 12:21:16.175465','2018-05-17 15:33:43.395191','Administrator','Administrator',0,'Translation','fields','DocType',4,'column_break_6',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('be60b3b046','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',21,'read_only','Read Only',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('bedfadfcf5','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',10,'text_webfont','Google Font (Text)',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_text_styles',0,0,NULL,NULL,0,NULL,'Add the name of a \"Google Web Font\" e.g. \"Open Sans\"',0,0,0,'',0,0,0,0,0,0),('bee5cd06ca','2016-12-29 12:59:48.638970','2018-05-17 15:33:43.427855','Administrator','Administrator',0,'Deleted Document','fields','DocType',2,'deleted_doctype','Deleted DocType',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('bef476a422','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',26,'misc_section','More Information',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('bf053251f3','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',23,'background_image','Background Image',NULL,'Attach Image',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'If image is selected, color will be ignored.',0,0,0,'',0,0,0,0,0,0),('bf3330581d','2016-09-22 04:16:48.829658','2018-05-17 15:33:48.026461','Administrator','Administrator',0,'LDAP Settings','fields','DocType',8,'ldap_first_name_field','LDAP First Name Field',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('bf46853a75','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',14,'condition','Condition',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,'Optional: The alert will be sent if this expression is true',1,0,0,NULL,0,1,0,0,0,0),('bf62718c24','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',18,'footer_color','Footer Color',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_text_styles',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('bf6b195690','2013-03-25 16:00:51.000000','2018-05-17 15:33:44.781044','Administrator','Administrator',0,'Blogger','fields','DocType',3,'full_name','Full Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('bf91291cb8','2013-05-24 13:41:00.000000','2018-05-17 15:33:46.746140','Administrator','Administrator',0,'Note','fields','DocType',5,'expire_notification_on','Expire Notification On',NULL,'Date',NULL,NULL,1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.notify_on_login && doc.public',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('bfb0d18d7a','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',23,'idx','Priority',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'0 is highest',0,0,0,NULL,0,0,0,0,0,0),('bfbebf699f','2016-12-08 12:01:07.993900','2018-05-17 15:33:46.198242','Administrator','Administrator',0,'Email Queue Recipient','fields','DocType',1,'recipient','Recipient',NULL,'Data',NULL,'Email',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('bfc9e05dcf','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',44,'modules_access','Modules Access',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,1,0,NULL,NULL,0,'','',0,0,0,'',0,0,0,0,0,0),('c023f3218c','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',21,'format_data','Format Data',NULL,'Code',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c02bf5c1fa','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',5,'time_zone','Time Zone',NULL,'Select',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('c02f51dee5','2017-08-17 14:51:33.577230','2018-05-17 15:34:43.329531','Administrator','Administrator',0,'Site Backup','fields','DocType',4,'stored_location','Stored Location',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('c03c99ab26','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',3,'version','Version',NULL,'Data',NULL,NULL,0,0,0,1,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.version != undefined',0,0,NULL,NULL,0,NULL,NULL,1,0,1,'',0,0,0,0,0,0),('c0c8edbcab','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',25,'column_break_17',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('c0cc722ff3','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',15,'column_break_10',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c13acc131e','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',26,'reference_doctype','Reference DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c19265a6f8','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',9,'method','Trigger Method',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.event==\'Method\'',0,0,NULL,NULL,0,NULL,'Trigger on valid methods like \"before_insert\", \"after_update\", etc (will depend on the DocType selected)',0,0,0,'',0,0,0,0,0,0),('c1ca0cc11f','2014-08-22 16:12:17.249590','2018-05-17 15:33:44.077470','Administrator','Administrator',0,'Language','fields','DocType',1,'language_code','Language Code',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('c26bcf31d1','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',3,'column_break_2',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c2a274b329','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',14,'section_break_8',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.is_folder',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c2dec89007','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',28,'icon','Icon',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('c313628eab','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',3,'apply_user_permissions','Apply User Permissions',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Filter records based on User Permissions defined for a user',0,0,0,'',0,0,0,0,0,0),('c37f5999af','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',20,'address','Address',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Address and other legal information you may want to put in the footer.',0,0,0,NULL,0,0,0,0,0,0),('c39f831064','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','fields','DocType',2,'page_html','Page HTML',NULL,'Section Break','Section Break',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('c3e2b00a61','2017-04-21 16:57:30.264478','2018-05-17 15:33:47.429402','Administrator','Administrator',0,'GSuite Settings','fields','DocType',1,'enable','Enable',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c3f1a03ef2','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',17,'css','Custom CSS',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c3f200e0fd','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',22,'phone','Phone',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c40abdef80','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',42,'no_remaining','No of emails remaining to be synced',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c4109278bc','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',18,'column_break_10',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c412414301','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',12,'frappe_client_secret','Frappe Client Secret',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c4245d6b6d','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',8,'file_size','File Size',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,NULL,0,0,0,0,0,0),('c42729abd3','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',2,'address_title','Address Title',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('c42a9ff936','2017-10-23 13:02:10.295824','2018-05-17 15:33:46.879565','Administrator','faris@erpnext.com',0,'Calendar View','fields','DocType',1,'reference_doctype','Reference DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('c44f70b948','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',7,'use_ssl','Use SSL',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c4521aa68b','2017-04-10 12:11:36.526508','2018-05-17 15:33:48.535494','Administrator','Administrator',0,'Gender','fields','DocType',1,'gender','Gender',NULL,'Data',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c463b49d16','2016-04-12 18:40:16.315024','2018-05-17 15:33:44.562489','Administrator','Administrator',0,'Footer Item','fields','DocType',4,'target','Target',NULL,'Select',NULL,'\ntarget = \"_blank\"',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Select target = \"_blank\" to open in a new page.',0,0,0,'',0,0,0,0,0,0),('c4641cce53','2013-01-08 15:50:01.000000','2018-05-17 15:33:42.301437','Administrator','Administrator',0,'Role','fields','DocType',4,'two_factor_auth','Two Factor Authentication',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('c47ba292b5','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',10,'phone','Phone','contact_no','Data','Data',NULL,0,0,0,0,0,0,1,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('c4e02c30f7','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',13,'section_break_13',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'repeat_this_event',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('c510657c1a','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',24,'timeline_field','Timeline Field',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.istable',0,0,NULL,NULL,0,NULL,'Comments and Communications will be associated with this linked document',0,0,0,'',0,0,0,0,0,0),('c5316bc548','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',30,'mail_settings','Mail Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c5643740da','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',21,'friday','Friday',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.repeat_this_event && doc.repeat_on===\"Every Day\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('c57c9299a6','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',31,'section_break_38','HTML Header & Robots',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c58411b84f','2014-02-20 17:22:37.000000','2018-05-17 15:33:42.687506','Administrator','Administrator',0,'Version','fields','DocType',6,'table_html','Table HTML',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c59aca5ecb','2016-07-15 05:51:29.224123','2018-05-17 15:33:46.911760','Administrator','Administrator',0,'Bulk Update','fields','DocType',2,'field','Field',NULL,'Select',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c5c43c08d0','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',4,'is_manual','Is Manual',NULL,'Check',NULL,NULL,0,0,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Is Feedback request triggered manually ?',0,0,0,'',0,0,0,0,0,0),('c5c9d41d03','2013-02-22 01:28:08.000000','2018-05-17 15:33:45.105834','Administrator','Administrator',0,'Top Bar Item','fields','DocType',1,'label','Label',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'120px','120px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('c5d6b54bb1','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','fields','DocType',7,'module','Module','module','Link','Select','Module Def',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,1,0,NULL,0,0,0,0,0,0),('c5f1f8ea6f','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',1,'introduction_section','Introduction',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('c5fbbb1dec','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',38,'email_signature','Email Signature',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('c625f5b234','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',12,'float_precision','Float Precision',NULL,'Select',NULL,'\n2\n3\n4\n5\n6\n7\n8\n9',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('c672c55d98','2017-09-08 16:27:39.195379','2018-05-17 15:33:47.111323','Administrator','Administrator',0,'Webhook Header','fields','DocType',1,'key','Key',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('c6cb85c44a','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',15,'permissions','Permissions',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c6ee03690a','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',26,'permlevel','Perm Level','permlevel','Int','Int',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,'0',NULL,0,0,0,NULL,0,0,0,0,0,0),('c71fb27fff','2017-04-21 16:57:30.264478','2018-05-17 15:33:47.429402','Administrator','Administrator',0,'GSuite Settings','fields','DocType',8,'script_code','Script Code',NULL,'HTML',NULL,'<pre>// ERPNEXT GSuite integration\n//\n\nfunction doGet(e){\n  return ContentService.createTextOutput(\'ok\');\n}\n\nfunction doPost(e) {\n  var p = JSON.parse(e.postData.contents);\n\n  switch(p.exec){\n    case \'new\':\n      var url = createDoc(p);\n      result = { \'url\': url };\n      break;\n    case \'test\':\n      result = { \'test\':\'ping\' , \'version\':\'1.0\'}\n  }\n  return ContentService.createTextOutput(JSON.stringify(result)).setMimeType(ContentService.MimeType.JSON);\n}\n\nfunction replaceVars(body,p){\n  for (key in p) {\n    if (p.hasOwnProperty(key)) {\n      if (p[key] != null) {\n        body.replaceText(\'{{\'+key+\'}}\', p[key]);\n      }\n    }\n  }    \n}\n\nfunction createDoc(p) {\n  if(p.destination){\n    var folder = DriveApp.getFolderById(p.destination);\n  } else {\n    var folder = DriveApp.getRootFolder();\n  }\n  var template = DriveApp.getFileById( p.template )\n  var newfile = template.makeCopy( p.filename , folder );\n\n  switch(newfile.getMimeType()){\n    case MimeType.GOOGLE_DOCS:\n      var body = DocumentApp.openById(newfile.getId()).getBody();\n      replaceVars(body,p.vars);\n      break;\n    case MimeType.GOOGLE_SHEETS:\n      //TBD\n    case MimeType.GOOGLE_SLIDES:\n      //TBD\n  }\n  return newfile.getUrl()\n}\n\n</pre>',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'','Copy and paste this code into and empty Code.gs in your project at script.google.com',0,0,1,'',0,0,0,0,0,0),('c726a7ee5c','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',7,'user_permission_doctypes','User Permission DocTypes',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,'JSON list of DocTypes used to apply User Permissions. If empty, all linked DocTypes will be used to apply User Permissions.',0,0,1,NULL,0,0,0,0,0,0),('c72a3f2d93','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',2,'properties','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'doc_type',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('c743a0116a','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',13,'test_email_id','Test Email Address',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'A Lead with this Email Address should exist',0,0,0,NULL,0,0,0,0,0,0),('c75d431706','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',45,'last_sync_timestamp','Last Sync Timestamp',NULL,'Float',NULL,NULL,0,1,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,1,'',0,0,0,0,0,0),('c7734503fe','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',20,'column_break_17',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c7adc6c92a','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',37,'redis_socketio','Redis Socketio',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c7bbae2375','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',10,'report_filters','Report Filters',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.report_type !== \'Report Builder\'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c8471f5af5','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',30,'remember_last_selected_value','Remember Last Selected Value',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:(doc.fieldtype == \'Link\')',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c8527c2a81','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',10,'percent_complete','Percent Complete',NULL,'Percent',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('c913c674af','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',15,'column_break_6',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('c92e074a6d','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','fields','DocType',6,'level','Level',NULL,'Select',NULL,'Beginner\nIntermediate\nExpert',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c9417c2a4e','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',14,'link','Link',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('c9556a1955','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',11,'brand_html','Brand HTML',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Brand is what appears on the top-left of the toolbar. If it is an image, make sure it\nhas a transparent background and use the &lt;img /&gt; tag. Keep size as 200px x 30px',0,0,0,NULL,0,0,0,0,0,0),('c99543aff7','2016-08-29 05:29:16.726172','2018-05-17 15:33:46.785383','Administrator','Administrator',0,'Note Seen By','fields','DocType',1,'user','User',NULL,'Link',NULL,'User',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('c9afaec576','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',2,'forward_to_email','Forward To Email Address',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Send enquiries to this email address',0,0,0,NULL,0,0,0,0,0,0),('c9b2da4fbf','2013-03-07 11:55:11.000000','2018-05-17 15:33:44.447683','Administrator','Administrator',0,'About Us Team Member','fields','DocType',2,'image_link','Image Link',NULL,'Attach',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'150px',NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('c9dca672d1','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',27,'print_hide_if_no_value','Print Hide If No Value',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:[\"Int\", \"Float\", \"Currency\", \"Percent\"].indexOf(doc.fieldtype)!==-1',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ca339a129f','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',24,'show_sidebar','Show Sidebar',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('ca46f5b03a','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',50,'in_create','User Cannot Create','in_create','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('ca5988a5f7','2013-03-08 09:41:11.000000','2018-05-17 15:33:45.136706','Administrator','Administrator',0,'Blog Category','fields','DocType',3,'published','Published',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('ca5ed255f3','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',57,'last_ip','Last IP','last_ip','Read Only','Read Only',NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('ca84f7ea07','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',26,'github_username','Github Username',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('cad06fec2e','2017-05-03 16:28:11.295095','2018-05-17 15:33:43.606890','Administrator','Administrator',0,'Domain Settings','fields','DocType',2,'domains_html','Domains HTML',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('cb47d4dd39','2016-03-30 10:04:25.828742','2018-05-17 15:33:44.194278','Administrator','Administrator',0,'User Email','fields','DocType',1,'email_account','Email Account',NULL,'Link',NULL,'Email Account',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('cb90c9a09e','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',13,'country','Country',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('cba8ded834','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',8,'track_changes','Track Changes',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.istable',0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('cbc356847f','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',19,'roles','Roles Assigned',NULL,'Table',NULL,'Has Role',0,1,0,1,0,0,0,0,0,0,0,0,NULL,NULL,NULL,1,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('cbc8ba5aa8','2017-08-22 17:15:31.459897','2018-05-17 15:34:43.427815','Administrator','Administrator',0,'Bench Manager Command','fields','DocType',3,'column_break_2',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('cbc922c0a6','2012-11-22 17:45:46.000000','2018-05-17 15:33:48.057067','Administrator','Administrator',0,'Letter Head','fields','DocType',1,'letter_head_name','Letter Head Name','letter_head_name','Data','Data',NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('cbd057e3c3','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',36,'thread_notify','Send Notifications for Transactions I Follow',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('cbedc51daa','2017-08-31 04:16:38.764465','2018-05-17 15:33:43.506765','Administrator','Administrator',0,'Role Profile','fields','DocType',3,'roles','Roles Assigned',NULL,'Table',NULL,'Has Role',0,1,0,1,0,0,0,0,0,0,0,0,NULL,NULL,NULL,1,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('cbf9e1c1bd','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',57,'advanced','Advanced',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('cc0ebddda4','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',8,'section_break_5',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('cc571318cc','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',12,'value_changed','Value Changed',NULL,'Select',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.event==\"Value Change\"',0,0,NULL,NULL,0,NULL,'Send alert if this field\'s value changes',0,0,0,NULL,0,0,0,0,0,0),('cc767058d6','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',22,'hide_footer_signup','Hide Footer Signup',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('cc76ffa271','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',29,'payment_button_label','Button Label',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'accept_payment',0,0,NULL,NULL,0,'Buy Now',NULL,0,0,0,'',0,0,0,0,0,0),('ccbecc2f56','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',21,'reqd','Is Mandatory Field','reqd','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('cd1b3595db','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',21,'description','Message',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('cd3742ccb9','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',27,'total','Total',NULL,'Float',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('cd40872d50','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',18,'amended_from','Amended From',NULL,'Link',NULL,'Data Import',0,0,0,1,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('cd4acf75a0','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',6,'current_mapping_delete_start','Current Mapping Delete Start',NULL,'Int',NULL,NULL,0,1,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('cd6cfd430a','2014-02-20 17:22:37.000000','2018-05-17 15:33:42.687506','Administrator','Administrator',0,'Version','fields','DocType',3,'docname','Document Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('cd795cc456','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',9,'options','Options','options','Text','Text',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('cdd667960b','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',44,'default_print_format','Default Print Format',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('cdd74602a7','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',10,'developer_flag','Developer Flag',NULL,'Int',NULL,NULL,0,1,0,1,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,1,'',0,0,0,0,0,0),('cdf628901b','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',15,'example','Example',NULL,'HTML',NULL,'<h5>Message Example</h5>\n\n<pre>&lt;h3&gt;Issue Resolved&lt;/h3&gt;\n\n&lt;p&gt;Issue {{ doc.name }} Is resolved. Please check and confirm the same.&lt;/p&gt;\n\n&lt;p&gt; Your Feedback is important for us. Please give us your Feedback for {{ doc.name }}&lt;/p&gt;\n\n&lt;h4&gt;Details&lt;/h4&gt;</pre>',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ce313b8513','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',17,'top_bar_text_color','Top Bar Text Color',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_text_styles',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ce71d57bd2','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',6,'precision','Precision',NULL,'Select',NULL,'\n1\n2\n3\n4\n5\n6\n7\n8\n9',0,0,0,1,0,0,0,0,0,0,0,0,NULL,NULL,'eval:in_list([\"Float\", \"Currency\", \"Percent\"], doc.fieldtype)',0,0,NULL,NULL,0,NULL,'Set non-standard precision for a Float or Currency field',0,0,0,NULL,0,0,0,0,0,0),('ce9506fce9','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',6,'filter_data','Filter Data',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('cea99752ed','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',17,'name_case','Name Case','name_case','Select','Select','\nTitle Case\nUPPER CASE',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('cec319fe55','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',18,'section_break_17','Sidebar and Comments',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('cf02cd50b9','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',16,'update_bench_on_update','Update Bench On Update',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('cf2837660b','2012-12-28 10:49:55.000000','2018-05-17 15:33:45.395726','Administrator','Administrator',0,'Workflow','fields','DocType',3,'is_active','Is Active',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'If checked, all other workflows become inactive.',0,0,0,NULL,0,0,0,0,0,0),('cf33e61dfa','2017-09-04 20:57:20.129205','2018-05-17 15:33:47.174204','Administrator','Administrator',0,'S3 Backup Settings','fields','DocType',2,'notify_email','Send Notifications To',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('cfa15ee222','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',3,'disabled','Disabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('cfc571dcf8','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',2,'first_name','First Name','first_name','Data','Data',NULL,0,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('cfef996362','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',13,'rebase_on_pull','Rebase On Pull',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('cffcf6c5cd','2016-09-21 10:12:57.399174','2018-05-17 15:33:47.640308','Administrator','Administrator',0,'Dropbox Settings','fields','DocType',8,'dropbox_access_secret','Dropbox Access Secret',NULL,'Password',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('d00d5b29c0','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',15,'assigned_by','Assigned By',NULL,'Link',NULL,'User',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d05317059d','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',29,'default_outgoing','Default Outgoing',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'enable_outgoing',0,0,NULL,NULL,0,NULL,'Notifications and bulk mails will be sent from this outgoing server.',0,0,0,'',0,0,0,0,0,0),('d0a12885d1','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','fields','DocType',8,'parent_error_snapshot','Parent Error Snapshot',NULL,'Data',NULL,'',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d0c6e33060','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',11,'filters_display','Filters Display',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d10226a9bd','2013-03-25 16:00:51.000000','2018-05-17 15:33:44.781044','Administrator','Administrator',0,'Blogger','fields','DocType',2,'short_name','Short Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Will be used in url (usually first name).',0,0,0,NULL,0,0,0,0,0,0),('d110015ae3','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',17,'section_break_8','Fonts',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d1305fb784','2013-03-25 16:00:51.000000','2018-05-17 15:33:44.781044','Administrator','Administrator',0,'Blogger','fields','DocType',1,'disabled','Disabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d133ccef12','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',18,'status','Status',NULL,'Select',NULL,'Open\nReplied\nClosed\nLinked',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,'eval:doc.communication_type===\"Communication\"',0,0,NULL,NULL,0,NULL,NULL,1,1,0,'',0,0,0,0,0,0),('d14ae6ae22','2017-09-04 20:57:20.129205','2018-05-17 15:33:47.174204','Administrator','Administrator',0,'S3 Backup Settings','fields','DocType',5,'secret_access_key','Secret Access Key',NULL,'Password',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('d14b23851d','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',4,'frappe_git_branch','Frappe Git Branch',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('d1882c9eab','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',11,'section_break_11',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d1a48fc6ce','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',59,'last_known_versions','Last Known Versions',NULL,'Text',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Stores the JSON of last known versions of various installed apps. It is used to show release notes.',0,0,1,'',0,0,0,0,0,0),('d1b2c6d361','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',3,'route','Route',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,1,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d249ad36d0','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',35,'print_hide','Print Hide','print_hide','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d27d3116f9','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',35,'timeline_doctype','Timeline DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('d28cdbe500','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',3,'address_type','Address Type',NULL,'Select',NULL,'Billing\nShipping\nOffice\nPersonal\nPlant\nPostal\nShop\nSubsidiary\nWarehouse\nCurrent\nPermanent\nOther',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('d2b85e2f11','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',7,'github','GitHub',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d2ca8d2491','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',9,'apply_user_permissions','Apply User Permissions',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:[\"Query Report\", \"Script Report\"].indexOf(doc.report_type)!==-1',0,0,NULL,NULL,0,'1',NULL,0,0,0,NULL,0,0,0,0,0,0),('d318c2895a','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',31,'hide_heading','Hide Heading','hide_heading','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d328467fc9','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',11,'smtp_server','SMTP Server',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,'e.g. smtp.gmail.com',0,0,0,'',0,0,0,0,0,0),('d36a5e33c1','2017-09-14 12:08:50.302810','2018-05-17 15:33:47.458968','Administrator','Administrator',0,'Webhook Data','fields','DocType',3,'key','Key',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('d389fc0c53','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',13,'collapsible_depends_on','Collapsible Depends On',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.fieldtype==\"Section Break\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d39dba3330','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',4,'email_field','Email Field',NULL,'Select',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,'eval: doc.document_type',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d3a37534df','2013-01-16 13:09:40.000000','2018-05-17 15:33:43.738664','Administrator','Administrator',0,'Error Log','fields','DocType',3,'error','Error',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('d3bfdfe028','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',6,'date','Due Date','date','Date','Date',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('d3e8f1bad8','2016-10-19 12:26:04.809812','2018-05-17 15:33:46.932403','Administrator','Administrator',0,'Kanban Board','fields','DocType',6,'filters','Filters',NULL,'Text',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('d470d2028b','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',17,'monday','Monday',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.repeat_this_event && doc.repeat_on===\"Every Day\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d49152cb77','2017-04-24 09:53:41.813982','2018-05-17 15:33:47.580015','Administrator','Administrator',0,'GSuite Templates','fields','DocType',5,'destination_id','Destination ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d53c593856','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',14,'use_ssl','Use SSL',NULL,'Check',NULL,'domain.use_ssl',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.domain && doc.enable_incoming',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d5586cea44','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','fields','DocType',10,'exception','Exception',NULL,'Code',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d5b56cfea3','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',3,'content','Message',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'400',NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d5deca7b7d','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',6,'skip_authorization','Skip Authorization',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'If checked, users will not see the Confirm Access dialog.',0,0,0,'',0,0,0,0,0,0),('d5f4eab6d6','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',11,'column_break0',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50%',NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d5fff84b23','2017-10-23 13:02:10.295824','2018-05-17 15:33:46.879565','Administrator','faris@erpnext.com',0,'Calendar View','fields','DocType',2,'subject_field','Subject Field',NULL,'Select',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('d60059d300','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',12,'delete','Delete',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,1,0,0,'',0,0,0,0,0,0),('d615c265c8','2017-08-22 17:15:31.459897','2018-05-17 15:34:43.427815','Administrator','Administrator',0,'Bench Manager Command','fields','DocType',2,'time_taken','Time taken',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('d668444e81','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',27,'accept_payment','Accept Payment',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d6732bbc03','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',9,'cb_3',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d69b933fea','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',3,'facebook_client_secret','Facebook Client Secret',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d6c513b479','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',4,'fieldname','Fieldname','fieldname','Data','Data',NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,NULL,0,0,0,0,0,0),('d6d57cbb4f','2017-02-13 17:33:25.157332','2018-05-17 15:33:43.349585','Administrator','Administrator',0,'User Permission for Page and Report','fields','DocType',7,'section_break_8',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d7492ea331','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',2,'filters','Filters',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d782fcecff','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',1,'module_name','Module Name',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d786a6b219','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',14,'fields','Fields','fields','Table','Table','DocField',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d7a85a7be5','2016-09-21 10:12:57.399174','2018-05-17 15:33:47.640308','Administrator','Administrator',0,'Dropbox Settings','fields','DocType',5,'app_secret_key','App Secret Key',NULL,'Password',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d7ccfab5a7','2017-02-13 17:33:25.157332','2018-05-17 15:33:44.054341','Administrator','Administrator',0,'Role Permission for Page and Report','fields','DocType',4,'roles_permission','Allow Roles',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d7dab5f3b8','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',13,'maintenance_mode','Maintenance Mode',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d7e56faf18','2013-03-25 16:00:51.000000','2018-05-17 15:33:44.781044','Administrator','Administrator',0,'Blogger','fields','DocType',7,'posts','Posts',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,NULL,0,0,0,0,0,0),('d83b777659','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',17,'pull_insert','Pull Insert',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d85b836826','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',9,'read','Read','read','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,'1',NULL,1,0,0,NULL,0,0,0,0,0,0),('d898bb90b6','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',7,'user_permission_doctypes','User Permission DocTypes',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,'JSON list of DocTypes used to apply User Permissions. If empty, all linked DocTypes will be used to apply User Permissions.',0,0,1,'',0,0,0,0,0,0),('d8f3db2767','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',21,'notify_if_unreplied','Notify if unreplied',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d8ff4e57ea','2013-02-22 01:28:08.000000','2018-05-17 15:33:45.105834','Administrator','Administrator',0,'Top Bar Item','fields','DocType',3,'url','URL',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'200px','200px',0,NULL,'Link to the page you want to open. Leave blank if you want to make it a group parent.',1,0,0,NULL,0,0,0,0,0,0),('d90f610ef5','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',27,'groups','Groups',NULL,'Column Break','Column Break',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50%','50%',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d9261f3649','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',35,'sb2','Permission Rules',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.istable',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('d9393db84f','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','fields','DocType',5,'blog_category','Blog Category',NULL,'Link',NULL,'Blog Category',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('d95cb8440a','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',5,'section_break_5',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d96e65fed9','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',13,'feedback_rating','Feedback Rating',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('d9b0221596','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',11,'create','Create','create','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,'1',NULL,1,0,0,'',0,0,0,0,0,0),('d9cbe6c035','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',32,'logout_all_sessions','Logout from all devices while changing Password',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('da4154454b','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',12,'test_the_newsletter','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('da696fa188','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',16,'day_of_week','Day of Week',NULL,'Select',NULL,'Monday\nTuesday\nWednesday\nThursday\nFriday\nSaturday\nSunday',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.frequency==\'Weekly\'',0,0,NULL,NULL,0,'Monday',NULL,0,0,0,'',0,0,0,0,0,0),('daa500a084','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','fields','DocType',1,'is_sent','Is Sent',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('dad13e8052','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',33,'link_doctype','Link DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('dae8094a42','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',2,'domain_name','domain name',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,1,'',0,0,0,0,0,0),('daf80b2425','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',1,'facebook','Facebook',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('dafb2443d7','2013-01-08 15:50:01.000000','2018-05-17 15:33:42.301437','Administrator','Administrator',0,'Role','fields','DocType',5,'restrict_to_domain','Restrict To Domain',NULL,'Link',NULL,'Domain',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('db069fbcb3','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',25,'allow_on_submit','Allow on Submit','allow_on_submit','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('db13c8c235','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',3,'label_help','Label Help',NULL,'HTML','HTML',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('db1c4f6780','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',40,'allow_import','Allow Import (via Data Import Tool)',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('db466b9424','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',17,'linked_with','Reference',NULL,'Section Break',NULL,'fa fa-pushpin',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('db536e871e','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',4,'send_reminder','Send an email reminder in the morning',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,NULL,0,0,0,0,0,0),('db560ebb48','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',28,'deny_multiple_sessions','Allow only one session per user',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Note: Multiple sessions will be allowed in case of mobile device',0,0,0,'',0,0,0,0,0,0),('db757dd9a4','2017-07-17 14:25:27.881871','2018-05-17 15:33:42.627169','Administrator','Administrator',0,'User Permission','fields','DocType',4,'apply_for_all_roles','Apply for all Roles for this User',NULL,'Check',NULL,NULL,0,0,0,0,0,0,1,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1','If you un-check this, you will have to apply manually for each Role + Document Type combination',0,0,0,'',0,0,0,0,0,0),('db988e5548','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',8,'in_standard_filter','In Standard Filter',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('dbe195fdd4','2016-10-19 12:26:42.569185','2018-05-17 15:33:46.626499','Administrator','Administrator',0,'Kanban Board Column','fields','DocType',1,'column_name','Column Name',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('dbebd21537','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',11,'default_redirect_uri','Default Redirect URI',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('dbf0f08afd','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',21,'apply_strict_user_permissions','Apply Strict User Permissions',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0','If Apply Strict User Permission is checked and User Permission is defined for a DocType for a User, then all the documents where value of the link is blank, will not be shown to that User',0,0,0,'',0,0,0,0,0,0),('dc3bd6a991','2016-03-30 01:39:20.586927','2018-05-17 15:33:42.598490','Administrator','Administrator',0,'Portal Menu Item','fields','DocType',1,'title','Title',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,2,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('dcaec15dd5','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',1,'email_id','Email Address',NULL,'Data',NULL,'',0,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('dccd228068','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',9,'read','Read','read','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'32px','32px',0,'1',NULL,1,0,0,'',0,0,0,0,0,0),('dcd3d9c516','2016-08-04 04:58:40.457416','2018-05-17 15:33:47.300993','Administrator','Administrator',0,'Integration Request','fields','DocType',2,'integration_request_service','Integration Request Service',NULL,'Data',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('dd154d52f6','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','fields','DocType',11,'owner','Owner',NULL,'Link',NULL,'User',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'user',NULL,0,0,0,'',0,0,0,0,0,0),('dd4ae056d1','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',20,'section_break_4','Background',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,'apply_style',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('dd7afb41fb','2013-01-10 16:34:03.000000','2018-05-17 15:33:42.969016','Administrator','Administrator',0,'Module Def','fields','DocType',1,'module_name','Module Name','module_name','Data','Data',NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('dd8974582e','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',20,'collapsible','Collapsible',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.fieldtype==\"Section Break\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('dd93ab70ae','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',5,'pdf_page_size','PDF Page Size',NULL,'Select',NULL,'A4\nLetter',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'A4',NULL,0,0,0,NULL,0,0,0,0,0,0),('dda2b7bf0d','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','fields','DocType',24,'google_analytics_id','Google Analytics ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Add Google Analytics ID: eg. UA-89XXX57-1. Please search help on Google Analytics for more information.',0,0,0,NULL,0,0,0,0,0,0),('ddf5040112','2016-09-21 10:12:57.399174','2018-05-17 15:33:47.640308','Administrator','Administrator',0,'Dropbox Settings','fields','DocType',2,'send_notifications_to','Send Notifications To',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('de035752ae','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',14,'import_status','Import Status',NULL,'Select',NULL,'\nSuccessful\nFailed\nIn Progress\nPartially Successful',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('de4df8c67b','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',2,'login_id_is_different','Use Different Email Login ID',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('de4fd98f7e','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',10,'column_break_7',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('de59e53b07','2016-09-21 08:03:01.009852','2018-05-17 15:33:47.615721','Administrator','Administrator',0,'PayPal Settings','fields','DocType',1,'api_username','API Username',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('de72b11f6f','2013-01-10 16:34:01.000000','2018-05-17 15:33:46.459140','Administrator','Administrator',0,'Custom Script','fields','DocType',1,'dt','DocType','dt','Link','Link','DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('de9099f7b4','2017-06-26 10:57:19.976624','2018-05-17 15:33:42.924877','Administrator','Administrator',0,'Test Runner','fields','DocType',1,'module_path','Module Path',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('de914707da','2017-09-14 12:08:50.302810','2018-05-17 15:33:47.458968','Administrator','Administrator',0,'Webhook Data','fields','DocType',2,'cb_doc_data',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('dec0e4c117','2016-08-04 04:58:40.457416','2018-05-17 15:33:47.300993','Administrator','Administrator',0,'Integration Request','fields','DocType',1,'integration_type','Integration Type',NULL,'Select',NULL,'\nHost\nRemote',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('dec50afffa','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',10,'published','Published',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0','',0,0,0,'',0,0,0,0,0,0),('df090cb1f3','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',9,'status','Status',NULL,'Select',NULL,'\nSuccess\nFailed\nLinked\nClosed',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('df3d83d274','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',19,'user','User',NULL,'Link',NULL,'User',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,1,NULL,NULL,0,'__user',NULL,0,0,1,'',0,0,0,0,0,0),('df6afc6378','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',27,'reference_name','Reference Name',NULL,'Dynamic Link',NULL,'reference_doctype',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('df855e2fd9','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',23,'section_break_38',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('dff35d0392','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',48,'column_break_47',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e00259c51d','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',13,'smtp_port','Port',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,'If non standard port (e.g. 587)',0,0,0,'',0,0,0,0,0,0),('e0040b3fc9','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',2,'label','Label',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e03eea38a7','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','fields','DocType',7,'col_break_1',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e06698c2f4','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',4,'module','Module',NULL,'Link',NULL,'Module Def',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('e09e31e81f','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',8,'bcc','BCC',NULL,'Code',NULL,'Email',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.communication_medium===\"Email\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e0d257a2a4','2017-02-13 17:33:25.157332','2018-05-17 15:33:44.054341','Administrator','Administrator',0,'Role Permission for Page and Report','fields','DocType',5,'roles_html','Roles Html',NULL,'HTML',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e0eee424c0','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',43,'no_failed','no failed attempts',NULL,'Int',NULL,NULL,0,1,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('e13cd85a7b','2016-12-29 12:59:48.638970','2018-05-17 15:33:43.427855','Administrator','Administrator',0,'Deleted Document','fields','DocType',3,'column_break_3',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e1822e6701','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',1,'mapping_name','Mapping Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('e18d995b47','2014-02-20 17:22:37.000000','2018-05-17 15:33:42.687506','Administrator','Administrator',0,'Version','fields','DocType',4,'data','Data',NULL,'Code',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('e1a120ab6f','2017-09-04 20:57:20.129205','2018-05-17 15:33:47.174204','Administrator','Administrator',0,'S3 Backup Settings','fields','DocType',7,'backup_limit','Backup Limit',NULL,'Int',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e221009eca','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',5,'site_alias','Site Alias',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,'',0,0,0,0,0,0),('e2364681a3','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','fields','DocType',9,'likes','Likes',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('e2439236ca','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',4,'cb_1',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e272902a14','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','fields','DocType',11,'ignore_encoding_errors','Ignore encoding errors',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,'0',NULL,0,0,0,'',0,0,0,0,0,0),('e286998e43','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',14,'image_field','Image Field',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Must be of type \"Attach Image\"',0,0,0,'',0,0,0,0,0,0),('e295b18f1a','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',40,'uidvalidity','UIDVALIDITY',NULL,'Data',NULL,NULL,0,1,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e2f71eebb4','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',36,'redis_and_ports','Redis and Ports',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e2ff97cf80','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',21,'fields','Fields',NULL,'Table',NULL,'Customize Form Field',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,1,0),('e351863705','2017-07-17 14:25:27.881871','2018-05-17 15:33:42.627169','Administrator','Administrator',0,'User Permission','fields','DocType',3,'for_value','For Value',NULL,'Dynamic Link',NULL,'allow',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,'',0,0,0,0,0,0),('e365b41dfa','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',7,'owner','Allocated To',NULL,'Link',NULL,'User',0,0,0,0,0,0,0,1,0,0,0,0,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,0,1,0,NULL,0,0,0,0,0,0),('e3875d80c6','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',11,'color','Color',NULL,'Color',NULL,'',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'',NULL,0,0,0,'',0,0,0,0,0,0),('e38a6d952e','2016-09-03 11:42:42.575525','2018-05-17 15:33:47.140631','Administrator','Administrator',0,'OAuth Provider Settings','fields','DocType',1,'skip_authorization','Skip Authorization',NULL,'Select',NULL,'Force\nAuto',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e3a89957f3','2016-03-30 01:40:20.001775','2018-05-17 15:33:44.625767','Administrator','Administrator',0,'Portal Settings','fields','DocType',1,'default_role','Default Role at Time of Signup',NULL,'Link',NULL,'Role',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e3b9807acb','2017-02-13 17:33:25.157332','2018-05-17 15:33:44.054341','Administrator','Administrator',0,'Role Permission for Page and Report','fields','DocType',6,'roles','Roles',NULL,'Table',NULL,'Has Role',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('e40a27b1d8','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',15,'sb1','Naming',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('e49f9a6603','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',5,'repeat_this_event','Repeat this Event',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('e4b896e284','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','fields','DocType',2,'seen','Seen',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,1,0,0,0),('e4fc5377c9','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',17,'column_break_5',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e50a156300','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','fields','DocType',12,'other_settings','Other Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!(doc.__islocal)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e52e0710bc','2013-01-10 16:34:24.000000','2018-05-17 15:33:43.053495','Administrator','Administrator',0,'SMS Settings','fields','DocType',6,'use_post','Use POST',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e5308a77e2','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',13,'frappe_server_url','Frappe Server URL',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e5389bb784','2015-03-18 06:15:59.321619','2018-05-17 15:33:45.955127','Administrator','Administrator',0,'Email Group Member','fields','DocType',2,'email','Email',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('e658b41c4e','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',4,'if_owner','If user is the owner',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Apply this rule if the User is the Owner',0,0,0,'',0,0,0,0,0,0),('e6afadcaac','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',15,'email_to','Email To',NULL,'Small Text',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e6e52a2097','2017-02-13 17:33:25.157332','2018-05-17 15:33:43.349585','Administrator','Administrator',0,'User Permission for Page and Report','fields','DocType',6,'roles','Roles',NULL,'Table',NULL,'Has Role',0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('e777a215d6','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','fields','DocType',2,'category','Category',NULL,'Link',NULL,'Help Category',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,'',0,0,0,0,0,0),('e79fe932c2','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',26,'column_break_22',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('e7b0c2306a','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',10,'frappe','Frappe',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e7c2779efb','2013-02-22 01:27:58.000000','2018-05-17 15:33:44.024893','Administrator','Administrator',0,'SMS Parameter','fields','DocType',2,'value','Value',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'150px','150px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('e7d7cab065','2017-08-11 05:03:27.091416','2018-05-17 15:33:48.613509','Administrator','Administrator',0,'Data Migration Connector','fields','DocType',5,'hostname','Hostname',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'',NULL,1,0,0,'',0,0,0,0,0,0),('e804cb1f00','2017-08-17 01:25:56.910716','2018-05-17 15:33:48.101538','Administrator','Administrator',0,'Print Style','fields','DocType',1,'print_style_name','Print Style Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e80ad38289','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',29,'no_copy','No Copy','no_copy','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('e825b330ef','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',17,'attachments','Attachments',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('e829eb083e','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',21,'communication_date','Date',NULL,'Datetime',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Now',NULL,0,0,0,NULL,0,0,0,0,0,0),('e834e64d85','2014-02-20 17:22:37.000000','2018-05-17 15:33:42.687506','Administrator','Administrator',0,'Version','fields','DocType',1,'ref_doctype','DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,1,0,NULL,0,0,0,0,0,0),('e8622cd9e2','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',5,'reqd','Mandatory','reqd','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('e88bbb9bc9','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',22,'saturday','Saturday',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.repeat_this_event && doc.repeat_on===\"Every Day\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('e8b5aa4ebd','2016-09-22 04:16:48.829658','2018-05-17 15:33:48.026461','Administrator','Administrator',0,'LDAP Settings','fields','DocType',5,'password','Password for Base DN',NULL,'Password',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e8d7597d83','2013-01-08 15:50:01.000000','2018-05-17 15:33:42.301437','Administrator','Administrator',0,'Role','fields','DocType',1,'role_name','Role Name','role_name','Data','Data',NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('e922b13e27','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',28,'smtp_port','Port',NULL,'Data',NULL,'domain.smtp_port',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.domain && doc.enable_outgoing',0,0,NULL,NULL,0,NULL,'If non standard port (e.g. 587)',0,0,0,'',0,0,0,0,0,0),('e96ec4a9ff','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','fields','DocType',4,'app_description','App Description',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('e981bbf3d8','2017-08-17 14:51:33.577230','2018-05-17 15:34:43.329531','Administrator','Administrator',0,'Site Backup','fields','DocType',5,'public_file_backup','Public File Backup',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('e9c3b981ce','2013-03-25 16:00:51.000000','2018-05-17 15:33:44.781044','Administrator','Administrator',0,'Blogger','fields','DocType',4,'user','User',NULL,'Link',NULL,'User',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('e9cfd9a12d','2017-02-13 14:53:36.240122','2018-05-17 15:33:43.077118','Administrator','Administrator',0,'Custom Role','fields','DocType',6,'ref_doctype','Reference Doctype',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('e9ffac0081','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',2,'subject','Subject',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('ea4ac49c7c','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',1,'section_break_1',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ea6200062e','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',37,'auto_reply_message','Auto Reply Message',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'enable_auto_reply',0,0,NULL,NULL,0,NULL,'ProTip: Add <code>Reference: {{ reference_doctype }} {{ reference_name }}</code> to send document reference',0,0,0,'',0,0,0,0,0,0),('ea7b724380','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',32,'display','Display',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('ea96bc095f','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',40,'_user_tags','User Tags',NULL,'Data',NULL,NULL,0,1,0,1,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('eaae895aba','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',35,'auto_reply','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('eacc93ef44','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',1,'client_id','App Client ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'',NULL,0,0,1,NULL,0,0,0,0,0,0),('eae2425a42','2016-08-24 14:10:17.471264','2018-05-17 15:33:47.989214','Administrator','Administrator',0,'OAuth Bearer Token','fields','DocType',3,'scopes','Scopes',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('eb6f8c074d','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','fields','DocType',12,'frames','Frames',NULL,'Code',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ebae9d0336','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',11,'image_view','Image View',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval: doc.image_field',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ec070696b4','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',9,'attach_view_link','Send document web view link in email',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1','',0,0,0,'',0,0,0,0,0,0),('ec19abdc2e','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',9,'font_size','Font Size',NULL,'Select',NULL,'\n12px\n13px\n14px\n15px\n16px\n17px\n18px',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_text_styles',0,0,NULL,NULL,0,'',NULL,0,0,0,'',0,0,0,0,0,0),('ec5317bcfe','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',17,'link_doctype','Link DocType',NULL,'Link',NULL,'DocType',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('ec54919419','2016-08-04 04:58:40.457416','2018-05-17 15:33:47.300993','Administrator','Administrator',0,'Integration Request','fields','DocType',5,'output','Output',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('ec63bc043b','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','fields','DocType',12,'use_tls','Use TLS',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ec738de4d0','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',7,'fieldtype','Field Type','fieldtype','Select','Select','Attach\nAttach Image\nButton\nCheck\nCode\nColor\nColumn Break\nCurrency\nData\nDate\nDatetime\nDynamic Link\nFloat\nGeolocation\nHTML\nImage\nInt\nLink\nLong Text\nPassword\nPercent\nRead Only\nSection Break\nSelect\nSmall Text\nTable\nText\nText Editor\nTime\nSignature',0,0,0,0,0,1,1,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Data',NULL,1,0,0,NULL,0,0,1,0,0,0),('ec74666d98','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',3,'remote_primary_key','Remote Primary Key',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('ec8811d016','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',23,'column_break_14',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ed03abec2d','2016-02-17 12:21:16.175465','2018-05-17 15:33:43.395191','Administrator','Administrator',0,'Translation','fields','DocType',2,'section_break_4',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ed2be11026','2016-09-21 10:12:57.399174','2018-05-17 15:33:47.640308','Administrator','Administrator',0,'Dropbox Settings','fields','DocType',6,'allow_dropbox_access','Allow Dropbox Access',NULL,'Button',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ed352b1221','2013-03-07 11:55:11.000000','2018-05-17 15:33:44.447683','Administrator','Administrator',0,'About Us Team Member','fields','DocType',1,'full_name','Full Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'150px',NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('ed3df3ad7e','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','fields','DocType',15,'font','Font',NULL,'Select',NULL,'Default\nArial\nHelvetica\nVerdana\nMonospace',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.custom_format',0,0,NULL,NULL,0,'Default',NULL,0,0,0,'',0,0,0,0,0,0),('ed3f990f04','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',8,'operation','Operation',NULL,'Select',NULL,'\nLogin\nLogout',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ed72ba7c6f','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',5,'additional_info','More Information',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ed75f83295','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',13,'allow_print_for_cancelled','Allow Print for Cancelled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,'',0,0,0,0,0,0),('ed97219977','2016-09-22 04:16:48.829658','2018-05-17 15:33:48.026461','Administrator','Administrator',0,'LDAP Settings','fields','DocType',3,'organizational_unit','Organizational Unit',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('edc9ff9deb','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',2,'country','Country',NULL,'Link',NULL,'Country',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ee010669f8','2013-03-19 12:02:15.000000','2018-05-17 15:33:44.755304','Administrator','Administrator',0,'About Us Settings','fields','DocType',8,'team_members','Team Members',NULL,'Table',NULL,'About Us Team Member',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('ee5648867f','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',4,'google','Google',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('ee6287fc69','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',10,'unsubscribed','Unsubscribed',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('ee895c1fd9','2013-01-28 10:06:02.000000','2018-05-17 15:33:46.538180','Administrator','Administrator',0,'Currency','fields','DocType',1,'currency_name','Currency Name','currency_name','Data','Data',NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('eee33ffd97','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',12,'html_8',NULL,NULL,'HTML',NULL,'<p><strong>Condition Examples:</strong></p>\n<pre>doc.status==\"Closed\"\ndoc.due_date==nowdate()\ndoc.total &gt; 40000\n</pre>',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('eee7621adb','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',30,'new_password','Set New Password',NULL,'Password',NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('eeede7209c','2013-01-10 16:34:04.000000','2018-05-17 15:33:42.420279','Administrator','Administrator',0,'Property Setter','fields','DocType',10,'default_value','Default Value',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('ef2f1f6416','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',4,'current_mapping','Current Mapping',NULL,'Data',NULL,'',0,1,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('ef4cba7682','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',1,'theme','Theme',NULL,'Data',NULL,NULL,1,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('efee12ac14','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','fields','DocType',19,'export','Export',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('f048ed4bce','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',33,'amount_field','Amount Field',NULL,'Select',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.accept_payment && doc.amount_based_on_field',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f04f66e48c','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','fields','DocType',7,'section_break_14',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'apply_style',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f058305e96','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','fields','DocType',23,'send_notification_to','Send Notification to',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'notify_if_unreplied',0,0,NULL,NULL,0,NULL,'Email Addresses',0,0,0,'',0,0,0,0,0,0),('f0e59ae99a','2015-03-24 14:28:15.882903','2018-05-17 15:33:42.727881','Administrator','Administrator',0,'Block Module','fields','DocType',1,'module','Module',NULL,'Data',NULL,'',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('f0ffd4b38e','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',30,'in_filter','In Filter','in_filter','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f1045fe14a','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',8,'precision','Precision',NULL,'Select',NULL,'\n1\n2\n3\n4\n5\n6\n7\n8\n9',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:in_list([\"Float\", \"Currency\", \"Percent\"], doc.fieldtype)',0,0,NULL,NULL,0,NULL,'Set non-standard precision for a Float or Currency field',0,0,0,'',0,0,0,0,0,0),('f1430dd3ae','2014-09-01 14:14:14.292173','2018-05-17 15:33:42.552770','Administrator','Administrator',0,'Web Form Field','fields','DocType',6,'hidden','Hidden',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f1446c6d17','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',7,'sb_1','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,'',0,0,0,0,0,0),('f15a875637','2016-08-04 04:58:40.457416','2018-05-17 15:33:47.300993','Administrator','Administrator',0,'Integration Request','fields','DocType',6,'error','Error',NULL,'Code',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('f161e11ee0','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','fields','DocType',5,'address_line2','Address Line 2',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f17f50c3b3','2013-02-22 01:28:08.000000','2018-05-17 15:33:44.943298','Administrator','Administrator',0,'Company History','fields','DocType',2,'highlight','Highlight',NULL,'Text',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'300px','300px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('f191a04293','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',9,'quick_entry','Quick Entry',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('f1973e8548','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',10,'section_break_6','Reference',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f1a9b49415','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',2,'email_group','Email Group',NULL,'Table',NULL,'Newsletter Email Group',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,1,0,'',0,0,0,0,0,0),('f20d5d3b45','2017-02-26 16:20:52.654136','2018-05-17 15:33:46.022273','Administrator','Administrator',0,'Newsletter Email Group','fields','DocType',1,'email_group','Email Group',NULL,'Link',NULL,'Email Group',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('f21748c729','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','fields','DocType',9,'reference_name','Reference DocName',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,NULL,0,0,0,0,0,0),('f22d63edbb','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','fields','DocType',7,'hidden','Hidden',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f232c7271c','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',42,'oldfieldname',NULL,'oldfieldname','Data','Data',NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f2b2d8a81d','2012-12-28 10:49:55.000000','2018-05-17 15:33:45.395726','Administrator','Administrator',0,'Workflow','fields','DocType',8,'transitions','Transitions',NULL,'Table',NULL,'Workflow Transition',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Rules defining transition of state in the workflow.',0,0,0,NULL,0,0,0,0,0,0),('f2c25c9fd0','2017-04-21 16:57:30.264478','2018-05-17 15:33:47.429402','Administrator','Administrator',0,'GSuite Settings','fields','DocType',9,'run_script_test','Run Script Test',NULL,'Button',NULL,NULL,0,0,0,1,1,0,0,0,0,0,1,0,NULL,NULL,'eval:(doc.client_id && doc.client_secret && doc.authorization_code && doc.refresh_token && doc.script_url)',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f2d81d85b0','2013-02-22 01:27:36.000000','2018-05-17 15:33:45.439712','Administrator','Administrator',0,'Workflow Document State','fields','DocType',2,'doc_status','Doc Status',NULL,'Select',NULL,'0\n1\n2',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'80px','80px',0,NULL,'0 - Draft; 1 - Submitted; 2 - Cancelled',1,0,0,NULL,0,0,0,0,0,0),('f32515ef9b','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',10,'column_break_10',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f3724125ef','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',20,'properties','',NULL,'Column Break','Column Break',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50%','50%',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f3769fcc0f','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',22,'read_receipt','Sent Read Receipt',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('f3784da518','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',36,'columns','Columns',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:cur_frm.doc.istable',0,0,NULL,NULL,0,NULL,'Number of columns for a field in a Grid (Total Columns in a grid should be less than 11)',0,0,0,'',0,0,0,0,0,0),('f37df6ae2f','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','fields','DocType',3,'status','Status',NULL,'Select',NULL,'Pending\nStarted\nPartial Success\nSuccess\nFail\nError',0,0,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'Pending',NULL,1,0,1,'',0,0,0,0,0,0),('f3c7cbb0e7','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','fields','DocType',19,'column_break_17',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f3cc6362b2','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',5,'column_break_4',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f3ec6f4301','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',8,'ends_on','Ends on',NULL,'Datetime',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f4063a9ea0','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',33,'in_filter','In Filter','in_filter','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f4d04320cf','2017-09-08 16:27:39.195379','2018-05-17 15:33:47.111323','Administrator','Administrator',0,'Webhook Header','fields','DocType',2,'value','Value',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('f4f1703bd0','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',8,'global_help_setup','Global Help Setup',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f50ff26e58','2017-08-11 05:03:27.091416','2018-05-17 15:33:48.613509','Administrator','Administrator',0,'Data Migration Connector','fields','DocType',2,'connector_type','Connector Type',NULL,'Select',NULL,'\nFrappe\nCustom',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.is_custom',0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('f52780c2d5','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',2,'label','Label','label','Data','Data',NULL,0,0,0,0,0,0,1,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,1,0,0,0),('f54ce17bfd','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',27,'remember_last_selected_value','Remember Last Selected Value',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:(doc.fieldtype == \'Link\')',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f5acdb6bd3','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','fields','DocType',25,'description','Description','description','Text Editor','Text',NULL,0,0,0,0,0,0,0,1,0,0,0,0,NULL,NULL,NULL,0,0,'300px','300px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f5ebf77588','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',31,'ignore_xss_filter','Ignore XSS Filter',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Don\'t HTML Encode HTML tags like &lt;script&gt; or just characters like &lt; or &gt;, as they could be intentionally used in this field',0,0,0,'',0,0,0,0,0,0),('f5f383caa5','2016-03-30 01:39:20.586927','2018-05-17 15:33:42.598490','Administrator','Administrator',0,'Portal Menu Item','fields','DocType',5,'role','Role',NULL,'Link',NULL,'Role',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,2,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('f62c8db364','2017-10-16 17:13:05.684227','2018-05-17 15:33:47.076864','Administrator','Administrator',0,'Google Maps','fields','DocType',1,'enabled','Enabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f654b6919d','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',15,'permission_rules','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f66b01eb6e','2013-03-07 15:53:15.000000','2018-05-17 15:33:45.003160','Administrator','Administrator',0,'Website Slideshow','fields','DocType',2,'sb0','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.__islocal',0,0,NULL,NULL,0,NULL,'Note: For best results, images must be of the same size and width must be greater than height.',0,0,0,NULL,0,0,0,0,0,0),('f66c5ae084','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',21,'dropbox_settings','Dropbox Settings',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f6d087cdc8','2017-03-09 17:18:29.458397','2018-05-17 15:33:47.156384','Administrator','Administrator',0,'Stripe Settings','fields','DocType',2,'secret_key','Secret Key',NULL,'Password',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f727e29e4b','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',31,'mail_port','Mail Port',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f74059670d','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',16,'sb1','Roles',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'enabled',1,0,NULL,NULL,0,NULL,'',0,0,1,NULL,0,0,0,0,0,0),('f7a24c0ba9','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',14,'options','Options','options','Text','Text',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'For Links, enter the DocType as range.\nFor Select, enter list of Options, each on a new line.',1,0,0,NULL,0,0,0,0,0,0),('f7fddcaae8','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',12,'content','Message',NULL,'Text Editor',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'400',NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f86c950042','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','fields','DocType',1,'receipient','Receipient',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f8a64251fb','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',12,'pincode','Pincode',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f8c9f70765','2013-03-07 12:26:33.000000','2018-05-17 15:33:45.033410','Administrator','Administrator',0,'Website Slideshow Item','fields','DocType',2,'heading','Heading',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'200px','200px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('f8e4413581','2016-12-29 12:59:48.638970','2018-05-17 15:33:43.427855','Administrator','Administrator',0,'Deleted Document','fields','DocType',6,'section_break_6',NULL,NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f906124e48','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.874067','Administrator','Administrator',0,'DocPerm','fields','DocType',20,'import','Import',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f906e79182','2013-02-22 01:27:32.000000','2018-05-17 15:33:46.369043','Administrator','Administrator',0,'Customize Form Field','fields','DocType',18,'hidden','Hidden','hidden','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'50px','50px',0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f926e35f1e','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',38,'unread_notification_sent','Unread Notification Sent',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'0',NULL,0,0,1,'',0,0,0,0,0,0),('f9570b698e','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',13,'reference_owner','Reference Owner',NULL,'Read Only',NULL,'reference_name.owner',1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f95fb31c63','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',1,'subject','Subject',NULL,'Small Text',NULL,NULL,0,0,0,0,0,1,0,1,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('f981b8f171','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',31,'allow_error_traceback','Show Full Error and Allow Reporting of Issues to the Developer',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1','',0,0,0,'',0,0,0,0,0,0),('f98c23f661','2016-09-21 08:03:01.009852','2018-05-17 15:33:47.615721','Administrator','Administrator',0,'PayPal Settings','fields','DocType',2,'api_password','API Password',NULL,'Password',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('f9a86250dc','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','fields','DocType',9,'migration_id_field','Migration ID Field',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,1,'',0,0,0,0,0,0),('f9b93358dd','2017-08-11 05:03:27.091416','2018-05-17 15:33:48.613509','Administrator','Administrator',0,'Data Migration Connector','fields','DocType',8,'password','Password',NULL,'Password',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fa05b1e30d','2017-08-17 14:51:33.577230','2018-05-17 15:34:43.329531','Administrator','Administrator',0,'Site Backup','fields','DocType',3,'time','Time',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,1,'',0,0,0,0,0,0),('fa2d2262b5','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',3,'document_type','Document Type',NULL,'Link',NULL,'DocType',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fa44d5b13c','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','fields','DocType',2,'app_name','App Name',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fa4ecc1495','2016-09-21 10:12:57.399174','2018-05-17 15:33:47.640308','Administrator','Administrator',0,'Dropbox Settings','fields','DocType',9,'dropbox_access_token','Dropbox Access Token',NULL,'Password',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fa4f4e0195','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','fields','DocType',7,'cc','CC',NULL,'Code',NULL,'Email',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.communication_medium===\"Email\"',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fa50bc8c25','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','fields','DocType',7,'with_letterhead','Print with letterhead',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1','',0,0,0,NULL,0,0,0,0,0,0),('fa59e70e4a','2015-03-18 09:41:20.216319','2018-05-17 15:33:46.333335','Administrator','Administrator',0,'Email Unsubscribe','fields','DocType',1,'email','Email',NULL,'Data',NULL,NULL,0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,'',0,0,0,0,0,0),('fa7cb17e82','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','fields','DocType',28,'section_break_37',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fa8ce14160','2013-02-22 01:28:08.000000','2018-05-17 15:33:45.105834','Administrator','Administrator',0,'Top Bar Item','fields','DocType',4,'target','Target',NULL,'Select',NULL,'\ntarget = \"_blank\"',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Select target = \"_blank\" to open in a new page.',0,0,0,NULL,0,0,0,0,0,0),('faee41b9ea','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','fields','DocType',4,'default_print_format','Default Print Format',NULL,'Link',NULL,'Print Format',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('fb355f8fe5','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','fields','DocType',2,'facebook_client_id','Facebook Client ID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('fb3c3ef775','2016-07-15 05:51:29.224123','2018-05-17 15:33:46.911760','Administrator','Administrator',0,'Bulk Update','fields','DocType',4,'condition','Condition',NULL,'Small Text',NULL,NULL,0,0,0,0,0,0,1,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'SQL Conditions. Example: status=\"Open\"',0,0,0,'',0,0,0,0,0,0),('fba6275eb3','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','fields','DocType',1,'enabled','Enabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fc023b97c2','2016-09-20 03:44:03.799402','2018-05-17 15:33:47.340008','Administrator','Administrator',0,'Razorpay Settings','fields','DocType',3,'redirect_to','Redirect To',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Mention transaction completion page URL',0,0,0,'',0,0,0,0,0,0),('fc22adbe40','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','fields','DocType',8,'disabled','Disabled',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('fc332d1bbb','2014-07-11 17:19:37.037109','2018-05-17 15:33:45.474737','Administrator','Administrator',0,'Email Alert Recipient','fields','DocType',4,'condition','Condition',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Expression, Optional',1,0,0,NULL,0,0,0,0,0,0),('fc63e956a5','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','fields','DocType',19,'columns','Columns',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Number of columns for a field in a List View or a Grid (Total Columns should be less than 11)',0,0,0,'',0,0,0,0,0,0),('fc85ffb6df','2016-08-24 14:10:17.471264','2018-05-17 15:33:47.989214','Administrator','Administrator',0,'OAuth Bearer Token','fields','DocType',7,'expires_in','Expires In',NULL,'Int',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fcb12c2251','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',30,'read_only_onload','Show Print First','read_only_onload','Check','Check',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('fcdf66a26f','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',7,'quick_entry','Quick Entry',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.istable && !doc.issingle',0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0),('fce8f15abd','2013-02-22 01:27:36.000000','2018-05-17 15:33:45.439712','Administrator','Administrator',0,'Workflow Document State','fields','DocType',1,'state','State',NULL,'Link',NULL,'Workflow State',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'160px','160px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('fcfd2cecde','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','fields','DocType',6,'address','Address',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('fd2161b8f8','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',27,'ignore_user_permissions','Ignore User Permissions',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'User permissions should not apply for this Link',0,0,0,NULL,0,0,0,0,0,0),('fd999131b1','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','fields','DocType',3,'published','Published',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('fdbc9ab041','2017-04-21 16:57:30.264478','2018-05-17 15:33:47.429402','Administrator','Administrator',0,'GSuite Settings','fields','DocType',6,'google_apps_script','Google Apps Script',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,1,0,0,0,NULL,NULL,'eval:doc.enable',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fdccb1b1ab','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','fields','DocType',6,'is_home_folder','Is Home Folder',NULL,'Check',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fde2bceb63','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','fields','DocType',12,'filters','Filters',NULL,'Text',NULL,NULL,0,1,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fe09c1128c','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',13,'column_break_10',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('fe24d6434b','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','fields','DocType',22,'search_fields','Search Fields','search_fields','Data','Data',NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:!doc.istable',0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('fe52434cb1','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','fields','DocType',41,'disable_standard_email_footer','Disable Standard Email Footer',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fe5826ec9b','2016-03-30 01:40:20.001775','2018-05-17 15:33:44.625767','Administrator','Administrator',0,'Portal Settings','fields','DocType',6,'custom_menu','Custom Menu Items',NULL,'Table',NULL,'Portal Menu Item',0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fe64844bea','2013-02-22 01:27:33.000000','2018-05-17 15:33:41.742607','Administrator','Administrator',0,'DocField','fields','DocType',12,'bold','Bold',NULL,'Check',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ff0e4ee9cb','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','fields','DocType',48,'defaults','User Defaults',NULL,'Table',NULL,'DefaultValue',0,1,0,0,0,0,0,0,0,0,1,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'Enter default value fields (keys) and values. If you add multiple values for a field,the first one will be picked. These defaults are also used to set \"match\" permission rules. To see list of fields,go to \"Customize Form\".',0,0,0,NULL,0,0,0,0,0,0),('ff30798f5c','2013-02-22 01:27:36.000000','2018-05-17 15:33:45.339696','Administrator','Administrator',0,'Workflow Transition','fields','DocType',3,'next_state','Next State',NULL,'Link',NULL,'Workflow State',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,NULL,0,0,'200px','200px',0,NULL,NULL,1,0,0,NULL,0,0,0,0,0,0),('ff397d7876','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','fields','DocType',34,'amount','Amount',NULL,'Currency',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,'eval:doc.accept_payment && !doc.amount_based_on_field',0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('ff55f08dc7','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','fields','DocType',8,'sb1','Content',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,'',0,0,0,NULL,0,0,0,0,0,0),('ffb6f29d6f','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','fields','DocType',1,'description_and_status','',NULL,'Section Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,0,0,0),('ffecdb7b48','2016-04-14 09:41:45.892975','2018-05-17 15:33:46.225853','Administrator','Administrator',0,'Unhandled Email','fields','DocType',2,'uid','UID',NULL,'Data',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fff8a4f2d9','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','fields','DocType',4,'column_break_5',NULL,NULL,'Column Break',NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,NULL,NULL,0,0,0,'',0,0,0,0,0,0),('fffe1fd277','2015-02-04 04:33:36.330477','2018-05-17 15:33:42.835173','Administrator','Administrator',0,'DocShare','fields','DocType',9,'notify_by_email','Notify by email',NULL,'Check',NULL,NULL,0,0,0,1,0,0,0,0,0,0,0,0,NULL,NULL,NULL,0,0,NULL,NULL,0,'1',NULL,0,0,0,'',0,0,0,0,0,0);
/*!40000 ALTER TABLE `tabDocField` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabDocPerm`
--

DROP TABLE IF EXISTS `tabDocPerm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabDocPerm` (
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `permlevel` int(11) DEFAULT '0',
  `role` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `match` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `read` int(1) NOT NULL DEFAULT '1',
  `write` int(1) NOT NULL DEFAULT '1',
  `create` int(1) NOT NULL DEFAULT '1',
  `submit` int(1) NOT NULL DEFAULT '0',
  `cancel` int(1) NOT NULL DEFAULT '0',
  `delete` int(1) NOT NULL DEFAULT '1',
  `amend` int(1) NOT NULL DEFAULT '0',
  `report` int(1) NOT NULL DEFAULT '1',
  `export` int(1) NOT NULL DEFAULT '1',
  `import` int(1) NOT NULL DEFAULT '0',
  `share` int(1) NOT NULL DEFAULT '1',
  `print` int(1) NOT NULL DEFAULT '1',
  `email` int(1) NOT NULL DEFAULT '1',
  `user_permission_doctypes` longtext COLLATE utf8mb4_unicode_ci,
  `apply_user_permissions` int(1) NOT NULL DEFAULT '0',
  `set_user_permissions` int(1) NOT NULL DEFAULT '0',
  `if_owner` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabDocPerm`
--

LOCK TABLES `tabDocPerm` WRITE;
/*!40000 ALTER TABLE `tabDocPerm` DISABLE KEYS */;
INSERT INTO `tabDocPerm` VALUES ('00d2798b89','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','permissions','DocType',3,0,'Report Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('01b9a6806f','2016-03-29 10:50:48.848239','2018-05-17 15:33:46.285419','Administrator','Administrator',0,'Email Domain','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,0,0,NULL,0,1,0),('036f490eb8','2013-03-25 16:00:51.000000','2018-05-17 15:33:44.781044','Administrator','Administrator',0,'Blogger','permissions','DocType',2,0,'Blogger',NULL,1,1,0,0,0,0,0,0,0,0,1,1,1,NULL,0,0,0),('0471f97dc5','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','permissions','DocType',1,0,'Knowledge Base Editor',NULL,1,1,1,0,0,1,0,1,1,1,0,1,1,NULL,0,0,0),('052d7affb8','2017-08-11 05:15:51.482165','2018-05-17 15:33:48.742058','Administrator','Administrator',0,'Data Migration Plan','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('06d3938d64','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','permissions','DocType',2,0,'System Manager',NULL,1,1,1,0,0,0,0,1,1,0,1,1,1,NULL,0,0,0),('071db3e057','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','permissions','DocType',1,0,'Administrator',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('0836c736c5','2017-09-11 12:55:27.597728','2018-05-17 15:33:48.675835','Administrator','faris@erpnext.com',0,'Data Migration Run','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('0852b3e7f1','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','permissions','DocType',6,0,'Maintenance Manager',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('0aed847083','2013-05-24 13:41:00.000000','2018-05-17 15:33:46.746140','Administrator','Administrator',0,'Note','permissions','DocType',1,0,'All',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('0df3e4de08','2017-01-27 15:43:33.780808','2018-05-17 15:33:43.465640','Administrator','Administrator',0,'Feedback Request','permissions','DocType',1,0,'System Manager',NULL,1,0,0,0,0,0,0,1,1,0,1,1,1,NULL,0,0,0),('0e65242654','2013-01-10 16:34:01.000000','2018-05-17 15:33:46.459140','Administrator','Administrator',0,'Custom Script','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('0e947f40a3','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','permissions','DocType',1,0,'System Manager',NULL,1,0,1,0,0,1,0,1,0,0,1,0,1,NULL,0,0,0),('0ffe2e1d80','2013-01-10 16:34:24.000000','2018-05-17 15:33:48.135202','Administrator','Administrator',0,'Print Heading','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('129bf6de35','2017-01-11 04:21:35.217943','2018-05-17 15:33:43.677739','Administrator','Administrator',0,'Custom DocPerm','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('19cfa1bea9','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','permissions','DocType',12,1,'All','',1,0,0,0,0,0,0,1,0,0,0,0,0,NULL,0,0,0),('1a5a488113','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','permissions','DocType',3,0,'All',NULL,1,0,0,0,0,1,0,0,0,0,0,0,1,'[\"Email Account\"]',1,0,1),('1c5b6efae3','2015-12-15 22:26:45.221162','2018-05-17 15:33:44.000821','Administrator','Administrator',0,'Payment Gateway','permissions','DocType',1,0,'System Manager',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('1fd0a5f5a6','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','permissions','DocType',1,0,'Sales User',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('1fe460b662','2017-04-24 09:53:41.813982','2018-05-17 15:33:47.580015','Administrator','Administrator',0,'GSuite Templates','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('215395bb94','2017-08-11 05:11:49.975801','2018-05-17 15:33:48.805591','Administrator','Administrator',0,'Data Migration Mapping','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('22eea9611e','2014-08-22 16:12:17.249590','2018-05-17 15:33:44.077470','Administrator','Administrator',0,'Language','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,0,0,0,NULL,0,0,0),('2846c76ffc','2013-01-28 10:06:02.000000','2018-05-17 15:33:46.538180','Administrator','Administrator',0,'Currency','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('2b6f19cf2b','2015-02-04 04:33:36.330477','2018-05-17 15:33:42.835173','Administrator','Administrator',0,'DocShare','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,1,1,0,0,NULL,0,0,0),('2cab622ba4','2013-01-10 16:34:03.000000','2018-05-17 15:33:42.969016','Administrator','Administrator',0,'Module Def','permissions','DocType',1,0,'Administrator',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('33effb0153','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('396053c514','2017-08-28 16:04:38.088230','2018-05-17 15:34:43.262572','Administrator','Administrator',0,'Bench Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('3a1de601f8','2015-03-18 06:15:59.321619','2018-05-17 15:33:45.955127','Administrator','Administrator',0,'Email Group Member','permissions','DocType',1,0,'Newsletter Manager',NULL,1,1,1,0,0,1,0,1,1,1,1,1,1,NULL,0,0,0),('3af732bdca','2017-03-13 09:20:56.387135','2018-05-17 15:33:45.930240','Administrator','Administrator',0,'Email Rule','permissions','DocType',1,0,'System Manager',NULL,1,0,0,0,0,0,0,1,1,0,1,1,1,NULL,0,0,0),('3b7dab0f44','2016-09-21 10:12:57.399174','2018-05-17 15:33:47.640308','Administrator','Administrator',0,'Dropbox Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,1,0,1,1,1,NULL,0,0,0),('3c95fac7ca','2013-01-19 10:23:30.000000','2018-05-17 15:33:46.593643','Administrator','Administrator',0,'Country','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('3d89f4336b','2013-01-29 17:55:08.000000','2018-05-17 15:33:46.495443','Administrator','Administrator',0,'Customize Form','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,0,0,0,0,0,1,1,1,NULL,0,0,0),('3dba811927','2012-12-28 10:49:55.000000','2018-05-17 15:33:45.395726','Administrator','Administrator',0,'Workflow','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('3fc20ef6c5','2014-06-19 05:20:26.331041','2018-05-17 15:33:46.049180','Administrator','Administrator',0,'Standard Reply','permissions','DocType',1,0,'All',NULL,1,1,1,0,0,0,0,0,0,0,1,0,0,NULL,0,0,0),('4090c72069','2017-08-03 14:45:11.981784','2018-05-17 15:34:43.132154','Administrator','Administrator',0,'Site','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('40f267b9f9','2017-04-10 12:17:58.071915','2018-05-17 15:33:48.503675','Administrator','Administrator',0,'Salutation','permissions','DocType',2,0,'All',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('4255de227c','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','permissions','DocType',1,0,'Administrator',NULL,1,1,1,0,0,0,0,0,0,0,1,1,1,NULL,0,0,0),('435b5e07da','2016-09-22 04:16:48.829658','2018-05-17 15:33:48.026461','Administrator','Administrator',0,'LDAP Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,1,0,1,1,1,NULL,0,0,0),('446bef2f24','2013-03-08 09:41:11.000000','2018-05-17 15:33:45.136706','Administrator','Administrator',0,'Blog Category','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,1,0),('4686bc1322','2014-07-11 17:18:09.923399','2018-05-17 15:33:45.854556','Administrator','Administrator',0,'Email Alert','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,0,0,NULL,0,0,0),('48afa196b8','2013-01-10 16:34:31.000000','2018-05-17 15:33:45.513587','Administrator','Administrator',0,'Newsletter','permissions','DocType',1,0,'Newsletter Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,1,0),('4aadea9f32','2017-08-22 17:15:31.459897','2018-05-17 15:34:43.427815','Administrator','Administrator',0,'Bench Manager Command','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('4bb57e1cab','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','permissions','DocType',1,0,'Administrator',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('4d744f8ae6','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','permissions','DocType',10,0,'Maintenance User',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('4dace4de93','2015-11-28 00:57:39.766888','2018-05-17 15:33:43.762574','Administrator','Administrator',0,'Error Snapshot','permissions','DocType',1,0,'Administrator',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('4fff1a0c9d','2016-12-09 14:27:32.720061','2018-05-17 15:33:43.627900','Administrator','Administrator',0,'Data Import','permissions','DocType',1,0,'System Manager',NULL,1,1,1,1,0,1,0,0,0,0,1,0,1,NULL,0,0,0),('50ce795c0d','2016-02-17 12:21:16.175465','2018-05-17 15:33:43.395191','Administrator','Administrator',0,'Translation','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('52d60dab0c','2013-01-10 16:34:04.000000','2018-05-17 15:33:42.420279','Administrator','Administrator',0,'Property Setter','permissions','DocType',2,0,'System Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('537b958c71','2017-08-17 01:25:56.910716','2018-05-17 15:33:48.101538','Administrator','Administrator',0,'Print Style','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('553861b2d2','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','permissions','DocType',8,0,'Sales User',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('5610fbbab6','2013-03-25 16:00:51.000000','2018-05-17 15:33:44.781044','Administrator','Administrator',0,'Blogger','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,1,0,1,1,1,1,1,1,NULL,0,1,0),('56e498251a','2017-05-03 16:28:11.295095','2018-05-17 15:33:43.606890','Administrator','Administrator',0,'Domain Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('5923f5158a','2017-04-10 12:11:36.526508','2018-05-17 15:33:48.535494','Administrator','Administrator',0,'Gender','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('5ad5ffeeb8','2013-01-19 10:23:30.000000','2018-05-17 15:33:46.593643','Administrator','Administrator',0,'Country','permissions','DocType',2,0,'All',NULL,1,0,0,0,0,0,0,1,0,0,0,1,1,NULL,0,0,0),('5c307be9b6','2017-07-17 14:25:27.881871','2018-05-17 15:33:42.627169','Administrator','Administrator',0,'User Permission','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('5d24007f73','2016-09-21 08:03:01.009852','2018-05-17 15:33:47.615721','Administrator','Administrator',0,'PayPal Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,1,0,1,1,1,NULL,0,0,0),('5d8c873423','2014-02-20 17:22:37.000000','2018-05-17 15:33:42.687506','Administrator','Administrator',0,'Version','permissions','DocType',1,0,'System Manager',NULL,1,0,0,0,0,0,0,1,1,0,0,0,0,NULL,0,0,0),('5e73e6d0d8','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,1,1,1,1,NULL,0,0,0),('5f0c9fd4d7','2013-01-10 16:34:03.000000','2018-05-17 15:33:42.969016','Administrator','Administrator',0,'Module Def','permissions','DocType',2,0,'System Manager',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('609f2d502f','2017-02-13 14:53:36.240122','2018-05-17 15:33:43.077118','Administrator','Administrator',0,'Custom Role','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('609f4e217e','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','permissions','DocType',3,0,'All',NULL,1,0,0,0,0,1,0,0,0,0,0,0,1,'[\"Email Account\"]',1,0,1),('60fe6cdb7a','2016-08-04 04:58:40.457416','2018-05-17 15:33:47.300993','Administrator','Administrator',0,'Integration Request','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('62f87c301e','2016-05-25 09:49:07.125394','2018-05-17 15:33:43.025756','Administrator','Administrator',0,'Tag Category','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,1,1,0,0,0,NULL,0,0,0),('648064b4f2','2016-04-20 15:29:39.785172','2018-05-17 15:33:45.989784','Administrator','Administrator',0,'Email Flag Queue','permissions','DocType',1,0,'System Manager',NULL,1,0,0,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('65aa28e2f9','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','permissions','DocType',5,0,'Purchase Manager',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('689c2be2f9','2016-02-22 03:47:45.387068','2018-05-17 15:33:46.821744','Administrator','Administrator',0,'Desktop Icon','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('6947f9b175','2016-09-03 11:42:42.575525','2018-05-17 15:33:47.140631','Administrator','Administrator',0,'OAuth Provider Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('69da3d6387','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','permissions','DocType',2,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,1,1,1,1,NULL,0,0,0),('6c67b1740c','2017-08-11 05:03:27.091416','2018-05-17 15:33:48.613509','Administrator','Administrator',0,'Data Migration Connector','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('6c6a7ddfea','2017-01-24 15:46:38.366213','2018-05-17 15:33:42.762509','Administrator','Administrator',0,'Feedback Trigger','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('6d2fbc0f34','2012-11-22 17:45:46.000000','2018-05-17 15:33:48.057067','Administrator','Administrator',0,'Letter Head','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('6d4fcfba6f','2013-01-17 11:36:45.000000','2018-05-17 15:33:43.372622','Administrator','Administrator',0,'Patch Log','permissions','DocType',1,0,'Administrator',NULL,1,0,0,0,0,0,0,1,0,0,0,1,1,NULL,0,0,0),('6d854e529a','2017-10-23 13:02:10.295824','2018-05-17 15:33:46.879565','Administrator','faris@erpnext.com',0,'Calendar View','permissions','DocType',2,0,'All',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('6df8a8472d','2013-03-07 15:53:15.000000','2018-05-17 15:33:45.003160','Administrator','Administrator',0,'Website Slideshow','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('6e085e7b17','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','permissions','DocType',4,0,'All',NULL,1,0,0,0,0,0,0,1,0,0,0,1,1,NULL,0,0,0),('70fe9a6033','2017-02-13 17:33:25.157332','2018-05-17 15:33:44.054341','Administrator','Administrator',0,'Role Permission for Page and Report','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('769226bb6d','2013-01-10 16:34:04.000000','2018-05-17 15:33:42.420279','Administrator','Administrator',0,'Property Setter','permissions','DocType',1,0,'Administrator',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('7802580bcd','2016-12-29 07:48:06.319665','2018-05-17 15:33:45.075668','Administrator','Administrator',0,'Website Sidebar','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('78458027ec','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','permissions','DocType',3,0,'Purchase Master Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('78cc6c268b','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','permissions','DocType',2,1,'All',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('7a855139a1','2016-08-24 14:07:21.955052','2018-05-17 15:33:47.530466','Administrator','Administrator',0,'OAuth Client','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('7af2b41da6','2017-09-08 16:16:13.060641','2018-05-17 15:33:47.488876','Administrator','Administrator',0,'Webhook','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('7e34740cb0','2017-04-10 12:17:58.071915','2018-05-17 15:33:48.503675','Administrator','Administrator',0,'Salutation','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('812d56cb1b','2012-12-28 10:49:56.000000','2018-05-17 15:33:45.302169','Administrator','Administrator',0,'Workflow State','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('8230258e59','2017-04-10 12:17:58.071915','2018-05-17 15:33:48.503675','Administrator','Administrator',0,'Salutation','permissions','DocType',3,0,'Administrator',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('82cf989380','2017-04-10 12:11:36.526508','2018-05-17 15:33:48.535494','Administrator','Administrator',0,'Gender','permissions','DocType',2,0,'All',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('82f1bc7ddd','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','permissions','DocType',3,0,'Maintenance User',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('8314ef0b40','2017-04-21 16:57:30.264478','2018-05-17 15:33:47.429402','Administrator','Administrator',0,'GSuite Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('834399e283','2017-03-09 17:18:29.458397','2018-05-17 15:33:47.156384','Administrator','Administrator',0,'Stripe Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('841ee1e9b4','2017-10-16 17:13:05.684227','2018-05-17 15:33:47.076864','Administrator','Administrator',0,'Google Maps','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('8695785969','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','permissions','DocType',4,0,'Accounts User',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('875a5641b4','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','permissions','DocType',4,0,'Sales Manager',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('8a3ed1fa5a','2012-12-28 10:49:56.000000','2018-05-17 15:33:45.367553','Administrator','Administrator',0,'Workflow Action','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('8a48cb56af','2013-01-08 15:50:01.000000','2018-05-17 15:33:42.301437','Administrator','Administrator',0,'Role','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('8bd5511796','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.352520','Administrator','Administrator',0,'Address','permissions','DocType',2,0,'Purchase User',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('8c33faa28a','2016-09-01 01:34:34.985457','2018-05-17 15:33:45.567114','Administrator','Administrator',0,'Auto Email Report','permissions','DocType',2,0,'Report Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('8d58e82c9d','2013-01-10 16:34:24.000000','2018-05-17 15:33:43.053495','Administrator','Administrator',0,'SMS Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,0,0,0,0,0,1,0,0,NULL,0,0,0),('8ebe69120f','2014-09-01 14:08:48.624556','2018-05-17 15:33:42.463834','Administrator','Administrator',0,'Web Form','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,0,0,NULL,0,0,0),('8fa3865ea3','2016-10-19 12:26:04.809812','2018-05-17 15:33:46.932403','Administrator','Administrator',0,'Kanban Board','permissions','DocType',1,0,'All',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('8fc4ebcd6b','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,1,1,1,1,NULL,0,0,0),('926680acc8','2013-06-10 13:17:47.000000','2018-05-17 15:33:46.667443','Administrator','Administrator',0,'Event','permissions','DocType',1,0,'All',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('98d17dfbe3','2016-08-24 14:12:13.647159','2018-05-17 15:33:47.360639','Administrator','Administrator',0,'OAuth Authorization Code','permissions','DocType',1,0,'System Manager',NULL,1,0,0,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('9a6602c465','2013-04-30 12:58:46.000000','2018-05-17 15:33:44.887332','Administrator','Administrator',0,'Website Settings','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,0,0,0,0,0,1,1,1,NULL,0,0,0),('9bdb3ab62c','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,1,0,0,0,0,0,0,0,NULL,0,0,0),('9d2304e767','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','permissions','DocType',3,0,'Guest',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('9e8a95122f','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','permissions','DocType',2,0,'Blogger',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('a029843b80','2017-02-13 17:33:25.157332','2018-05-17 15:33:43.349585','Administrator','Administrator',0,'User Permission for Page and Report','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('a0e6432f75','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','permissions','DocType',1,0,'System Manager',NULL,1,0,1,0,0,1,0,1,0,0,1,0,1,NULL,0,0,0),('a12e95e751','2013-03-08 09:41:11.000000','2018-05-17 15:33:45.136706','Administrator','Administrator',0,'Blog Category','permissions','DocType',2,0,'Blogger',NULL,1,0,0,0,0,0,0,0,0,0,0,1,1,NULL,0,0,0),('a6f5384b15','2012-11-22 17:45:46.000000','2018-05-17 15:33:48.057067','Administrator','Administrator',0,'Letter Head','permissions','DocType',2,0,'All',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('a73a51e892','2017-10-23 13:02:10.295824','2018-05-17 15:33:46.879565','Administrator','faris@erpnext.com',0,'Calendar View','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('a7c99b0781','2015-03-18 06:08:32.729800','2018-05-17 15:33:46.258530','Administrator','Administrator',0,'Email Group','permissions','DocType',1,0,'Newsletter Manager',NULL,1,1,1,0,0,1,0,1,1,1,1,1,1,NULL,0,0,0),('a8f0de92ef','2016-09-20 03:44:03.799402','2018-05-17 15:33:47.340008','Administrator','Administrator',0,'Razorpay Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,1,0,1,1,1,NULL,0,0,0),('a9e956690b','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','permissions','DocType',7,0,'Accounts Manager',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('aa4e50c700','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,0,0,0,1,1,NULL,0,0,0),('ab10cf521e','2013-01-29 10:47:14.000000','2018-05-17 15:33:43.809453','Administrator','Administrator',0,'Communication','permissions','DocType',2,1,'System Manager',NULL,1,0,0,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('abc016eecc','2013-01-10 16:34:01.000000','2018-05-17 15:33:42.348456','Administrator','Administrator',0,'Custom Field','permissions','DocType',2,0,'System Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('abedf31074','2016-12-29 12:59:48.638970','2018-05-17 15:33:43.427855','Administrator','Administrator',0,'Deleted Document','permissions','DocType',1,0,'System Manager',NULL,1,0,0,0,0,1,0,0,1,0,0,0,0,NULL,0,0,0),('affeaa2704','2017-10-05 11:10:38.780133','2018-05-17 15:33:43.111557','Administrator','Administrator',0,'Activity Log','permissions','DocType',2,1,'System Manager',NULL,1,0,0,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('b288363f5f','2017-05-03 15:07:39.752820','2018-05-17 15:33:42.894332','Administrator','makarand@erpnext.com',0,'Domain','permissions','DocType',1,0,'Administrator',NULL,1,1,1,0,0,0,0,1,1,0,1,1,1,NULL,0,0,0),('ba6b057c69','2016-04-14 09:41:45.892975','2018-05-17 15:33:46.225853','Administrator','Administrator',0,'Unhandled Email','permissions','DocType',1,0,'System Manager',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('bcdc257c28','2013-03-09 15:45:57.000000','2018-05-17 15:33:43.538275','Administrator','Administrator',0,'Report','permissions','DocType',2,0,'System Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('bd459d2da8','2014-04-17 16:53:52.640856','2018-05-17 15:33:44.108162','Administrator','Administrator',0,'System Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,0,0,0,0,0,1,0,0,NULL,0,0,0),('bf5d4830c3','2012-12-20 17:16:49.000000','2018-05-17 15:33:43.308215','Administrator','Administrator',0,'Page','permissions','DocType',2,0,'System Manager',NULL,1,1,0,0,0,0,0,0,0,0,1,1,1,NULL,0,0,0),('bfc84515b2','2014-07-17 06:54:20.782907','2018-05-17 15:33:48.234664','Administrator','Administrator',0,'Print Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,0,0,0,0,0,1,0,0,NULL,0,0,0),('bfe7898938','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','permissions','DocType',2,0,'Sales Master Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('c0b2ca544a','2014-06-05 02:22:36.029850','2018-05-17 15:33:48.310068','Administrator','Administrator',0,'Address Template','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,0,0,NULL,0,1,0),('c2bce61e02','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','permissions','DocType',11,0,'Accounts User',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('c525e75fe8','2013-01-28 10:06:02.000000','2018-05-17 15:33:46.538180','Administrator','Administrator',0,'Currency','permissions','DocType',3,0,'Sales User',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('c7e069fa1c','2015-03-18 09:41:20.216319','2018-05-17 15:33:46.333335','Administrator','Administrator',0,'Email Unsubscribe','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('c88d86b378','2013-01-23 19:54:43.000000','2018-05-17 15:33:48.169058','Administrator','Administrator',0,'Print Format','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('c8c13beba4','2014-03-04 08:29:52.000000','2018-05-17 15:33:47.396991','Administrator','Administrator',0,'Social Login Keys','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,0,0,0,0,0,1,0,0,NULL,0,0,0),('c96d835224','2012-07-03 13:30:35.000000','2018-05-17 15:33:46.970812','Administrator','Administrator',0,'ToDo','permissions','DocType',1,0,'All',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('ce2f4ca877','2014-02-20 17:22:37.000000','2018-05-17 15:33:42.687506','Administrator','Administrator',0,'Version','permissions','DocType',2,0,'Administrator',NULL,1,0,0,0,0,1,0,0,0,0,0,0,0,NULL,0,0,0),('cf8543a3ac','2014-10-30 14:23:30.958074','2018-05-17 15:33:45.170674','Administrator','Administrator',0,'Help Category','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('d00a9d5ef2','2014-03-11 14:55:00.000000','2018-05-17 15:33:42.186235','Administrator','Administrator',0,'User','permissions','DocType',2,1,'System Manager',NULL,1,1,0,0,0,0,0,1,0,0,0,0,0,NULL,0,0,0),('d45ed9282c','2016-08-24 14:10:17.471264','2018-05-17 15:33:47.989214','Administrator','Administrator',0,'OAuth Bearer Token','permissions','DocType',1,0,'System Manager',NULL,1,0,0,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('d6000f33ea','2017-08-17 14:51:33.577230','2018-05-17 15:34:43.329531','Administrator','Administrator',0,'Site Backup','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('d8fcc35e4e','2017-09-04 20:57:20.129205','2018-05-17 15:33:47.174204','Administrator','Administrator',0,'S3 Backup Settings','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('ddc31f6eea','2013-01-10 16:34:24.000000','2018-05-17 15:33:48.135202','Administrator','Administrator',0,'Print Heading','permissions','DocType',2,0,'All',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('ddc61c5877','2014-09-11 12:04:34.163728','2018-05-17 15:33:46.088085','Administrator','Administrator',0,'Email Account','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,0,0,0,NULL,0,1,0),('deefda74c3','2013-01-28 10:06:02.000000','2018-05-17 15:33:46.538180','Administrator','Administrator',0,'Currency','permissions','DocType',2,0,'Accounts User',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('e0574eb9d3','2013-01-28 10:06:02.000000','2018-05-17 15:33:46.538180','Administrator','Administrator',0,'Currency','permissions','DocType',4,0,'Purchase User',NULL,1,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0),('e134aa1c09','2016-07-15 05:51:29.224123','2018-05-17 15:33:46.911760','Administrator','Administrator',0,'Bulk Update','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('e4be6ac7f1','2017-08-31 04:16:38.764465','2018-05-17 15:33:43.506765','Administrator','Administrator',0,'Role Profile','permissions','DocType',2,1,'System Manager',NULL,1,1,0,0,0,0,0,1,1,0,1,1,1,NULL,0,0,0),('e5a8fbf350','2015-02-18 12:46:38.168929','2018-05-17 15:33:44.647102','Administrator','Administrator',0,'Website Theme','permissions','DocType',2,0,'Administrator',NULL,1,1,1,0,0,1,0,0,1,1,0,0,0,NULL,0,0,0),('e60779a092','2013-02-18 13:36:19.000000','2018-05-17 15:33:42.034975','Administrator','Administrator',0,'DocType','permissions','DocType',2,0,'Administrator',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('e684f127ed','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.707916','Administrator','Administrator',0,'Blog Post','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,1,0),('e7206c745f','2017-08-31 04:16:38.764465','2018-05-17 15:33:43.506765','Administrator','Administrator',0,'Role Profile','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('e870848fbb','2017-06-26 10:57:19.976624','2018-05-17 15:33:42.924877','Administrator','Administrator',0,'Test Runner','permissions','DocType',1,0,'Administrator',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('eb5563f722','2016-03-30 01:40:20.001775','2018-05-17 15:33:44.625767','Administrator','Administrator',0,'Portal Settings','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,1,0,0,0,0,1,1,1,NULL,0,0,0),('f101bf676e','2013-03-28 10:35:30.000000','2018-05-17 15:33:44.818940','Administrator','Administrator',0,'Web Page','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,1,0,0,0,1,1,0,0,NULL,0,0,0),('f1ab1a619f','2013-01-16 13:09:40.000000','2018-05-17 15:33:43.738664','Administrator','Administrator',0,'Error Log','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('f1bcede0da','2013-03-19 12:02:15.000000','2018-05-17 15:33:44.755304','Administrator','Administrator',0,'About Us Settings','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,0,0,0,0,0,1,1,1,NULL,0,0,0),('f2f968bb6c','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','permissions','DocType',9,0,'Purchase User',NULL,1,1,1,0,0,0,0,1,0,0,1,1,1,NULL,0,0,0),('f3c297c882','2014-06-19 05:20:26.331041','2018-05-17 15:33:46.049180','Administrator','Administrator',0,'Standard Reply','permissions','DocType',2,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,1,1,0,1,NULL,0,0,0),('f42fbac320','2017-08-03 18:38:10.895126','2018-05-17 15:34:43.376075','Administrator','Administrator',0,'App','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,0),('f567fbb4d2','2013-01-10 16:34:32.000000','2018-05-17 15:33:48.423782','Administrator','Administrator',0,'Contact','permissions','DocType',1,0,'System Manager',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('f6559239dd','2012-12-27 11:51:24.000000','2018-05-17 15:33:45.061220','Administrator','Administrator',0,'Website Script','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,0,0,0,0,0,1,1,1,NULL,0,0,0),('f855c318b4','2012-12-12 11:19:22.000000','2018-05-17 15:33:43.225885','Administrator','Administrator',0,'File','permissions','DocType',2,0,'All',NULL,1,1,1,0,0,1,0,1,1,0,1,1,1,NULL,0,0,1),('faaf81b63c','2012-08-02 15:17:28.000000','2018-05-17 15:33:45.787592','Administrator','Administrator',0,'Email Queue','permissions','DocType',1,0,'System Manager',NULL,1,0,0,0,0,1,0,1,0,0,0,1,1,NULL,0,0,0),('fc1a81923d','2013-03-11 17:48:16.000000','2018-05-17 15:33:44.495707','Administrator','Administrator',0,'Blog Settings','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,0,0,0,0,0,1,1,1,NULL,0,0,0),('fd28ee02a3','2013-01-10 16:34:01.000000','2018-05-17 15:33:46.459140','Administrator','Administrator',0,'Custom Script','permissions','DocType',2,0,'Administrator',NULL,1,1,1,0,0,1,0,1,0,0,1,1,1,NULL,0,0,0),('ff39fc5782','2013-02-21 20:12:42.000000','2018-05-17 15:33:44.591350','Administrator','Administrator',0,'Contact Us Settings','permissions','DocType',1,0,'Website Manager',NULL,1,1,1,0,0,0,0,0,0,0,1,1,1,NULL,0,0,0),('ff54aeb542','2014-10-30 14:25:53.780105','2018-05-17 15:33:44.517939','Administrator','Administrator',0,'Help Article','permissions','DocType',2,0,'Knowledge Base Contributor',NULL,1,1,1,0,0,0,0,0,0,0,0,0,0,NULL,1,0,0);
/*!40000 ALTER TABLE `tabDocPerm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabDocShare`
--

DROP TABLE IF EXISTS `tabDocShare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabDocShare` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `everyone` int(1) NOT NULL DEFAULT '0',
  `share_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `read` int(1) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `share` int(1) NOT NULL DEFAULT '0',
  `write` int(1) NOT NULL DEFAULT '0',
  `notify_by_email` int(1) NOT NULL DEFAULT '1',
  `user` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `share_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `share_name` (`share_name`),
  KEY `user` (`user`),
  KEY `share_doctype` (`share_doctype`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`),
  KEY `user_share_doctype_index` (`user`,`share_doctype`),
  KEY `share_doctype_share_name_index` (`share_doctype`,`share_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabDocShare`
--

LOCK TABLES `tabDocShare` WRITE;
/*!40000 ALTER TABLE `tabDocShare` DISABLE KEYS */;
INSERT INTO `tabDocShare` VALUES ('256330063d','2018-05-17 15:33:51.065927','2018-05-17 15:33:52.116726','Administrator','Administrator',0,NULL,NULL,NULL,0,0,'Administrator',1,NULL,NULL,1,1,1,'Administrator','User',NULL,NULL),('4bb7111b72','2018-05-17 15:33:51.181181','2018-05-17 15:33:51.181181','Administrator','Administrator',0,NULL,NULL,NULL,0,0,'Guest',1,NULL,NULL,1,1,1,'Guest','User',NULL,NULL);
/*!40000 ALTER TABLE `tabDocShare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabDocType`
--

DROP TABLE IF EXISTS `tabDocType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabDocType` (
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `search_fields` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issingle` int(1) NOT NULL DEFAULT '0',
  `istable` int(1) NOT NULL DEFAULT '0',
  `editable_grid` int(1) NOT NULL DEFAULT '1',
  `track_changes` int(1) NOT NULL DEFAULT '1',
  `module` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `restrict_to_domain` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `autoname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_case` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timeline_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'modified',
  `sort_order` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'DESC',
  `description` text COLLATE utf8mb4_unicode_ci,
  `colour` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `read_only` int(1) NOT NULL DEFAULT '0',
  `in_create` int(1) NOT NULL DEFAULT '0',
  `menu_index` int(11) DEFAULT NULL,
  `parent_node` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smallicon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_copy` int(1) NOT NULL DEFAULT '0',
  `allow_rename` int(1) NOT NULL DEFAULT '0',
  `allow_import` int(1) NOT NULL DEFAULT '0',
  `hide_toolbar` int(1) NOT NULL DEFAULT '0',
  `hide_heading` int(1) NOT NULL DEFAULT '0',
  `track_seen` int(1) NOT NULL DEFAULT '0',
  `max_attachments` int(11) NOT NULL DEFAULT '0',
  `print_outline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `read_only_onload` int(1) NOT NULL DEFAULT '0',
  `document_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag_fields` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_last_update` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `engine` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'InnoDB',
  `default_print_format` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_submittable` int(1) NOT NULL DEFAULT '0',
  `show_name_in_global_search` int(1) NOT NULL DEFAULT '0',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `custom` int(1) NOT NULL DEFAULT '0',
  `beta` int(1) NOT NULL DEFAULT '0',
  `image_view` int(1) NOT NULL DEFAULT '0',
  `has_web_view` int(1) NOT NULL DEFAULT '0',
  `allow_guest_to_view` int(1) NOT NULL DEFAULT '0',
  `route` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `quick_entry` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `module` (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabDocType`
--

LOCK TABLES `tabDocType` WRITE;
/*!40000 ALTER TABLE `tabDocType` DISABLE KEYS */;
INSERT INTO `tabDocType` VALUES ('About Us Settings','2013-03-19 12:02:15.000000','2016-12-29 14:40:41.692119','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,1,0,0,1,'Website',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Settings for the About Us Page',NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Other','fa fa-group',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('About Us Team Member','2013-03-07 11:55:11.000000','2016-07-11 03:27:57.756510','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,1,1,0,'Website',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Activity Log','2017-10-05 11:10:38.780133','2017-11-21 12:39:23.659308','Administrator','Administrator',0,NULL,NULL,NULL,0,'subject',0,0,0,1,'Core',NULL,NULL,'','','subject',NULL,NULL,'modified','DESC','Keep track of all update feeds',NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,1,0,NULL,0,'Setup','fa fa-comment',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Address','2013-01-10 16:34:32.000000','2017-06-21 11:30:20.719590','Administrator','Administrator',0,NULL,NULL,NULL,5,'country, state',0,0,0,0,'Contacts',NULL,NULL,NULL,'Title Case',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,1,0,0,0,0,NULL,0,'Setup','fa fa-map-marker',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Address Template','2014-06-05 02:22:36.029850','2017-04-10 13:09:53.761009','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,0,'Contacts',NULL,NULL,'field:country','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'Setup','fa fa-map-marker',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('App','2017-08-03 18:38:10.895126','2017-09-21 12:04:19.803797','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Bench Manager',NULL,NULL,'field:app_name','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Auto Email Report','2016-09-01 01:34:34.985457','2017-06-30 12:54:13.350902','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Email',NULL,NULL,'','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Bench Manager Command','2017-08-22 17:15:31.459897','2017-08-31 18:37:02.085199','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Bench Manager',NULL,NULL,'field:key','','',NULL,NULL,'modified','DESC',NULL,NULL,0,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Bench Settings','2017-08-28 16:04:38.088230','2017-09-25 02:30:28.788340','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,1,'Bench Manager',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Block Module','2015-03-24 14:28:15.882903','2017-10-31 19:36:18.586834','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,0,'Core',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Other',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Blog Category','2013-03-08 09:41:11.000000','2017-03-06 16:29:05.035486','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Website',NULL,NULL,'field:category_name',NULL,NULL,NULL,NULL,NULL,'DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'Setup','fa fa-tag',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,1,1,NULL,'published',NULL,NULL,NULL,1),('Blog Post','2013-03-28 10:35:30.000000','2017-03-06 16:25:33.410910','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Website',NULL,NULL,NULL,NULL,'title',NULL,NULL,NULL,'ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,5,NULL,0,'Setup','fa fa-quote-left',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,1,1,'/blog','published',NULL,NULL,NULL,0),('Blog Settings','2013-03-11 17:48:16.000000','2016-12-29 14:40:41.629468','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,1,0,0,1,'Website',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Blog Settings',NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,NULL,'fa fa-cog',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Blogger','2013-03-25 16:00:51.000000','2016-12-29 14:40:40.407657','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Website',NULL,NULL,'field:short_name',NULL,'full_name',NULL,NULL,NULL,NULL,'User ID of a Blogger',NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,1,NULL,0,'Setup','fa fa-user',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Bulk Update','2016-07-15 05:51:29.224123','2016-12-29 14:40:31.929701','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,1,'Desk',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Calendar View','2017-10-23 13:02:10.295824','2017-11-14 14:14:11.544811','Administrator','faris@erpnext.com',0,NULL,NULL,NULL,0,NULL,0,0,1,0,'Desk',NULL,NULL,'Prompt','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Communication','2013-01-29 10:47:14.000000','2017-11-13 12:00:44.238575','Administrator','Administrator',0,NULL,NULL,NULL,1,'subject',0,0,0,1,'Core',NULL,NULL,'',NULL,'subject',NULL,NULL,NULL,'DESC','Keep a track of all communications',NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,1,0,NULL,0,'Setup','fa fa-comment',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Company History','2013-02-22 01:28:08.000000','2016-07-11 03:27:58.848351','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,1,1,0,'Website',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Contact','2013-01-10 16:34:32.000000','2017-06-21 17:17:44.694188','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,0,'Contacts',NULL,NULL,NULL,'Title Case',NULL,'image',NULL,NULL,'ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,1,0,0,0,0,NULL,0,'Setup','fa fa-user',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Contact Us Settings','2013-02-21 20:12:42.000000','2017-09-04 21:41:55.580325','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,1,0,0,1,'Website',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Settings for Contact Us Page',NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,NULL,'fa fa-cog',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Country','2013-01-19 10:23:30.000000','2016-12-29 14:40:34.951894','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Geo',NULL,NULL,'field:country_name',NULL,NULL,NULL,NULL,'country_name','ASC',NULL,NULL,0,0,0,NULL,NULL,0,1,1,0,0,0,0,NULL,0,'Setup','fa fa-globe',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Currency','2013-01-28 10:06:02.000000','2016-12-29 14:40:39.187557','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Geo',NULL,NULL,'field:currency_name',NULL,NULL,NULL,NULL,NULL,'DESC','**Currency** Master',NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup','fa fa-bitcoin',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Custom DocPerm','2017-01-11 04:21:35.217943','2017-01-11 04:21:35.217943','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,0,'Core',NULL,NULL,'hash','',NULL,NULL,NULL,'modified','ASC',NULL,NULL,1,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Custom Field','2013-01-10 16:34:01.000000','2017-10-24 11:40:37.986457','Administrator','Administrator',0,NULL,NULL,NULL,1,'dt,label,fieldtype,options',0,0,0,1,'Custom',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ASC','Adds a custom field to a DocType',NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'Setup','fa fa-glass',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Custom Role','2017-02-13 14:53:36.240122','2017-03-20 12:56:09.164494','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,0,'Core',NULL,NULL,'hash','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,1,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Custom Script','2013-01-10 16:34:01.000000','2017-08-17 07:43:08.093341','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Custom',NULL,NULL,'CustomScript.####',NULL,NULL,NULL,NULL,NULL,'ASC','Adds a custom script (client or server) to a DocType',NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'Document','fa fa-glass',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Customize Form','2013-01-29 17:55:08.000000','2017-04-21 16:59:12.752428','Administrator','Administrator',0,NULL,NULL,NULL,1,'doc_type',1,0,1,1,'Custom',NULL,NULL,'DL.####',NULL,NULL,NULL,NULL,NULL,'DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,1,0,0,0,NULL,0,'Document','fa fa-glass',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Customize Form Field','2013-02-22 01:27:32.000000','2017-10-24 11:41:31.075929','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,1,1,0,'Custom',NULL,NULL,'hash',NULL,NULL,NULL,NULL,NULL,'ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Data Import','2016-12-09 14:27:32.720061','2017-12-15 14:49:24.622128','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Core',NULL,NULL,'','','',NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,1,0,0,0,0,1,1,NULL,0,'Document',NULL,NULL,NULL,NULL,'InnoDB',NULL,1,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Data Migration Connector','2017-08-11 05:03:27.091416','2017-12-01 13:38:55.992499','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Data Migration',NULL,NULL,'field:connector_name','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,1,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Data Migration Mapping','2017-08-11 05:11:49.975801','2017-09-27 18:06:43.275207','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Data Migration',NULL,NULL,'field:mapping_name','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,1,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Data Migration Mapping Detail','2017-08-11 05:09:10.900237','2017-09-28 17:13:31.337005','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,1,'Data Migration',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Data Migration Plan','2017-08-11 05:15:51.482165','2017-09-13 15:47:26.336541','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Data Migration',NULL,NULL,'field:plan_name','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Data Migration Plan Mapping','2017-08-11 05:15:38.390831','2017-09-20 21:43:04.908650','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,1,'Data Migration',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,1,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Data Migration Run','2017-09-11 12:55:27.597728','2017-10-02 05:12:16.094991','Administrator','faris@erpnext.com',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Data Migration',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('DefaultValue','2013-02-22 01:27:32.000000','2016-07-11 03:27:59.126216','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,1,1,0,'Core',NULL,NULL,'hash',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Deleted Document','2016-12-29 12:59:48.638970','2016-12-29 14:39:45.724494','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Core',NULL,NULL,NULL,'','deleted_name',NULL,NULL,'modified','DESC',NULL,NULL,0,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Desktop Icon','2016-02-22 03:47:45.387068','2017-05-08 15:41:31.121652','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Desk',NULL,NULL,NULL,'','module_name',NULL,NULL,'modified','DESC',NULL,NULL,1,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('DocField','2013-02-22 01:27:33.000000','2017-10-24 11:39:56.795852','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,1,1,0,'Core',NULL,NULL,'hash',NULL,NULL,NULL,NULL,NULL,'ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('DocPerm','2013-02-22 01:27:33.000000','2017-03-03 16:18:18.890031','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,1,1,0,'Core',NULL,NULL,'hash',NULL,NULL,NULL,NULL,NULL,'ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('DocShare','2015-02-04 04:33:36.330477','2017-09-15 15:58:34.126438','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Core',NULL,NULL,'hash','',NULL,NULL,NULL,'modified','DESC','Internal record of document shares',NULL,1,1,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'System',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('DocType','2013-02-18 13:36:19.000000','2017-05-03 16:15:40.198072','Administrator','Administrator',0,NULL,NULL,NULL,6,'module',0,0,0,1,'Core',NULL,NULL,'Prompt',NULL,NULL,NULL,NULL,'modified','DESC','DocType is a Table / Form in the application.',NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'Document','fa fa-bolt',NULL,NULL,NULL,'InnoDB',NULL,0,1,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Domain','2017-05-03 15:07:39.752820','2017-09-15 12:26:21.827149','Administrator','makarand@erpnext.com',0,NULL,NULL,NULL,0,'domain',0,0,1,0,'Core',NULL,NULL,'field:domain','','domain',NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Domain Settings','2017-05-03 16:28:11.295095','2017-12-05 17:36:46.842134','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,1,'Core',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Dropbox Settings','2016-09-21 10:12:57.399174','2018-03-22 16:02:00.597029','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,1,'Integrations',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,1,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Dynamic Link','2017-01-13 04:55:18.835023','2017-01-17 14:25:49.140730','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,1,'Core',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Email Account','2014-09-11 12:04:34.163728','2017-03-10 17:00:50.782901','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Email',NULL,NULL,'field:email_account_name','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'Setup','fa fa-inbox',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Email Alert','2014-07-11 17:18:09.923399','2017-09-26 20:10:00.061780','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Email',NULL,NULL,'Prompt','','subject',NULL,NULL,'modified','DESC',NULL,NULL,0,0,0,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'System','fa fa-envelope',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Email Alert Recipient','2014-07-11 17:19:37.037109','2016-12-30 11:09:35.562857','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,0,'Email',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Email Domain','2016-03-29 10:50:48.848239','2016-12-23 13:31:58.408528','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,0,'Email',NULL,NULL,'field:domain_name','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup','icon-inbox',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Email Flag Queue','2016-04-20 15:29:39.785172','2017-09-20 15:27:12.142079','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,0,'Email',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,1,NULL,NULL,NULL,1,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Email Group','2015-03-18 06:08:32.729800','2017-02-27 19:01:17.203845','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Email',NULL,NULL,'field:title','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'Setup',NULL,NULL,NULL,NULL,NULL,NULL,0,1,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Email Group Member','2015-03-18 06:15:59.321619','2017-02-17 17:00:42.551806','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Email',NULL,NULL,'hash','','email',NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'Document',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Email Queue','2012-08-02 15:17:28.000000','2017-09-25 15:39:21.781324','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Email',NULL,NULL,'hash',NULL,NULL,NULL,NULL,NULL,'DESC','Email Queue records.',NULL,0,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System','fa fa-envelope',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Email Queue Recipient','2016-12-08 12:01:07.993900','2016-12-08 14:05:33.578240','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,0,0,'Email',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Email Rule','2017-03-13 09:20:56.387135','2017-03-13 09:26:38.441858','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,0,'Email',NULL,NULL,'field:email_id','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,1,NULL,NULL,NULL,1,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Email Unsubscribe','2015-03-18 09:41:20.216319','2016-12-29 14:40:21.633193','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Email',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Error Log','2013-01-16 13:09:40.000000','2017-03-14 12:21:44.292471','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Core',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'ASC','Log of Scheduler Errors',NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System','fa fa-warning-sign',NULL,NULL,NULL,'MyISAM',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Error Snapshot','2015-11-28 00:57:39.766888','2016-12-29 14:40:38.619106','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Core',NULL,NULL,NULL,'','evalue',NULL,NULL,'timestamp','DESC',NULL,NULL,0,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Event','2013-06-10 13:17:47.000000','2017-08-03 16:34:54.657796','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Desk',NULL,NULL,'EV.#####',NULL,'subject',NULL,NULL,NULL,'DESC',NULL,NULL,1,0,NULL,NULL,NULL,0,0,1,0,0,1,0,NULL,0,'Document','fa fa-calendar',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Feedback Request','2017-01-27 15:43:33.780808','2017-03-03 08:11:09.718589','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Core',NULL,NULL,'','','reference_name',NULL,NULL,'modified','DESC',NULL,NULL,0,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Feedback Trigger','2017-01-24 15:46:38.366213','2017-05-29 16:36:04.178592','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Core',NULL,NULL,'field:document_type','','document_type',NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('File','2012-12-12 11:19:22.000000','2018-01-15 03:41:23.876072','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Core',NULL,NULL,'',NULL,'file_name',NULL,NULL,NULL,'ASC',NULL,NULL,0,0,0,NULL,NULL,0,0,1,0,0,0,0,NULL,0,NULL,'fa fa-file',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Footer Item','2016-04-12 18:40:16.315024','2016-07-11 03:28:00.573336','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,0,'Website',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Other',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Gender','2017-04-10 12:11:36.526508','2017-04-10 12:17:04.848338','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Contacts',NULL,NULL,'field:gender','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Google Maps','2017-10-16 17:13:05.684227','2017-10-16 17:13:05.684227','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,1,'Integrations',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('GSuite Settings','2017-04-21 16:57:30.264478','2017-10-20 16:11:47.757030','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,1,'Integrations',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,1,1,NULL,NULL,NULL,1,0,0,0,0,0,0,NULL,0,'System',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('GSuite Templates','2017-04-24 09:53:41.813982','2017-05-12 16:50:08.074882','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Integrations',NULL,NULL,'field:template_name','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Has Domain','2017-05-03 15:20:22.326623','2017-05-04 11:05:54.750351','Administrator','makarand@erpnext.com',0,NULL,NULL,NULL,0,NULL,0,1,1,1,'Core',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Has Role','2013-02-22 01:27:34.000000','2017-02-13 14:00:08.116312','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,1,1,0,'Core',NULL,NULL,'hash',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Help Article','2014-10-30 14:25:53.780105','2017-03-06 16:27:58.333205','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Website',NULL,NULL,NULL,'','title',NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'','icon-file-alt',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,1,1,NULL,'published',NULL,NULL,NULL,0),('Help Category','2014-10-30 14:23:30.958074','2016-12-29 14:39:56.092427','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Website',NULL,NULL,'','Title Case',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'','icon-list',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Integration Request','2016-08-04 04:58:40.457416','2017-03-08 14:40:00.783063','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Integrations',NULL,NULL,NULL,'','integration_request_service',NULL,NULL,'modified','DESC',NULL,NULL,0,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Kanban Board','2016-10-19 12:26:04.809812','2017-03-14 23:02:13.267243','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,0,'Desk',NULL,NULL,'field:kanban_board_name','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Kanban Board Column','2016-10-19 12:26:42.569185','2017-01-17 15:23:43.520379','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,1,'Desk',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Language','2014-08-22 16:12:17.249590','2016-12-29 14:40:33.210645','Administrator','Administrator',0,NULL,NULL,NULL,0,'language_name',0,0,0,1,'Core',NULL,NULL,'field:language_code','','language_name',NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'Setup','fa fa-globe',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('LDAP Settings','2016-09-22 04:16:48.829658','2017-03-08 17:16:01.087365','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,1,'Integrations',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,1,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Letter Head','2012-11-22 17:45:46.000000','2018-04-21 17:23:55.709575','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Printing',NULL,NULL,'field:letter_head_name',NULL,NULL,NULL,NULL,NULL,'ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,3,NULL,0,'Setup','fa fa-font',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Module Def','2013-01-10 16:34:03.000000','2017-07-13 03:05:28.213656','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Core',NULL,NULL,'field:module_name',NULL,NULL,NULL,NULL,NULL,'ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,NULL,'fa fa-sitemap',NULL,NULL,NULL,'InnoDB',NULL,0,1,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Newsletter','2013-01-10 16:34:31.000000','2017-09-14 15:38:01.891251','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Email',NULL,NULL,'',NULL,'subject',NULL,NULL,NULL,'ASC','Create and Send Newsletters',NULL,0,0,0,NULL,NULL,0,1,0,0,0,0,3,NULL,0,'Other','fa fa-envelope',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,1,0,'newsletters','published',NULL,NULL,NULL,0),('Newsletter Email Group','2017-02-26 16:20:52.654136','2017-02-26 16:23:57.351167','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,1,'Email',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Note','2013-05-24 13:41:00.000000','2017-02-17 17:02:57.095556','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Desk',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ASC','',NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,1,'Document','fa fa-file-text',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Note Seen By','2016-08-29 05:29:16.726172','2016-08-29 06:02:41.531341','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,0,'Desk',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('OAuth Authorization Code','2016-08-24 14:12:13.647159','2017-03-08 14:40:04.113884','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Integrations',NULL,NULL,'field:authorization_code','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Document',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('OAuth Bearer Token','2016-08-24 14:10:17.471264','2017-03-08 14:40:04.209039','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Integrations',NULL,NULL,'field:access_token','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Document',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('OAuth Client','2016-08-24 14:07:21.955052','2017-10-05 21:07:39.476360','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Integrations',NULL,NULL,'','','app_name',NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Document',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('OAuth Provider Settings','2016-09-03 11:42:42.575525','2016-12-29 14:40:30.718685','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,1,'Integrations',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Page','2012-12-20 17:16:49.000000','2017-05-03 17:24:10.162110','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Core',NULL,NULL,'field:page_name',NULL,NULL,NULL,NULL,NULL,'ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'System','fa fa-file',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Patch Log','2013-01-17 11:36:45.000000','2016-12-29 14:40:35.048570','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Core',NULL,NULL,'PATCHLOG.#####',NULL,NULL,NULL,NULL,NULL,NULL,'List of patches executed',NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System','fa fa-cog',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Payment Gateway','2015-12-15 22:26:45.221162','2017-03-09 12:40:56.176464','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,0,'Core',NULL,NULL,'field:gateway','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('PayPal Settings','2016-09-21 08:03:01.009852','2016-12-29 14:40:31.574789','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,1,'Integrations',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,1,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Portal Menu Item','2016-03-30 01:39:20.586927','2017-02-23 00:56:55.063155','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,0,'Website',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Portal Settings','2016-03-30 01:40:20.001775','2016-12-29 14:40:31.476181','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,0,1,'Website',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Print Format','2013-01-23 19:54:43.000000','2017-09-05 14:01:05.658719','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Printing',NULL,NULL,'Prompt',NULL,NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,NULL,'fa fa-print',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Print Heading','2013-01-10 16:34:24.000000','2017-05-03 05:59:09.131569','Administrator','Administrator',0,NULL,NULL,NULL,1,'print_heading',0,0,0,0,'Printing',NULL,NULL,'field:print_heading',NULL,NULL,NULL,NULL,NULL,'DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,1,0,0,0,0,NULL,0,'Setup','fa fa-font',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Print Settings','2014-07-17 06:54:20.782907','2017-08-18 01:04:26.692081','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,0,1,'Printing',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System','fa fa-cog',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Print Style','2017-08-17 01:25:56.910716','2017-08-17 02:18:08.132853','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Printing',NULL,NULL,'field:print_style_name','',NULL,'preview',NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Property Setter','2013-01-10 16:34:04.000000','2016-12-29 14:39:50.172883','Administrator','Administrator',0,NULL,NULL,NULL,1,'doc_type,property',0,0,0,1,'Custom',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'DESC','Property Setter overrides a standard DocType or Field property',NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup','fa fa-glass',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Razorpay Settings','2016-09-20 03:44:03.799402','2016-12-29 14:40:31.658270','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,1,'Integrations',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,1,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Report','2013-03-09 15:45:57.000000','2017-02-22 14:42:06.597755','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Core',NULL,NULL,'field:report_name',NULL,NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System','fa fa-table',NULL,NULL,NULL,'InnoDB',NULL,0,1,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Role','2013-01-08 15:50:01.000000','2017-07-06 12:42:57.097914','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Core',NULL,NULL,'field:role_name',NULL,NULL,NULL,NULL,NULL,'ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'Document','fa fa-bookmark',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Role Permission for Page and Report','2017-02-13 17:33:25.157332','2017-03-11 02:35:32.369043','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,0,'Core',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,1,0,0,1,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Role Profile','2017-08-31 04:16:38.764465','2017-10-17 11:05:11.183066','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Core',NULL,NULL,'role_profile','','role_profile',NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('S3 Backup Settings','2017-09-04 20:57:20.129205','2017-10-06 18:27:09.022674','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,1,'Integrations',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,1,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Salutation','2017-04-10 12:17:58.071915','2017-04-10 12:55:18.855578','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Contacts',NULL,NULL,'field:salutation','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Site','2017-08-03 14:45:11.981784','2017-09-25 00:58:08.920454','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Bench Manager',NULL,NULL,'field:site_name','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Site Backup','2017-08-17 14:51:33.577230','2017-08-31 15:57:37.805893','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Bench Manager',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('SMS Parameter','2013-02-22 01:27:58.000000','2017-10-13 16:48:00.518463','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,1,1,0,'Core',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('SMS Settings','2013-01-10 16:34:24.000000','2017-11-01 12:57:20.943845','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,1,0,0,0,'Core',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,1,0,0,0,0,0,0,NULL,0,NULL,'fa fa-cog',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Social Login Keys','2014-03-04 08:29:52.000000','2016-12-29 14:40:30.397643','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,1,0,0,1,'Integrations',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System','icon-signin',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Standard Reply','2014-06-19 05:20:26.331041','2017-11-10 08:25:07.708599','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Email',NULL,NULL,'Prompt','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,1,0,0,0,0,NULL,0,'Document','fa fa-comment',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Stripe Settings','2017-03-09 17:18:29.458397','2017-03-09 17:19:25.087475','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,0,'Integrations',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('System Settings','2014-04-17 16:53:52.640856','2017-10-15 20:29:46.700707','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,0,1,'Core',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'System','fa fa-cog',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Tag','2016-05-25 09:43:44.767581','2016-05-31 08:29:01.773065','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,0,0,'Core',NULL,NULL,'','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Tag Category','2016-05-25 09:49:07.125394','2016-12-29 14:40:37.489085','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Core',NULL,NULL,'field:category_name','Title Case',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Tag Doc Category','2016-05-25 13:09:20.996154','2016-05-30 15:04:45.454688','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,0,0,'Core',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Test Runner','2017-06-26 10:57:19.976624','2017-07-19 03:22:33.221169','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,1,'Core',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('ToDo','2012-07-03 13:30:35.000000','2017-09-30 13:57:29.398598','Administrator','Administrator',0,NULL,NULL,NULL,2,'description, reference_type, reference_name',0,0,0,1,'Desk',NULL,NULL,'hash',NULL,'description',NULL,NULL,NULL,'DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,1,0,NULL,0,'Setup','fa fa-check',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Top Bar Item','2013-02-22 01:28:08.000000','2016-07-11 03:28:09.550365','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,1,1,0,'Website',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ASC',NULL,NULL,0,0,0,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Other',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Translation','2016-02-17 12:21:16.175465','2016-12-29 14:39:48.571006','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Core',NULL,NULL,'hash','','source_name',NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'Setup',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Unhandled Email','2016-04-14 09:41:45.892975','2017-09-19 16:28:00.042256','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,0,'Email',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('User','2014-03-11 14:55:00.000000','2017-11-01 09:04:51.151347','Administrator','Administrator',0,NULL,NULL,NULL,413,'full_name',0,0,0,1,'Core',NULL,NULL,NULL,NULL,'full_name','user_image',NULL,NULL,'DESC','Represents a User in the system.',NULL,0,0,0,NULL,NULL,0,1,1,0,0,0,5,NULL,0,'','fa fa-user',NULL,NULL,NULL,'InnoDB',NULL,0,1,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('User Email','2016-03-30 10:04:25.828742','2017-03-29 16:48:51.320616','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,0,'Core',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('User Permission','2017-07-17 14:25:27.881871','2017-10-26 09:51:47.663104','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Core',NULL,NULL,NULL,'','user',NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('User Permission for Page and Report','2017-02-13 17:33:25.157332','2017-12-21 04:24:24.963988','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,1,0,1,0,'Core',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,1,0,0,1,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Version','2014-02-20 17:22:37.000000','2018-04-10 14:39:45.926836','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Core',NULL,NULL,'hash',NULL,'docname',NULL,NULL,NULL,'ASC',NULL,NULL,0,1,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup','fa fa-copy',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Web Form','2014-09-01 14:08:48.624556','2017-09-13 18:55:07.031224','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,0,1,'Website',NULL,NULL,'','','title',NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Document','icon-edit',NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,1,0,NULL,'published',NULL,NULL,NULL,0),('Web Form Field','2014-09-01 14:14:14.292173','2017-02-27 15:55:00.850515','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,0,'Website',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Web Page','2013-03-28 10:35:30.000000','2017-09-24 20:58:41.588919','Administrator','Administrator',0,NULL,NULL,NULL,1,'title',0,0,0,1,'Website',NULL,NULL,NULL,NULL,'title',NULL,NULL,NULL,'ASC','Page to show on the website\n',NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,20,NULL,0,'Document','fa fa-file-alt',NULL,NULL,NULL,NULL,NULL,0,1,NULL,0,0,0,1,1,NULL,'published',NULL,NULL,NULL,0),('Webhook','2017-09-08 16:16:13.060641','2017-09-14 13:16:53.974340','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Integrations',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Webhook Data','2017-09-14 12:08:50.302810','2017-09-14 13:16:58.252176','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,0,'Integrations',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Webhook Header','2017-09-08 16:27:39.195379','2017-09-08 16:28:20.025612','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,1,'Integrations',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Website Script','2012-12-27 11:51:24.000000','2016-12-29 14:40:38.777912','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,1,0,0,1,'Website',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ASC','Script to attach to all web pages.',NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Other','fa fa-code',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Website Settings','2013-04-30 12:58:46.000000','2017-03-07 14:45:46.127265','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,1,0,0,1,'Website',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ASC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,10,NULL,0,'Other','fa fa-cog',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Website Sidebar','2016-12-29 07:48:06.319665','2016-12-29 07:50:05.633460','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,0,1,1,'Website',NULL,NULL,'field:title','',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'Document',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Website Sidebar Item','2016-12-29 07:42:26.246725','2016-12-29 07:42:26.246725','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,0,1,1,1,'Website',NULL,NULL,NULL,'',NULL,NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'',NULL,NULL,NULL,NULL,'InnoDB',NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Website Slideshow','2013-03-07 15:53:15.000000','2017-10-05 18:56:20.094625','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Website',NULL,NULL,'field:slideshow_name',NULL,NULL,NULL,NULL,NULL,NULL,'Slideshow like display for the website',NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,10,NULL,0,'Document','fa fa-play',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Website Slideshow Item','2013-03-07 12:26:33.000000','2016-07-11 03:28:09.983213','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,1,1,0,'Website',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,10,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Website Theme','2015-02-18 12:46:38.168929','2016-12-29 14:40:33.634557','Administrator','Administrator',0,NULL,NULL,NULL,0,'',0,0,0,1,'Website',NULL,NULL,'field:theme','','',NULL,NULL,'modified','DESC',NULL,NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'Setup',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Workflow','2012-12-28 10:49:55.000000','2017-02-20 13:33:38.497751','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Workflow',NULL,NULL,'field:workflow_name',NULL,NULL,NULL,NULL,NULL,'ASC','Defines workflow states and rules for a document.',NULL,0,0,0,NULL,NULL,0,1,0,0,0,0,0,NULL,0,'Document','fa fa-random',NULL,NULL,NULL,'InnoDB',NULL,0,1,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Workflow Action','2012-12-28 10:49:56.000000','2016-12-29 14:40:34.796187','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Workflow',NULL,NULL,'field:workflow_action_name',NULL,NULL,NULL,NULL,NULL,NULL,'Workflow Action Master',NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,NULL,'fa fa-flag',NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Workflow Document State','2013-02-22 01:27:36.000000','2016-07-11 03:28:10.056638','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,1,1,0,'Workflow',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Represents the states allowed in one document and role assigned to change the state.',NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,'Setup',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0),('Workflow State','2012-12-28 10:49:56.000000','2017-02-20 13:33:44.011509','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,0,0,1,'Workflow',NULL,NULL,'field:workflow_state_name',NULL,NULL,NULL,NULL,NULL,'ASC','Workflow state represents the current state of a document.',NULL,0,0,NULL,NULL,NULL,0,0,1,0,0,0,0,NULL,0,'Setup','fa fa-flag',NULL,NULL,NULL,NULL,NULL,0,1,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,1),('Workflow Transition','2013-02-22 01:27:36.000000','2016-07-11 03:28:10.146195','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,0,1,1,0,'Workflow',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Defines actions on states and the next step and allowed roles.',NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,0,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `tabDocType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabDomain`
--

DROP TABLE IF EXISTS `tabDomain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabDomain` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `domain` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabDomain`
--

LOCK TABLES `tabDomain` WRITE;
/*!40000 ALTER TABLE `tabDomain` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabDomain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabDynamic Link`
--

DROP TABLE IF EXISTS `tabDynamic Link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabDynamic Link` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `link_title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`),
  KEY `link_doctype_link_name_index` (`link_doctype`,`link_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabDynamic Link`
--

LOCK TABLES `tabDynamic Link` WRITE;
/*!40000 ALTER TABLE `tabDynamic Link` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabDynamic Link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabEmail Account`
--

DROP TABLE IF EXISTS `tabEmail Account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabEmail Account` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `default_outgoing` int(1) NOT NULL DEFAULT '0',
  `domain` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_id` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uidnext` int(11) NOT NULL DEFAULT '0',
  `use_imap` int(1) NOT NULL DEFAULT '0',
  `auto_reply_message` longtext COLLATE utf8mb4_unicode_ci,
  `use_ssl` int(1) NOT NULL DEFAULT '0',
  `always_use_account_email_id_as_sender` int(1) NOT NULL DEFAULT '0',
  `send_notification_to` text COLLATE utf8mb4_unicode_ci,
  `email_server` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_incoming` int(1) NOT NULL DEFAULT '0',
  `uidvalidity` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `smtp_port` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_server` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_failed` int(11) NOT NULL DEFAULT '0',
  `use_tls` int(1) NOT NULL DEFAULT '0',
  `enable_incoming` int(1) NOT NULL DEFAULT '0',
  `attachment_limit` int(11) NOT NULL DEFAULT '1',
  `email_sync_option` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'UNSEEN',
  `awaiting_password` int(1) NOT NULL DEFAULT '0',
  `login_id` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `initial_sync_count` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT '250',
  `enable_auto_reply` int(1) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `send_unsubscribe_message` int(1) NOT NULL DEFAULT '1',
  `enable_outgoing` int(1) NOT NULL DEFAULT '0',
  `add_signature` int(1) NOT NULL DEFAULT '0',
  `password` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `email_account_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer` longtext COLLATE utf8mb4_unicode_ci,
  `unreplied_for_mins` int(11) NOT NULL DEFAULT '30',
  `login_id_is_different` int(1) NOT NULL DEFAULT '0',
  `no_remaining` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `append_to` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature` longtext COLLATE utf8mb4_unicode_ci,
  `notify_if_unreplied` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabEmail Account`
--

LOCK TABLES `tabEmail Account` WRITE;
/*!40000 ALTER TABLE `tabEmail Account` DISABLE KEYS */;
INSERT INTO `tabEmail Account` VALUES ('Notifications','2018-05-17 15:33:51.295886','2018-05-17 15:33:51.295886','Administrator','Administrator',0,NULL,NULL,NULL,0,1,'example.com','notifications@example.com',0,0,NULL,0,0,NULL,'imap.example.com',0,NULL,'',NULL,NULL,'smtp.example.com',0,0,0,1,'UNSEEN',0,NULL,NULL,'250',0,NULL,1,0,0,NULL,NULL,'Notifications',NULL,30,0,NULL,NULL,NULL,0),('Replies','2018-05-17 15:33:51.307149','2018-05-17 15:33:51.307149','Administrator','Administrator',0,NULL,NULL,NULL,0,0,'example.com','replies@example.com',0,0,NULL,0,0,NULL,'imap.example.com',1,NULL,'',NULL,NULL,'smtp.example.com',0,0,0,1,'UNSEEN',0,NULL,NULL,'250',0,NULL,1,0,0,NULL,NULL,'Replies',NULL,30,0,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `tabEmail Account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabEmail Alert`
--

DROP TABLE IF EXISTS `tabEmail Alert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabEmail Alert` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `enabled` int(1) NOT NULL DEFAULT '1',
  `module` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `days_in_advance` int(11) NOT NULL DEFAULT '0',
  `message` longtext COLLATE utf8mb4_unicode_ci,
  `event` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attach_print` int(1) NOT NULL DEFAULT '0',
  `condition` longtext COLLATE utf8mb4_unicode_ci,
  `date_changed` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `set_property_after_alert` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `is_standard` int(1) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `property_value` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `print_format` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value_changed` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `event` (`event`),
  KEY `document_type` (`document_type`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabEmail Alert`
--

LOCK TABLES `tabEmail Alert` WRITE;
/*!40000 ALTER TABLE `tabEmail Alert` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabEmail Alert` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabEmail Alert Recipient`
--

DROP TABLE IF EXISTS `tabEmail Alert Recipient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabEmail Alert Recipient` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `cc` longtext COLLATE utf8mb4_unicode_ci,
  `email_by_role` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_by_document_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condition` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabEmail Alert Recipient`
--

LOCK TABLES `tabEmail Alert Recipient` WRITE;
/*!40000 ALTER TABLE `tabEmail Alert Recipient` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabEmail Alert Recipient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabEmail Domain`
--

DROP TABLE IF EXISTS `tabEmail Domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabEmail Domain` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `email_id` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `smtp_port` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_server` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domain_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `use_tls` int(1) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `append_to` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_limit` int(11) NOT NULL DEFAULT '1',
  `use_ssl` int(1) NOT NULL DEFAULT '0',
  `email_server` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `use_imap` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabEmail Domain`
--

LOCK TABLES `tabEmail Domain` WRITE;
/*!40000 ALTER TABLE `tabEmail Domain` DISABLE KEYS */;
INSERT INTO `tabEmail Domain` VALUES ('example.com','2018-05-17 15:33:51.286922','2018-05-17 15:33:51.286922','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'account@example.com',NULL,NULL,'smtp.example.com','example.com',0,NULL,NULL,1,0,'imap.example.com',NULL,1);
/*!40000 ALTER TABLE `tabEmail Domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabEmail Flag Queue`
--

DROP TABLE IF EXISTS `tabEmail Flag Queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabEmail Flag Queue` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `uid` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `communication` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_completed` int(1) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `action` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_account` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabEmail Flag Queue`
--

LOCK TABLES `tabEmail Flag Queue` WRITE;
/*!40000 ALTER TABLE `tabEmail Flag Queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabEmail Flag Queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabEmail Group`
--

DROP TABLE IF EXISTS `tabEmail Group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabEmail Group` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `total_subscribers` int(11) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabEmail Group`
--

LOCK TABLES `tabEmail Group` WRITE;
/*!40000 ALTER TABLE `tabEmail Group` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabEmail Group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabEmail Group Member`
--

DROP TABLE IF EXISTS `tabEmail Group Member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabEmail Group Member` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `unsubscribed` int(1) NOT NULL DEFAULT '0',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_group` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `unique_email_group_email` (`email_group`,`email`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabEmail Group Member`
--

LOCK TABLES `tabEmail Group Member` WRITE;
/*!40000 ALTER TABLE `tabEmail Group Member` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabEmail Group Member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabEmail Queue`
--

DROP TABLE IF EXISTS `tabEmail Queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabEmail Queue` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Not Sent',
  `reference_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `sender` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `communication` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add_unsubscribe_link` int(1) NOT NULL DEFAULT '1',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `unsubscribe_param` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expose_recipients` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `send_after` datetime(6) DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `error` longtext COLLATE utf8mb4_unicode_ci,
  `show_as_cc` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci,
  `unsubscribe_method` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `message_id` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachments` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `communication` (`communication`),
  KEY `message_id` (`message_id`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`),
  KEY `index_bulk_flush` (`status`,`send_after`,`priority`,`creation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabEmail Queue`
--

LOCK TABLES `tabEmail Queue` WRITE;
/*!40000 ALTER TABLE `tabEmail Queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabEmail Queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabEmail Queue Recipient`
--

DROP TABLE IF EXISTS `tabEmail Queue Recipient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabEmail Queue Recipient` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Not Sent',
  `recipient` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `error` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabEmail Queue Recipient`
--

LOCK TABLES `tabEmail Queue Recipient` WRITE;
/*!40000 ALTER TABLE `tabEmail Queue Recipient` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabEmail Queue Recipient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabEmail Rule`
--

DROP TABLE IF EXISTS `tabEmail Rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabEmail Rule` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `email_id` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `is_spam` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabEmail Rule`
--

LOCK TABLES `tabEmail Rule` WRITE;
/*!40000 ALTER TABLE `tabEmail Rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabEmail Rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabEmail Unsubscribe`
--

DROP TABLE IF EXISTS `tabEmail Unsubscribe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabEmail Unsubscribe` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `reference_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `global_unsubscribe` int(1) NOT NULL DEFAULT '0',
  `reference_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabEmail Unsubscribe`
--

LOCK TABLES `tabEmail Unsubscribe` WRITE;
/*!40000 ALTER TABLE `tabEmail Unsubscribe` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabEmail Unsubscribe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabError Log`
--

DROP TABLE IF EXISTS `tabError Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabError Log` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `error` longtext COLLATE utf8mb4_unicode_ci,
  `seen` int(1) NOT NULL DEFAULT '0',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `method` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabError Log`
--

LOCK TABLES `tabError Log` WRITE;
/*!40000 ALTER TABLE `tabError Log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabError Log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabError Snapshot`
--

DROP TABLE IF EXISTS `tabError Snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabError Snapshot` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `exception` longtext COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `timestamp` datetime(6) DEFAULT NULL,
  `evalue` longtext COLLATE utf8mb4_unicode_ci,
  `traceback` longtext COLLATE utf8mb4_unicode_ci,
  `frames` longtext COLLATE utf8mb4_unicode_ci,
  `parent_error_snapshot` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pyver` longtext COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `etype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seen` int(1) NOT NULL DEFAULT '0',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `relapses` int(11) NOT NULL DEFAULT '1',
  `locals` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabError Snapshot`
--

LOCK TABLES `tabError Snapshot` WRITE;
/*!40000 ALTER TABLE `tabError Snapshot` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabError Snapshot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabEvent`
--

DROP TABLE IF EXISTS `tabEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabEvent` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `event_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tuesday` int(1) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `thursday` int(1) NOT NULL DEFAULT '0',
  `saturday` int(1) NOT NULL DEFAULT '0',
  `subject` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `repeat_this_event` int(1) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `all_day` int(1) NOT NULL DEFAULT '0',
  `repeat_till` date DEFAULT NULL,
  `sunday` int(1) NOT NULL DEFAULT '0',
  `send_reminder` int(1) NOT NULL DEFAULT '1',
  `ref_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `monday` int(1) NOT NULL DEFAULT '0',
  `friday` int(1) NOT NULL DEFAULT '0',
  `_seen` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `starts_on` datetime(6) DEFAULT NULL,
  `wednesday` int(1) NOT NULL DEFAULT '0',
  `ends_on` datetime(6) DEFAULT NULL,
  `repeat_on` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `event_type` (`event_type`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabEvent`
--

LOCK TABLES `tabEvent` WRITE;
/*!40000 ALTER TABLE `tabEvent` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabEvent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabFeedback Request`
--

DROP TABLE IF EXISTS `tabFeedback Request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabFeedback Request` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `reference_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `rating` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_sent` int(1) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `key` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_manual` int(1) NOT NULL DEFAULT '0',
  `feedback_trigger` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_feedback_submitted` int(1) NOT NULL DEFAULT '0',
  `reference_communication` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabFeedback Request`
--

LOCK TABLES `tabFeedback Request` WRITE;
/*!40000 ALTER TABLE `tabFeedback Request` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabFeedback Request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabFeedback Trigger`
--

DROP TABLE IF EXISTS `tabFeedback Trigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabFeedback Trigger` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `condition` text COLLATE utf8mb4_unicode_ci,
  `check_communication` int(1) NOT NULL DEFAULT '1',
  `email_fieldname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enabled` int(1) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `message` longtext COLLATE utf8mb4_unicode_ci,
  `document_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `subject` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabFeedback Trigger`
--

LOCK TABLES `tabFeedback Trigger` WRITE;
/*!40000 ALTER TABLE `tabFeedback Trigger` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabFeedback Trigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabFile`
--

DROP TABLE IF EXISTS `tabFile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabFile` (
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `file_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_url` longtext COLLATE utf8mb4_unicode_ci,
  `module` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attached_to_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_size` int(11) NOT NULL DEFAULT '0',
  `attached_to_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lft` int(11) NOT NULL DEFAULT '0',
  `is_attachments_folder` int(1) NOT NULL DEFAULT '0',
  `is_folder` int(1) NOT NULL DEFAULT '0',
  `content_hash` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rgt` int(11) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `is_home_folder` int(1) NOT NULL DEFAULT '0',
  `folder` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `attached_to_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `is_private` int(1) NOT NULL DEFAULT '0',
  `old_parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail_url` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `attached_to_name` (`attached_to_name`),
  KEY `attached_to_doctype` (`attached_to_doctype`),
  KEY `attached_to_doctype_attached_to_name_index` (`attached_to_doctype`,`attached_to_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabFile`
--

LOCK TABLES `tabFile` WRITE;
/*!40000 ALTER TABLE `tabFile` DISABLE KEYS */;
INSERT INTO `tabFile` VALUES ('Home','2018-05-17 15:33:51.343388','2018-05-17 15:33:51.355297','Administrator','Administrator',0,NULL,NULL,NULL,0,'Home',NULL,NULL,NULL,0,NULL,1,0,1,NULL,4,NULL,1,NULL,NULL,NULL,NULL,NULL,0,'',NULL),('Home/Attachments','2018-05-17 15:33:51.351223','2018-05-17 15:33:51.355297','Administrator','Administrator',0,NULL,NULL,NULL,0,'Attachments',NULL,NULL,NULL,0,NULL,2,1,1,NULL,3,NULL,0,'Home',NULL,NULL,NULL,NULL,0,'Home',NULL);
/*!40000 ALTER TABLE `tabFile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabFooter Item`
--

DROP TABLE IF EXISTS `tabFooter Item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabFooter Item` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `url` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_label` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabFooter Item`
--

LOCK TABLES `tabFooter Item` WRITE;
/*!40000 ALTER TABLE `tabFooter Item` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabFooter Item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabGSuite Templates`
--

DROP TABLE IF EXISTS `tabGSuite Templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabGSuite Templates` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `template_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `destination_id` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `related_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `template_id` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'New Document for {name} ',
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabGSuite Templates`
--

LOCK TABLES `tabGSuite Templates` WRITE;
/*!40000 ALTER TABLE `tabGSuite Templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabGSuite Templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabGender`
--

DROP TABLE IF EXISTS `tabGender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabGender` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `gender` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabGender`
--

LOCK TABLES `tabGender` WRITE;
/*!40000 ALTER TABLE `tabGender` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabGender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabHas Domain`
--

DROP TABLE IF EXISTS `tabHas Domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabHas Domain` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `domain` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabHas Domain`
--

LOCK TABLES `tabHas Domain` WRITE;
/*!40000 ALTER TABLE `tabHas Domain` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabHas Domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabHas Role`
--

DROP TABLE IF EXISTS `tabHas Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabHas Role` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `role` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabHas Role`
--

LOCK TABLES `tabHas Role` WRITE;
/*!40000 ALTER TABLE `tabHas Role` DISABLE KEYS */;
INSERT INTO `tabHas Role` VALUES ('06b988edb0','2013-10-04 13:49:33.000000','2018-05-17 15:33:47.065354','Administrator','Administrator',0,'setup-wizard','roles','Page',1,'System Manager'),('17acaace9f','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',19,'Accounts Manager'),('210e4ac536','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',4,'Guest'),('2123473946','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',14,'Maintenance User'),('2c19c1bab0','2018-05-17 15:33:51.159574','2018-05-17 15:33:51.159574','Administrator','Administrator',0,'Guest','roles','User',1,'Guest'),('2cc9d103dd','2013-12-23 11:01:52.000000','2018-05-17 15:33:47.058460','Administrator','Administrator',0,'applications','roles','Page',1,'System Manager'),('2d4b398a47','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',5,'Report Manager'),('33376f3c76','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',8,'Knowledge Base Editor'),('34d379c027','2016-08-18 16:44:14.322642','2018-05-17 15:33:44.297704','Administrator','Administrator',0,'background_jobs','roles','Page',1,'System Manager'),('35c62ba2c0','2013-04-09 11:45:31.000000','2018-05-17 15:33:47.051486','Administrator','Administrator',0,'activity','roles','Page',1,'All'),('379bd8b595','2018-05-17 15:33:51.040710','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',2,'System Manager'),('41486d68c9','2013-02-25 14:26:30.000000','2018-05-17 15:33:47.070882','Administrator','Administrator',0,'ToDo','roles','Report',1,'System Manager'),('41588b004a','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',12,'Accounts User'),('473659a897','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',9,'Blogger'),('4c1120c9f8','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',15,'Purchase Manager'),('5295e74196','2017-01-19 12:57:22.881566','2018-05-17 15:33:48.565346','Administrator','Administrator',0,'Addresses And Contacts','roles','Report',4,'Accounts User'),('56e45e3a1d','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',20,'Purchase Master Manager'),('5ae9dc3f18','2015-09-24 01:26:06.225378','2018-05-17 15:33:47.035311','Administrator','Administrator',0,'backups','roles','Page',1,'System Manager'),('6ca82eeb8e','2014-06-03 05:20:35.218263','2018-05-17 15:33:44.362199','Administrator','Administrator',0,'Permitted Documents For User','roles','Report',1,'System Manager'),('750dc3be28','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',1,'Administrator'),('827bdbe54d','2015-01-27 04:35:43.872918','2018-05-17 15:33:48.271614','Administrator','Administrator',0,'print-format-builder','roles','Page',1,'System Manager'),('90d6e804c3','2017-02-05 20:38:21.890174','2018-05-17 15:33:44.350785','Administrator','Administrator',0,'Feedback Ratings','roles','Report',1,'System Manager'),('95432f23f2','2017-01-19 12:57:22.881566','2018-05-17 15:33:48.565346','Administrator','Administrator',0,'Addresses And Contacts','roles','Report',3,'Maintenance User'),('95e9fd09cc','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',17,'Sales Master Manager'),('9a74af4840','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',16,'Maintenance Manager'),('aec2cf8398','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',11,'Sales User'),('bb2e38a4ed','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',7,'Knowledge Base Contributor'),('bda81dbbea','2017-01-19 12:57:22.881566','2018-05-17 15:33:48.565346','Administrator','Administrator',0,'Addresses And Contacts','roles','Report',1,'Sales User'),('bf83cb622b','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',6,'Website Manager'),('c1d234bde8','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',10,'Newsletter Manager'),('c274f69ef6','2017-01-19 12:57:22.881566','2018-05-17 15:33:48.565346','Administrator','Administrator',0,'Addresses And Contacts','roles','Report',2,'Purchase User'),('d48f64cc9d','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',13,'Purchase User'),('e7e88a2f06','2013-02-14 17:37:37.000000','2018-05-17 15:33:44.305049','Administrator','Administrator',0,'desktop','roles','Page',1,'All'),('e8e38934f4','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',3,'All'),('f4cfcc26af','2012-06-14 18:44:56.000000','2018-05-17 15:33:47.042767','Administrator','Administrator',0,'chat','roles','Page',1,'All'),('f571786d69','2016-06-02 18:14:53.475842','2018-05-17 15:33:44.315803','Administrator','Administrator',0,'usage-info','roles','Page',1,'System Manager'),('f701700758','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,'Administrator','roles','User',18,'Sales Manager'),('f945ebfe9f','2015-02-05 06:01:35.060098','2018-05-17 15:33:44.356865','Administrator','Administrator',0,'Document Share Report','roles','Report',1,'System Manager'),('fd9adf9fe5','2013-01-01 11:00:01.000000','2018-05-17 15:33:44.310464','Administrator','Administrator',0,'permission-manager','roles','Page',1,'System Manager');
/*!40000 ALTER TABLE `tabHas Role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabHelp Article`
--

DROP TABLE IF EXISTS `tabHelp Article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabHelp Article` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `category` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `route` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'user_fullname',
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `likes` int(11) NOT NULL DEFAULT '0',
  `published` int(1) NOT NULL DEFAULT '0',
  `level` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabHelp Article`
--

LOCK TABLES `tabHelp Article` WRITE;
/*!40000 ALTER TABLE `tabHelp Article` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabHelp Article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabHelp Category`
--

DROP TABLE IF EXISTS `tabHelp Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabHelp Category` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `route` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` int(1) NOT NULL DEFAULT '0',
  `help_articles` int(11) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `category_description` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `category_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabHelp Category`
--

LOCK TABLES `tabHelp Category` WRITE;
/*!40000 ALTER TABLE `tabHelp Category` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabHelp Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabIntegration Request`
--

DROP TABLE IF EXISTS `tabIntegration Request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabIntegration Request` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Queued',
  `reference_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `integration_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `error` longtext COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `integration_request_service` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_docname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `output` longtext COLLATE utf8mb4_unicode_ci,
  `data` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabIntegration Request`
--

LOCK TABLES `tabIntegration Request` WRITE;
/*!40000 ALTER TABLE `tabIntegration Request` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabIntegration Request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabKanban Board`
--

DROP TABLE IF EXISTS `tabKanban Board`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabKanban Board` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `reference_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `private` int(1) NOT NULL DEFAULT '0',
  `filters` text COLLATE utf8mb4_unicode_ci,
  `kanban_board_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  UNIQUE KEY `kanban_board_name` (`kanban_board_name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabKanban Board`
--

LOCK TABLES `tabKanban Board` WRITE;
/*!40000 ALTER TABLE `tabKanban Board` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabKanban Board` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabKanban Board Column`
--

DROP TABLE IF EXISTS `tabKanban Board Column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabKanban Board Column` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Active',
  `indicator` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'darkgrey',
  `order` longtext COLLATE utf8mb4_unicode_ci,
  `column_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabKanban Board Column`
--

LOCK TABLES `tabKanban Board Column` WRITE;
/*!40000 ALTER TABLE `tabKanban Board Column` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabKanban Board Column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabLanguage`
--

DROP TABLE IF EXISTS `tabLanguage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabLanguage` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `based_on` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flag` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `language_code` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabLanguage`
--

LOCK TABLES `tabLanguage` WRITE;
/*!40000 ALTER TABLE `tabLanguage` DISABLE KEYS */;
INSERT INTO `tabLanguage` VALUES ('af','2018-05-17 15:33:51.887462','2018-05-17 15:33:51.887462','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'af','Afrikaans',NULL),('am','2018-05-17 15:33:51.889496','2018-05-17 15:33:51.889496','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'am','አማርኛ',NULL),('ar','2018-05-17 15:33:51.891394','2018-05-17 15:33:51.891394','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'ar','العربية',NULL),('bg','2018-05-17 15:33:51.893215','2018-05-17 15:33:51.893215','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'bg','Bǎlgarski',NULL),('bn','2018-05-17 15:33:51.895018','2018-05-17 15:33:51.895018','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'bn','বাঙালি',NULL),('bo','2018-05-17 15:33:51.896809','2018-05-17 15:33:51.896809','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'bo','ལྷ་སའི་སྐད་',NULL),('bs','2018-05-17 15:33:51.898671','2018-05-17 15:33:51.898671','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'bs','Bosanski',NULL),('ca','2018-05-17 15:33:51.900490','2018-05-17 15:33:51.900490','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'ca','Català',NULL),('cs','2018-05-17 15:33:51.902289','2018-05-17 15:33:51.902289','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'cs','česky',NULL),('da','2018-05-17 15:33:51.904137','2018-05-17 15:33:51.904137','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'da','Dansk',NULL),('da-DK','2018-05-17 15:33:51.905944','2018-05-17 15:33:51.905944','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'da-DK','Dansk (Danmark)',NULL),('de','2018-05-17 15:33:51.907723','2018-05-17 15:33:51.907723','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'de','Deutsch',NULL),('el','2018-05-17 15:33:51.909549','2018-05-17 15:33:51.909549','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'el','ελληνικά',NULL),('en','2018-05-17 15:33:51.911478','2018-05-17 15:33:51.911478','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'en','English',NULL),('en-GB','2018-05-17 15:33:51.913268','2018-05-17 15:33:51.913268','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'en-GB','English (United Kingdom)',NULL),('en-US','2018-05-17 15:33:51.915094','2018-05-17 15:33:51.915094','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'en-US','English (United States)',NULL),('es','2018-05-17 15:33:51.916905','2018-05-17 15:33:51.916905','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'es','Español',NULL),('es-AR','2018-05-17 15:33:51.918783','2018-05-17 15:33:51.918783','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'es-AR','Español (Argentina)',NULL),('es-BO','2018-05-17 15:33:51.920760','2018-05-17 15:33:51.920760','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'es-BO','Español (Bolivia)',NULL),('es-CL','2018-05-17 15:33:51.922821','2018-05-17 15:33:51.922821','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'es-CL','Español (Chile)',NULL),('es-CO','2018-05-17 15:33:51.925074','2018-05-17 15:33:51.925074','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'es-CO','Español (Colombia)',NULL),('es-DO','2018-05-17 15:33:51.926912','2018-05-17 15:33:51.926912','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'es-DO','Español (República Dominicana)',NULL),('es-EC','2018-05-17 15:33:51.928849','2018-05-17 15:33:51.928849','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'es-EC','Español (Ecuador)',NULL),('es-GT','2018-05-17 15:33:51.930682','2018-05-17 15:33:51.930682','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'es-GT','Español (Guatemala)',NULL),('es-MX','2018-05-17 15:33:51.932514','2018-05-17 15:33:51.932514','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'es-MX','Español (México)',NULL),('es-NI','2018-05-17 15:33:51.934499','2018-05-17 15:33:51.934499','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'es-NI','Español (Nicaragua)',NULL),('es-PE','2018-05-17 15:33:51.936652','2018-05-17 15:33:51.936652','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'es-PE','Español (Perú)',NULL),('et','2018-05-17 15:33:51.938467','2018-05-17 15:33:51.938467','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'et','Eesti',NULL),('fa','2018-05-17 15:33:51.940296','2018-05-17 15:33:51.940296','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'fa','پارسی',NULL),('fi','2018-05-17 15:33:51.942114','2018-05-17 15:33:51.942114','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'fi','Suomi',NULL),('fr','2018-05-17 15:33:51.943924','2018-05-17 15:33:51.943924','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'fr','Français',NULL),('fr-CA','2018-05-17 15:33:51.945784','2018-05-17 15:33:51.945784','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'fr-CA','Français Canadien',NULL),('gu','2018-05-17 15:33:51.947623','2018-05-17 15:33:51.947623','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'gu','ગુજરાતી',NULL),('he','2018-05-17 15:33:51.949452','2018-05-17 15:33:51.949452','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'he','עברית',NULL),('hi','2018-05-17 15:33:51.951288','2018-05-17 15:33:51.951288','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'hi','हिंदी',NULL),('hr','2018-05-17 15:33:51.953055','2018-05-17 15:33:51.953055','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'hr','Hrvatski',NULL),('hu','2018-05-17 15:33:51.954875','2018-05-17 15:33:51.954875','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'hu','Magyar',NULL),('id','2018-05-17 15:33:51.956697','2018-05-17 15:33:51.956697','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'id','Indonesia',NULL),('is','2018-05-17 15:33:51.958961','2018-05-17 15:33:51.958961','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'is','íslenska',NULL),('it','2018-05-17 15:33:51.960829','2018-05-17 15:33:51.960829','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'it','Italiano',NULL),('ja','2018-05-17 15:33:51.962633','2018-05-17 15:33:51.962633','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'ja','日本語',NULL),('km','2018-05-17 15:33:51.964485','2018-05-17 15:33:51.964485','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'km','ភាសាខ្មែរ',NULL),('kn','2018-05-17 15:33:51.966244','2018-05-17 15:33:51.966244','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'kn','ಕನ್ನಡ',NULL),('ko','2018-05-17 15:33:51.968065','2018-05-17 15:33:51.968065','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'ko','한국의',NULL),('ku','2018-05-17 15:33:51.969936','2018-05-17 15:33:51.969936','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'ku','کوردی',NULL),('lo','2018-05-17 15:33:51.971878','2018-05-17 15:33:51.971878','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'lo','ລາວ',NULL),('lt','2018-05-17 15:33:51.973924','2018-05-17 15:33:51.973924','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'lt','Lietuvių kalba',NULL),('lv','2018-05-17 15:33:51.975805','2018-05-17 15:33:51.975805','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'lv','Latviešu valoda',NULL),('mk','2018-05-17 15:33:51.978048','2018-05-17 15:33:51.978048','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'mk','македонски',NULL),('ml','2018-05-17 15:33:51.980015','2018-05-17 15:33:51.980015','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'ml','മലയാളം',NULL),('mr','2018-05-17 15:33:51.981973','2018-05-17 15:33:51.981973','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'mr','मराठी',NULL),('ms','2018-05-17 15:33:51.983870','2018-05-17 15:33:51.983870','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'ms','Melayu',NULL),('my','2018-05-17 15:33:51.985922','2018-05-17 15:33:51.985922','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'my','မြန်မာ',NULL),('nl','2018-05-17 15:33:51.988053','2018-05-17 15:33:51.988053','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'nl','Nederlands',NULL),('no','2018-05-17 15:33:51.989879','2018-05-17 15:33:51.989879','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'no','Norsk',NULL),('pl','2018-05-17 15:33:51.991843','2018-05-17 15:33:51.991843','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'pl','Polski',NULL),('ps','2018-05-17 15:33:51.993833','2018-05-17 15:33:51.993833','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'ps','پښتو',NULL),('pt','2018-05-17 15:33:51.996306','2018-05-17 15:33:51.996306','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'pt','Português',NULL),('pt-BR','2018-05-17 15:33:51.998415','2018-05-17 15:33:51.998415','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'pt-BR','Português Brasileiro',NULL),('ro','2018-05-17 15:33:52.000955','2018-05-17 15:33:52.000955','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'ro','Român',NULL),('ru','2018-05-17 15:33:52.003350','2018-05-17 15:33:52.003350','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'ru','русский',NULL),('rw','2018-05-17 15:33:52.005584','2018-05-17 15:33:52.005584','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'rw','Kinyarwanda',NULL),('si','2018-05-17 15:33:52.007769','2018-05-17 15:33:52.007769','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'si','සිංහල',NULL),('sk','2018-05-17 15:33:52.009625','2018-05-17 15:33:52.009625','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'sk','Slovenčina (Slovak)',NULL),('sl','2018-05-17 15:33:52.011595','2018-05-17 15:33:52.011595','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'sl','Slovenščina (Slovene)',NULL),('sq','2018-05-17 15:33:52.013740','2018-05-17 15:33:52.013740','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'sq','Shqiptar',NULL),('sr','2018-05-17 15:33:52.015674','2018-05-17 15:33:52.015674','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'sr','српски',NULL),('sr-SP','2018-05-17 15:33:52.017552','2018-05-17 15:33:52.017552','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'sr-SP','Srpski',NULL),('sv','2018-05-17 15:33:52.019463','2018-05-17 15:33:52.019463','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'sv','Svenska',NULL),('sw','2018-05-17 15:33:52.021312','2018-05-17 15:33:52.021312','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'sw','Swahili',NULL),('ta','2018-05-17 15:33:52.023346','2018-05-17 15:33:52.023346','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'ta','தமிழ்',NULL),('te','2018-05-17 15:33:52.025212','2018-05-17 15:33:52.025212','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'te','తెలుగు',NULL),('th','2018-05-17 15:33:52.027043','2018-05-17 15:33:52.027043','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'th','ไทย',NULL),('tr','2018-05-17 15:33:52.028950','2018-05-17 15:33:52.028950','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'tr','Türk',NULL),('uk','2018-05-17 15:33:52.030868','2018-05-17 15:33:52.030868','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'uk','українська',NULL),('ur','2018-05-17 15:33:52.032925','2018-05-17 15:33:52.032925','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'ur','اردو',NULL),('uz','2018-05-17 15:33:52.034897','2018-05-17 15:33:52.034897','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'uz','Ўзбек',NULL),('vi','2018-05-17 15:33:52.036762','2018-05-17 15:33:52.036762','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'vi','Việt',NULL),('zh','2018-05-17 15:33:52.038606','2018-05-17 15:33:52.038606','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'zh','简体中文',NULL),('zh-TW','2018-05-17 15:33:52.040461','2018-05-17 15:33:52.040461','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'zh-TW','繁體中文',NULL);
/*!40000 ALTER TABLE `tabLanguage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabLetter Head`
--

DROP TABLE IF EXISTS `tabLetter Head`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabLetter Head` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `footer` longtext COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `letter_head_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` int(1) NOT NULL DEFAULT '0',
  `disabled` int(1) NOT NULL DEFAULT '0',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `is_default` (`is_default`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabLetter Head`
--

LOCK TABLES `tabLetter Head` WRITE;
/*!40000 ALTER TABLE `tabLetter Head` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabLetter Head` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabModule Def`
--

DROP TABLE IF EXISTS `tabModule Def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabModule Def` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `app_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `restrict_to_domain` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabModule Def`
--

LOCK TABLES `tabModule Def` WRITE;
/*!40000 ALTER TABLE `tabModule Def` DISABLE KEYS */;
INSERT INTO `tabModule Def` VALUES ('Bench Manager','2018-05-17 15:34:43.110915','2018-05-17 15:34:43.110915','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'bench_manager',NULL,NULL,NULL,'Bench Manager',NULL),('Contacts','2018-05-17 15:33:48.332638','2018-05-17 15:33:48.332638','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'frappe',NULL,NULL,NULL,'Contacts',NULL),('Core','2018-05-17 15:33:42.988725','2018-05-17 15:33:42.988725','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'frappe',NULL,NULL,NULL,'Core',NULL),('Custom','2018-05-17 15:33:46.437129','2018-05-17 15:33:46.437129','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'frappe',NULL,NULL,NULL,'Custom',NULL),('Data Migration','2018-05-17 15:33:48.595103','2018-05-17 15:33:48.595103','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'frappe',NULL,NULL,NULL,'Data Migration',NULL),('Desk','2018-05-17 15:33:46.648783','2018-05-17 15:33:46.648783','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'frappe',NULL,NULL,NULL,'Desk',NULL),('Email','2018-05-17 15:33:45.496715','2018-05-17 15:33:45.496715','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'frappe',NULL,NULL,NULL,'Email',NULL),('Geo','2018-05-17 15:33:46.568021','2018-05-17 15:33:46.568021','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'frappe',NULL,NULL,NULL,'Geo',NULL),('Integrations','2018-05-17 15:33:47.091387','2018-05-17 15:33:47.091387','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'frappe',NULL,NULL,NULL,'Integrations',NULL),('Printing','2018-05-17 15:33:48.082613','2018-05-17 15:33:48.082613','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'frappe',NULL,NULL,NULL,'Printing',NULL),('Website','2018-05-17 15:33:44.465577','2018-05-17 15:33:44.465577','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'frappe',NULL,NULL,NULL,'Website',NULL),('Workflow','2018-05-17 15:33:45.322731','2018-05-17 15:33:45.322731','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'frappe',NULL,NULL,NULL,'Workflow',NULL);
/*!40000 ALTER TABLE `tabModule Def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabNewsletter`
--

DROP TABLE IF EXISTS `tabNewsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabNewsletter` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `send_unsubscribe_link` int(1) NOT NULL DEFAULT '1',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `route` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_attachements` int(1) NOT NULL DEFAULT '0',
  `send_from` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scheduled_to_send` int(11) NOT NULL DEFAULT '0',
  `email_sent` int(1) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `published` int(1) NOT NULL DEFAULT '0',
  `message` longtext COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `test_email_id` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabNewsletter`
--

LOCK TABLES `tabNewsletter` WRITE;
/*!40000 ALTER TABLE `tabNewsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabNewsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabNewsletter Email Group`
--

DROP TABLE IF EXISTS `tabNewsletter Email Group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabNewsletter Email Group` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `total_subscribers` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_group` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabNewsletter Email Group`
--

LOCK TABLES `tabNewsletter Email Group` WRITE;
/*!40000 ALTER TABLE `tabNewsletter Email Group` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabNewsletter Email Group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabNote`
--

DROP TABLE IF EXISTS `tabNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabNote` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `expire_notification_on` date DEFAULT NULL,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `notify_on_every_login` int(1) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `notify_on_login` int(1) NOT NULL DEFAULT '0',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `public` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `expire_notification_on` (`expire_notification_on`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabNote`
--

LOCK TABLES `tabNote` WRITE;
/*!40000 ALTER TABLE `tabNote` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabNote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabNote Seen By`
--

DROP TABLE IF EXISTS `tabNote Seen By`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabNote Seen By` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `user` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabNote Seen By`
--

LOCK TABLES `tabNote Seen By` WRITE;
/*!40000 ALTER TABLE `tabNote Seen By` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabNote Seen By` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabOAuth Authorization Code`
--

DROP TABLE IF EXISTS `tabOAuth Authorization Code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabOAuth Authorization Code` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `redirect_uri_bound_to_authorization_code` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `authorization_code` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `validity` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiration_time` datetime(6) DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabOAuth Authorization Code`
--

LOCK TABLES `tabOAuth Authorization Code` WRITE;
/*!40000 ALTER TABLE `tabOAuth Authorization Code` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabOAuth Authorization Code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabOAuth Bearer Token`
--

DROP TABLE IF EXISTS `tabOAuth Bearer Token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabOAuth Bearer Token` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `access_token` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `expires_in` int(11) NOT NULL DEFAULT '0',
  `client` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiration_time` datetime(6) DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `refresh_token` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabOAuth Bearer Token`
--

LOCK TABLES `tabOAuth Bearer Token` WRITE;
/*!40000 ALTER TABLE `tabOAuth Bearer Token` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabOAuth Bearer Token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabOAuth Client`
--

DROP TABLE IF EXISTS `tabOAuth Client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabOAuth Client` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `redirect_uris` text COLLATE utf8mb4_unicode_ci,
  `app_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grant_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_redirect_uri` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `response_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Code',
  `client_id` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skip_authorization` int(1) NOT NULL DEFAULT '0',
  `client_secret` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `user` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabOAuth Client`
--

LOCK TABLES `tabOAuth Client` WRITE;
/*!40000 ALTER TABLE `tabOAuth Client` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabOAuth Client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabPage`
--

DROP TABLE IF EXISTS `tabPage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabPage` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `system_page` int(1) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `restrict_to_domain` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `standard` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `icon` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `standard` (`standard`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabPage`
--

LOCK TABLES `tabPage` WRITE;
/*!40000 ALTER TABLE `tabPage` DISABLE KEYS */;
INSERT INTO `tabPage` VALUES ('activity','2013-04-09 11:45:31.000000','2013-07-11 14:40:20.000001','Administrator','Administrator',0,NULL,NULL,NULL,1,0,NULL,'Activity',NULL,NULL,NULL,'Desk','Yes','activity',NULL,'fa fa-play'),('applications','2013-12-23 11:01:52.000000','2015-11-18 06:20:09.586810','Administrator','Administrator',0,NULL,NULL,NULL,1,0,NULL,'App Installer',NULL,NULL,NULL,'Desk','Yes','applications',NULL,'fa fa-magic'),('background_jobs','2016-08-18 16:44:14.322642','2016-08-18 16:48:11.577611','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,'Background Jobs',NULL,NULL,NULL,'Core','Yes','background_jobs',NULL,NULL),('backups','2015-09-24 01:26:06.225378','2015-09-24 01:26:06.225378','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,'Download Backups',NULL,NULL,NULL,'Desk','Yes','backups',NULL,NULL),('chat','2012-06-14 18:44:56.000000','2016-03-31 02:02:13.503910','Administrator','Administrator',0,NULL,NULL,NULL,1,0,NULL,'Chat',NULL,NULL,NULL,'Desk','Yes','chat',NULL,''),('desktop','2013-02-14 17:37:37.000000','2013-07-11 14:41:56.000000','Administrator','Administrator',0,NULL,NULL,NULL,1,0,NULL,'Desktop',NULL,NULL,NULL,'Core','Yes','desktop',NULL,'fa fa-th'),('modules','2016-03-07 04:46:00.420330','2016-03-07 04:46:00.420330','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,'Modules',NULL,NULL,NULL,'Desk','Yes','modules',NULL,NULL),('modules_setup','2012-10-04 18:45:29.000000','2017-04-12 18:44:53.283640','Administrator','Administrator',0,NULL,NULL,NULL,1,1,NULL,'Modules Setup',NULL,NULL,NULL,'Core','Yes','modules_setup',NULL,'fa fa-cog'),('permission-manager','2013-01-01 11:00:01.000000','2013-07-11 14:43:43.000000','Administrator','Administrator',0,NULL,NULL,NULL,1,0,NULL,'Role Permissions Manager',NULL,NULL,NULL,'Core','Yes','permission-manager',NULL,'fa fa-lock'),('print-format-builder','2015-01-27 04:35:43.872918','2017-05-03 05:59:33.702308','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,'Print Format Builder',NULL,NULL,NULL,'Printing','Yes','print-format-builder',NULL,NULL),('setup-wizard','2013-10-04 13:49:33.000000','2017-04-12 18:45:00.774654','Administrator','Administrator',0,NULL,NULL,NULL,1,1,NULL,'Setup Wizard',NULL,NULL,NULL,'Desk','Yes','setup-wizard',NULL,NULL),('usage-info','2016-06-02 18:14:53.475842','2016-06-02 18:14:53.475842','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,'Usage Info',NULL,NULL,NULL,'Core','Yes','usage-info',NULL,NULL);
/*!40000 ALTER TABLE `tabPage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabPatch Log`
--

DROP TABLE IF EXISTS `tabPatch Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabPatch Log` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `patch` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabPatch Log`
--

LOCK TABLES `tabPatch Log` WRITE;
/*!40000 ALTER TABLE `tabPatch Log` DISABLE KEYS */;
INSERT INTO `tabPatch Log` VALUES ('PATCHLOG00001','2018-05-17 15:33:50.357390','2018-05-17 15:33:50.357390','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"\"\"update `tabPatch Log` set patch=replace(patch, \'.4_0.\', \'.v4_0.\')\"\"\") #2014-05-12'),('PATCHLOG00002','2018-05-17 15:33:50.363829','2018-05-17 15:33:50.363829','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.convert_to_barracuda_and_utf8mb4'),('PATCHLOG00003','2018-05-17 15:33:50.365955','2018-05-17 15:33:50.365955','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.utils.global_search.setup_global_search_table()'),('PATCHLOG00004','2018-05-17 15:33:50.368466','2018-05-17 15:33:50.368466','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.update_global_search_table'),('PATCHLOG00005','2018-05-17 15:33:50.370448','2018-05-17 15:33:50.370448','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.update_auth'),('PATCHLOG00006','2018-05-17 15:33:50.372366','2018-05-17 15:33:50.372366','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.drop_in_dialog #2017-09-22'),('PATCHLOG00007','2018-05-17 15:33:50.374224','2018-05-17 15:33:50.374224','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_2.remove_in_filter'),('PATCHLOG00008','2018-05-17 15:33:50.376201','2018-05-17 15:33:50.376201','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'doctype\', force=True) #2017-09-22'),('PATCHLOG00009','2018-05-17 15:33:50.378134','2018-05-17 15:33:50.378134','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'docfield\', force=True) #2017-03-03'),('PATCHLOG00010','2018-05-17 15:33:50.380007','2018-05-17 15:33:50.380007','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'docperm\') #2017-03-03'),('PATCHLOG00011','2018-05-17 15:33:50.381880','2018-05-17 15:33:50.381880','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'module_def\') #2017-09-22'),('PATCHLOG00012','2018-05-17 15:33:50.383822','2018-05-17 15:33:50.383822','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'version\') #2017-04-01'),('PATCHLOG00013','2018-05-17 15:33:50.385724','2018-05-17 15:33:50.385724','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'activity_log\')'),('PATCHLOG00014','2018-05-17 15:33:50.387598','2018-05-17 15:33:50.387598','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_1.rename_scheduler_log_to_error_log'),('PATCHLOG00015','2018-05-17 15:33:50.389533','2018-05-17 15:33:50.389533','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_1.rename_file_data'),('PATCHLOG00016','2018-05-17 15:33:50.391388','2018-05-17 15:33:50.391388','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.re_route #2016-06-27'),('PATCHLOG00017','2018-05-17 15:33:50.393310','2018-05-17 15:33:50.393310','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.drop_is_custom_from_docperm'),('PATCHLOG00018','2018-05-17 15:33:50.395243','2018-05-17 15:33:50.395243','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.update_records_in_global_search #11-05-2017'),('PATCHLOG00019','2018-05-17 15:33:50.397112','2018-05-17 15:33:50.397112','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.update_published_in_global_search'),('PATCHLOG00020','2018-05-17 15:33:50.399043','2018-05-17 15:33:50.399043','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'custom_docperm\')'),('PATCHLOG00021','2018-05-17 15:33:50.400915','2018-05-17 15:33:50.400915','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'deleted_document\')'),('PATCHLOG00022','2018-05-17 15:33:50.402954','2018-05-17 15:33:50.402954','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'domain_settings\')'),('PATCHLOG00023','2018-05-17 15:33:50.404901','2018-05-17 15:33:50.404901','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.rename_page_role_to_has_role #2017-03-16'),('PATCHLOG00024','2018-05-17 15:33:50.406867','2018-05-17 15:33:50.406867','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_2.setup_custom_perms #2017-01-19'),('PATCHLOG00025','2018-05-17 15:33:50.408772','2018-05-17 15:33:50.408772','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.set_user_permission_for_page_and_report #2017-03-20'),('PATCHLOG00026','2018-05-17 15:33:50.410669','2018-05-17 15:33:50.410669','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'role\') #2017-05-23'),('PATCHLOG00027','2018-05-17 15:33:50.412616','2018-05-17 15:33:50.412616','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'user\') #2017-10-27'),('PATCHLOG00028','2018-05-17 15:33:50.414465','2018-05-17 15:33:50.414465','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'custom\', \'doctype\', \'custom_field\') #2015-10-19'),('PATCHLOG00029','2018-05-17 15:33:50.416422','2018-05-17 15:33:50.416422','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'page\') #2013-13-26'),('PATCHLOG00030','2018-05-17 15:33:50.418428','2018-05-17 15:33:50.418428','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'report\') #2014-06-03'),('PATCHLOG00031','2018-05-17 15:33:50.420368','2018-05-17 15:33:50.420368','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'translation\') #2016-03-03'),('PATCHLOG00032','2018-05-17 15:33:50.422410','2018-05-17 15:33:50.422410','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'email\', \'doctype\', \'email_alert\') #2014-07-15'),('PATCHLOG00033','2018-05-17 15:33:50.424308','2018-05-17 15:33:50.424308','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'desk\', \'doctype\', \'todo\') #2014-12-31-1'),('PATCHLOG00034','2018-05-17 15:33:50.426258','2018-05-17 15:33:50.426258','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'custom\', \'doctype\', \'property_setter\') #2014-12-31-1'),('PATCHLOG00035','2018-05-17 15:33:50.428188','2018-05-17 15:33:50.428188','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'patch_log\') #2016-10-31'),('PATCHLOG00036','2018-05-17 15:33:50.430088','2018-05-17 15:33:50.430088','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doctype(\"File\") # 2015-10-19'),('PATCHLOG00037','2018-05-17 15:33:50.432038','2018-05-17 15:33:50.432038','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'core\', \'doctype\', \'error_snapshot\')'),('PATCHLOG00038','2018-05-17 15:33:50.434097','2018-05-17 15:33:50.434097','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.clear_cache()'),('PATCHLOG00039','2018-05-17 15:33:50.436086','2018-05-17 15:33:50.436086','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_1.rename_scheduler_log_to_error_log'),('PATCHLOG00040','2018-05-17 15:33:50.438361','2018-05-17 15:33:50.438361','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_1.sync_language_doctype'),('PATCHLOG00041','2018-05-17 15:33:50.440736','2018-05-17 15:33:50.440736','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.rename_bulk_email_to_email_queue'),('PATCHLOG00042','2018-05-17 15:33:50.442820','2018-05-17 15:33:50.442820','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_1.rename_chinese_language_codes'),('PATCHLOG00043','2018-05-17 15:33:50.444830','2018-05-17 15:33:50.444830','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"alter table `tabSessions` modify `user` varchar(255), engine=InnoDB\")'),('PATCHLOG00044','2018-05-17 15:33:50.446796','2018-05-17 15:33:50.446796','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"delete from `tabDocField` where parent=\'0\'\")'),('PATCHLOG00045','2018-05-17 15:33:50.448724','2018-05-17 15:33:50.448724','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.change_varchar_length'),('PATCHLOG00046','2018-05-17 15:33:50.450861','2018-05-17 15:33:50.450861','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_4.reduce_varchar_length'),('PATCHLOG00047','2018-05-17 15:33:50.453026','2018-05-17 15:33:50.453026','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_2.change_checks_to_not_null'),('PATCHLOG00048','2018-05-17 15:33:50.455068','2018-05-17 15:33:50.455068','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_9.int_float_not_null #2015-11-25'),('PATCHLOG00049','2018-05-17 15:33:50.457145','2018-05-17 15:33:50.457145','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.v4_to_v5'),('PATCHLOG00050','2018-05-17 15:33:50.459027','2018-05-17 15:33:50.459027','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.remove_shopping_cart_app'),('PATCHLOG00051','2018-05-17 15:33:50.460907','2018-05-17 15:33:50.460907','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.webnotes_to_frappe'),('PATCHLOG00052','2018-05-17 15:33:50.462812','2018-05-17 15:33:50.462812','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.permissions.reset_perms(\"Module Def\")'),('PATCHLOG00053','2018-05-17 15:33:50.465121','2018-05-17 15:33:50.465121','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:import frappe.installer;frappe.installer.make_site_dirs() #2014-02-19'),('PATCHLOG00054','2018-05-17 15:33:50.467063','2018-05-17 15:33:50.467063','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.rename_profile_to_user'),('PATCHLOG00055','2018-05-17 15:33:50.469049','2018-05-17 15:33:50.469049','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.deprecate_control_panel'),('PATCHLOG00056','2018-05-17 15:33:50.471040','2018-05-17 15:33:50.471040','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.remove_old_parent'),('PATCHLOG00057','2018-05-17 15:33:50.472923','2018-05-17 15:33:50.472923','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.rename_sitemap_to_route'),('PATCHLOG00058','2018-05-17 15:33:50.475181','2018-05-17 15:33:50.475181','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.website_sitemap_hierarchy'),('PATCHLOG00059','2018-05-17 15:33:50.477207','2018-05-17 15:33:50.477207','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.remove_index_sitemap'),('PATCHLOG00060','2018-05-17 15:33:50.479229','2018-05-17 15:33:50.479229','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.set_website_route_idx'),('PATCHLOG00061','2018-05-17 15:33:50.481133','2018-05-17 15:33:50.481133','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.add_delete_permission'),('PATCHLOG00062','2018-05-17 15:33:50.483045','2018-05-17 15:33:50.483045','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.set_todo_checked_as_closed'),('PATCHLOG00063','2018-05-17 15:33:50.484899','2018-05-17 15:33:50.484899','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.private_backups'),('PATCHLOG00064','2018-05-17 15:33:50.486800','2018-05-17 15:33:50.486800','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.set_module_in_report'),('PATCHLOG00065','2018-05-17 15:33:50.488678','2018-05-17 15:33:50.488678','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.update_datetime'),('PATCHLOG00066','2018-05-17 15:33:50.490641','2018-05-17 15:33:50.490641','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.file_manager_hooks'),('PATCHLOG00067','2018-05-17 15:33:50.492530','2018-05-17 15:33:50.492530','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.get_doc(\"User\", \"Guest\").save()'),('PATCHLOG00068','2018-05-17 15:33:50.494489','2018-05-17 15:33:50.494489','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.update_custom_field_insert_after'),('PATCHLOG00069','2018-05-17 15:33:50.496458','2018-05-17 15:33:50.496458','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.deprecate_link_selects'),('PATCHLOG00070','2018-05-17 15:33:50.498336','2018-05-17 15:33:50.498336','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.set_user_gravatar'),('PATCHLOG00071','2018-05-17 15:33:50.500270','2018-05-17 15:33:50.500270','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.set_user_permissions'),('PATCHLOG00072','2018-05-17 15:33:50.502214','2018-05-17 15:33:50.502214','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.create_custom_field_for_owner_match'),('PATCHLOG00073','2018-05-17 15:33:50.504146','2018-05-17 15:33:50.504146','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.enable_scheduler_in_system_settings'),('PATCHLOG00074','2018-05-17 15:33:50.506057','2018-05-17 15:33:50.506057','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"update tabReport set apply_user_permissions=1\") #2014-06-03'),('PATCHLOG00075','2018-05-17 15:33:50.508064','2018-05-17 15:33:50.508064','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.replace_deprecated_timezones'),('PATCHLOG00076','2018-05-17 15:33:50.510054','2018-05-17 15:33:50.510054','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:import frappe.website.render; frappe.website.render.clear_cache(\"login\"); #2014-06-10'),('PATCHLOG00077','2018-05-17 15:33:50.512011','2018-05-17 15:33:50.512011','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.fix_attach_field_file_url'),('PATCHLOG00078','2018-05-17 15:33:50.513917','2018-05-17 15:33:50.513917','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.permissions.reset_perms(\"User\") #2015-03-24'),('PATCHLOG00079','2018-05-17 15:33:50.516138','2018-05-17 15:33:50.516138','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"\"\"delete from `tabUserRole` where ifnull(parentfield, \'\')=\'\' or ifnull(`role`, \'\')=\'\'\"\"\") #2014-08-18'),('PATCHLOG00080','2018-05-17 15:33:50.518074','2018-05-17 15:33:50.518074','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_0.remove_user_owner_custom_field'),('PATCHLOG00081','2018-05-17 15:33:50.520035','2018-05-17 15:33:50.520035','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.delete_doc(\"DocType\", \"Website Template\")'),('PATCHLOG00082','2018-05-17 15:33:50.522011','2018-05-17 15:33:50.522011','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"\"\"update `tabProperty Setter` set property_type=\'Text\' where property in (\'options\', \'default\')\"\"\") #2014-06-20'),('PATCHLOG00083','2018-05-17 15:33:50.523912','2018-05-17 15:33:50.523912','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_1.enable_outgoing_email_settings'),('PATCHLOG00084','2018-05-17 15:33:50.525870','2018-05-17 15:33:50.525870','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"\"\"update `tabSingles` set `value`=`doctype` where `field`=\'name\'\"\"\") #2014-07-04'),('PATCHLOG00085','2018-05-17 15:33:50.527818','2018-05-17 15:33:50.527818','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_1.enable_print_as_pdf #2014-06-17'),('PATCHLOG00086','2018-05-17 15:33:50.529816','2018-05-17 15:33:50.529816','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"\"\"update `tabDocPerm` set email=1 where parent=\'User\' and permlevel=0 and `role`=\'All\' and `read`=1 and apply_user_permissions=1\"\"\") #2014-07-15'),('PATCHLOG00087','2018-05-17 15:33:50.531774','2018-05-17 15:33:50.531774','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"\"\"update `tabPrint Format` set print_format_type=\'Client\' where ifnull(print_format_type, \'\')=\'\'\"\"\") #2014-07-28'),('PATCHLOG00088','2018-05-17 15:33:50.533712','2018-05-17 15:33:50.533712','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_1.file_manager_fix'),('PATCHLOG00089','2018-05-17 15:33:50.535638','2018-05-17 15:33:50.535638','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_2.print_with_letterhead'),('PATCHLOG00090','2018-05-17 15:33:50.537542','2018-05-17 15:33:50.537542','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.delete_doc(\"DocType\", \"Control Panel\", force=1)'),('PATCHLOG00091','2018-05-17 15:33:50.539587','2018-05-17 15:33:50.539587','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'website\', \'doctype\', \'web_form\') #2014-09-04'),('PATCHLOG00092','2018-05-17 15:33:50.541576','2018-05-17 15:33:50.541576','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\'website\', \'doctype\', \'web_form_field\') #2014-09-04'),('PATCHLOG00093','2018-05-17 15:33:50.543501','2018-05-17 15:33:50.543501','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_2.refactor_website_routing'),('PATCHLOG00094','2018-05-17 15:33:50.545476','2018-05-17 15:33:50.545476','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_2.set_assign_in_doc'),('PATCHLOG00095','2018-05-17 15:33:50.547442','2018-05-17 15:33:50.547442','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v4_3.remove_allow_on_submit_customization'),('PATCHLOG00096','2018-05-17 15:33:50.549902','2018-05-17 15:33:50.549902','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.rename_table_fieldnames'),('PATCHLOG00097','2018-05-17 15:33:50.551857','2018-05-17 15:33:50.551857','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.communication_parent'),('PATCHLOG00098','2018-05-17 15:33:50.553808','2018-05-17 15:33:50.553808','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.clear_website_group_and_notifications'),('PATCHLOG00099','2018-05-17 15:33:50.555766','2018-05-17 15:33:50.555766','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"\"\"update tabComment set comment = substr(comment, 6, locate(\":\", comment)-6) where comment_type in (\"Assigned\", \"Assignment Completed\")\"\"\")'),('PATCHLOG00100','2018-05-17 15:33:50.557731','2018-05-17 15:33:50.557731','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"update `tabComment` set comment_type=\'Comment\' where comment_doctype=\'Blog Post\' and ifnull(comment_type, \'\')=\'\'\")'),('PATCHLOG00101','2018-05-17 15:33:50.559586','2018-05-17 15:33:50.559586','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.update_shared'),('PATCHLOG00102','2018-05-17 15:33:50.561573','2018-05-17 15:33:50.561573','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.reload_doc(\"core\", \"doctype\", \"docshare\") #2015-07-21'),('PATCHLOG00103','2018-05-17 15:33:50.563543','2018-05-17 15:33:50.563543','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_19.comment_feed_communication'),('PATCHLOG00104','2018-05-17 15:33:50.565484','2018-05-17 15:33:50.565484','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_16.star_to_like'),('PATCHLOG00105','2018-05-17 15:33:50.567408','2018-05-17 15:33:50.567408','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.bookmarks_to_stars'),('PATCHLOG00106','2018-05-17 15:33:50.569346','2018-05-17 15:33:50.569346','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.style_settings_to_website_theme'),('PATCHLOG00107','2018-05-17 15:33:50.571342','2018-05-17 15:33:50.571342','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.rename_ref_type_fieldnames'),('PATCHLOG00108','2018-05-17 15:33:50.573294','2018-05-17 15:33:50.573294','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.fix_email_alert'),('PATCHLOG00109','2018-05-17 15:33:50.575231','2018-05-17 15:33:50.575231','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.fix_null_date_datetime'),('PATCHLOG00110','2018-05-17 15:33:50.577119','2018-05-17 15:33:50.577119','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.force_sync_website'),('PATCHLOG00111','2018-05-17 15:33:50.579060','2018-05-17 15:33:50.579060','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.delete_doc(\"DocType\", \"Tag\")'),('PATCHLOG00112','2018-05-17 15:33:50.581124','2018-05-17 15:33:50.581124','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"delete from `tabProperty Setter` where `property` in (\'idx\', \'_idx\')\")'),('PATCHLOG00113','2018-05-17 15:33:50.584124','2018-05-17 15:33:50.584124','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.move_scheduler_last_event_to_system_settings'),('PATCHLOG00114','2018-05-17 15:33:50.586102','2018-05-17 15:33:50.586102','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"update tabUser set new_password=\'\' where ifnull(new_password, \'\')!=\'\'\")'),('PATCHLOG00115','2018-05-17 15:33:50.588083','2018-05-17 15:33:50.588083','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.fix_text_editor_file_urls'),('PATCHLOG00116','2018-05-17 15:33:50.590114','2018-05-17 15:33:50.590114','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.modify_session'),('PATCHLOG00117','2018-05-17 15:33:50.592164','2018-05-17 15:33:50.592164','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v5_0.expire_old_scheduler_logs'),('PATCHLOG00118','2018-05-17 15:33:50.594152','2018-05-17 15:33:50.594152','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.permissions.reset_perms(\"DocType\")'),('PATCHLOG00119','2018-05-17 15:33:50.596180','2018-05-17 15:33:50.596180','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"delete from `tabProperty Setter` where `property` = \'idx\'\")'),('PATCHLOG00120','2018-05-17 15:33:50.598161','2018-05-17 15:33:50.598161','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_0.communication_status_and_permission'),('PATCHLOG00121','2018-05-17 15:33:50.600198','2018-05-17 15:33:50.600198','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_0.make_task_log_folder'),('PATCHLOG00122','2018-05-17 15:33:50.602175','2018-05-17 15:33:50.602175','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_0.document_type_rename'),('PATCHLOG00123','2018-05-17 15:33:50.604354','2018-05-17 15:33:50.604354','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_0.fix_ghana_currency'),('PATCHLOG00124','2018-05-17 15:33:50.606444','2018-05-17 15:33:50.606444','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_2.ignore_user_permissions_if_missing'),('PATCHLOG00125','2018-05-17 15:33:50.608766','2018-05-17 15:33:50.608766','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"delete from tabSessions where user is null\")'),('PATCHLOG00126','2018-05-17 15:33:50.610818','2018-05-17 15:33:50.610818','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_2.rename_backup_manager'),('PATCHLOG00127','2018-05-17 15:33:50.613081','2018-05-17 15:33:50.613081','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.delete_doc(\"DocType\", \"Backup Manager\")'),('PATCHLOG00128','2018-05-17 15:33:50.615027','2018-05-17 15:33:50.615027','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"\"\"update `tabCommunication` set parenttype=null, parent=null, parentfield=null\"\"\") #2015-10-22'),('PATCHLOG00129','2018-05-17 15:33:50.617147','2018-05-17 15:33:50.617147','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.permissions.reset_perms(\"Web Page\")'),('PATCHLOG00130','2018-05-17 15:33:50.619093','2018-05-17 15:33:50.619093','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_6.user_last_active'),('PATCHLOG00131','2018-05-17 15:33:50.621122','2018-05-17 15:33:50.621122','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_6.fix_file_url'),('PATCHLOG00132','2018-05-17 15:33:50.623092','2018-05-17 15:33:50.623092','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_11.rename_field_in_email_account'),('PATCHLOG00133','2018-05-17 15:33:50.625033','2018-05-17 15:33:50.625033','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.create_private_file_folder'),('PATCHLOG00134','2018-05-17 15:33:50.626941','2018-05-17 15:33:50.626941','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_15.remove_property_setter_for_previous_field #2015-12-29'),('PATCHLOG00135','2018-05-17 15:33:50.628953','2018-05-17 15:33:50.628953','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_15.set_username'),('PATCHLOG00136','2018-05-17 15:33:50.630905','2018-05-17 15:33:50.630905','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.permissions.reset_perms(\"Error Snapshot\")'),('PATCHLOG00137','2018-05-17 15:33:50.632834','2018-05-17 15:33:50.632834','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_16.feed_doc_owner'),('PATCHLOG00138','2018-05-17 15:33:50.634802','2018-05-17 15:33:50.634802','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_21.print_settings_repeat_header_footer'),('PATCHLOG00139','2018-05-17 15:33:50.636780','2018-05-17 15:33:50.636780','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_24.set_language_as_code'),('PATCHLOG00140','2018-05-17 15:33:50.638786','2018-05-17 15:33:50.638786','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_20x.update_insert_after'),('PATCHLOG00141','2018-05-17 15:33:50.640767','2018-05-17 15:33:50.640767','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'finally:frappe.patches.v6_24.sync_desktop_icons'),('PATCHLOG00142','2018-05-17 15:33:50.642900','2018-05-17 15:33:50.642900','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_20x.set_allow_draft_for_print'),('PATCHLOG00143','2018-05-17 15:33:50.644913','2018-05-17 15:33:50.644913','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v6_20x.remove_roles_from_website_user'),('PATCHLOG00144','2018-05-17 15:33:50.646986','2018-05-17 15:33:50.646986','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.set_user_fullname'),('PATCHLOG00145','2018-05-17 15:33:50.648929','2018-05-17 15:33:50.648929','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.desktop_icons_hidden_by_admin_as_blocked'),('PATCHLOG00146','2018-05-17 15:33:50.650958','2018-05-17 15:33:50.650958','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.add_communication_in_doc'),('PATCHLOG00147','2018-05-17 15:33:50.652883','2018-05-17 15:33:50.652883','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.update_send_after_in_bulk_email'),('PATCHLOG00148','2018-05-17 15:33:50.655118','2018-05-17 15:33:50.655118','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\'\'\'delete from `tabSingles` where doctype=\"Email Settings\"\'\'\') # 2016-06-13'),('PATCHLOG00149','2018-05-17 15:33:50.657489','2018-05-17 15:33:50.657489','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"delete from `tabWeb Page` where ifnull(template_path, \'\')!=\'\'\")'),('PATCHLOG00150','2018-05-17 15:33:50.659986','2018-05-17 15:33:50.659986','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.rename_newsletter_list_to_email_group'),('PATCHLOG00151','2018-05-17 15:33:50.662867','2018-05-17 15:33:50.662867','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.replace_upgrade_link_limit'),('PATCHLOG00152','2018-05-17 15:33:50.665149','2018-05-17 15:33:50.665149','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.set_email_group'),('PATCHLOG00153','2018-05-17 15:33:50.667087','2018-05-17 15:33:50.667087','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_1.setup_integration_services #2016-10-27'),('PATCHLOG00154','2018-05-17 15:33:50.708919','2018-05-17 15:33:50.708919','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_1.rename_chinese_language_codes'),('PATCHLOG00155','2018-05-17 15:33:50.711172','2018-05-17 15:33:50.711172','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.core.doctype.language.language.update_language_names() # 2017-04-12'),('PATCHLOG00156','2018-05-17 15:33:50.713175','2018-05-17 15:33:50.713175','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.set_value(\"Print Settings\", \"Print Settings\", \"add_draft_heading\", 1)'),('PATCHLOG00157','2018-05-17 15:33:50.715139','2018-05-17 15:33:50.715139','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.cleanup_list_settings'),('PATCHLOG00158','2018-05-17 15:33:50.717185','2018-05-17 15:33:50.717185','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.set_default(\'language\', \'\')'),('PATCHLOG00159','2018-05-17 15:33:50.719206','2018-05-17 15:33:50.719206','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_1.refactor_integration_broker'),('PATCHLOG00160','2018-05-17 15:33:50.721218','2018-05-17 15:33:50.721218','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_1.set_backup_limit'),('PATCHLOG00161','2018-05-17 15:33:50.723143','2018-05-17 15:33:50.723143','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_2.set_doctype_engine'),('PATCHLOG00162','2018-05-17 15:33:50.725174','2018-05-17 15:33:50.725174','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_2.merge_knowledge_base'),('PATCHLOG00163','2018-05-17 15:33:50.727105','2018-05-17 15:33:50.727105','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_0.update_report_builder_json'),('PATCHLOG00164','2018-05-17 15:33:50.729267','2018-05-17 15:33:50.729267','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_2.set_in_standard_filter_property #1'),('PATCHLOG00165','2018-05-17 15:33:50.731207','2018-05-17 15:33:50.731207','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.drop_unwanted_indexes'),('PATCHLOG00166','2018-05-17 15:33:50.733237','2018-05-17 15:33:50.733237','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"update tabCommunication set communication_date = creation where time(communication_date) = 0\")'),('PATCHLOG00167','2018-05-17 15:33:50.735243','2018-05-17 15:33:50.735243','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_2.fix_email_queue_recipient'),('PATCHLOG00168','2018-05-17 15:33:50.737424','2018-05-17 15:33:50.737424','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_2.update_feedback_request # 2017-02-27'),('PATCHLOG00169','2018-05-17 15:33:50.740389','2018-05-17 15:33:50.740389','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.rename_doc(\'Country\', \'Macedonia, Republic of\', \'Macedonia\', ignore_if_exists=True)'),('PATCHLOG00170','2018-05-17 15:33:50.742365','2018-05-17 15:33:50.742365','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.rename_doc(\'Country\', \'Iran, Islamic Republic of\', \'Iran\', ignore_if_exists=True)'),('PATCHLOG00171','2018-05-17 15:33:50.744804','2018-05-17 15:33:50.744804','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.rename_doc(\'Country\', \'Tanzania, United Republic of\', \'Tanzania\', ignore_if_exists=True)'),('PATCHLOG00172','2018-05-17 15:33:50.746808','2018-05-17 15:33:50.746808','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.rename_doc(\'Country\', \'Syrian Arab Republic\', \'Syria\', ignore_if_exists=True)'),('PATCHLOG00173','2018-05-17 15:33:50.748857','2018-05-17 15:33:50.748857','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.rename_listsettings_to_usersettings'),('PATCHLOG00174','2018-05-17 15:33:50.750908','2018-05-17 15:33:50.750908','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_2.update_communications'),('PATCHLOG00175','2018-05-17 15:33:50.752905','2018-05-17 15:33:50.752905','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.deprecate_integration_broker'),('PATCHLOG00176','2018-05-17 15:33:50.754929','2018-05-17 15:33:50.754929','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.update_gender_and_salutation'),('PATCHLOG00177','2018-05-17 15:33:50.757205','2018-05-17 15:33:50.757205','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.setup_email_inbox #2017-03-29'),('PATCHLOG00178','2018-05-17 15:33:50.759203','2018-05-17 15:33:50.759203','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.newsletter_childtable_migrate'),('PATCHLOG00179','2018-05-17 15:33:50.761483','2018-05-17 15:33:50.761483','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"delete from `tabDesktop Icon` where module_name=\'Communication\'\")'),('PATCHLOG00180','2018-05-17 15:33:50.764367','2018-05-17 15:33:50.764367','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\"update `tabDesktop Icon` set type=\'list\' where _doctype=\'Communication\'\")'),('PATCHLOG00181','2018-05-17 15:33:50.766387','2018-05-17 15:33:50.766387','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.fix_non_english_desktop_icons # 2017-04-12'),('PATCHLOG00182','2018-05-17 15:33:50.768384','2018-05-17 15:33:50.768384','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.set_doctype_values_in_custom_role'),('PATCHLOG00183','2018-05-17 15:33:50.770432','2018-05-17 15:33:50.770432','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.install_new_build_system_requirements'),('PATCHLOG00184','2018-05-17 15:33:50.772473','2018-05-17 15:33:50.772473','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.set_currency_field_precision	# 2017-05-09'),('PATCHLOG00185','2018-05-17 15:33:50.774610','2018-05-17 15:33:50.774610','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.rename_print_to_printing'),('PATCHLOG00186','2018-05-17 15:33:50.776736','2018-05-17 15:33:50.776736','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v7_1.disabled_print_settings_for_custom_print_format'),('PATCHLOG00187','2018-05-17 15:33:50.778669','2018-05-17 15:33:50.778669','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_0.update_desktop_icons'),('PATCHLOG00188','2018-05-17 15:33:50.780784','2018-05-17 15:33:50.780784','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.db.sql(\'update tabReport set module=\"Desk\" where name=\"ToDo\"\')'),('PATCHLOG00189','2018-05-17 15:33:50.783308','2018-05-17 15:33:50.783308','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_1.enable_allow_error_traceback_in_system_settings'),('PATCHLOG00190','2018-05-17 15:33:50.785726','2018-05-17 15:33:50.785726','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_1.update_format_options_in_auto_email_report'),('PATCHLOG00191','2018-05-17 15:33:50.787715','2018-05-17 15:33:50.787715','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_1.delete_custom_docperm_if_doctype_not_exists'),('PATCHLOG00192','2018-05-17 15:33:50.789744','2018-05-17 15:33:50.789744','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_5.delete_email_group_member_with_invalid_emails'),('PATCHLOG00193','2018-05-17 15:33:50.791755','2018-05-17 15:33:50.791755','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_x.update_user_permission'),('PATCHLOG00194','2018-05-17 15:33:50.793696','2018-05-17 15:33:50.793696','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_5.patch_event_colors'),('PATCHLOG00195','2018-05-17 15:33:50.795622','2018-05-17 15:33:50.795622','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v8_10.delete_static_web_page_from_global_search'),('PATCHLOG00196','2018-05-17 15:33:50.797577','2018-05-17 15:33:50.797577','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v9_1.add_sms_sender_name_as_parameters'),('PATCHLOG00197','2018-05-17 15:33:50.799552','2018-05-17 15:33:50.799552','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v9_1.resave_domain_settings'),('PATCHLOG00198','2018-05-17 15:33:50.801553','2018-05-17 15:33:50.801553','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v9_1.revert_domain_settings'),('PATCHLOG00199','2018-05-17 15:33:50.803567','2018-05-17 15:33:50.803567','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v9_1.move_feed_to_activity_log'),('PATCHLOG00200','2018-05-17 15:33:50.805605','2018-05-17 15:33:50.805605','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'execute:frappe.delete_doc(\'Page\', \'data-import-tool\', ignore_missing=True)'),('PATCHLOG00201','2018-05-17 15:33:50.807586','2018-05-17 15:33:50.807586','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v10_0.reload_countries_and_currencies'),('PATCHLOG00202','2018-05-17 15:33:50.809522','2018-05-17 15:33:50.809522','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v10_0.set_no_copy_to_workflow_state'),('PATCHLOG00203','2018-05-17 15:33:50.811648','2018-05-17 15:33:50.811648','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'frappe.patches.v10_0.increase_single_table_column_length');
/*!40000 ALTER TABLE `tabPatch Log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabPayment Gateway`
--

DROP TABLE IF EXISTS `tabPayment Gateway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabPayment Gateway` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `gateway` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabPayment Gateway`
--

LOCK TABLES `tabPayment Gateway` WRITE;
/*!40000 ALTER TABLE `tabPayment Gateway` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabPayment Gateway` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabPortal Menu Item`
--

DROP TABLE IF EXISTS `tabPortal Menu Item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabPortal Menu Item` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `reference_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enabled` int(1) NOT NULL DEFAULT '0',
  `role` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabPortal Menu Item`
--

LOCK TABLES `tabPortal Menu Item` WRITE;
/*!40000 ALTER TABLE `tabPortal Menu Item` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabPortal Menu Item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabPrint Format`
--

DROP TABLE IF EXISTS `tabPrint Format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabPrint Format` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `module` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disabled` int(1) NOT NULL DEFAULT '0',
  `custom_format` int(1) NOT NULL DEFAULT '0',
  `font` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Default',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `html` longtext COLLATE utf8mb4_unicode_ci,
  `css` longtext COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `print_format_builder` int(1) NOT NULL DEFAULT '0',
  `standard` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'No',
  `align_labels_right` int(1) NOT NULL DEFAULT '0',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `print_format_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Server',
  `format_data` longtext COLLATE utf8mb4_unicode_ci,
  `doc_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_section_headings` int(1) NOT NULL DEFAULT '0',
  `line_breaks` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `standard` (`standard`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabPrint Format`
--

LOCK TABLES `tabPrint Format` WRITE;
/*!40000 ALTER TABLE `tabPrint Format` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabPrint Format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabPrint Heading`
--

DROP TABLE IF EXISTS `tabPrint Heading`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabPrint Heading` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `print_heading` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabPrint Heading`
--

LOCK TABLES `tabPrint Heading` WRITE;
/*!40000 ALTER TABLE `tabPrint Heading` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabPrint Heading` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabPrint Style`
--

DROP TABLE IF EXISTS `tabPrint Style`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabPrint Style` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `print_style_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `standard` int(1) NOT NULL DEFAULT '0',
  `disabled` int(1) NOT NULL DEFAULT '0',
  `preview` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `css` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabPrint Style`
--

LOCK TABLES `tabPrint Style` WRITE;
/*!40000 ALTER TABLE `tabPrint Style` DISABLE KEYS */;
INSERT INTO `tabPrint Style` VALUES ('Classic','2017-08-17 02:00:12.502887','2017-08-18 00:43:48.675833','Administrator','Administrator',0,NULL,NULL,NULL,1,NULL,'Classic',NULL,NULL,1,0,'/assets/frappe/images/help/print-style-classic.png',NULL,'/*\n	common style for whole page\n	This should include:\n	+ page size related settings\n	+ font family settings\n	+ line spacing settings\n*/\n.print-format div,\n.print-format span,\n.print-format td,\n.print-format h1,\n.print-format h2,\n.print-format h3,\n.print-format h4 {\n	font-family: Georgia, serif;\n}\n\n/* classic format: for-test */'),('Modern','2017-08-17 02:16:58.060374','2017-08-18 00:44:07.438147','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'Modern',NULL,NULL,1,0,'/assets/frappe/images/help/print-style-modern.png',NULL,'.print-heading {\n	text-align: right;\n	text-transform: uppercase;\n	color: #666;\n	padding-bottom: 20px;\n	margin-bottom: 20px;\n	border-bottom: 1px solid #d1d8dd;\n}\n\n.print-heading h2 {\n	font-size: 24px;\n}\n\n.print-format th {\n	background-color: #eee !important;\n	border-bottom: 0px !important;\n}\n\n/* modern format: for-test */'),('Monochrome','2017-08-17 02:16:20.992989','2017-08-18 00:44:25.023898','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'Monochrome',NULL,NULL,1,0,'/assets/frappe/images/help/print-style-monochrome.png',NULL,'.print-format * {\n	color: #000 !important;\n}\n\n.print-format .alert {\n	background-color: inherit;\n	border: 1px dashed #333;\n}\n\n.print-format .table-bordered,\n.print-format .table-bordered > thead > tr > th,\n.print-format .table-bordered > tbody > tr > th,\n.print-format .table-bordered > tfoot > tr > th,\n.print-format .table-bordered > thead > tr > td,\n.print-format .table-bordered > tbody > tr > td,\n.print-format .table-bordered > tfoot > tr > td {\n	border: 1px solid #333;\n}\n\n.print-format hr {\n	border-top: 1px solid #333;\n}\n\n.print-heading {\n	border-bottom: 2px solid #333;\n}\n');
/*!40000 ALTER TABLE `tabPrint Style` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabProperty Setter`
--

DROP TABLE IF EXISTS `tabProperty Setter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabProperty Setter` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `default_value` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `value` text COLLATE utf8mb4_unicode_ci,
  `doctype_or_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `property` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `doc_type` (`doc_type`),
  KEY `property` (`property`),
  KEY `field_name` (`field_name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabProperty Setter`
--

LOCK TABLES `tabProperty Setter` WRITE;
/*!40000 ALTER TABLE `tabProperty Setter` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabProperty Setter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabReport`
--

DROP TABLE IF EXISTS `tabReport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabReport` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `apply_user_permissions` int(1) NOT NULL DEFAULT '1',
  `javascript` longtext COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `add_total_row` int(1) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `ref_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `report_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disabled` int(1) NOT NULL DEFAULT '0',
  `json` longtext COLLATE utf8mb4_unicode_ci,
  `is_standard` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `report_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `query` longtext COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `letter_head` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabReport`
--

LOCK TABLES `tabReport` WRITE;
/*!40000 ALTER TABLE `tabReport` DISABLE KEYS */;
INSERT INTO `tabReport` VALUES ('Addresses And Contacts','2017-01-19 12:57:22.881566','2017-04-10 15:04:12.498920','Administrator','Administrator',0,NULL,NULL,NULL,2,1,NULL,NULL,0,NULL,NULL,'Address','Contacts','Addresses And Contacts',0,NULL,'Yes','Script Report',NULL,NULL,NULL),('Document Share Report','2015-02-05 06:01:35.060098','2017-02-24 20:01:16.232286','Administrator','Administrator',0,NULL,NULL,NULL,2,1,NULL,NULL,0,NULL,NULL,'DocShare','Core','Document Share Report',0,'{\"add_total_row\": 0, \"sort_by\": \"DocShare.modified\", \"sort_order\": \"desc\", \"sort_by_next\": null, \"filters\": [], \"sort_order_next\": \"desc\", \"columns\": [[\"name\", \"DocShare\"], [\"user\", \"DocShare\"], [\"share_doctype\", \"DocShare\"], [\"share_name\", \"DocShare\"], [\"read\", \"DocShare\"], [\"write\", \"DocShare\"], [\"share\", \"DocShare\"]]}','Yes','Report Builder',NULL,NULL,NULL),('Feedback Ratings','2017-02-05 20:38:21.890174','2017-02-24 19:56:51.141147','Administrator','Administrator',0,NULL,NULL,NULL,2,1,NULL,NULL,0,NULL,NULL,'Feedback Trigger','Core','Feedback Ratings',0,NULL,'Yes','Script Report',NULL,NULL,NULL),('Permitted Documents For User','2014-06-03 05:20:35.218263','2017-02-24 20:16:34.069990','Administrator','Administrator',0,NULL,NULL,NULL,3,1,NULL,NULL,0,NULL,NULL,'User','Core','Permitted Documents For User',0,NULL,'Yes','Script Report',NULL,NULL,NULL),('ToDo','2013-02-25 14:26:30.000000','2017-06-21 18:18:50.748793','Administrator','Administrator',0,NULL,NULL,NULL,3,1,NULL,NULL,0,NULL,NULL,'ToDo','Desk','ToDo',0,NULL,'Yes','Script Report',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tabReport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabRole`
--

DROP TABLE IF EXISTS `tabRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabRole` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `two_factor_auth` int(1) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `desk_access` int(1) NOT NULL DEFAULT '1',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `restrict_to_domain` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disabled` int(1) NOT NULL DEFAULT '0',
  `role_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabRole`
--

LOCK TABLES `tabRole` WRITE;
/*!40000 ALTER TABLE `tabRole` DISABLE KEYS */;
INSERT INTO `tabRole` VALUES ('Accounts Manager','2018-05-17 15:33:48.486046','2018-05-17 15:33:48.486046','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Accounts Manager',NULL),('Accounts User','2018-05-17 15:33:46.578607','2018-05-17 15:33:46.578607','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Accounts User',NULL),('Administrator','2018-05-17 15:33:43.014719','2018-05-17 15:33:43.014719','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Administrator',NULL),('All','2018-05-17 15:33:43.012319','2018-05-17 15:33:43.012319','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'All',NULL),('Blogger','2018-05-17 15:33:44.739988','2018-05-17 15:33:44.739988','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Blogger',NULL),('Guest','2018-05-17 15:33:43.016321','2018-05-17 15:33:43.016321','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Guest',NULL),('Knowledge Base Contributor','2018-05-17 15:33:44.548362','2018-05-17 15:33:44.548362','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Knowledge Base Contributor',NULL),('Knowledge Base Editor','2018-05-17 15:33:44.550733','2018-05-17 15:33:44.550733','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Knowledge Base Editor',NULL),('Maintenance Manager','2018-05-17 15:33:48.479003','2018-05-17 15:33:48.479003','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Maintenance Manager',NULL),('Maintenance User','2018-05-17 15:33:48.405854','2018-05-17 15:33:48.405854','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Maintenance User',NULL),('Newsletter Manager','2018-05-17 15:33:45.550452','2018-05-17 15:33:45.550452','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Newsletter Manager',NULL),('Purchase Manager','2018-05-17 15:33:48.475316','2018-05-17 15:33:48.475316','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Purchase Manager',NULL),('Purchase Master Manager','2018-05-17 15:33:48.488060','2018-05-17 15:33:48.488060','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Purchase Master Manager',NULL),('Purchase User','2018-05-17 15:33:46.581099','2018-05-17 15:33:46.581099','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Purchase User',NULL),('Report Manager','2018-05-17 15:33:43.577907','2018-05-17 15:33:43.577907','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Report Manager',NULL),('Sales Manager','2018-05-17 15:33:48.483823','2018-05-17 15:33:48.483823','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Sales Manager',NULL),('Sales Master Manager','2018-05-17 15:33:48.481807','2018-05-17 15:33:48.481807','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Sales Master Manager',NULL),('Sales User','2018-05-17 15:33:46.575765','2018-05-17 15:33:46.575765','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Sales User',NULL),('System Manager','2018-05-17 15:33:43.009805','2018-05-17 15:33:43.009805','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'System Manager',NULL),('Website Manager','2018-05-17 15:33:44.508359','2018-05-17 15:33:44.508359','Administrator','Administrator',0,NULL,NULL,NULL,0,0,NULL,1,NULL,NULL,NULL,0,'Website Manager',NULL);
/*!40000 ALTER TABLE `tabRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabRole Profile`
--

DROP TABLE IF EXISTS `tabRole Profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabRole Profile` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `role_profile` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `role_profile` (`role_profile`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabRole Profile`
--

LOCK TABLES `tabRole Profile` WRITE;
/*!40000 ALTER TABLE `tabRole Profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabRole Profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabSMS Parameter`
--

DROP TABLE IF EXISTS `tabSMS Parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabSMS Parameter` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `header` int(1) NOT NULL DEFAULT '0',
  `parameter` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabSMS Parameter`
--

LOCK TABLES `tabSMS Parameter` WRITE;
/*!40000 ALTER TABLE `tabSMS Parameter` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabSMS Parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabSalutation`
--

DROP TABLE IF EXISTS `tabSalutation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabSalutation` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `salutation` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabSalutation`
--

LOCK TABLES `tabSalutation` WRITE;
/*!40000 ALTER TABLE `tabSalutation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabSalutation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabSeries`
--

DROP TABLE IF EXISTS `tabSeries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabSeries` (
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabSeries`
--

LOCK TABLES `tabSeries` WRITE;
/*!40000 ALTER TABLE `tabSeries` DISABLE KEYS */;
INSERT INTO `tabSeries` VALUES ('PATCHLOG',203);
/*!40000 ALTER TABLE `tabSeries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabSessions`
--

DROP TABLE IF EXISTS `tabSessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabSessions` (
  `user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sessiondata` longtext COLLATE utf8mb4_unicode_ci,
  `ipaddress` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastupdate` datetime(6) DEFAULT NULL,
  `device` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'desktop',
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  KEY `sid` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabSessions`
--

LOCK TABLES `tabSessions` WRITE;
/*!40000 ALTER TABLE `tabSessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabSessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabSingles`
--

DROP TABLE IF EXISTS `tabSingles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabSingles` (
  `doctype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  KEY `singles_doctype_field_index` (`doctype`,`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabSingles`
--

LOCK TABLES `tabSingles` WRITE;
/*!40000 ALTER TABLE `tabSingles` DISABLE KEYS */;
INSERT INTO `tabSingles` VALUES ('Dropbox Settings','app_secret_key',NULL),('Dropbox Settings','modified_by','Administrator'),('Dropbox Settings','name','Dropbox Settings'),('Dropbox Settings','parent',NULL),('Dropbox Settings','app_access_key',NULL),('Dropbox Settings','send_notifications_to',NULL),('Dropbox Settings','creation','2018-05-17 15:33:48.991519'),('Dropbox Settings','enabled','0'),('Dropbox Settings','modified','2018-05-17 15:33:48.991519'),('Dropbox Settings','idx','0'),('Dropbox Settings','parenttype',NULL),('Dropbox Settings','backup_frequency',''),('Dropbox Settings','owner','Administrator'),('Dropbox Settings','docstatus','0'),('Dropbox Settings','dropbox_access_secret',NULL),('Dropbox Settings','dropbox_access_token',NULL),('Dropbox Settings','dropbox_access_key',NULL),('Dropbox Settings','parentfield',NULL),('User Permission for Page and Report','report',NULL),('User Permission for Page and Report','modified_by','Administrator'),('User Permission for Page and Report','name','User Permission for Page and Report'),('User Permission for Page and Report','parent',NULL),('User Permission for Page and Report','creation','2018-05-17 15:33:49.027941'),('User Permission for Page and Report','modified','2018-05-17 15:33:49.027941'),('User Permission for Page and Report','idx','0'),('User Permission for Page and Report','parenttype',NULL),('User Permission for Page and Report','owner','Administrator'),('User Permission for Page and Report','docstatus','0'),('User Permission for Page and Report','set_role_for',''),('User Permission for Page and Report','page',NULL),('User Permission for Page and Report','parentfield',NULL),('Domain Settings','modified_by','Administrator'),('Domain Settings','name','Domain Settings'),('Domain Settings','parent',NULL),('Domain Settings','creation','2018-05-17 15:33:49.087502'),('Domain Settings','modified','2018-05-17 15:33:49.087502'),('Domain Settings','idx','0'),('Domain Settings','parenttype',NULL),('Domain Settings','owner','Administrator'),('Domain Settings','docstatus','0'),('Domain Settings','parentfield',NULL),('SMS Settings','use_post','0'),('SMS Settings','modified_by','Administrator'),('SMS Settings','name','SMS Settings'),('SMS Settings','parent',NULL),('SMS Settings','receiver_parameter',NULL),('SMS Settings','creation','2018-05-17 15:33:49.144499'),('SMS Settings','message_parameter',NULL),('SMS Settings','modified','2018-05-17 15:33:49.144499'),('SMS Settings','idx','0'),('SMS Settings','parenttype',NULL),('SMS Settings','sms_gateway_url',NULL),('SMS Settings','owner','Administrator'),('SMS Settings','docstatus','0'),('SMS Settings','parentfield',NULL),('GSuite Settings','enable','0'),('GSuite Settings','modified_by','Administrator'),('GSuite Settings','name','GSuite Settings'),('GSuite Settings','parent',NULL),('GSuite Settings','authorization_code',NULL),('GSuite Settings','creation','2018-05-17 15:33:49.173475'),('GSuite Settings','modified','2018-05-17 15:33:49.173475'),('GSuite Settings','idx','0'),('GSuite Settings','parenttype',NULL),('GSuite Settings','script_url','https://script.google.com/macros/s/AKfycbxIFOx3301xwtF2IFPJ4pUQGqkNF3hBiBebppWkeKn6fKZRQvk/exec'),('GSuite Settings','client_id',NULL),('GSuite Settings','client_secret',NULL),('GSuite Settings','owner','Administrator'),('GSuite Settings','docstatus','0'),('GSuite Settings','refresh_token',NULL),('GSuite Settings','parentfield',NULL),('Google Maps','modified_by','Administrator'),('Google Maps','name','Google Maps'),('Google Maps','parent',NULL),('Google Maps','creation','2018-05-17 15:33:49.198054'),('Google Maps','enabled','0'),('Google Maps','modified','2018-05-17 15:33:49.198054'),('Google Maps','idx','0'),('Google Maps','parenttype',NULL),('Google Maps','owner','Administrator'),('Google Maps','docstatus','0'),('Google Maps','home_address',NULL),('Google Maps','client_key',NULL),('Google Maps','parentfield',NULL),('System Settings','disable_standard_email_footer','0'),('System Settings','email_footer_address',NULL),('System Settings','float_precision',''),('System Settings','minimum_password_score','2'),('System Settings','currency_precision',''),('System Settings','creation','2018-05-17 15:33:49.280037'),('System Settings','time_zone',NULL),('System Settings','setup_complete','0'),('System Settings','hide_footer_in_auto_email_reports','0'),('System Settings','owner','Administrator'),('System Settings','allow_login_using_user_name','0'),('System Settings','deny_multiple_sessions','0'),('System Settings','idx','0'),('System Settings','apply_strict_user_permissions','0'),('System Settings','docstatus','0'),('System Settings','bypass_2fa_for_retricted_ip_users','0'),('System Settings','session_expiry','06:00'),('System Settings','ignore_user_permissions_if_missing','0'),('System Settings','enable_two_factor_auth','0'),('System Settings','is_first_startup','0'),('System Settings','parent',NULL),('System Settings','scheduler_last_event',NULL),('System Settings','allow_error_traceback','1'),('System Settings','lifespan_qrcode_image','0'),('System Settings','number_format','#,###.##'),('System Settings','date_format','yyyy-mm-dd'),('System Settings','name','System Settings'),('System Settings','language',NULL),('System Settings','country',NULL),('System Settings','enable_password_policy','0'),('System Settings','allow_login_using_mobile_number','0'),('System Settings','parenttype',NULL),('System Settings','otp_issuer_name','Frappe Framework'),('System Settings','backup_limit','3'),('System Settings','two_factor_method','OTP App'),('System Settings','session_expiry_mobile','720:00'),('System Settings','parentfield',NULL),('S3 Backup Settings','modified_by','Administrator'),('S3 Backup Settings','name','S3 Backup Settings'),('S3 Backup Settings','parent',NULL),('S3 Backup Settings','secret_access_key',NULL),('S3 Backup Settings','creation','2018-05-17 15:33:49.669327'),('S3 Backup Settings','enabled','0'),('S3 Backup Settings','modified','2018-05-17 15:33:49.669327'),('S3 Backup Settings','notify_email',NULL),('S3 Backup Settings','idx','0'),('S3 Backup Settings','parenttype',NULL),('S3 Backup Settings','frequency','Daily'),('S3 Backup Settings','backup_limit','0'),('S3 Backup Settings','bucket',NULL),('S3 Backup Settings','access_key_id',NULL),('S3 Backup Settings','owner','Administrator'),('S3 Backup Settings','docstatus','0'),('S3 Backup Settings','parentfield',NULL),('Contact Us Settings','email_id',NULL),('Contact Us Settings','creation','2018-05-17 15:33:49.700949'),('Contact Us Settings','pincode',NULL),('Contact Us Settings','skype',NULL),('Contact Us Settings','owner','Administrator'),('Contact Us Settings','address_line2',NULL),('Contact Us Settings','city',NULL),('Contact Us Settings','address_line1',NULL),('Contact Us Settings','modified_by','Administrator'),('Contact Us Settings','address_title',NULL),('Contact Us Settings','introduction',NULL),('Contact Us Settings','state',NULL),('Contact Us Settings','docstatus','0'),('Contact Us Settings','query_options',NULL),('Contact Us Settings','parent',NULL),('Contact Us Settings','forward_to_email',NULL),('Contact Us Settings','phone',NULL),('Contact Us Settings','name','Contact Us Settings'),('Contact Us Settings','idx','0'),('Contact Us Settings','country',NULL),('Contact Us Settings','modified','2018-05-17 15:33:49.700949'),('Contact Us Settings','parenttype',NULL),('Contact Us Settings','heading',NULL),('Contact Us Settings','parentfield',NULL),('Test Runner','modified_by','Administrator'),('Test Runner','name','Test Runner'),('Test Runner','parent',NULL),('Test Runner','app',NULL),('Test Runner','creation','2018-05-17 15:33:49.767312'),('Test Runner','modified','2018-05-17 15:33:49.767312'),('Test Runner','idx','0'),('Test Runner','parenttype',NULL),('Test Runner','module_path',NULL),('Test Runner','owner','Administrator'),('Test Runner','docstatus','0'),('Test Runner','parentfield',NULL),('Role Permission for Page and Report','report',NULL),('Role Permission for Page and Report','modified_by','Administrator'),('Role Permission for Page and Report','name','Role Permission for Page and Report'),('Role Permission for Page and Report','parent',NULL),('Role Permission for Page and Report','creation','2018-05-17 15:33:49.882364'),('Role Permission for Page and Report','modified','2018-05-17 15:33:49.882364'),('Role Permission for Page and Report','idx','0'),('Role Permission for Page and Report','parenttype',NULL),('Role Permission for Page and Report','owner','Administrator'),('Role Permission for Page and Report','docstatus','0'),('Role Permission for Page and Report','set_role_for',''),('Role Permission for Page and Report','page',NULL),('Role Permission for Page and Report','parentfield',NULL),('Stripe Settings','publishable_key',NULL),('Stripe Settings','modified_by','Administrator'),('Stripe Settings','name','Stripe Settings'),('Stripe Settings','parent',NULL),('Stripe Settings','creation','2018-05-17 15:33:49.905609'),('Stripe Settings','modified','2018-05-17 15:33:49.905609'),('Stripe Settings','idx','0'),('Stripe Settings','parenttype',NULL),('Stripe Settings','owner','Administrator'),('Stripe Settings','docstatus','0'),('Stripe Settings','secret_key',NULL),('Stripe Settings','parentfield',NULL),('LDAP Settings','ldap_first_name_field',NULL),('LDAP Settings','modified_by','Administrator'),('LDAP Settings','name','LDAP Settings'),('LDAP Settings','parent',NULL),('LDAP Settings','creation','2018-05-17 15:33:49.931368'),('LDAP Settings','enabled','0'),('LDAP Settings','modified','2018-05-17 15:33:49.931368'),('LDAP Settings','ldap_email_field',NULL),('LDAP Settings','idx','0'),('LDAP Settings','parenttype',NULL),('LDAP Settings','organizational_unit',NULL),('LDAP Settings','base_dn',NULL),('LDAP Settings','owner','Administrator'),('LDAP Settings','docstatus','0'),('LDAP Settings','ldap_server_url',NULL),('LDAP Settings','password',NULL),('LDAP Settings','ldap_search_string',NULL),('LDAP Settings','ldap_username_field',NULL),('LDAP Settings','parentfield',NULL),('Website Settings','robots_txt',NULL),('Website Settings','creation','2018-05-17 15:33:49.957593'),('Website Settings','favicon',NULL),('Website Settings','owner','Administrator'),('Website Settings','banner_html',NULL),('Website Settings','google_analytics_id',NULL),('Website Settings','modified_by','Administrator'),('Website Settings','copyright',NULL),('Website Settings','home_page',NULL),('Website Settings','hide_footer_signup','0'),('Website Settings','brand_html',NULL),('Website Settings','head_html',NULL),('Website Settings','title_prefix',NULL),('Website Settings','subdomain',NULL),('Website Settings','parent',NULL),('Website Settings','disable_signup','0'),('Website Settings','banner_image',NULL),('Website Settings','docstatus','0'),('Website Settings','website_theme_image_link',NULL),('Website Settings','website_theme','Standard'),('Website Settings','name','Website Settings'),('Website Settings','idx','0'),('Website Settings','modified','2018-05-17 15:33:49.957593'),('Website Settings','parenttype',NULL),('Website Settings','navbar_search','0'),('Website Settings','address',NULL),('Website Settings','parentfield',NULL),('About Us Settings','modified_by','Administrator'),('About Us Settings','name','About Us Settings'),('About Us Settings','parent',NULL),('About Us Settings','footer',NULL),('About Us Settings','company_introduction',NULL),('About Us Settings','creation','2018-05-17 15:33:50.046919'),('About Us Settings','modified','2018-05-17 15:33:50.046919'),('About Us Settings','idx','0'),('About Us Settings','parenttype',NULL),('About Us Settings','company_history_heading',NULL),('About Us Settings','owner','Administrator'),('About Us Settings','docstatus','0'),('About Us Settings','team_members_heading',NULL),('About Us Settings','parentfield',NULL),('Blog Settings','modified_by','Administrator'),('Blog Settings','name','Blog Settings'),('Blog Settings','parent',NULL),('Blog Settings','creation','2018-05-17 15:33:50.071798'),('Blog Settings','modified','2018-05-17 15:33:50.071798'),('Blog Settings','idx','0'),('Blog Settings','parenttype',NULL),('Blog Settings','owner','Administrator'),('Blog Settings','docstatus','0'),('Blog Settings','writers_introduction',NULL),('Blog Settings','blog_title',NULL),('Blog Settings','blog_introduction',NULL),('Blog Settings','parentfield',NULL),('Website Script','javascript',NULL),('Website Script','modified_by','Administrator'),('Website Script','name','Website Script'),('Website Script','parent',NULL),('Website Script','creation','2018-05-17 15:33:50.095141'),('Website Script','modified','2018-05-17 15:33:50.095141'),('Website Script','idx','0'),('Website Script','parenttype',NULL),('Website Script','owner','Administrator'),('Website Script','docstatus','0'),('Website Script','parentfield',NULL),('Bulk Update','modified_by','Administrator'),('Bulk Update','name','Bulk Update'),('Bulk Update','parent',NULL),('Bulk Update','field',NULL),('Bulk Update','creation','2018-05-17 15:33:50.123443'),('Bulk Update','modified','2018-05-17 15:33:50.123443'),('Bulk Update','idx','0'),('Bulk Update','parenttype',NULL),('Bulk Update','limit','500'),('Bulk Update','owner','Administrator'),('Bulk Update','docstatus','0'),('Bulk Update','document_type',NULL),('Bulk Update','update_value',NULL),('Bulk Update','condition',NULL),('Bulk Update','parentfield',NULL),('Razorpay Settings','modified_by','Administrator'),('Razorpay Settings','name','Razorpay Settings'),('Razorpay Settings','parent',NULL),('Razorpay Settings','redirect_to',NULL),('Razorpay Settings','api_secret',NULL),('Razorpay Settings','creation','2018-05-17 15:33:50.150242'),('Razorpay Settings','modified','2018-05-17 15:33:50.150242'),('Razorpay Settings','idx','0'),('Razorpay Settings','parenttype',NULL),('Razorpay Settings','owner','Administrator'),('Razorpay Settings','docstatus','0'),('Razorpay Settings','api_key',NULL),('Razorpay Settings','parentfield',NULL),('PayPal Settings','paypal_sandbox','0'),('PayPal Settings','modified_by','Administrator'),('PayPal Settings','name','PayPal Settings'),('PayPal Settings','parent',NULL),('PayPal Settings','redirect_to',NULL),('PayPal Settings','creation','2018-05-17 15:33:50.174628'),('PayPal Settings','api_password',NULL),('PayPal Settings','modified','2018-05-17 15:33:50.174628'),('PayPal Settings','signature',NULL),('PayPal Settings','idx','0'),('PayPal Settings','parenttype',NULL),('PayPal Settings','api_username',NULL),('PayPal Settings','owner','Administrator'),('PayPal Settings','docstatus','0'),('PayPal Settings','parentfield',NULL),('Portal Settings','modified_by','Administrator'),('Portal Settings','name','Portal Settings'),('Portal Settings','parent',NULL),('Portal Settings','default_role',NULL),('Portal Settings','creation','2018-05-17 15:33:50.217273'),('Portal Settings','modified','2018-05-17 15:33:50.217273'),('Portal Settings','idx','0'),('Portal Settings','parenttype',NULL),('Portal Settings','hide_standard_menu','0'),('Portal Settings','owner','Administrator'),('Portal Settings','docstatus','0'),('Portal Settings','parentfield',NULL),('OAuth Provider Settings','modified_by','Administrator'),('OAuth Provider Settings','name','OAuth Provider Settings'),('OAuth Provider Settings','parent',NULL),('OAuth Provider Settings','creation','2018-05-17 15:33:50.263004'),('OAuth Provider Settings','modified','2018-05-17 15:33:50.263004'),('OAuth Provider Settings','idx','0'),('OAuth Provider Settings','parenttype',NULL),('OAuth Provider Settings','skip_authorization','Force'),('OAuth Provider Settings','owner','Administrator'),('OAuth Provider Settings','docstatus','0'),('OAuth Provider Settings','parentfield',NULL),('Social Login Keys','github_client_secret',NULL),('Social Login Keys','facebook_client_secret',NULL),('Social Login Keys','modified_by','Administrator'),('Social Login Keys','name','Social Login Keys'),('Social Login Keys','parent',NULL),('Social Login Keys','frappe_server_url',NULL),('Social Login Keys','creation','2018-05-17 15:33:50.292316'),('Social Login Keys','modified','2018-05-17 15:33:50.292316'),('Social Login Keys','facebook_client_id',NULL),('Social Login Keys','google_client_secret',NULL),('Social Login Keys','idx','0'),('Social Login Keys','parenttype',NULL),('Social Login Keys','frappe_client_id',NULL),('Social Login Keys','frappe_client_secret',NULL),('Social Login Keys','owner','Administrator'),('Social Login Keys','docstatus','0'),('Social Login Keys','google_client_id',NULL),('Social Login Keys','github_client_id',NULL),('Social Login Keys','parentfield',NULL),('Print Settings','creation','2018-05-17 15:33:49.732873'),('Print Settings','print_style','Modern'),('Print Settings','with_letterhead','1'),('Print Settings','owner','Administrator'),('Print Settings','font','Default'),('Print Settings','send_print_as_pdf','1'),('Print Settings','allow_page_break_inside_tables','0'),('Print Settings','modified_by','Administrator'),('Print Settings','repeat_header_footer','1'),('Print Settings','allow_print_for_draft','1'),('Print Settings','add_draft_heading','1'),('Print Settings','docstatus','0'),('Print Settings','parent',NULL),('Print Settings','attach_view_link','1'),('Print Settings','font_size','0'),('Print Settings','name','Print Settings'),('Print Settings','idx','0'),('Print Settings','modified','2018-05-17 15:33:52.042760'),('Print Settings','parenttype',NULL),('Print Settings','pdf_page_size','A4'),('Print Settings','allow_print_for_cancelled','0'),('Print Settings','parentfield',NULL),('System Settings','modified_by','Administrator'),('System Settings','modified','2018-05-17 15:34:11.828393'),('System Settings','enable_scheduler','0'),('Bench Settings','webserver_port','0'),('Bench Settings','frappe_git_branch',NULL),('Bench Settings','frappe_user',NULL),('Bench Settings','github_username',NULL),('Bench Settings','owner','Administrator'),('Bench Settings','redis_cache',NULL),('Bench Settings','auto_email_id',NULL),('Bench Settings','rebase_on_pull','0'),('Bench Settings','serve_default_site','0'),('Bench Settings','modified_by','Administrator'),('Bench Settings','mail_password',NULL),('Bench Settings','auto_update','0'),('Bench Settings','use_tls','0'),('Bench Settings','mail_login',NULL),('Bench Settings','admin_password',NULL),('Bench Settings','mail_port',NULL),('Bench Settings','docstatus','0'),('Bench Settings','dropbox_secret_key',NULL),('Bench Settings','redis_socketio',NULL),('Bench Settings','creation','2018-05-17 15:34:43.602838'),('Bench Settings','gunicorn_workers','0'),('Bench Settings','parent',NULL),('Bench Settings','file_watcher_port','0'),('Bench Settings','global_help_setup',NULL),('Bench Settings','root_password',NULL),('Bench Settings','redis_queue',NULL),('Bench Settings','shallow_clone','0'),('Bench Settings','restart_supervisor_on_update','0'),('Bench Settings','name','Bench Settings'),('Bench Settings','idx','0'),('Bench Settings','background_workers','0'),('Bench Settings','github_password',NULL),('Bench Settings','modified','2018-05-17 15:34:43.602838'),('Bench Settings','dropbox_access_key',NULL),('Bench Settings','parenttype',NULL),('Bench Settings','last_sync_timestamp','0'),('Bench Settings','socketio_port','0'),('Bench Settings','mail_server',NULL),('Bench Settings','update_bench_on_update','0'),('Bench Settings','parentfield',NULL);
/*!40000 ALTER TABLE `tabSingles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabSite`
--

DROP TABLE IF EXISTS `tabSite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabSite` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `install_erpnext` int(1) NOT NULL DEFAULT '0',
  `site_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `db_password` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bench_settings` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` decimal(18,6) NOT NULL DEFAULT '0.000000',
  `disable_website_cache` int(1) NOT NULL DEFAULT '0',
  `space` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `site_alias` text COLLATE utf8mb4_unicode_ci,
  `maintenance_mode` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pause_scheduler` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `app_list` text COLLATE utf8mb4_unicode_ci,
  `files_size` decimal(18,6) NOT NULL DEFAULT '0.000000',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `backup_size` decimal(18,6) NOT NULL DEFAULT '0.000000',
  `expiry` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `emails` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `developer_mode` int(1) NOT NULL DEFAULT '0',
  `database_size` decimal(18,6) NOT NULL DEFAULT '0.000000',
  `db_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `developer_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  UNIQUE KEY `site_name` (`site_name`),
  UNIQUE KEY `db_name` (`db_name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabSite`
--

LOCK TABLES `tabSite` WRITE;
/*!40000 ALTER TABLE `tabSite` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabSite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabSite Backup`
--

DROP TABLE IF EXISTS `tabSite Backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabSite Backup` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `site_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `public_file_backup` int(1) NOT NULL DEFAULT '0',
  `file_path` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stored_location` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `bench_settings` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `private_file_backup` int(1) NOT NULL DEFAULT '0',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `developer_flag` int(11) NOT NULL DEFAULT '0',
  `hash` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabSite Backup`
--

LOCK TABLES `tabSite Backup` WRITE;
/*!40000 ALTER TABLE `tabSite Backup` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabSite Backup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabStandard Reply`
--

DROP TABLE IF EXISTS `tabStandard Reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabStandard Reply` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `response` longtext COLLATE utf8mb4_unicode_ci,
  `subject` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabStandard Reply`
--

LOCK TABLES `tabStandard Reply` WRITE;
/*!40000 ALTER TABLE `tabStandard Reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabStandard Reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabTag`
--

DROP TABLE IF EXISTS `tabTag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabTag` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `tag_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabTag`
--

LOCK TABLES `tabTag` WRITE;
/*!40000 ALTER TABLE `tabTag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabTag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabTag Category`
--

DROP TABLE IF EXISTS `tabTag Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabTag Category` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `category_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabTag Category`
--

LOCK TABLES `tabTag Category` WRITE;
/*!40000 ALTER TABLE `tabTag Category` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabTag Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabTag Doc Category`
--

DROP TABLE IF EXISTS `tabTag Doc Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabTag Doc Category` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `tagdoc` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabTag Doc Category`
--

LOCK TABLES `tabTag Doc Category` WRITE;
/*!40000 ALTER TABLE `tabTag Doc Category` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabTag Doc Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabToDo`
--

DROP TABLE IF EXISTS `tabToDo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabToDo` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `assigned_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `reference_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Medium',
  `role` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Open',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `reference_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_seen` text COLLATE utf8mb4_unicode_ci,
  `assigned_by_full_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `sender` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`),
  KEY `reference_type_reference_name_index` (`reference_type`,`reference_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabToDo`
--

LOCK TABLES `tabToDo` WRITE;
/*!40000 ALTER TABLE `tabToDo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabToDo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabTop Bar Item`
--

DROP TABLE IF EXISTS `tabTop Bar Item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabTop Bar Item` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `url` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_label` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `right` int(1) NOT NULL DEFAULT '1',
  `target` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabTop Bar Item`
--

LOCK TABLES `tabTop Bar Item` WRITE;
/*!40000 ALTER TABLE `tabTop Bar Item` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabTop Bar Item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabTranslation`
--

DROP TABLE IF EXISTS `tabTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabTranslation` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `source_name` longtext COLLATE utf8mb4_unicode_ci,
  `language` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_name` longtext COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `language` (`language`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabTranslation`
--

LOCK TABLES `tabTranslation` WRITE;
/*!40000 ALTER TABLE `tabTranslation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabUnhandled Email`
--

DROP TABLE IF EXISTS `tabUnhandled Email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabUnhandled Email` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `uid` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `raw` longtext COLLATE utf8mb4_unicode_ci,
  `reason` longtext COLLATE utf8mb4_unicode_ci,
  `email_account` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `message_id` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabUnhandled Email`
--

LOCK TABLES `tabUnhandled Email` WRITE;
/*!40000 ALTER TABLE `tabUnhandled Email` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabUnhandled Email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabUser`
--

DROP TABLE IF EXISTS `tabUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabUser` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `email_signature` text COLLATE utf8mb4_unicode_ci,
  `last_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `frappe_userid` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_image` text COLLATE utf8mb4_unicode_ci,
  `simultaneous_sessions` int(11) NOT NULL DEFAULT '1',
  `github_username` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'System User',
  `phone` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_key` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_userid` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_known_versions` text COLLATE utf8mb4_unicode_ci,
  `fb_username` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_active` datetime(6) DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `new_password` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logout_all_sessions` int(1) NOT NULL DEFAULT '0',
  `github_userid` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `last_ip` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb_userid` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_style` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thread_notify` int(1) NOT NULL DEFAULT '1',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `background_image` text COLLATE utf8mb4_unicode_ci,
  `send_me_a_copy` int(1) NOT NULL DEFAULT '0',
  `role_profile_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `login_after` int(11) NOT NULL DEFAULT '0',
  `send_welcome_email` int(1) NOT NULL DEFAULT '1',
  `send_password_update_notification` int(1) NOT NULL DEFAULT '0',
  `language` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_before` int(11) NOT NULL DEFAULT '0',
  `gender` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enabled` int(1) NOT NULL DEFAULT '1',
  `time_zone` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mute_sounds` int(1) NOT NULL DEFAULT '0',
  `redirect_url` text COLLATE utf8mb4_unicode_ci,
  `birth_date` date DEFAULT NULL,
  `unsubscribed` int(1) NOT NULL DEFAULT '0',
  `restrict_ip` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `mobile_no` (`mobile_no`),
  UNIQUE KEY `username` (`username`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabUser`
--

LOCK TABLES `tabUser` WRITE;
/*!40000 ALTER TABLE `tabUser` DISABLE KEYS */;
INSERT INTO `tabUser` VALUES ('Administrator','2018-05-17 15:33:51.019659','2018-05-17 15:33:52.071013','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,1,NULL,'System User',NULL,NULL,NULL,'Administrator',NULL,NULL,NULL,'Administrator',NULL,NULL,'',NULL,NULL,0,NULL,'admin@example.com','administrator',NULL,NULL,NULL,'Fill Screen',1,NULL,NULL,0,NULL,NULL,NULL,0,1,0,NULL,0,NULL,1,NULL,0,NULL,NULL,0,NULL),('Guest','2018-05-17 15:33:51.159574','2018-05-17 15:33:51.159574','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,1,NULL,'System User',NULL,NULL,NULL,'Guest',NULL,NULL,NULL,'Guest',NULL,NULL,'',NULL,NULL,0,NULL,'guest@example.com','guest',NULL,NULL,NULL,'Fill Screen',1,NULL,NULL,0,NULL,NULL,NULL,0,1,0,NULL,0,NULL,1,NULL,0,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `tabUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabUser Email`
--

DROP TABLE IF EXISTS `tabUser Email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabUser Email` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `email_id` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_account` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable_outgoing` int(1) NOT NULL DEFAULT '0',
  `awaiting_password` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabUser Email`
--

LOCK TABLES `tabUser Email` WRITE;
/*!40000 ALTER TABLE `tabUser Email` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabUser Email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabUser Permission`
--

DROP TABLE IF EXISTS `tabUser Permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabUser Permission` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `apply_for_all_roles` int(1) NOT NULL DEFAULT '1',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `for_value` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabUser Permission`
--

LOCK TABLES `tabUser Permission` WRITE;
/*!40000 ALTER TABLE `tabUser Permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabUser Permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabVersion`
--

DROP TABLE IF EXISTS `tabVersion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabVersion` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `ref_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `docname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`),
  KEY `ref_doctype_docname_index` (`ref_doctype`,`docname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabVersion`
--

LOCK TABLES `tabVersion` WRITE;
/*!40000 ALTER TABLE `tabVersion` DISABLE KEYS */;
INSERT INTO `tabVersion` VALUES ('3021d8f9bd','2018-05-17 15:33:52.154095','2018-05-17 15:33:52.154095','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,'User',NULL,'Administrator','{\n \"added\": [\n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 3, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"e8e38934f4\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"All\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 4, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"210e4ac536\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Guest\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 5, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"2d4b398a47\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Report Manager\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 6, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"bf83cb622b\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Website Manager\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 7, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"bb2e38a4ed\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Knowledge Base Contributor\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 8, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"33376f3c76\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Knowledge Base Editor\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 9, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"473659a897\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Blogger\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 10, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"c1d234bde8\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Newsletter Manager\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 11, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"aec2cf8398\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Sales User\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 12, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"41588b004a\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Accounts User\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 13, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"d48f64cc9d\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Purchase User\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 14, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"2123473946\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Maintenance User\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 15, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"4c1120c9f8\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Purchase Manager\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 16, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"9a74af4840\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Maintenance Manager\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 17, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"95e9fd09cc\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Sales Master Manager\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 18, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"f701700758\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Sales Manager\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 19, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"17acaace9f\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Accounts Manager\"\n   }\n  ], \n  [\n   \"roles\", \n   {\n    \"creation\": \"2018-05-17 15:33:51.019659\", \n    \"docstatus\": 0, \n    \"doctype\": \"Has Role\", \n    \"idx\": 20, \n    \"modified\": \"2018-05-17 15:33:52.071013\", \n    \"modified_by\": \"Administrator\", \n    \"name\": \"56e45e3a1d\", \n    \"owner\": \"Administrator\", \n    \"parent\": \"Administrator\", \n    \"parentfield\": \"roles\", \n    \"parenttype\": \"User\", \n    \"role\": \"Purchase Master Manager\"\n   }\n  ]\n ], \n \"changed\": [], \n \"removed\": [], \n \"row_changed\": []\n}',NULL),('4f46435e3d','2018-05-17 15:33:51.191835','2018-05-17 15:33:51.191835','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,'User',NULL,'Guest','{\n \"comment\": \"Administrator shared this document with Guest\", \n \"comment_type\": \"Shared\"\n}',NULL),('887e1ecc33','2018-05-17 15:33:51.097686','2018-05-17 15:33:51.097686','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,NULL,'User',NULL,'Administrator','{\n \"comment\": \"Administrator shared this document with Administrator\", \n \"comment_type\": \"Shared\"\n}',NULL);
/*!40000 ALTER TABLE `tabVersion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWeb Form`
--

DROP TABLE IF EXISTS `tabWeb Form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWeb Form` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `allow_edit` int(1) NOT NULL DEFAULT '0',
  `amount_based_on_field` int(1) NOT NULL DEFAULT '0',
  `amount_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accept_payment` int(1) NOT NULL DEFAULT '0',
  `payment_button_help` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `breadcrumbs` longtext COLLATE utf8mb4_unicode_ci,
  `login_required` int(1) NOT NULL DEFAULT '0',
  `allow_delete` int(1) NOT NULL DEFAULT '0',
  `max_attachment_size` int(11) NOT NULL DEFAULT '0',
  `show_sidebar` int(1) NOT NULL DEFAULT '1',
  `payment_button_label` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Buy Now',
  `success_message` text COLLATE utf8mb4_unicode_ci,
  `introduction_text` longtext COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `allow_multiple` int(1) NOT NULL DEFAULT '0',
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `allow_print` int(1) NOT NULL DEFAULT '0',
  `allow_incomplete` int(1) NOT NULL DEFAULT '0',
  `is_standard` int(1) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `doc_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_comments` int(1) NOT NULL DEFAULT '0',
  `print_format` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(18,6) NOT NULL DEFAULT '0.000000',
  `success_url` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` int(1) NOT NULL DEFAULT '0',
  `web_page_link_text` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `route` (`route`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWeb Form`
--

LOCK TABLES `tabWeb Form` WRITE;
/*!40000 ALTER TABLE `tabWeb Form` DISABLE KEYS */;
INSERT INTO `tabWeb Form` VALUES ('edit-profile','2016-09-19 05:16:59.242754','2017-07-24 12:14:04.039284','Administrator','Administrator',0,NULL,NULL,NULL,0,1,0,NULL,'Core',NULL,0,NULL,'Update Profile','[{\"title\": _(\"My Account\"), \"route\": \"me\"}]',1,0,0,1,NULL,'Profile updated successfully.','',NULL,0,NULL,0,0,1,NULL,NULL,'User',0,NULL,'update-profile',0.000000,'/me',1,NULL,NULL);
/*!40000 ALTER TABLE `tabWeb Form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWeb Form Field`
--

DROP TABLE IF EXISTS `tabWeb Form Field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWeb Form Field` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `read_only` int(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `default` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_value` int(11) NOT NULL DEFAULT '0',
  `label` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_length` int(11) NOT NULL DEFAULT '0',
  `fieldname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fieldtype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reqd` int(1) NOT NULL DEFAULT '0',
  `hidden` int(1) NOT NULL DEFAULT '0',
  `options` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWeb Form Field`
--

LOCK TABLES `tabWeb Form Field` WRITE;
/*!40000 ALTER TABLE `tabWeb Form Field` DISABLE KEYS */;
INSERT INTO `tabWeb Form Field` VALUES ('01664c333b','2016-09-19 05:16:59.242754','2018-05-17 15:33:44.430000','Administrator','Administrator',0,'edit-profile','web_form_fields','Web Form',8,0,'',NULL,0,'Language',0,'language','Link',0,0,'Language'),('0c3854af28','2016-09-19 05:16:59.242754','2018-05-17 15:33:44.430000','Administrator','Administrator',0,'edit-profile','web_form_fields','Web Form',6,0,NULL,NULL,0,'Phone',0,'phone','Data',0,0,NULL),('2ca21a9681','2016-09-19 05:16:59.242754','2018-05-17 15:33:44.430000','Administrator','Administrator',0,'edit-profile','web_form_fields','Web Form',3,0,NULL,NULL,0,'Last Name',0,'last_name','Data',0,0,NULL),('44cc4f22b0','2016-09-19 05:16:59.242754','2018-05-17 15:33:44.430000','Administrator','Administrator',0,'edit-profile','web_form_fields','Web Form',2,0,NULL,NULL,0,'Middle Name (Optional)',0,'middle_name','Data',0,0,NULL),('7f4b38a060','2016-09-19 05:16:59.242754','2018-05-17 15:33:44.430000','Administrator','Administrator',0,'edit-profile','web_form_fields','Web Form',1,0,NULL,NULL,0,'First Name',0,'first_name','Data',1,0,NULL),('856a33933c','2016-09-19 05:16:59.242754','2018-05-17 15:33:44.430000','Administrator','Administrator',0,'edit-profile','web_form_fields','Web Form',7,0,NULL,NULL,0,'Mobile Number',0,'mobile_no','Data',0,0,NULL),('af0b9005fe','2016-09-19 05:16:59.242754','2018-05-17 15:33:44.430000','Administrator','Administrator',0,'edit-profile','web_form_fields','Web Form',4,0,'',NULL,0,'User Image',0,'user_image','Attach',0,0,NULL),('e5bee0d7e1','2016-09-19 05:16:59.242754','2018-05-17 15:33:44.430000','Administrator','Administrator',0,'edit-profile','web_form_fields','Web Form',5,0,NULL,NULL,0,'More Information',0,NULL,'Section Break',0,0,NULL);
/*!40000 ALTER TABLE `tabWeb Form Field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWeb Page`
--

DROP TABLE IF EXISTS `tabWeb Page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWeb Page` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `insert_style` int(1) NOT NULL DEFAULT '0',
  `header` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `breadcrumbs` longtext COLLATE utf8mb4_unicode_ci,
  `text_align` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_sidebar` int(1) NOT NULL DEFAULT '0',
  `enable_comments` int(1) NOT NULL DEFAULT '0',
  `css` longtext COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `insert_code` int(1) NOT NULL DEFAULT '0',
  `javascript` longtext COLLATE utf8mb4_unicode_ci,
  `slideshow` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `show_title` int(1) NOT NULL DEFAULT '1',
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `main_section` longtext COLLATE utf8mb4_unicode_ci,
  `route` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` int(1) NOT NULL DEFAULT '0',
  `website_sidebar` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `route` (`route`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWeb Page`
--

LOCK TABLES `tabWeb Page` WRITE;
/*!40000 ALTER TABLE `tabWeb Page` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabWeb Page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWebhook`
--

DROP TABLE IF EXISTS `tabWebhook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWebhook` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `webhook_docevent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `request_url` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `webhook_doctype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWebhook`
--

LOCK TABLES `tabWebhook` WRITE;
/*!40000 ALTER TABLE `tabWebhook` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabWebhook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWebhook Data`
--

DROP TABLE IF EXISTS `tabWebhook Data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWebhook Data` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `fieldname` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWebhook Data`
--

LOCK TABLES `tabWebhook Data` WRITE;
/*!40000 ALTER TABLE `tabWebhook Data` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabWebhook Data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWebhook Header`
--

DROP TABLE IF EXISTS `tabWebhook Header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWebhook Header` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `value` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWebhook Header`
--

LOCK TABLES `tabWebhook Header` WRITE;
/*!40000 ALTER TABLE `tabWebhook Header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabWebhook Header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWebsite Sidebar`
--

DROP TABLE IF EXISTS `tabWebsite Sidebar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWebsite Sidebar` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWebsite Sidebar`
--

LOCK TABLES `tabWebsite Sidebar` WRITE;
/*!40000 ALTER TABLE `tabWebsite Sidebar` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabWebsite Sidebar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWebsite Sidebar Item`
--

DROP TABLE IF EXISTS `tabWebsite Sidebar Item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWebsite Sidebar Item` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `route` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWebsite Sidebar Item`
--

LOCK TABLES `tabWebsite Sidebar Item` WRITE;
/*!40000 ALTER TABLE `tabWebsite Sidebar Item` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabWebsite Sidebar Item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWebsite Slideshow`
--

DROP TABLE IF EXISTS `tabWebsite Slideshow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWebsite Slideshow` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `slideshow_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `header` longtext COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWebsite Slideshow`
--

LOCK TABLES `tabWebsite Slideshow` WRITE;
/*!40000 ALTER TABLE `tabWebsite Slideshow` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabWebsite Slideshow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWebsite Slideshow Item`
--

DROP TABLE IF EXISTS `tabWebsite Slideshow Item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWebsite Slideshow Item` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `image` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `heading` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWebsite Slideshow Item`
--

LOCK TABLES `tabWebsite Slideshow Item` WRITE;
/*!40000 ALTER TABLE `tabWebsite Slideshow Item` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabWebsite Slideshow Item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWebsite Theme`
--

DROP TABLE IF EXISTS `tabWebsite Theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWebsite Theme` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `heading_style` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_color` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'Website',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `footer_text_color` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heading_webfont` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_color` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apply_style` int(1) NOT NULL DEFAULT '1',
  `background_image` text COLLATE utf8mb4_unicode_ci,
  `top_bar_text_color` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `apply_text_styles` int(1) NOT NULL DEFAULT '0',
  `custom` int(1) NOT NULL DEFAULT '1',
  `theme` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_color` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `css` longtext COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `text_webfont` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `js` longtext COLLATE utf8mb4_unicode_ci,
  `text_color` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `font_size` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `top_bar_color` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bootstrap` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `theme` (`theme`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWebsite Theme`
--

LOCK TABLES `tabWebsite Theme` WRITE;
/*!40000 ALTER TABLE `tabWebsite Theme` DISABLE KEYS */;
INSERT INTO `tabWebsite Theme` VALUES ('Standard','2015-02-19 13:37:33.925909','2016-12-29 05:40:17.289226','Administrator','Administrator',0,NULL,NULL,NULL,26,'','','Website',NULL,'','',NULL,0,NULL,'',NULL,0,0,'Standard','','.navbar-header {\n  display: none;\n}',NULL,'',NULL,'',NULL,'14px','',NULL);
/*!40000 ALTER TABLE `tabWebsite Theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWorkflow`
--

DROP TABLE IF EXISTS `tabWorkflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWorkflow` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `override_status` int(1) NOT NULL DEFAULT '0',
  `workflow_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `workflow_state_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'workflow_state',
  `document_type` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWorkflow`
--

LOCK TABLES `tabWorkflow` WRITE;
/*!40000 ALTER TABLE `tabWorkflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabWorkflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWorkflow Action`
--

DROP TABLE IF EXISTS `tabWorkflow Action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWorkflow Action` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `workflow_action_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWorkflow Action`
--

LOCK TABLES `tabWorkflow Action` WRITE;
/*!40000 ALTER TABLE `tabWorkflow Action` DISABLE KEYS */;
INSERT INTO `tabWorkflow Action` VALUES ('Approve','2018-05-17 15:33:51.259623','2018-05-17 15:33:51.259623','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'Approve',NULL,NULL,NULL),('Reject','2018-05-17 15:33:51.261342','2018-05-17 15:33:51.261342','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'Reject',NULL,NULL,NULL),('Review','2018-05-17 15:33:51.263134','2018-05-17 15:33:51.263134','Administrator','Administrator',0,NULL,NULL,NULL,0,NULL,'Review',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tabWorkflow Action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWorkflow Document State`
--

DROP TABLE IF EXISTS `tabWorkflow Document State`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWorkflow Document State` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `allow_edit` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update_field` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_status` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `update_value` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWorkflow Document State`
--

LOCK TABLES `tabWorkflow Document State` WRITE;
/*!40000 ALTER TABLE `tabWorkflow Document State` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabWorkflow Document State` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWorkflow State`
--

DROP TABLE IF EXISTS `tabWorkflow State`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWorkflow State` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `style` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_liked_by` text COLLATE utf8mb4_unicode_ci,
  `_user_tags` text COLLATE utf8mb4_unicode_ci,
  `_assign` text COLLATE utf8mb4_unicode_ci,
  `_comments` text COLLATE utf8mb4_unicode_ci,
  `workflow_state_name` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWorkflow State`
--

LOCK TABLES `tabWorkflow State` WRITE;
/*!40000 ALTER TABLE `tabWorkflow State` DISABLE KEYS */;
INSERT INTO `tabWorkflow State` VALUES ('Approved','2018-05-17 15:33:51.233527','2018-05-17 15:33:51.233527','Administrator','Administrator',0,NULL,NULL,NULL,0,'Success',NULL,NULL,NULL,NULL,'Approved','ok-sign'),('Pending','2018-05-17 15:33:51.230964','2018-05-17 15:33:51.230964','Administrator','Administrator',0,NULL,NULL,NULL,0,'',NULL,NULL,NULL,NULL,'Pending','question-sign'),('Rejected','2018-05-17 15:33:51.239576','2018-05-17 15:33:51.239576','Administrator','Administrator',0,NULL,NULL,NULL,0,'Danger',NULL,NULL,NULL,NULL,'Rejected','remove');
/*!40000 ALTER TABLE `tabWorkflow State` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabWorkflow Transition`
--

DROP TABLE IF EXISTS `tabWorkflow Transition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabWorkflow Transition` (
  `name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `modified_by` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docstatus` int(1) NOT NULL DEFAULT '0',
  `parent` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentfield` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parenttype` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idx` int(8) NOT NULL DEFAULT '0',
  `action` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `next_state` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allowed` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `parent` (`parent`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabWorkflow Transition`
--

LOCK TABLES `tabWorkflow Transition` WRITE;
/*!40000 ALTER TABLE `tabWorkflow Transition` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabWorkflow Transition` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-05  5:32:07
